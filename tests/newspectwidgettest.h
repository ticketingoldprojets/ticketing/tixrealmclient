#ifndef NEWSPECTWIDGETTEST_H
#define NEWSPECTWIDGETTEST_H

#include <QObject>
#include "../src/stock/newspectwidget.h"
#include "mockednetworkaccessmanager.h"
#include <QSignalSpy>
#include "basetixrtestclass.h"

class NewSpectWidgetTest : public QObject//BaseTixrTestClass
{
    Q_OBJECT

    TixrNewSpectWidget *newSpectWidget;
    TixRealmCore *tixrCore;
    QStandardItem *root;//spectacles items root
    MockedNetworkAccessManager *mockedNAM;
    QNetworkAccessManager *trueNAM;
    QSignalSpy *sigSpy;

public:
    explicit NewSpectWidgetTest(QObject *parent = 0);

public slots:
    void singleShot1TestCase1Slot();

private Q_SLOTS:
    void initTestCase();
    void cleanupTestCase();

    void testCase1();
    void testCase1_data();

    void cleanup();
};

#endif // NEWSPECTWIDGETTEST_H
