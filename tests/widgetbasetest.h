#ifndef WIDGETBASETEST_H
#define WIDGETBASETEST_H

#include "../src/widget.h"
#include "basetixrtestclass.h"

class WidgetBaseTest : public QObject//BaseTixrTestClass
{
    Q_OBJECT

    TixRealmCore *tixrCore;
    TixrWidget *tixrWidget;
    QStandardItem *rootItem;

private slots:
    void initTestCase();
    void cleanupTestCase();

    void init();
    void cleanup();

    void testCase1();
    void testCase2();

public:
    explicit WidgetBaseTest(QObject *parent = 0);

public slots:
    void singleShot1TestCase2Slot();
};

#endif // WIDGETBASETEST_H
