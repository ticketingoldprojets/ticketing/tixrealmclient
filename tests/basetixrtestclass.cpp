#include "basetixrtestclass.h"
#include <QApplication>
#include <QMessageBox>
#include <QTest>

BaseTixrTestClass::BaseTixrTestClass(QObject *parent) : QObject(parent)
{
    t = new QTimer(this);
    connect(t, SIGNAL(timeout()), this, SLOT(closeMessageBoxes()));

    eL = new QEventLoop(this);
    //eL->exec();

    t->start(3000);
}

void BaseTixrTestClass::closeMessageBoxes()
{
    QWidgetList topWidgets = QApplication::topLevelWidgets();
    foreach (QWidget *w, topWidgets)
    {
        if (QMessageBox *mb = qobject_cast<QMessageBox *>(w))
        {
            QTest::keyClick(mb, Qt::Key_Enter);
        }
    }
}

void BaseTixrTestClass::exitEventLoop()
{
    eL->exit();
}
