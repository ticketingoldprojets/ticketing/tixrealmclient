#include <QtTest>
#include <QCoreApplication>

#include "tst_logintest.h"
#include "tariffstest.h"
#include "newseancetest.h"
#include "jsonmodelfactorytest.h"
#include "site2dviewtest.h"
#include "widgetbasetest.h"
#include "checkwidgetstest.h"
#include "newspectwidgettest.h"
//#include "gtest/gtest.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    //QTEST_DISABLE_KEYPAD_NAVIGATION

    /*BaseTixrTestClass *tool = new BaseTixrTestClass(&app);
    QObject::connect(&app, &QApplication::lastWindowClosed, tool, &BaseTixrTestClass::exitEventLoop);*/

    int r = 0;

    LoginTest loginTest;
    r = QTest::qExec(&loginTest, argc, argv);

    JsonModelFactoryTest jsonModelFactoryTest;
    r |= QTest::qExec(&jsonModelFactoryTest, argc, argv);

    TariffsTest tariffsTest;
    r |= QTest::qExec(&tariffsTest, argc, argv);

    WidgetBaseTest widgetBaseTest;
    r |= QTest::qExec(&widgetBaseTest, argc, argv);

    CheckWidgetsTest checkWidgetsTest;
    r = QTest::qExec(&checkWidgetsTest, argc, argv);

    NewSeanceTest newSeanceTest;
    r |= QTest::qExec(&newSeanceTest, argc, argv);

    NewSpectWidgetTest newSpectWidgetTest;
    r |= QTest::qExec(&newSpectWidgetTest, argc, argv);

    Site2dViewTest site2dViewTest;
    r |= QTest::qExec(&site2dViewTest, argc, argv);

    return r;
}
