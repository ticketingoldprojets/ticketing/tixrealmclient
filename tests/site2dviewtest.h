#ifndef SITE2DVIEWTEST_H
#define SITE2DVIEWTEST_H

#include <QObject>
#include "../TixrClientEditorCommon/site2dview.h"
#include "basetixrtestclass.h"

class Site2dViewTest : public QObject//BaseTixrTestClass
{
    Q_OBJECT
public:
    explicit Site2dViewTest(QObject *parent = 0);

private:
    TixRealmCore *tixrCore;
    Site2dView *site2dView;

private Q_SLOTS:
    void initTestCase();
    void init();
    void cleanupTestCase();
    void cleanup();

    void testCase1();
    void testCase1_data();
};

#endif // SITE2DVIEWTEST_H
