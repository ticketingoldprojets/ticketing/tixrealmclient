#include "checkwidgetstest.h"
#include "../src/core/datarole.h"
#include "../src/core/datacolumn.h"
#include <QtTest/QTest>
#include <QDirIterator>
#include <QUiLoader>

CheckWidgetsTest::CheckWidgetsTest(QObject *parent) :
    QObject(parent)
{
}

void CheckWidgetsTest::testCase1()
{
    QFETCH(QString, uiFileName);

    QFile f(uiFileName);
    QVERIFY(f.open(QIODevice::ReadOnly));
    QUiLoader uiLoader;
    QWidget *w = uiLoader.load(&f);
    f.close();
    QVERIFY(w);

    QList<QWidget*> wList = w->findChildren<QWidget*>();
    for(int i=0, n=wList.size(); i<n; i++)
    {
        QWidget *child = wList.at(i);

        QString attName = child->property("tixrJsonKey").toString();

        if(attName.isEmpty())
            continue;

        QString wN = child->objectName();

        QStringList nameList = attName.split("_", QString::SkipEmptyParts);
        if(nameList.size() == 2)
        {
            QVERIFY2(DataColumn::exists(nameList.at(0)), wN.toUtf8().constData());
            QVERIFY2(DataRole::exists(nameList.at(1)), wN.toUtf8().constData());
        }
        else
            QVERIFY2(DataColumn::exists(attName) || DataRole::exists(attName), wN.toUtf8().constData());
    }
}

void CheckWidgetsTest::testCase1_data()
{
    //localizar los skins
    QString skinPath;
    QDir dir(QCoreApplication::applicationDirPath());
    while(dir.cdUp())
    {
        QDirIterator dirIt(dir.absolutePath());
        while(dirIt.hasNext())
        {
            dirIt.next();
            if (QFileInfo(dirIt.filePath()).isDir() && dirIt.fileName() == "SkinsBuilder")
            {
                skinPath = dirIt.filePath() + "/SeatFreeSkin/formconfig";
                break;
            }
        }
        if(!skinPath.isEmpty())
            break;
    }

    QVERIFY(!skinPath.isEmpty());

    QDirIterator dIter(skinPath, QDirIterator::Subdirectories);

    QTest::addColumn<QString>("uiFileName");

    while(dIter.hasNext())
    {
        QString fn = dIter.next();

        if(dIter.fileName() == "." || dIter.fileName() == "..")
            continue;

        QTest::newRow(fn.toUtf8().constData()) << fn;
    }
}
