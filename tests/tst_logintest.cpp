#include "tst_logintest.h"
#include <QtTest>

//Q_DECLARE_METATYPE( QStandardItem* )

LoginTest::LoginTest()
{
}

void LoginTest::initTestCase()
{
    tixrCore = new TixRealmCore;
    //tixrCore->setConnParam("santrossoft.ddns.net", 8086);
    tixrCore->setConnParam("localhost", 8086);
}

void LoginTest::cleanupTestCase()
{
    delete tixrCore;
}

void LoginTest::testCase1()
{
    QTimer::singleShot(200, this, SLOT(singleShot1TestCase1Slot()));
    qApp->exec();
}

void LoginTest::singleShot1TestCase1Slot()
{
    QFETCH(QString, user);
    QFETCH(QString, password);

    tixrCore->setCredentials(QString("%1:%2").arg(user, password).toUtf8().toBase64());
    tixrCore->setPgFunction("main.signin");
    tixrCore->sendMsg(0, HTTP_METHOD_ID::GET, "/api", this, "testCase1ReadResponse");
}

void LoginTest::testCase1_data()
{
    QTest::addColumn<QString>("user");
    QTest::addColumn<QString>("password");

    QTest::newRow("connect as sainttropez") << QString("sainttropez") << QString("sainttropez");
}

void LoginTest::testCase1ReadResponse(QString respError, PendingRequest *curReq)
{
    qDebug() << "Executing testCase1ReadResponse";
    qApp->exit();

    QVERIFY2(respError.isEmpty(), QString("Failure: %1").arg(respError).toUtf8().constData());

    QStandardItem *responseItem = curReq->getResponseItem();
    QVERIFY(responseItem);

    QStandardItem *sessionItem = responseItem->child(0);
    QVERIFY(sessionItem);

    int r = DataRole::stoken();
    QVERIFY(r >= 0);

    QVariant v = sessionItem->data(r);
    QVERIFY(v.isValid());

    tixrCore->setSessionId(v.toByteArray());//Set the session's id.
}

void LoginTest::testCase2()
{
    QSKIP("not now");

    //test session ID validity
    if(qstrcmp(QTest::currentDataTag(), "getallmodepaiem") == 0)
        QTest::qWait(6000);
    else if(qstrcmp(QTest::currentDataTag(), "getalldevises") == 0)
        QTest::qWait(22000);

    QVERIFY(tixrCore->credentials.isEmpty());
    oldSessionId = tixrCore->sessionId;
    QVERIFY(!oldSessionId.isEmpty());

    QFETCH(int, method);
    QFETCH(QByteArray, data);
    QFETCH(QString, pgFunction);

    tixrCore->setPgFunction(pgFunction);
    tixrCore->sendMsg(0, HTTP_METHOD_ID(method), "/api", this, "testCase2ReadResponse", data);

    qApp->exec();
}

void LoginTest::testCase2_data()
{
    QTest::addColumn<int>("method");
    QTest::addColumn<QByteArray>("data");
    QTest::addColumn<QString>("pgFunction");
    QTest::addColumn<bool>("sameToken");

    QTest::newRow("getallfamcatprods") << int(HTTP_METHOD_ID::POST)
                                       << QByteArray("{\"srvikey\": \"0\"}")
                                       << QString("main.getallfamcatprods")
                                       << true;

    QTest::newRow("getallmodepaiem") << int(HTTP_METHOD_ID::GET)
                                     << QByteArray()
                                     << QString("main.getallmodepaiem")
                                     << true;

    QTest::newRow("getalldevises") << int(HTTP_METHOD_ID::GET)
                                   << QByteArray()
                                   << QString("main.getalldevises")
                                   << true;
}

/*void LoginTest::testCase3()
{
    QVERIFY(tixrCore->applyStyle());
    tariffWidget = new GrillesAdminPage(tixrCore);
    tariffWidget->setGeometry(1, 1, 400, 400);
    tariffWidget->show();
    qApp->exec();
}*/

void LoginTest::testCase2ReadResponse(QString respError, PendingRequest *curReq)
{
    qApp->exit();
    
    QFETCH(bool, sameToken);
    
    QVERIFY2(respError.isEmpty(), respError.toUtf8().constData());

    QString newSessionId = tixrCore->sessionId;

    QVERIFY2((oldSessionId == newSessionId) == sameToken, tixrCore->sessionId.constData());
}
