#ifndef TARIFFSTEST_H
#define TARIFFSTEST_H

#include "../src/tariff/grillesadminpage.h"
#include "basetixrtestclass.h"

class TariffsTest : public QObject//BaseTixrTestClass
{
    Q_OBJECT

public:
    TariffsTest();

public slots:
    void testCase1TimerSlot();
    void testCase1FinishSlot(PendingRequest *curReq);

private slots:
    void initTestCase();
    void cleanupTestCase();
    void init();
    void cleanup();

    void testCase1();

private:
    GrillesAdminPage *tariffWidget;
    TixRealmCore *tixrCore;
    QTimer *timer;
    int index;
};

#endif // TARIFFSTEST_H
