#ifndef TST_LOGINTEST_H
#define TST_LOGINTEST_H
#include <QString>

#include "../src/core/tixrcore.h"
#include "../src/tariff/grillesadminpage.h"
#include "basetixrtestclass.h"

class LoginTest : public QObject//BaseTixrTestClass
{
    Q_OBJECT

public:
    LoginTest();

public slots:
    void singleShot1TestCase1Slot();

private Q_SLOTS:
    void initTestCase();
    void cleanupTestCase();

    void testCase1ReadResponse(QString respError, PendingRequest *curReq);
    void testCase2ReadResponse(QString respError, PendingRequest *curReq);

    void testCase1();
    void testCase1_data();

    void testCase2();
    void testCase2_data();

    //void testCase3();

private:
    TixRealmCore *tixrCore;
    QString oldSessionId;
    GrillesAdminPage *tariffWidget;
};

#endif // TST_LOGINTEST_H
