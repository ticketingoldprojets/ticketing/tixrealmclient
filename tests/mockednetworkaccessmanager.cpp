#include "mockednetworkaccessmanager.h"
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>
#include <QFile>

MockedNetworkAccessManager::MockedNetworkAccessManager(QObject *parent) :
    QNetworkAccessManager(parent)
{
}

QJsonValue MockedNetworkAccessManager::getExpectedOutgoingData() const
{
    return expectedOutgoingData;
}

void MockedNetworkAccessManager::setExpectedOutgoingData(const QJsonValue &value)
{
    expectedOutgoingData = value;
}


QString MockedNetworkAccessManager::getOutputFile() const
{
    return outputFile;
}

void MockedNetworkAccessManager::setOutputFile(const QString &value)
{
    outputFile = value;
}
QNetworkReply *MockedNetworkAccessManager::createRequest(QNetworkAccessManager::Operation op, const QNetworkRequest &req, QIODevice *outgoingData)
{
    if(outgoingData && !expectedOutgoingData.isNull())
    {
        outgoingData->open(QIODevice::ReadOnly);
        QByteArray ba = outgoingData->readAll();
        QJsonDocument jsonDoc = QJsonDocument::fromJson(ba);
        outgoingData->close();

        if(!outputFile.isEmpty())
        {
            QFile f(outputFile);
            Q_ASSERT(f.open(QIODevice::WriteOnly));
            f.write(ba);
            f.close();
        }

        Q_ASSERT(jsonDoc.isObject() || jsonDoc.isArray());

        QJsonValue outgoingDataJson;
        if(jsonDoc.isObject())
            outgoingDataJson = QJsonValue(jsonDoc.object());
        else
            outgoingDataJson = QJsonValue(jsonDoc.array());

        Q_ASSERT(outgoingDataJson == expectedOutgoingData);

        outgoingData->open(QIODevice::ReadOnly);
    }

    return QNetworkAccessManager::createRequest(op, req, outgoingData);
}
