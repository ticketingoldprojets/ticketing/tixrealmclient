#ifndef BASETIXRTESTCLASS_H
#define BASETIXRTESTCLASS_H

#include <QObject>
#include <QTimer>
#include <QEventLoop>

class BaseTixrTestClass : public QObject
{
    Q_OBJECT
    QTimer *t;
    QEventLoop *eL;

private slots:
    void closeMessageBoxes();

public:
    explicit BaseTixrTestClass(QObject *parent = 0);

public slots:
    void exitEventLoop();
};

#endif // BASETIXRTESTCLASS_H
