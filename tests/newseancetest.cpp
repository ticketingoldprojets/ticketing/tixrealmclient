#include "newseancetest.h"
#include <QtTest>
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>
#include <QJsonValue>

NewSeanceTest::NewSeanceTest(QObject *parent) :
    QObject(parent)
{
}

void NewSeanceTest::initTestCase()
{
    tixrCore = new TixRealmCore;
    //tixrCore->setConnParam("santrossoft.ddns.net", 8086);
    tixrCore->setConnParam("localhost", 8086);
    tixrCore->setCredentials(QString("%1:%2").arg("sainttropez", "sainttropez").toUtf8().toBase64());

    tixrCore->netManager = mockedNAM = new MockedNetworkAccessManager(tixrCore);
}

void NewSeanceTest::cleanupTestCase()
{
    if(sigSpy)
        delete sigSpy;

    delete tixrCore;
}

void NewSeanceTest::cleanup()
{
    delete newSeanceWidget;
}

void NewSeanceTest::testCase1()
{
    //QSKIP("not now");

    QFETCH(int, ikey);
    QFETCH(QString, qtdisplayrole);

    root = new QStandardItem;
    tixrCore->stdModel->appendRow(root);
    tixrCore->keyToItemMap.insert("stock", root);

    QStandardItem *spectItem = new QStandardItem(qtdisplayrole);
    root->appendRow(spectItem);
    QStandardItem *tgtItem = JsonQStandardItemFactory::getTargetItem(QStringLiteral("fptitlecol_ikey"), spectItem);
    tgtItem->setData(ikey, DataRole::ikey());

    QStandardItem *child = new QStandardItem;
    spectItem->appendRow(child);

    //load new test ui
    QFETCH(QString, skinFile);
    QVERIFY(tixrCore->loadSkin(skinFile));

    //create ui
    newSeanceWidget = new TixrNewSeanceWidget(tixrCore, child);

    newSeanceWidget->show();
    QTimer::singleShot(200, this, SLOT(singleShot1TestCase1Slot()));

    qApp->exec();
}

void NewSeanceTest::testCase1_data()
{
    QTest::addColumn<int>("ikey");
    QTest::addColumn<QString>("qtdisplayrole");
    QTest::addColumn<QString>("skinFile");
    QTest::addColumn<QString>("expectedOutgoingData");
    QTest::addColumn<int>("tariffIdx");
    QTest::addColumn<QString>("outputFile");
    QTest::addColumn<QString>("imgFileName");

    QTest::newRow("LES DANGERS DU FROMAGE") << 19
                                            << QStringLiteral("LES DANGERS DU FROMAGE")
                                            << QFINDTESTDATA("data/default.skin")
                                            << QFINDTESTDATA("data/NewSeanceTest/expectedOutgoingDataWithoutPlan.json")
                                            << 1
                                            << QFINDTESTDATA("data/NewSeanceTest/outputFile.txt")
                                            << QFINDTESTDATA("data/NewSeanceTest/calc.png");
}

#define exitBeforeVerify(exp) r = exp; if(!r){newSeanceWidget->close();qApp->exit(-1);} QVERIFY(r)
#define exitBeforeVerify2(exp,msg) r = exp; if(!r){newSeanceWidget->close();qApp->exit(-1);} QVERIFY2(r,msg)

void NewSeanceTest::singleShot1TestCase1Slot()
{
    bool r;

    sigSpy = new QSignalSpy(newSeanceWidget, SIGNAL(noPendingRequests()));
    exitBeforeVerify(sigSpy->wait(6000));
    QCOMPARE(sigSpy->count(), 1);

    QAbstractItemModel *model = newSeanceWidget->seanceGriList->model();
    exitBeforeVerify(model);

    QModelIndex rIdx = newSeanceWidget->seanceGriList->rootIndex();
    int n = model->rowCount(rIdx);

    QFETCH(int, tariffIdx);

    exitBeforeVerify(tariffIdx>=0 && tariffIdx<n);

    QModelIndex idx = rIdx.child(tariffIdx, 0);
    QRect rect = newSeanceWidget->seanceGriList->visualRect(idx);
    QPoint p = rect.center();

    qRegisterMetaType<QNetworkReply*>();
    sigSpy = new QSignalSpy(tixrCore->netManager, SIGNAL(finished(QNetworkReply*)));

    QTest::mouseClick(newSeanceWidget->seanceGriList->viewport(), Qt::LeftButton, 0, p, 100);

    exitBeforeVerify(sigSpy->wait());

    QCOMPARE(sigSpy->count(), 1);

    QString error = newSeanceWidget->getErrorMsg();
    exitBeforeVerify2(error.isEmpty(), error.toUtf8().constData());

    //load venue plan
    if(newSeanceWidget->gbVenuePlan && newSeanceWidget->gbVenuePlan->isChecked())
    {
        exitBeforeVerify(newSeanceWidget->loadVenuePlan(newSeanceWidget->lePlano->text()));
    }

    //load expected outgoing data
    QFETCH(QString, expectedOutgoingData);
    QFile f(expectedOutgoingData);
    exitBeforeVerify(f.open(QIODevice::ReadOnly));
    QByteArray ba = f.readAll();
    QJsonParseError jsonError;
    QJsonDocument jsonDoc = QJsonDocument::fromJson(ba, &jsonError);
    f.close();

    exitBeforeVerify(f.open(QIODevice::WriteOnly));
    f.write(jsonDoc.toJson());
    f.close();

    exitBeforeVerify2(jsonError.error == QJsonParseError::NoError,jsonError.errorString().toUtf8().constData());
    exitBeforeVerify(jsonDoc.isArray() || jsonDoc.isObject());

    //set expected outgoing data
    QJsonValue jsonValue = jsonDoc.isArray() ? QJsonValue(jsonDoc.array())
                                             : QJsonValue(jsonDoc.object());
    mockedNAM->setExpectedOutgoingData(jsonValue);

    QFETCH(QString, outputFile);
    mockedNAM->setOutputFile(outputFile);

    QLabel *imgViewer = newSeanceWidget->findChild<QLabel*>("imgViewer");
    exitBeforeVerify(imgViewer);
    QFETCH(QString, imgFileName);
    QPixmap pxm;
    exitBeforeVerify(pxm.load(imgFileName));
    imgViewer->setPixmap(pxm);

    QPushButton *pb = newSeanceWidget->findChild<QPushButton*>("pbValiderNewSeance");
    exitBeforeVerify(pb);

    delete sigSpy;
    sigSpy = new QSignalSpy(tixrCore->netManager, SIGNAL(finished(QNetworkReply*)));

    //send newSeance request
    QTest::mouseClick(pb, Qt::LeftButton);

    exitBeforeVerify(sigSpy->wait());

    exitBeforeVerify(sigSpy->count()==1);

    exitBeforeVerify(newSeanceWidget->getErrorMsg().isEmpty());

    qApp->exit();
}
