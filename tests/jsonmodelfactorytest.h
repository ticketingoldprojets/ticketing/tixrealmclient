#ifndef JSONMODELFACTORYTEST_H
#define JSONMODELFACTORYTEST_H

#include <QObject>
#include "../src/core/tixrcore.h"
#include "basetixrtestclass.h"

class JsonModelFactoryTest : public QObject//BaseTixrTestClass
{
    Q_OBJECT
public:
    JsonModelFactoryTest();

private:
    TixRealmCore *tixrCore;

private Q_SLOTS:
    void initTestCase();
    void cleanupTestCase();

    void testCase1WidgetToJson();
    void testCase1WidgetToJson_data();

    void testCase2JsonToModel();
};

#endif // JSONMODELFACTORYTEST_H
