#ifndef MOCKEDNETWORKACCESSMANAGER_H
#define MOCKEDNETWORKACCESSMANAGER_H

#include <QNetworkAccessManager>
#include <QJsonValue>

class MockedNetworkAccessManager : public QNetworkAccessManager
{
    Q_OBJECT

    QJsonValue expectedOutgoingData;
    QString outputFile;

protected:
    QNetworkReply *createRequest(Operation op, const QNetworkRequest & req, QIODevice * outgoingData = 0);

public:
    explicit MockedNetworkAccessManager(QObject *parent = 0);
    QJsonValue getExpectedOutgoingData() const;
    void setExpectedOutgoingData(const QJsonValue &value);
    QString getOutputFile() const;
    void setOutputFile(const QString &value);
};

#endif // MOCKEDNETWORKACCESSMANAGER_H
