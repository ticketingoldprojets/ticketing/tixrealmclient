#include "site2dviewtest.h"
#include <QTest>
#include <QJsonObject>
#include <QJsonDocument>

Site2dViewTest::Site2dViewTest(QObject *parent) :
    QObject(parent)
{
}
void Site2dViewTest::initTestCase()
{
    tixrCore = new TixRealmCore;
    //tixrCore->setConnParam("santrossoft.ddns.net", 8086);
    tixrCore->setConnParam("localhost", 8086);
    tixrCore->setCredentials(QString("%1:%2").arg("sainttropez", "sainttropez").toUtf8().toBase64());
}

void Site2dViewTest::cleanupTestCase()
{
    tixrCore->deleteLater();
}

void Site2dViewTest::init()
{
    QFETCH(QString, skinFile);
    //load new test ui
    QVERIFY(tixrCore->loadSkin(skinFile));

    site2dView = new Site2dView(0, false, false, tixrCore);
    QVERIFY(site2dView);
}

void Site2dViewTest::cleanup()
{
    site2dView->deleteLater();
}

void Site2dViewTest::testCase1()
{
    QFETCH(QString, inputSvgFile);
    QFETCH(QString, expectedJsonFile);

    QFile f(inputSvgFile);
    qDebug() << "Loading venue plan";
    site2dView->loadPlan(&f);

    QVERIFY2(site2dView->eMsg.isEmpty(), site2dView->eMsg.toUtf8().constData());

    QJsonObject planObj;
    QString err;
    qDebug() << "Exporting venue plan as json";
    site2dView->exportAsJson(planObj, err);
    QVERIFY2(err.isEmpty(), err.toUtf8().constData());

    QFile g(expectedJsonFile);
    QVERIFY(g.open(QIODevice::ReadOnly));
    QJsonDocument jsonDoc = QJsonDocument::fromJson(g.readAll());

    QJsonDocument jsonDoc2(planObj);
    QFETCH(QString, outputJsonFile);
    QFile out(outputJsonFile);
    QVERIFY(out.open(QIODevice::WriteOnly));
    QByteArray ba = jsonDoc2.toJson();
    out.write(ba);
    out.close();

    QVERIFY(ba == jsonDoc.toJson());

    g.close();
}

void Site2dViewTest::testCase1_data()
{
    QTest::addColumn<QString>("skinFile");
    QTest::addColumn<QString>("inputSvgFile");
    QTest::addColumn<QString>("expectedJsonFile");
    QTest::addColumn<QString>("outputJsonFile");

    QTest::newRow("OlympiaV5") << QFINDTESTDATA("data/default.skin")
                               << QFINDTESTDATA("data/Site2dViewTest/OlympiaV5.svg")
                               << QFINDTESTDATA("data/Site2dViewTest/exported_plan.json")
                               << QFINDTESTDATA("data/Site2dViewTest/OlympiaV5.json");
}

