#include "jsonmodelfactorytest.h"
#include "../src/core/jsonqstandarditemfactory.h"
#include "../src/core/metaqenum.h"
#include <QtTest>
#include <QUiLoader>
#include <QLineEdit>

JsonModelFactoryTest::JsonModelFactoryTest()
{
}

void JsonModelFactoryTest::initTestCase()
{
    tixrCore = new TixRealmCore;
}

void JsonModelFactoryTest::cleanupTestCase()
{
    delete tixrCore;
}

void JsonModelFactoryTest::testCase1WidgetToJson()
{
    QFETCH(QString, uiFile);
    QFETCH(QString, expectedJson);

    QUiLoader loader;
    QFile f(uiFile);
    QVERIFY(f.open(QFile::ReadOnly));
    QWidget *dynWidget = loader.load(&f);
    f.close();

    /*QLineEdit *lineEdit = dynWidget->findChild<QLineEdit*>("lineEdit");
    QVERIFY(lineEdit);
    lineEdit->setText("titulo");*/

    QJsonObject *jsonObj = tixrCore->widgetToJson(dynWidget);

    //jsonObj->insert("titre", QJsonValue());

    QJsonDocument jsonDoc(*jsonObj);

    QByteArray json = jsonDoc.toJson(QJsonDocument::Compact);

    QVERIFY2(QString(json) == expectedJson, json.constData());
}

void JsonModelFactoryTest::testCase1WidgetToJson_data()
{
    QTest::addColumn<QString>("uiFile");
    QTest::addColumn<QString>("expectedJson");

    QTest::newRow("newSeance.ui") << QFINDTESTDATA("data/JsonModelFactoryTest/testCase1WidgetToJson/newSeance.ui")
                                  << QString("{\"cpaddress\":{\"address1\":\"1 rue Condorcet\",\"address2\":\"\",\"city\":\"Talence\",\"country\":\"France\",\"zipcode\":33400},\"cpcosto\":1,\"cpdat\":\"2014-09-26T20:00:00\",\"cpddvente\":\"2014-09-25T15:00:00\",\"cpdescript\":\"This is the performance record for Unit Test #1\",\"cpdfvente\":\"2014-09-26T19:40:00\",\"cpisaflow\":false,\"cpiva\":20,\"cpivalcosto\":5,\"cpnoseasonticket\":false,\"cptitre\":\"Title #1\",\"cptitre_cptcolor\":\"#ff0000\"}");
}

void JsonModelFactoryTest::testCase2JsonToModel()
{
    QFile f(QFINDTESTDATA("data/JsonModelFactoryTest/testCase2JsonToModel/test1.json"));
    QVERIFY(f.open(QFile::ReadOnly));

    QString error;
    PendingRequest *pReq = new PendingRequest(HTTP_METHOD_ID::NONE);
    pReq->setPgFunction("TEST JsonModelFactoryTest::testCase2JsonToModel");
    QStandardItem *result = JsonQStandardItemFactory::jsonToModel(&f, error, pReq);
    f.close();

    QVERIFY2(error.isEmpty(), error.toUtf8().constData());
    QVERIFY(result);

    QCOMPARE(result->rowCount(), 2);
    QCOMPARE(result->columnCount(), 8);

    //seance #1
    QVERIFY(result->child(0,0));
    QCOMPARE(result->child(0,0)->data(DataRole::FromName("cptcolor")).toString(), QStringLiteral("#FF00BB"));
    QVERIFY(result->child(0,1));
    QCOMPARE(result->child(0,1)->text(), QStringLiteral("Title #1"));
    QVERIFY(result->child(0,2));
    QCOMPARE(result->child(0,2)->data(Qt::DisplayRole).toDateTime(), QDateTime::fromString("2014-09-26T20:00:00", Qt::ISODate));
    QVERIFY(result->child(0,3) == 0);
    QVERIFY(result->child(0,4) == 0);
    QVERIFY(result->child(0,5));
    QCOMPARE(result->child(0,5)->text(), QStringLiteral("This the description for performance #1"));
    QVERIFY(result->child(0,6) == 0);
    QVERIFY(result->child(0,7));
    QCOMPARE(result->child(0,7)->data(Qt::DisplayRole).toDouble(), 19.5);

    //seance #2
    QVERIFY(result->child(1,0));
    QCOMPARE(result->child(1,0)->data(DataRole::FromName("cptcolor")).toString(), QStringLiteral("#AABBCC"));
    QVERIFY(result->child(1,1));
    QCOMPARE(result->child(1,1)->text(), QStringLiteral("Title #2"));
    QVERIFY(result->child(1,2));
    QCOMPARE(result->child(1,2)->data(Qt::DisplayRole).toDateTime(), QDateTime::fromString("2015-09-26T20:00:00", Qt::ISODate));
    QVERIFY(result->child(1,3));
    QCOMPARE(result->child(1,3)->data(Qt::DisplayRole).toDateTime(), QDateTime::fromString("2016-09-26T20:00:00", Qt::ISODate));
    QVERIFY(result->child(1,4));
    QCOMPARE(result->child(1,4)->data(Qt::DisplayRole).toDateTime(), QDateTime::fromString("2017-09-26T20:00:00", Qt::ISODate));
    QVERIFY(result->child(1,5) == 0);
    QVERIFY(result->child(1,6) == 0);
}
