#ifndef CHECKWIDGETSTEST_H
#define CHECKWIDGETSTEST_H

#include <QObject>
#include "basetixrtestclass.h"

class CheckWidgetsTest : public QObject//BaseTixrTestClass
{
    Q_OBJECT

private Q_SLOTS:
    void testCase1();
    void testCase1_data();

public:
    explicit CheckWidgetsTest(QObject *parent = 0);
};

#endif // CHECKWIDGETSTEST_H
