#ifndef NEWSEANCETEST_H
#define NEWSEANCETEST_H

#include <QObject>
#include "../src/stock/newseancewidget.h"
#include "mockednetworkaccessmanager.h"
#include <QSignalSpy>
#include "basetixrtestclass.h"

class NewSeanceTest : public QObject//BaseTixrTestClass
{
    Q_OBJECT
    TixrNewSeanceWidget *newSeanceWidget;
    TixRealmCore *tixrCore;
    QStandardItem *root;//spectacle item
    MockedNetworkAccessManager *mockedNAM;
    QSignalSpy *sigSpy;

public:
    explicit NewSeanceTest(QObject *parent = 0);

public slots:
    void singleShot1TestCase1Slot();

private Q_SLOTS:
    void initTestCase();
    void cleanupTestCase();

    void testCase1();
    void testCase1_data();

    void cleanup();
};

#endif // NEWSEANCETEST_H
