#include "tariffstest.h"
#include <QtTest>

TariffsTest::TariffsTest()
{
}

void TariffsTest::initTestCase()
{
    index = 0;
    timer = new QTimer(this);

    tixrCore = new TixRealmCore;
    //tixrCore->setConnParam("santrossoft.ddns.net", 8086);
    tixrCore->setConnParam("localhost", 8086);
    tixrCore->setCredentials(QString("%1:%2").arg("sainttropez", "sainttropez").toUtf8().toBase64());

    QString skinFilename = QFINDTESTDATA("data/default.skin");
    QVERIFY(tixrCore->loadSkin(skinFilename));

}

void TariffsTest::cleanupTestCase()
{
    delete tixrCore;
}

void TariffsTest::init()
{
    tariffWidget = new GrillesAdminPage(tixrCore);
    connect(tariffWidget, &TixrWidget::responseReady, this, &TariffsTest::testCase1FinishSlot);
}

void TariffsTest::cleanup()
{
    timer->stop();
    delete tariffWidget;
}

void TariffsTest::testCase1()
{
    //QSKIP("not now");

    tariffWidget->show();
    qApp->exec();
}

void TariffsTest::testCase1TimerSlot()
{
    QAbstractItemModel *model = tariffWidget->grilleListView->model();
    QVERIFY(model);
    QModelIndex rIdx = tariffWidget->grilleListView->rootIndex();
    int n = model->rowCount(rIdx);
    int inc = n / 2;
    if(index < n)
    {
        QModelIndex idx = rIdx.child(index, 0);
        QRect rect = tariffWidget->grilleListView->visualRect(idx);
        QPoint p = rect.center();

        QTest::mouseClick(tariffWidget->grilleListView->viewport(), Qt::LeftButton, 0, p);//, 100);

        index += inc;

        QVERIFY(idx.data(DataRole::ikey()).isValid());
        qDebug() << "key = " << idx.data(DataRole::ikey());
    }
    else
    {
        timer->stop();
        qApp->exit();
    }
}

void TariffsTest::testCase1FinishSlot(PendingRequest *curReq)
{
    QString error = tariffWidget->getErrorMsg();

    if(!error.isEmpty())
        qApp->exit();

    QVERIFY2(error.isEmpty(), error.toUtf8().constData());

    if(curReq->getPgFunction() != "main.getallgrilles")
        return;

    connect(timer, SIGNAL(timeout()), this, SLOT(testCase1TimerSlot()));
    timer->start(1000);
}
