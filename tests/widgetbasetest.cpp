#include "widgetbasetest.h"
#include <QTest>
#include <QFile>
#include <QUiLoader>

WidgetBaseTest::WidgetBaseTest(QObject *parent) :
    QObject(parent)
{
}

void WidgetBaseTest::initTestCase()
{
    tixrCore = new TixRealmCore;

    QString skinFilename = QFINDTESTDATA("data/default.skin");
    QVERIFY(tixrCore->loadSkin(skinFilename));
}

void WidgetBaseTest::cleanupTestCase()
{
    //tixrCore->deleteLater();
    delete tixrCore;
}

void WidgetBaseTest::init()
{
    rootItem = new QStandardItem;
    tixrCore->stdModel->appendRow(rootItem);

    QString uiName = qstrcmp(QTest::currentTestFunction(), "testCase1") == 0 ? "newSeance" : "newSpect";

    tixrWidget = new TixrWidget(uiName, tixrCore, QString::null, 0, rootItem);
}

void WidgetBaseTest::cleanup()
{
    //tixrWidget->deleteLater();
    delete tixrWidget;
}

void WidgetBaseTest::testCase1()
{
    //QSKIP("not now");

    QVERIFY(tixrWidget->currentItem);
    QStandardItem *p;
    QVERIFY(p = tixrWidget->currentItem->parent());
    QVERIFY(p == tixrWidget->rootItem);

    //verificar que se asignaron los valores por defecto de los widgets
    QStandardItem *titleItem;
    QVERIFY(titleItem = p->child(tixrWidget->currentItem->row(), DataColumn::FromName("cptitre")));
    QCOMPARE(titleItem->text(), QString("Title #1"));

    QStandardItem *dateItem;
    QVERIFY(dateItem = p->child(titleItem->row(), DataColumn::FromName("cpdat")));
    QCOMPARE(dateItem->text(), QString("2014-09-26T20:00:00"));

    QStandardItem *ivaItem;
    QVERIFY(ivaItem = p->child(titleItem->row(), DataColumn::FromName("cpiva")));
    QCOMPARE(ivaItem->data(Qt::EditRole).toFloat(), 20.);

    QColor c = JsonQStandardItemFactory::getTargetItem(QStringLiteral("cptitre_cptcolor"), tixrWidget->currentItem)->data(DataRole::FromName("cptcolor")).value<QColor>();
    qDebug() << c;
    QCOMPARE(c.name(), QColor(Qt::red).name());
}

void WidgetBaseTest::testCase2()
{
    //QSKIP("not now");

    QVERIFY(tixrWidget->currentItem);
    QStandardItem *p;
    QVERIFY(p = tixrWidget->currentItem->parent());
    QVERIFY(p == tixrWidget->rootItem);

    tixrWidget->show();
    QTimer::singleShot(1000, this, SLOT(singleShot1TestCase2Slot()));

    qApp->exec();
}

void WidgetBaseTest::singleShot1TestCase2Slot()
{
    qApp->exit();

    //verificar que los datos entrados en los widgets van a parar al modelo
    QLineEdit *leTheme = tixrWidget->findChild<QLineEdit*>("leTheme");
    leTheme->setFocus();
    leTheme->clear();
    QTest::keyClicks(leTheme, "Theme #2");
    QCOMPARE(leTheme->text(), QString("Theme #2"));

    QLineEdit *leTitre = tixrWidget->findChild<QLineEdit*>("leTitre");
    leTitre->setFocus();

    QVERIFY(tixrWidget->currentItem);
    QStandardItem *p;
    QVERIFY(p = tixrWidget->currentItem->parent());
    QVERIFY(p == tixrWidget->rootItem);

    //verificar que se asignaron los nuevos valores de los widgets
    QStandardItem *temaItem;
    QVERIFY(temaItem = p->child(tixrWidget->currentItem->row(), DataColumn::FromName("fpthemecol")));
    QCOMPARE(temaItem->text(), leTheme->text());
}
