#include "newspectwidgettest.h"
#include <QtTest>
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>
#include <QJsonValue>

NewSpectWidgetTest::NewSpectWidgetTest(QObject *parent) :
    QObject(parent), sigSpy(0)
{
}

void NewSpectWidgetTest::initTestCase()
{
    tixrCore = new TixRealmCore;
    //tixrCore->setConnParam("santrossoft.ddns.net", 8086);
    tixrCore->setConnParam("localhost", 8086);
    tixrCore->setCredentials(QString("%1:%2").arg("sainttropez", "sainttropez").toUtf8().toBase64());

    tixrCore->netManager = mockedNAM = new MockedNetworkAccessManager(tixrCore);
    qDebug("initTestCase 1");
}

void NewSpectWidgetTest::cleanupTestCase()
{
    if(sigSpy)
        delete sigSpy;

    tixrCore->deleteLater();
}

void NewSpectWidgetTest::cleanup()
{
    newSpectWidget->deleteLater();
}

void NewSpectWidgetTest::testCase1()
{
    //load new test ui
    qDebug("testCase1 1");
    QFETCH(QString, skinFile);
    QVERIFY(tixrCore->loadSkin(skinFile));

    root = new QStandardItem;
    tixrCore->stdModel->appendRow(root);

    qDebug("testCase1 2");

    QTimer::singleShot(200, this, SLOT(singleShot1TestCase1Slot()));

    qApp->exec();

    qDebug("testCase1 3");
}

void NewSpectWidgetTest::testCase1_data()
{
    QTest::addColumn<QString>("skinFile");
    QTest::addColumn<QString>("outgoingData");
    QTest::addColumn<QString>("expectedOutgoingData");
    QTest::addColumn<QString>("theme");
    QTest::addColumn<QString>("title");
    QTest::addColumn<QString>("imgFileName");

    QTest::newRow("New Spect(Les SpyderMans)") << QFINDTESTDATA("data/default.skin")
                                    << QFINDTESTDATA("data/NewSpectWidgetTest/outgoingData.json")
                                    << QFINDTESTDATA("data/NewSpectWidgetTest/expectedOutgoingData.json")
                                    << QStringLiteral("Super Heros")
                                    << QStringLiteral("Les SpyderMans")
                                    << QFINDTESTDATA("data/NewSpectWidgetTest/spydermans.jpg");
}

#define exitBeforeVerify(exp) r = exp; if(!r){newSpectWidget->close();qApp->exit(-1);} QVERIFY(r)
#define exitBeforeVerify2(exp,msg) r = exp; if(!r){newSpectWidget->close();qApp->exit(-1);} QVERIFY2(r,msg)

void NewSpectWidgetTest::singleShot1TestCase1Slot()
{
    //create ui
    qDebug("linea 74");
    newSpectWidget = new TixrNewSpectWidget(tixrCore, root);

    newSpectWidget->show();

    qDebug("linea 78");
    qRegisterMetaType<QNetworkReply*>();

    bool r;
    //load expected outgoing data
    QFETCH(QString, expectedOutgoingData);
    QFile f(expectedOutgoingData);
    exitBeforeVerify(f.open(QIODevice::ReadOnly));
    QByteArray ba = f.readAll();
    QJsonParseError jsonError;
    QJsonDocument jsonDoc = QJsonDocument::fromJson(ba, &jsonError);
    f.close();

    exitBeforeVerify(f.open(QIODevice::WriteOnly));
    f.write(jsonDoc.toJson());
    f.close();

    qDebug("linea 95");
    exitBeforeVerify2(jsonError.error == QJsonParseError::NoError, jsonError.errorString().toUtf8().constData());

    exitBeforeVerify(jsonDoc.isArray() || jsonDoc.isObject());

    //set expected outgoing data
    QJsonValue jsonValue = jsonDoc.isArray() ? QJsonValue(jsonDoc.array())
                                             : QJsonValue(jsonDoc.object());
    mockedNAM->setExpectedOutgoingData(jsonValue);

    qDebug("linea 105");
    QFETCH(QString, outgoingData);
    mockedNAM->setOutputFile(outgoingData);

    QLabel *imgViewer = newSpectWidget->findChild<QLabel*>("imgViewer");
    exitBeforeVerify(imgViewer);
    QFETCH(QString, imgFileName);
    QPixmap pxm;
    exitBeforeVerify(pxm.load(imgFileName));
    imgViewer->setPixmap(pxm);

    QFETCH(QString, theme);
    QLineEdit *leTheme = newSpectWidget->findChild<QLineEdit*>("leTheme");
    leTheme->clear();
    QTest::keyClicks(leTheme, theme);

    QFETCH(QString, title);
    QLineEdit *leTitre = newSpectWidget->findChild<QLineEdit*>("leTitre");
    leTitre->clear();
    QTest::keyClicks(leTitre, title);

    QPushButton *pb = newSpectWidget->findChild<QPushButton*>("pbValiderNewSpect");
    exitBeforeVerify(pb);

    if(sigSpy)
        delete sigSpy;
    sigSpy = new QSignalSpy(tixrCore->netManager, SIGNAL(finished(QNetworkReply*)));

    qDebug("linea 133");
    //send newSeance request
    QTest::mouseClick(pb, Qt::LeftButton);

    exitBeforeVerify(sigSpy->wait(20000));

    qDebug("linea 139");

    exitBeforeVerify(sigSpy->count()==1);

    qDebug("linea 143");

    qApp->exit();

    qDebug("linea 147");
}
