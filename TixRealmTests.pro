#-------------------------------------------------
#
# Project created by QtCreator 2014-05-09T14:50:33
#
#-------------------------------------------------

QT  += gui widgets network testlib sql xml svg opengl uitools printsupport

CONFIG += c++11

TARGET = tixrealmtests
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

INCLUDEPATH += $$(INCLUDEPATHQt5) #$$(IncludePathNCReport)

CONFIG(debug, debug|release)
{
    OBJECTS_DIR = debug/obj/.obj
    MOC_DIR = debug/obj/.moc
    RCC_DIR = debug/obj/.rcc
    UI_DIR = debug/obj/.ui
}

CONFIG(release, debug|release)
{
    OBJECTS_DIR = release/obj/.obj
    MOC_DIR = release/obj/.moc
    RCC_DIR = release/obj/.rcc
    UI_DIR = release/obj/.ui
}

SOURCES += tests/tst_logintest.cpp \
    tests/main.cpp \
    tests/tariffstest.cpp \
    tests/newseancetest.cpp \
    tests/jsonmodelfactorytest.cpp \
    tests/site2dviewtest.cpp \
    tests/mockednetworkaccessmanager.cpp \
    tests/widgetbasetest.cpp \
    tests/checkwidgetstest.cpp \
    tests/newspectwidgettest.cpp \
    tests/basetixrtestclass.cpp

HEADERS += tests/tst_logintest.h \
    tests/tariffstest.h \
    tests/newseancetest.h \
    tests/jsonmodelfactorytest.h \
    tests/site2dviewtest.h \
    tests/mockednetworkaccessmanager.h \
    tests/widgetbasetest.h \
    tests/checkwidgetstest.h \
    tests/newspectwidgettest.h \
    tests/basetixrtestclass.h

include(TixRealmClientsrc.pri)
include(../TixrClientEditorCommon/tixrclienteditorcommon.pri)

CONFIG(debug, debug|release)
{
    Debug:LIBS += -L$$PWD/../TixRealmTests-build-Desktop/debug/ -lgtestd #-lNCReportDebug2 -L$$(IncludePathNCReport)/../lib

}

CONFIG(release, debug|release)
{
    Release:LIBS += -L$$PWD/../TixRealmTests-build-Desktop/release/ -lgtest #-lNCReport2 -L$$(IncludePathNCReport)/../lib

}

DISTFILES +=
