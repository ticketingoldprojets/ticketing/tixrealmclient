#-------------------------------------------------
#
# Project created by QtCreator 2010-10-27T10:29:59
#
#-------------------------------------------------

QT       += core network sql xml svg opengl printsupport

CONFIG += c++11

greaterThan(QT_MAJOR_VERSION, 4) {
        INCLUDEPATH += $$(INCLUDEPATHQt5) #$$(IncludePathNCReport)
        QT += widgets uitools
    } else {
        INCLUDEPATH += $$(INCLUDEPATHQt4) #$$(IncludePathNCReportQt4)
        QT += gui
        CONFIG += uitools
    }

TARGET = TixRealmApp
TEMPLATE = app

CONFIG(debug, debug|release)
{
    Debug:DEFINES += DEBUG_MODE=1
    OBJECTS_DIR = debug/obj/.obj
    MOC_DIR = debug/obj/.moc
    RCC_DIR = debug/obj/.rcc
    UI_DIR = debug/obj/.ui

    #greaterThan(QT_MAJOR_VERSION, 4) {
    #        Debug:LIBS += -lNCReportDebug2 -L$$(IncludePathNCReport)/../lib
    #    } else {
    #        Debug:LIBS += -lncreportd2 -L$$(IncludePathNCReportQt4)/../lib
    #    }
}

CONFIG(release, debug|release)
{
    OBJECTS_DIR = release/obj/.obj
    MOC_DIR = release/obj/.moc
    RCC_DIR = release/obj/.rcc
    UI_DIR = release/obj/.ui

    Release:DEFINES += QT_NO_DEBUG_OUTPUT

    #greaterThan(QT_MAJOR_VERSION, 4) {
    #        Release:LIBS += -lNCReport2 -L$$(IncludePathNCReport)/../lib
    #    } else {
    #        Release:LIBS += -lncreport2 -L$$(IncludePathNCReportQt4)/../lib
    #    }
}

SOURCES += $$PWD/src/main/main.cpp

include(../TixrClientEditorCommon/tixrclienteditorcommon.pri)
include(TixRealmClientsrc.pri)

TRANSLATIONS = assets/i18n/tixrealmclient_en.ts \
               assets/i18n/tixrealmclient_fr.ts

CODECFORSRC = UTF-8

RESOURCES += assets/resources.qrc
