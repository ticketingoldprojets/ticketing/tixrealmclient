#ifndef SCCARTWIDGET_H
#define SCCARTWIDGET_H

#include "core/tixrcore.h"
#include "printmanager.h"
#if (QT_VERSION < QT_VERSION_CHECK(5, 0, 0))
#include <QtGui>
#else
#include <QtWidgets>
#endif
#include "../TixRealmClientPlugins/PictureFlowViewPlugin/pictureflow.h"
#include "checkdialog.h"
#include "crm/tiersfinderdialog.h"

#define TIXR_CARTS 9

class TixrCartWidget : public QDockWidget
{
    Q_OBJECT

    QString errorStr;
    QPushButton *pbPrint;
    QStackedWidget *cartsSW;
    PictureFlowView *fvModePaiem;
    QComboBox *cbDevises;
    QDoubleSpinBox *dsbAmount;
    QDoubleSpinBox *dsbRestantDu;
    QString dsbRestantDuStyleSheet;
    QTableView *paymTView;
    QComboBox *cbPayeur;
    QPushButton *pbRecordPay;
    QPushButton *pbAjourner;
    QPushButton *pbPayer;
    QPushButton *pbErasePay;
    QString pbErasePayStyleSheet;
    QStandardItemModel *cartHeaderModel;
    QStandardItemModel *paymTViewHModel;
    TixrCheckDialog *checkDetailsDialog;
    TixrTiersFinderDialog *tiersFinderDialog;
    QToolButton *tbReset;
    QGroupBox *gbBalance;
    QDoubleSpinBox *dsbCurBalance;
    QDoubleSpinBox *dsbFinBalance;

    QLabel *waitLabel;
    QList< QList<PendingRequest*> > curTasksList;
    QMovie *waitMovie;

    // QStandardItemModel index row of each cart maps to a print job pointer (if cart currently has one active)
    QMap<int, PrintJob*> cartsPrintJobs;
    int prevCartIndex;

    PrintManager *printMgr;

    void createCart(QStandardItem *currentCartItem = 0);
    void assignCart(QStandardItem *currentCartItem, QStandardItem *&newCartItem);
    void destroyCurrentCart();
    void updateHistory(QStandardItem *cartItm);
    void delCurCart();
    void enleverProds(QList<QStandardItem*> &prodsList, bool refreshCartWidgets = false, int amount = 0);
    void embedProdItem(QStandardItem *cartItm, QStandardItem *catCliItm, QStandardItem *pItm);
    inline void filterProducts(QList<QStandardItem*> *prodsList);
    void recordPay(bool dette, QStandardItem *modeItem);
    void updatePrintStateItem(QStandardItem *item, int printImgNum, PrintManager::StatusCode statusCode, int numPrinted = -1, int qty = -1);
    bool isEmpty(QStandardItem *cItm = 0);
    inline void updateCurrentCartWidgets();
    inline void updatePaymentItem(QStandardItem *payItem, const QJsonObject &rawPaym, int orderId);
    inline QList<QStandardItem*> findPaymentsForItem(QStandardItem *paymsRoot, int ordId, int itId, QVariant paymId = QVariant());
    void mergePaymInfo(QStandardItem *P, QStandardItem *Q);
    void decodeDataRecursive(QDataStream &stream, QStandardItem *item);//gestión de drop events
    bool beginHandleResponse(QString respError, PendingRequest *curReq);
    void endHandleResponse(PendingRequest *curReq);
    void handlePayWithFund(const QStandardItem *orderItem, const QJsonObject &rawResp);

private slots:
    void on_pbPayer_clicked();
    void checkDialogAccepted();
    void on_pbRecordPay_clicked();
    void on_pbPrint_clicked();
    void printResult(PrintManager::StatusCode statusCode, QString statusMsg, PrintResultData printResultData);
    void on_pbNew_clicked();
    void on_pbDel_clicked();
    void on_pbErasePay_clicked();
    void on_pbEnleverProd_clicked();
    void on_tbFind_clicked();
    void setPayor();
    void on_tbReset_clicked();
    void animateIcon(const QRect & rect);
    void changeCurrentCart(const QModelIndex & index);
    void on_paymTView_doubleClicked(const QModelIndex & index);
    void handleCartDropEvent(QDropEvent *event);
    void cartViewSelectionChanged(const QItemSelection &, const QItemSelection &);
    void paymTViewSelectionChanged(const QItemSelection &, const QItemSelection &);
    void handleAddCart(QStandardItem *newCartItm);
    void handleFindCart(int clientid, QStandardItem *cartRoot);
    void responseGetModePaiem(QString respError, PendingRequest *curReq);
    void responseGetDevises(QString respError, PendingRequest *curReq);
    void responseDoPayment(QString respError, PendingRequest *curReq);
    void responseEnleverProduits(QString respError, PendingRequest *curReq);
    void responseGetBanques(QString respError, PendingRequest *curReq);
    void responseGetTiers(QString respError, PendingRequest *curReq);
    void responseAnnulPaiem(QString respError, PendingRequest *curReq);
    void responseAssignCart(QString respError, PendingRequest *curReq);

    void on_fvModePaiem_clicked(const QModelIndex &index);

protected:
    void showEvent(QShowEvent * event);
    void hideEvent(QHideEvent *event);

public:
    explicit TixrCartWidget(TixRealmCore *mObj, QWidget *parent = 0);
    ~TixrCartWidget();
    QString getErrorMsg(){return errorStr;}
    int getScWtype(){return TIXR_CARTS;}
    void updatePrintButton();
    QList<QStandardItem*> getProds(bool forgetSelection = false, QStandardItem *cItm = 0);//get all products(leaf items) in the cItm cart (or current cart if cItm==0) if there is no item selected else return all product items under selected item; return only selected item if it is a prod. item.
    PrintItems getPrintableProducts();
    void setAlwaysOnTopDialogVisibility(bool visible);

    QTreeView *cartView;
    QDoubleSpinBox *totalDSB;
    QDoubleSpinBox *sousTotalDSB;
    QStandardItem *cartsItem;//Contenedor de todos los cart's item
    PictureFlowView *cartsFView;

    TixRealmCore *tixrCore;

signals:
    void notifyTaskDone(PendingRequest *curReq);
    void cartRequestResult(bool, QStandardItem*);

public slots:
    void responseAddToCar(QString respError, PendingRequest *curReq);
    void addToCarFromPlan(QList<QStandardItem*>);//venta desde un mapa de sala
};

#endif // SCCARTWIDGET_H

