#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "core/tixrcore.h"
#if (QT_VERSION < QT_VERSION_CHECK(5, 0, 0))
#include <QtGui>
#else
#include <QtWidgets>
#endif
#include "cartwidget.h"

/*!
 * \class MainWindow mainwindow.h
 * \brief MainWindow est responsable de la géstion de la fenêtre principale de l'interface d'utilisateur.
 */
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(TixRealmCore *mainObj);
    ~MainWindow();
    void setAlwaysOnTopWidgetsVisibility(bool visible);

    /*!
     * \brief Réimplémentation de <em>QMetaObject::connectSlotsByName</em>.
     *
     * Plusieurs avantages, dans le contexte de TixRealmClient, par rapport à <em>QMetaObject::connectSlotsByName</em>:
     *  - Permet paramétrer le point de départ \a r dans la hiérrarchie avec racine \a o. Si \a r es 0 on prend comme point de départ \a o.
     *      \a t est le type de connexion à établir, au contraire de la méthode originale on peut l'exécuter plusieurs fois sans le risque de créer de connexions dupliquées (en utilisant la valeur par défaut pour \a t, évidamment).
     *  - Permet connecter les signals de plusieurs objets au même <I>slot</I> en utilisant une règle pour nommer les <I>slot</I> et les objets dont ils appartiennent:
     *      tous les objets dont leurs nom termine par ___D, étant D un nombre naturel, seront traités comme en ayant le même nom résultant de supprimer le sufixe. Par example btn___1 et btn___2 seront connectés au <I>slot</I> on_btn_clicked.
     */
    static void tixrConnectSlotsByName(QObject *o, QObject *r = 0, Qt::ConnectionType t = Qt::UniqueConnection);//!<Reimplementación de QMetaObject::connectSlotsByName

signals:
    //void sitAtSignal(double x, double y, double z);//QString placeId);
    void sitAtSignal(QString placeId);

private:
    void setupUI();

    QMenu *menuOutils; //A ELIMINAR, LUEGO DE RECUPERAR EL CODIGO CONECTADO A ESTO

    QDockWidget *mainToolBar;
    QTabWidget *mainTabWidget;
    QPushButton *pbFloatingPanel;
    QComboBox *cbProductType;
    //QToolButton *tb3dView;
    //QToolButton *tbLoadCart;

    //Panier
    TixrCartWidget *cartWidget;

    TixRealmCore *tixrCore;
    //ModelView *view3D;

private slots:
    //void print();

    void on_pbStock_clicked();
    void on_pbFloatingPanel_clicked();
    //void on_tb3dView_clicked();

    // CRM (HECHO POR PACHECO) (debería ser una clase)
    void on_pbCRM_clicked();            // evento incorporado para la manipulacion de clientes (cypc)
    // FIN HECHO POR PACHECO

    void on_mainTabWidget_tabCloseRequested(int index);
    //void on_mainTabWidget_currentChanged(int index);

    //Panier
    //void on_tbLoadCart_clicked();

    //Configuration
    void on_pbConfig_clicked();

    void on_cartWidget_dockLocationChanged(Qt::DockWidgetArea area);

    void on_pbClose_clicked();
};

#endif // MAINWINDOW_H

