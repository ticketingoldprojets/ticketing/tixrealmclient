#include "remboursementdialog.h"
#include "crm/newtierswidget.h"
#include "mainwindow.h"

TixrRemboursementDialog::TixrRemboursementDialog(TixRealmCore *mo, QWidget *blockedWidget, QStandardItem *cItm, QWidget *parent) :
    TixrDialog(":/formconfig/remboursement.ui", blockedWidget, parent), cliItem(cItm)
{
    tixrCore = mo;

    fvModePaiem = (PictureFlowView*)findChild<QAbstractItemView*>("fvModePaiem");
    dsbAmount = findChild<QDoubleSpinBox*>("dsbAmount");
    leComent = findChild<QLineEdit*>("leComent");
    rbInternal = findChild<QRadioButton*>("rbInternal");

    fvModePaiem->setModel(tixrCore->stdModel);
    fvModePaiem->setRootIndex(tixrCore->keyToItemMap.value("modpaym")->index());

    MainWindow::tixrConnectSlotsByName(this);
}

void TixrRemboursementDialog::setAmount()
{
    dsbAmount->setMaximum(cliItem->parent()->child(cliItem->row(), DataColumn::taccountcol())->data(Qt::DisplayRole).toDouble());
    dsbAmount->setValue(dsbAmount->maximum());
}

void TixrRemboursementDialog::on_pbOk_clicked()
{
    QStandardItem *modelSubItem = new QStandardItem(1, 5);
    modelSubItem->setData("RemboursementParams", DataRole::jsonProperty());
    modelSubItem->setData(JsonObject, DataRole::jsonType()); //elemento

    QStandardItem *modelSubItem2 = new QStandardItem(cliItem->data(DataRole::ikey()).toString());
    modelSubItem2->setData("clientid", DataRole::jsonProperty());
    modelSubItem2->setData(JsonSimple, DataRole::jsonType());
    modelSubItem->setChild(0, 0, modelSubItem2);

    modelSubItem2 = new QStandardItem;
    modelSubItem2->setData(dsbAmount->value(), Qt::DisplayRole);
    modelSubItem2->setData("mont", DataRole::jsonProperty());
    modelSubItem2->setData(JsonSimple, DataRole::jsonType());
    modelSubItem->setChild(0, 1, modelSubItem2);

    QStandardItem *mpItm = tixrCore->stdModel->itemFromIndex(fvModePaiem->rootIndex().child(fvModePaiem->currentSlide(), 0));
    modelSubItem2 = new QStandardItem(mpItm->data(DataRole::ikey()).toString());
    modelSubItem2->setData("idmodpay", DataRole::jsonProperty());
    modelSubItem2->setData(JsonSimple, DataRole::jsonType());
    modelSubItem->setChild(0, 2, modelSubItem2);

    modelSubItem2 = new QStandardItem(rbInternal->isChecked()?"1":"0");
    modelSubItem2->setData("external", DataRole::jsonProperty());
    modelSubItem2->setData(JsonSimple, DataRole::jsonType());
    modelSubItem->setChild(0, 3, modelSubItem2);

    modelSubItem2 = new QStandardItem(leComent->text());
    modelSubItem2->setData("descrip", DataRole::jsonProperty());
    modelSubItem2->setData(JsonSimple, DataRole::jsonType());
    modelSubItem->setChild(0, 4, modelSubItem2);

    modelSubItem->setData(QVariant(reinterpret_cast<tixrqptr>(mpItm)), DataRole::indicator());

    TixrNewTiersWidget *ntW = reinterpret_cast<TixrNewTiersWidget*>(parent());
    tixrCore->setPgFunction("main.remboursement");
    ntW->currentTaskList << tixrCore->sendMsg(modelSubItem, HTTP_METHOD_ID::POST, "/api", ntW, "responseRemboursement");

    TixrDialog::on_pbOk_clicked();
}
