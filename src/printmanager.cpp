#include "printmanager.h"
#include "core/tixrcore.h"
#include <QMetaType>
#include <QBuffer>
#include <QtNetwork>
#include <QSqlDatabase>

//! NCReport report generator/rendering engine
/*#include <ncreport/ncreport.h>
#include <ncreport/ncreportoutput.h>
#include <ncreport/ncreportpreviewoutput.h>
#include <ncreport/ncreportpreviewwindow.h>*/

/*!
 * \brief PrintManager::PrintManager
 * \param mObj
 * \param parent
 */
PrintManager::PrintManager(TixRealmCore *mObj, QObject *parent) :
    QObject(parent), tixrCore(mObj), netAccMgr(new QNetworkAccessManager(this)), visiblePrintJob(0)
{
    // Register as meta type so can connect signals and slots using it as
    // a parameter. Note it has to be fully qualified with the class name
    // both here and in the connect call otherwise Qt fails to match them.
    qRegisterMetaType<StatusCode>("PrintManager::StatusCode");
    qRegisterMetaType<StatusCode>("PrintResultData");
    qRegisterMetaType<StatusCode>("PrintItems");

    // Seed random numbers (for creating unique temporary filenames)
    QTime time = QTime::currentTime();
    qsrand((uint)time.msec());

    // Registrer les serveurs d'impression
    QStandardItem *printServersRoot = new QStandardItem;
    tixrCore->stdModel->appendRow(printServersRoot);
    tixrCore->keyToItemMap.insert("printservers", printServersRoot);

    // Add server 1
    QStandardItem *serverItem = new QStandardItem("http://192.168.1.3:9085");
    printServersRoot->appendRow(serverItem);

    // Add server 2
//    QStandardItem *serverItem = new QStandardItem("http://localhost:9085");
//    printServersRoot->appendRow(serverItem);
}


/*!
 * \brief PrintManager::~PrintManager
 */
PrintManager::~PrintManager()
{
    delete netAccMgr;
}


/*!
 * \brief PrintManager::print
 * \param reportFileName Report source filename
 * \param printItems
 * \param threadParent
 * \param uiParent
 * \return A print job thread object pointer.
 *  A null pointer if printing failed to start with a printResult signal also being emitted
 * (this is automatically connected to the caller's printResult slot).
 * \note The print job thread automatically gets deleted once the printing has finished or been aborted.
 */
PrintJob *PrintManager::print(QString reportFileName, PrintItems printItems, QObject *threadParent, QWidget *uiParent, QVariant userData)
{
////TEST
//  printItems.clear(); printItems << QPair<"666", 0> << QPair<"616", 0>;

    // Send the request to the print server
    QStandardItem *printServersRoot = tixrCore->keyToItemMap.value("printservers");
    QStringList printServers;
    QModelIndex printServerRootIdx = tixrCore->stdModel->indexFromItem(printServersRoot);
    int numRows = tixrCore->stdModel->rowCount(printServerRootIdx);
    for (int row = 0; row < numRows; ++row) {
         QStandardItem *printServer = printServersRoot->child(row);
         printServers << printServer->text();
    }

//TODO CHOOSE UNOCCUPIED PRINT SERVER FROM LIST...
//NOTE PRINTER TO CHOOSE COULD ALSO DEPEND ON PRODUCT (CATEGORY) OF TICKET BEING PRINTED
// I.E. PRINTERS COULD HAVE CATEGORIES SPECIFIED FOR THEM (OR CATEGORY GROUP ID PERTAINING TO SET OF CATEGORIES)???
    QString printServerUrl = printServers.at(0);
    PrintJob *printJob = addPrintJob(printServerUrl, threadParent, uiParent, userData);

    PrintManager::StatusCode statusCode = Ok;
    QString errorMsg;
//  QList<QByteArray*> pages;
//  createPrintPages(pages, reportFileName, idList, statusCode, errorMsg);
//  qDeleteAll(pages);
//  pages.clear();

    if (statusCode == Ok) {
//      printJob->print(pages);
        printJob->print(reportFileName, printItems);
    }
    else {
        // Note the connection type is UniqueConnection to avoid multiple connections to the thread parent's same slot on subsequent print calls!
        connect(this, SIGNAL(printResult(PrintManager::StatusCode, QString, PrintResultData)), threadParent,
                SLOT(printResult(PrintManager::StatusCode, QString, PrintResultData)), Qt::UniqueConnection);
        PrintResultData printResultData;
        printResultData.insert("userData", userData);
        emit printResult(statusCode, errorMsg, printResultData);
    }

    return printJob;
}


/*!
 * \brief PrintManager::lockCreatePrintPagesMutex
 * \param lock Defaults to true i.e. locking the mutex; false will unlock it.
 */
void PrintManager::lockCreatePrintPagesMutex(bool lock)
{
    if (lock)
        createPrintPagesMutex.lock();
    else
        createPrintPagesMutex.unlock();
}


/*!
 * CustomNCReportImageOutput
 *
 * Custom report image output class (copied from NCReportImageOutput) that
 * stores the resultant pages in memory only, i.e. they are not written to disk.
 */
class CustomNCReportImageOutput //: public NCReportOutput
{
//    Q_OBJECT
public:
    explicit CustomNCReportImageOutput(QObject *parent = 0) : //NCReportOutput(parent),
//      m_imageFormat("JPG"), m_format(QImage::Format_RGB32), m_maxHeight(20000)
        m_imageFormat("JPG"), m_format(QImage::Format_Mono), m_maxHeight(20000)
    {
        //setOutput(Image);
    }

    ~CustomNCReportImageOutput()
    {
//      qDeleteAll(m_pages.begin(), m_pages.end());
        m_pages.clear();
    }

    void begin()
    {
        QPainter *p = new QPainter();
        p->setRenderHint(QPainter::SmoothPixmapTransform, true);
        setPainter(p);
        newPage();
    }

    void end()
    {
        //painter()->end();
        //deletePainter();

//HS DON'T SAVE OUT THE PAGES TO DISK HERE!!
//      QFileInfo fi(fileName());
//      if (isSuccessive()) {
//          // COPY RESULT TO A NEW IMAGE
//          if (m_pages.count()>0) {
//              imageSize.setHeight( scale().mmToPixel(finalPosMM().y() + option().bottomMargin() ) );
//              m_result = m_pages.at(0).copy( QRect(QPoint(0,0),imageSize) );
//              m_result.save(fileName());
//          }
//      } else {
//          for (int i = 0; i < m_pages.size(); ++i) {
//              QString pageFile = QString("%1_p%2.%3").arg(fi.path()+"/"+fi.baseName()).arg(i+1).arg(fi.suffix());
//              if ( !m_pages.at(i).save(pageFile, fi.suffix().toLatin1() ) ) // writes image into ba in XXX format
//                   return;
//          }
//      }

        // HS - Inverse pixels as monochrome image is coming out with a black background?
        for (int i = 0; i < m_pages.size(); ++i) {
            m_pages[i].invertPixels();
        }
    }

    void newPage()
    {
        /*if (painter() && painter()->isActive()) {
            painter()->end();
        }

        imageSize.setWidth(scale().mmToPixel(option().pageWidthMM()));

        if (isSuccessive())
            imageSize.setHeight(m_maxHeight);
        else
            imageSize.setHeight(scale().mmToPixel(option().pageHeightMM()));

        QImage image(imageSize, m_format);
        m_pages.append(image);

        painter()->begin(&m_pages.last());
//      if (resolution() != 96)
//          painter()->scale(resolution()/96,resolution()/96);

        // draw page background
        painter()->setPen(Qt::NoPen);
        painter()->setBrush(QBrush(Qt::white));
//      painter()->setPen(QPen(Qt::black));
//      painter()->setBrush(QBrush(Qt::black));
        painter()->drawRect(QRect(QPoint(0,0), imageSize));*/
    }

    bool setup() { return true; }
    void cleanup() {}  // Pages not delete as are needed

    void setImageFormat(const char *format) { m_imageFormat = format; }
    const QImage &page(int i) const { return m_pages.at(i); }
    // Sets maximum height of image which affects only when endless media mode is active.
    void setMaximumHeight(int height) { m_maxHeight = height; }
    int maximumHeight() const { return m_maxHeight; }
    void setFormat(QImage::Format format) { m_format = format; }
    QImage::Format format() const { return m_format; }
    const QImage &successiveResult() const { return m_result; }
//  void setPixelWidth(int width) { option().setPageWidth(scale().pixelToMM(width)); }

protected:
    void setPainter(QPainter *painter) { /*NCReportOutput::setPainter(painter);*/ }

private:
    QString m_imageFormat;
    QImage::Format m_format;
    int m_maxHeight;
    QSize imageSize;
    QList<QImage> m_pages;
    QImage m_result;
};


/*!
 * \brief PrintManager::createPrintPages
 *
 * \warning Memory will be allocated if pages_out has empty or insufficient QByteArray
 *  pointers, so it is the responsability of the client code to free these.
 * \param pages_out A list of byte array pointers. If empty it will be filled with allocated arrays.
 * \param reportFileName
 * \param printItems
 * \return The status code determining if the operation was successful.
 *  On success creates a list of byte arrays in pages_out each containing an image for the report pages in PNG format.
 */
PrintManager::StatusCode PrintManager::createPrintPages(QList<QByteArray*> *pages_out, QString reportFileName, PrintItems printItems)
{
    PrintManager::StatusCode statusCode = Ok;

    // Obtain server database connection details from the TixRealm main object
    // Note this has to be done here as they are not available when the PrintManager ctor is called
    QStandardItem *userItm = tixrCore->keyToItemMap.value("login");
    QString dbServer = userItm->data(DataRole::host()).toString();
    int dbPort = userItm->data(DataRole::port()).toInt();
    QString dbName = userItm->data(DataRole::database()).toString();
    QString dbUsername = dbName % "_" % userItm->data(DataRole::slogin()).toString(); // Real name of the user used is prefixed by the database name
    QString dbPassword = userItm->data(DataRole::passw()).toString();

    // The report uses the default database connection when connecting externally from the report
    // I.e. database connection details are not stored within the report template itself
    QSqlDatabase oldDefaultSqlDatabase = QSqlDatabase::database();
    QSqlDatabase defaultDB = QSqlDatabase::addDatabase("QPSQL");
    if (!defaultDB.isValid())
        return ErrorGeneratingReport;
    defaultDB.setHostName(dbServer);
    defaultDB.setDatabaseName(dbName);
    defaultDB.setPort(dbPort);
    defaultDB.setUserName(dbUsername);
    defaultDB.setPassword(dbPassword);
    if (!defaultDB.open())
        return ErrorGeneratingReport;

    //NCReport *report = new NCReport;
    //report->setReportFile(reportFileName);
    QStringList idsList;
    foreach (PrintItem item, printItems)
        idsList << item.first;
    QString idListStr = idsList.join(",");
    QString dcIds = "'{" + idListStr + "}'";    // PL/pgSQL array literal constant '{n1, n2, n3}'
    //report->addParameter("detailscommande_ids", dcIds);

    qint64 millisecs = QDateTime::currentMSecsSinceEpoch();
    QString uniqueSuffix = QString::number(millisecs) + "_" + QString::number(qrand());
    QString tempFileNameBase = "temp-print-page_" + uniqueSuffix;
    QString tempFileNameExt = ".png";
    numPages = -1;
    //connect(report, SIGNAL(dataSourceOpened(const QString &, int)), SLOT(on_reportDataSourceOpened(const QString &, int)));

    // Generate print report pages
/// report->runReportToImage(tempFileNameBase + tempFileNameExt);
    CustomNCReportImageOutput *reportOutput = new CustomNCReportImageOutput;
    //reportOutput->setAutoDelete(FALSE);
    //report->setOutput(reportOutput);
    //report->runReport();
    //QString errorMsg = report->lastErrorMsg();

    if (/*!report->hasError()*/1) {
        for (int i = 0; i++ < numPages && statusCode == Ok;) {
///         QString tempFileNamePageSuffix = "_p" % QString::number(i);
///         QString tempPageFileName = tempFileNameBase + tempFileNamePageSuffix + tempFileNameExt;
///         QImage img(tempPageFileName);
            QImage img = reportOutput->page(i-1);
            if (!img.isNull()) {
//              img = img.convertToFormat(QImage::Format_Mono, Qt::ThresholdDither); // Page format must be monochrome
//              // Rotation done server side now when outputting print buffer as quicker
//              QTransform rotTrans;
//              rotTrans.rotate(-90);
//              img = img.transformed(rotTrans, Qt::FastTransformation);

                // Save generated monochrome/rotated version of image into memory buffer
                QBuffer imgBuffer;
                img.save(&imgBuffer, "PNG");
                const QByteArray imgData = imgBuffer.buffer();
                if (!imgData.isEmpty()) {
                    if (i > pages_out->count())
                        pages_out->append(new QByteArray);
                    *(*pages_out)[i-1] = imgData;  // Note 'i' is one-based
                }
                else
                    statusCode = ImageConversionError;
            }
            else
                statusCode = InvalidImageError;

            // Delete image file created by NCReport for current page
///         QFile::remove(tempPageFileName);
        }

        if (numPages == 0) {
            statusCode = NothingToPrint;

            // Delete blank image file created by NCReport when no report pages have been generated!
///         QFile::remove(tempFileNameBase % "_p1" % tempFileNameExt);
        }
    }
    else
        statusCode = ErrorGeneratingReport;

    delete reportOutput;
    //delete report;  // Destruct report object

    // Restore old default database (to be safe and not have the side effect of changing this)
    QString defaultDbDriver = oldDefaultSqlDatabase.driverName();
    QSqlDatabase::addDatabase(defaultDbDriver);

    return statusCode;
}


/*!
 * \brief on_reportDataSourceOpened
 * \param dsId
 * \param totalRows
 */
void PrintManager::on_reportDataSourceOpened(const QString &dsId, int totalRows)
{
    Q_UNUSED(dsId);

    // The number of table result sets (rows) returned by the report's data source, i.e. the PL/pgSQL
    // stored procedure return values, will be the number of pages (e.g. individual tickets) to print
    numPages = totalRows;
}


/*!
 * \brief PrintManager::addPrintJob
 * \param printServerUrl
 * \param threadParent
 * \param uiParent
 * \param userData
 * \return A new print job thread pointer.
 */
PrintJob *PrintManager::addPrintJob(QString printServerUrl, QObject *threadParent, QWidget *uiParent, QVariant userData)
{
    PrintJob *printJob = new PrintJob(this, printServerUrl, threadParent, uiParent, userData);
    printJobs.append(printJob);
    showPrintJob(printJob);
    return printJob;
}


/*!
 * \brief PrintManager::removePrintJob
 * \param printJob
 */
void PrintManager::removePrintJob(PrintJob *printJob)
{
    int idx = printJobs.indexOf(printJob);
    printJobs.removeAt(idx);

    // Deferred deletion of the print job object. Note that any modal message boxes shown by the print job client
    // will block this happening (and thus keep the print job's progress widget visible) until it gets closed.
    printJob->deleteLater();
}


/*!
 * \brief PrintManager::statusCodeToString
 * \param statusCode
 * \param format %1 places the stringified error code in the output string, %2 places the error code value.
 * \return The error code enum in string format.
 */
QString PrintManager::statusCodeToString(PrintManager::StatusCode statusCode, const QString format)
{
    // Retrieve error code enum as string from meta object
    QMetaEnum metaEnum = staticMetaObject.enumerator(staticMetaObject.indexOfEnumerator("StatusCode"));
    return format.arg(metaEnum.valueToKey(statusCode)).arg(statusCode);
}


/*!
 * \brief PrintManager::generalStatusMsg
 * \param statusCode
 * \return
 */
QString PrintManager::generalStatusMsg(StatusCode statusCode)
{
    switch (statusCode) {
    case Ok:
    case PrintStarted:
        return tr("Printing has started");          // "L'impression a commencé"
    case PrintFinished:
        return tr("Printing has finished");         // "L'impression est terminée"
    case PrintNotStarted:
        return tr("Printing not started");          // "L'impression n'a pas commencé"
    case PrintNotFinished:
        return tr("Printing not finished");         // "L'impression n'a pas terminé"
    case PrintPaused:
        return tr("Printing has been paused");      // "L'impression est suspendue"
    case PrintResumed:
        return tr("Printing has been resumed");     // "Reprise de l'impression"
    case PrintAborted:
        return tr("Printing has been terminated");  // "L'impression a été annulée"
    case ConfirmAbortPrinting:
        return tr("Are you sure you wish to abort the print job?");  // "Etes-vous sûr de vouloir annuler l'impression ?"
    case ImageConversionError:
        return tr("Image conversion error (data buffer empty after buffer saved)");
    case InvalidImageError:
        return tr("Invalid image error");
    case NothingToPrint:
        return tr("There are no tickets to print");
    case ErrorGeneratingReport:
        return tr("Error generating the report pages");
    default:
        return tr("Print job failed");              // "Échec de l'impression"
    }
}


/*!
 * \brief PrintManager::showPrintJob
 * \param printJob Print job to show, will hide the one currently visible.
 *  If zero then the currently visible print job will be hidden and no others shown.
 * \note Could make this so that zero pointers will do nothing
 *  i.e. not cause the currently visible print job to be hidden.
 */
void PrintManager::showPrintJob(PrintJob *printJob)
{
    if (visiblePrintJob && printJob != visiblePrintJob)
        visiblePrintJob->hide();
    visiblePrintJob = printJob;
    if (printJob)
        printJob->show();
}


/*!
 * \brief PrintManager::hidePrintJob
 * \param printJob Print job to hide, does nothing if it is not the one currently visible.
 * \todo Another active print job (perhaps the most recent) should be
 *  chosen and made visible from the list to replace the one just hidden.
 */
void PrintManager::hidePrintJob(PrintJob *printJob)
{
    if (printJob == visiblePrintJob) {
        printJob->hide();
        visiblePrintJob = 0;

        //TODO: SHOULD PICK AN ACTIVE PRINTJOB FROM PRINTJOBS LIST TO REPLACE THE ONE HIDDEN
        PrintJob *anotherPrintJob = 0;
        showPrintJob(anotherPrintJob);
    }
}


/*!
 * \brief PrintJob::PrintJob
 * \param printMgr
 * \param printServerUrl
 * \param threadParent
 * \param uiParent
 * \param userData
 */
PrintJob::PrintJob(PrintManager *printMgr, QString printServerUrl, QObject *threadParent, QWidget *uiParent, QVariant userData) :
    QObject(threadParent), printServerUrl(printServerUrl), printMgr(printMgr), userData(userData),
    totalPages(0), currentPage(0), itemsIndex(-1), printId(0), netReply(0), printingPaused(false), abortPrintPending(false)
{
    // Start progress dialog
    printProgressWidget = new PrintProgressWidget(0, totalPages, uiParent);
    printProgressWidget->show();
    connect(this, SIGNAL(printResult(PrintManager::StatusCode, QString, PrintResultData)), threadParent,
            SLOT(printResult(PrintManager::StatusCode, QString, PrintResultData)));
}


/*!
 * \brief PrintJob::~PrintJob
 */
PrintJob::~PrintJob()
{
    if (netReply) {
        netReply->disconnect();
        netReply->deleteLater();
    }

    // Needed so the print manager can clear its visiblePrintJob if it is this one
    printMgr->hidePrintJob(this);

    qDeleteAll(pages);
    pages.clear();
    delete printProgressWidget;
}


/*!
 * \brief PrintJob::print
 * \param pages
 */
void PrintJob::print(QList<QByteArray*> pages)
{
    this->pages = pages;
    totalPages = pages.count();
    printProgressWidget->setMaximum(totalPages);
    sendPrintPageRequest();
}


/*!
 * \brief PrintJob::print
 * \param reportFileName
 * \param printItems
 */
void PrintJob::print(QString reportFileName, PrintItems printItems)
{
    this->reportFileName = reportFileName;
    this->printItems = printItems;
    itemsIndex = 0;

    // The number of ids in the list no longer equates to the number of pages as each (panier item) id
    // can relate to several tickets so the server side stored procedure must generate the records for all
    // these and return them back to the report generator. This is done in the print job's worker thread.
//  totalPages = idList.count();
//  printProgressWidget->setMaximum(totalPages);
    sendPrintPageRequest();
}


/*!
 * \brief PrintJob::sendPrintPageRequest
 */
void PrintJob::sendPrintPageRequest()
{
    if (currentPage == 0)
        emitSignal_printResult(PrintManager::PrintStarted);

    networkReplyErrorSignalled = false;

    // Multiple pages produced by NCReport for the current id list?
    // This is done by the print job worker thread (see below)
    // (Either that or all pages were batch pre-generated by the PrintManager)
    if (!pages.isEmpty()) {
        sendCreatedPrintPageRequest();
    }
    else if (!printItems.isEmpty()) {
        // Create a worker thread to asynchronously generate the page image to print (so as not to block the main thread)
        PrintJobWorker *printJobWorker = new PrintJobWorker(this);
        connect(printJobWorker, SIGNAL(pagesGenerated(PrintManager::StatusCode)), this, SLOT(on_pagesGenerated(PrintManager::StatusCode)));

        // Generate & print all pages! (Could send these to the worker thread one id at a time if processing all ids
        // at once takes too long, but will need to create a DB server procedure to count all pages to be generated)
        printJobWorker->startGeneratingPages(&pages, reportFileName, printItems);
        itemsIndex += printItems.count();  // Go to end of list
//      printJobWorker->startGeneratingPages(&pages, reportFileName, idList.at(itemsIndex));
//      ++itemsIndex;
    }
    else
    {
        // No ids provided in list!
        // Deferred so client code doesn't attempt to remove a print job from a list before the print manager's print
        // method has returned and hence the print job pointer would not have yet been provided to go in said list!
        deferredEmitSignal_printResult(PrintManager::NothingToPrint);
        printMgr->removePrintJob(this);
    }
}


/*!
 * \brief PrintJobWorker::startGeneratingPages
 * \param pages
 * \param reportFileName
 * \param printItems
 * \return An array of bytes in data member 'page' containing the image for the report page 'id'.
 */
void PrintJobWorker::startGeneratingPages(QList<QByteArray*> *pages, QString reportFileName, PrintItems printItems)
{
    this->reportFileName = reportFileName;
//  idList << id;
//  pages.append(page);
    this->printItems = printItems;

///NOT SURE IF THIS IS OK BECAUSE THE THREAD WILL RUN IN A DIFFERENT EXECUTION CONTEXT SO THE POINTER WILL BE POINTING TO MEMORY THAT DOES NOT BELONG TO IT??
    this->pages = pages;

///TEST
{
        ///DO WORK HERE IN THE MAIN THREAD AS A TEST...
        owner->printMgr->lockCreatePrintPagesMutex();
        PrintManager::StatusCode statusCode = owner->printMgr->createPrintPages(pages, reportFileName, printItems);
        owner->printMgr->lockCreatePrintPagesMutex(false);
        emit pagesGenerated(statusCode);
        deleteLater();
        return;
}

    // Begin thread execution
    start();
}


/*!
 * \brief PrintJobWorker::run
 * \note When run() is invoked then it is inside another thread and any objects created in there belong to that thread.
 *  Whereas the QThread object itself belongs to the thread that it was actually created in which is typically the main thread.
 */
void PrintJobWorker::run()
{
    // Lock access to the PrintManager::createPrintPages(). This thread will block
    // if another thread is calling it until finished and the mutex is unlocked again.
    owner->printMgr->lockCreatePrintPagesMutex();
    PrintManager::StatusCode statusCode = owner->printMgr->createPrintPages(pages, reportFileName, printItems);
    owner->printMgr->lockCreatePrintPagesMutex(false);
//  Q_ASSERT(pages.count() == 1);
//  qDeleteAll(pages);  // No need to delete pointer memory as should point to PrintJob object's page member

    emit pagesGenerated(statusCode);

    // **Poof** thread will self destruct now that it's finished doing its work
}


/*!
 * \brief PrintJob::on_pagesGenerated
 * \param statusCode
 */
void PrintJob::on_pagesGenerated(PrintManager::StatusCode statusCode)
{
    if (statusCode == PrintManager::Ok) {
        totalPages = pages.count();
        printProgressWidget->setMaximum(totalPages);
        sendCreatedPrintPageRequest();
    }
    else
        signalNetworkReplyError(statusCode);
}


/*!
 * \brief PrintJob::sendCreatedPrintPageRequest
 */
void PrintJob::sendCreatedPrintPageRequest()
{
    setNextTicketInSeqPrintState("in progress");

    QBuffer xmlReqBody;
    QXmlStreamWriter xmlStream;

    Q_ASSERT(!pages.isEmpty());
    QByteArray &currPage = *pages.takeFirst();

    createXmlReqBody(xmlReqBody, xmlStream, currPage);

//  networkReplyErrorSignalled = false;
    lastHttpStatusCode = 0;
    lastHttpReasonPhrase.clear();
    lastResponseErrorMessage.clear();

    QNetworkRequest netRequest(QUrl(printServerUrl % "/api/print"));
//  netRequest.setRawHeader("User-Agent", "TixRealmClient client application");
    netRequest.setHeader(QNetworkRequest::ContentLengthHeader, xmlReqBody.buffer().length());
    netReply = printMgr->netAccMgr->post(netRequest, xmlReqBody.buffer());

//  connect(printMgr->netAccMgr, SIGNAL(finish(QNetworkReply*)), this, SLOT(on_networkReply_finish(QNetworkReply*)));
    connect(netReply, SIGNAL(readyRead()), this, SLOT(on_networkReply_readyRead()));
    connect(netReply, SIGNAL(error(QNetworkReply::NetworkError)), this, SLOT(on_networkReply_error(QNetworkReply::NetworkError)));
    connect(netReply, SIGNAL(sslErrors(QList<QSslError>)), this, SLOT(on_networkReply_sslErrors(QList<QSslError>)));

    if (abortPrintPending)
        abortPrinting();
}


/*!
 * \brief PrintJob::createXmlReqBody
 * \param xmlReqBody
 * \param xmlStream
 * \param page
 */
void PrintJob::createXmlReqBody(QIODevice &xmlReqBody, QXmlStreamWriter &xmlStream, const QByteArray &page)
{
    xmlReqBody.open(QIODevice::ReadWrite); // Must open device prior to reading/writing

    if (!abortPrintPending) {
        // Write compressed page binary image data before the XML request
        QByteArray compressedPage = qCompress(page);
        qint32 numBytes = compressedPage.length();
        xmlReqBody.write(reinterpret_cast<char*>(&numBytes), sizeof(qint32));
        xmlReqBody.write(compressedPage, numBytes);
    }

    // Write XML request content
    xmlStream.setDevice(&xmlReqBody);

    xmlStream.setAutoFormatting(true);
    xmlStream.writeStartDocument();

    xmlStream.writeStartElement("Command");
    xmlStream.writeAttribute("name", "print");

    xmlStream.writeStartElement("PrintCommandParams");
    QString printerName = "TSP700II";   //TODO NEEDS TO BE PROVIDED! (NEED TO INTERROGATE SERVER FOR AVAILABLE PRINTERS?)
    xmlStream.writeAttribute("printerName", printerName);
    xmlStream.writeAttribute("pageFormat", "png");  // Output PNG as Qt compresses it better than JPG
    xmlStream.writeAttribute("totalPages", QString::number(totalPages));
    xmlStream.writeAttribute("currentPage", QString::number(currentPage+1));
//  xmlStream.writeAttribute("emulate", "true");
    if (currentPage != 0) {
        Q_ASSERT(printId);  // Check server has sent back Print Id for this client
        xmlStream.writeAttribute("printId", QString::number(printId));
    }

    QString paperCut = "single";        //TODO NEEDS TO BE PROVIDED!
    xmlStream.writeAttribute("paperCut", paperCut);

    if (abortPrintPending) {
        xmlStream.writeAttribute("abortPrinting", "true");
        xmlStream.writeTextElement("Page", ""); // Write empty page as printing is going to be aborted
    }
    else {
        // Add page image data
///     const QString imgDataBase64 = page.toBase64();
///     xmlStream.writeTextElement("Page", imgDataBase64); // Convenience method that starts, writes & ends an element
    }

    xmlStream.writeEndElement();    // /PrintCommandParams
    xmlStream.writeEndElement();    // /Command

    xmlStream.writeEndDocument();

//  QString xmlReqStr = xmlReqBody.buffer();
//  QMessageBox::information(0, __FUNCTION__, xmlStream);
}


/*!
 * \brief PrintJob::show
 */
void PrintJob::show()
{
    printProgressWidget->show();
}


/*!
 * \brief PrintJob::hide
 */
void PrintJob::hide()
{
    printProgressWidget->hide();
}


/*!
 * \brief PrintJob::pagePrinted
 */
void PrintJob::pagePrinted()
{
    setNextTicketInSeqPrintState("finished");

//@@    while (printingPaused) {}   // Wait until printing resumes

    // Advance to next page
    ++currentPage;

    printProgressWidget->setValue(currentPage);

    emitSignal_printResult(PrintManager::PagePrinted, QString(tr("Page %1 of %2 has been printed")).arg(currentPage).arg(totalPages));

    if (currentPage < totalPages) {
        printProgressWidget->update(currentPage);
        sendPrintPageRequest();
    }
    else {
///     printProgressWidget->setText(PrintManager::generalStatusMsg(PrintManager::Ok));
        printProgressWidget->setText(PrintManager::generalStatusMsg(PrintManager::PrintFinished));
        finishPrinting();
    }
}


/*!
 * \brief PrintJob::setNextTicketInSeqPrintState
 * \param state
 */
void PrintJob::setNextTicketInSeqPrintState(QString state)
{
//return;
    // Send request to TixRealm server to set the next billet state in the sequence order as printed
    QStandardItem *modelItem = new QStandardItem(1, 2);         // 1 row with 2 columns in the standard item model (doesn't correspond with generated xml request!)
    modelItem->setData("Command", DataRole::jsonProperty());
    modelItem->setData(JsonObject, DataRole::jsonType());

    QStandardItem *modelSubItem = new QStandardItem("main.setnextticketinseqprintstate");   // DB stored procedure name (without sc_..._ex affixes)

    modelSubItem->setData("name", DataRole::jsonProperty());
    modelSubItem->setData(JsonSimple, DataRole::jsonType());                               // Xml attribute of element Command
    modelItem->setChild(0, 0, modelSubItem);                    // Make child of Command item

    modelSubItem = new QStandardItem(1, 1);                     // 1 row of 1 column
    modelSubItem->setData("SetNextTicketInSeqPrintStateParams", DataRole::jsonProperty());
    modelSubItem->setData(JsonObject, DataRole::jsonType());                               // Xml child element of element Command
    modelItem->setChild(0, 1, modelSubItem);                    // Make child of Command item

    QStandardItem *modelSubItem2 = new QStandardItem(state);    // Ctor adds text to item's display role
    modelSubItem2->setData("printstate", DataRole::jsonProperty());             // Note the PL/pgSQL store procedure parameter must match this!
    modelSubItem2->setData(JsonSimple, DataRole::jsonType());                              // Xml attribute of element ItemId
    modelSubItem->setChild(0, 0, modelSubItem2);                // Make child of ItemId item

    // Was a PL/pgSQL array literal constant e.g. "'{id1, id2, id3}'" containing detailscommande table ids
    foreach (PrintItem printItem, printItems) {
        modelSubItem2 = new QStandardItem(1, 1);
        modelSubItem2->setData("DetailsCommande", DataRole::jsonProperty());
        modelSubItem2->setData(JsonObject, DataRole::jsonType());                          // Xml child element of element SetNextTicketInSeqPrintStateParams
        modelSubItem->appendRow(modelSubItem2);

        QString dcId = printItem.first;
        QStandardItem *modelSubItem3 = new QStandardItem(dcId);
        modelSubItem3->setData("detailscommandeid", DataRole::jsonProperty());  // Note the PL/pgSQL store procedure parameter must match this!
        modelSubItem3->setData(JsonSimple, DataRole::jsonType());                          // Xml attribute of element DetailsCommande
        modelSubItem2->setChild(0, 0, modelSubItem3);
    }

    // Used for communicating with the TixRealm server
    PendingRequest *curReq = printMgr->getScMainObj()->sendMsg(modelItem, HTTP_METHOD_ID::POST, "/api", this, "responseSetNextTicketInSeqPrintState");
    Q_UNUSED(curReq);
}


/*!
 * \brief PrintJob::responseSetNextTicketInSeqPrintState
 * \param respError
 * \param requestItem
 * \param responseItem
 * \param curReq
 */
void PrintJob::responseSetNextTicketInSeqPrintState(QString respError, PendingRequest *curReq)
{
    QString error = respError;
    if (!error.isEmpty())
    {
        // Application (TixRealm) server error so terminate print job
//      QMessageBox::information(0, qApp->applicationName(), tr(error.toUtf8().constData()));
        abortPrinting(PrintManager::AppServerError);
    }
}


/*!
 * \brief PrintJob::finishPrinting
 */
void PrintJob::finishPrinting()
{
    printMgr->removePrintJob(this);
    emitSignal_printResult(PrintManager::PrintFinished);
}


/*!
 * \brief PrintJob::on_networkReply_finish
 *
 * Slot to receive signal emitted by QNetReply after all data has been read by its post() method.
 *
 * \param reply
 */
void PrintJob::on_networkReply_finish(QNetworkReply *reply)
{
    Q_UNUSED(reply);
//  reply->disconnect();
}


/*!
 * \brief PrintJob::confirmAbortPrinting
 */
void PrintJob::confirmAbortPrinting()
{
//    QNetworkRequest netRequest(QUrl(printServerUrl % "/pauseprinting?printId=" % QString::number(printId)));
//    printMgr->netAccMgr->get(netRequest);
    emitSignal_printResult(PrintManager::PrintPaused);

    printingPaused = true;
    QMessageBox::StandardButton answer = QMessageBox::question(0, tr("Abort printing"), PrintManager::generalStatusMsg(PrintManager::ConfirmAbortPrinting),
        QMessageBox::Yes | QMessageBox::No);
    printingPaused = false;
    if (answer == QMessageBox::Yes) {
      abortPrintPending = true;
//    abortPrinting();
    }
    else {
//      QNetworkRequest netRequest(QUrl(printServerUrl % "/resumeprinting?printId=" % QString::number(printId)));
//      printMgr->netAccMgr->get(netRequest);
        emitSignal_printResult(PrintManager::PrintResumed);
     }
}


/*!
 * \brief PrintJob::abortPrinting
 * \param abortReason
 */
void PrintJob::abortPrinting(PrintManager::StatusCode reason)
{
    abortReason = reason;

//    QNetworkRequest netRequest(QUrl(printServerUrl % "/abortprinting?printId=" % QString::number(printId)));
//    printMgr->netAccMgr->get(netRequest);

//    if (netReply)
//        netReply->abort();  // This will cause a OperationCanceledError error and a printResult to be emitted
    if (netReply)
        netReply->close();  // This will cause a OperationCanceledError error and a printResult to be emitted
//    emitSignal_printResult(PrintManager::PrintAborted);

    printMgr->removePrintJob(this);
}


/*!
 * \brief PrintJob::readResponse
 *
 * Reads the HTTP header and XML body returned by the TixRealm print server.
 *  The body is structured as follows:
 *  \code{.xml}
 *  <Response>
 *    <ErrorMessage [msg=""] printSuccessful="true|false" printId="id"/>
 *  </Response>
 *  \endcode
 *  where msg will be provided only if any errors were encountered at the server side and
 *  printSuccessful is true if there were no errors. Print Id is used so the client can retrieve
 *  the printer locked by the server for subsequent page print requests for batch prints.
 *
 * \note The HTTP status code that the server returns is generally 200 Ok regardless of whether
 *  an error occurred or not, therefore the XML body must be parsed to determine if any error
 *  actually happened.
 *
 * \return An error code as defined by an PrintManager enum that corresponds to either the
 *  HTTP status code or XML body's error message attribute. In addition, signalNetworkReplyError()
 *  will be called if an error occured, subsequently cancelling the print job and emitting the
 *  printResult signal to the print job thread's parent.
 */
PrintManager::StatusCode PrintJob::readResponse()
{
    PrintManager::StatusCode statusCode = PrintManager::Ok;

    // Check for errors returned by the network
    QNetworkReply::NetworkError netError = netReply->error();
    switch (netError) {
    case QNetworkReply::NoError:
        break;
    case QNetworkReply::HostNotFoundError:
        statusCode = PrintManager::ServerNotFound;
        break;
    case QNetworkReply::ConnectionRefusedError:
        statusCode = PrintManager::ServerRefusedConnection;
        break;
    case QNetworkReply::TimeoutError:
        statusCode = PrintManager::ServerTimeout;
        break;
    case QNetworkReply::OperationCanceledError:
        statusCode = abortReason;
        signalNetworkReplyError(statusCode);
        return statusCode;
    default:
        statusCode = PrintManager::ServerError;
        break;
    }
    if (statusCode != PrintManager::Ok) {
        signalNetworkReplyError(statusCode);
        return statusCode;
    }

    QVariant header = netReply->header(QNetworkRequest::ContentLengthHeader);

    lastHttpStatusCode = netReply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();
    lastHttpReasonPhrase = netReply->attribute(QNetworkRequest::HttpReasonPhraseAttribute).toString();

    // The TixRealm print server (nearly) always returns a http status code 200 for invalid requests
    // etc, so the client has to analyse the content body's error message to know if an error occurred
    QXmlStreamReader xml(netReply);
    QMap<QString, QXmlStreamAttributes> elems; // Flattened out XML elements
    while (!xml.atEnd()) {
        xml.readNext();
        if (xml.isStartElement()) {
            QString elemName = xml.name().toString();
            QXmlStreamAttributes attribs = xml.attributes();
            elems.insert(elemName, attribs);
        }
    }

    lastResponseErrorMessage.clear();
    bool hasValidResponseContent = false;
    if (elems.contains("Response")) {
        if (elems.contains("ErrorMessage")) {
            hasValidResponseContent = true;
            QXmlStreamAttributes attribs = elems.value("ErrorMessage");

            // Id sent back by server to identify the client request for next page (so can use the printer that was locked for it)
            if (attribs.hasAttribute("printId"))
                printId = attribs.value("printId").toString().toInt();

            // No news is good news - if msg attribute is absent or empty then it means there were no errors
            if (attribs.hasAttribute("msg"))
                lastResponseErrorMessage = attribs.value("msg").toString();

            if (!lastResponseErrorMessage.isEmpty()) {
                statusCode = PrintManager::BadRequest;
                signalNetworkReplyError(statusCode);
                return statusCode;
            }

            // Page print was successful so advance to next one
            pagePrinted();
        }
    }

    // Has error reading XML response or in HTTP status code? (4xx = client error, 5xx = server error)
    if (xml.hasError() || !hasValidResponseContent || lastHttpStatusCode >= 400 || printId == 0) {
        statusCode = lastHttpStatusCode >= 500 ? PrintManager::ServerError : PrintManager::BadRequest;
        signalNetworkReplyError(statusCode);
        return statusCode;
    }

    return statusCode;
}


/*!
 * \brief PrintJob::on_networkReply_readyRead
 */
void PrintJob::on_networkReply_readyRead()
{
    PrintManager::StatusCode statusCode = readResponse();
    if (statusCode == PrintManager::Ok) {
    }
}


/*!
 * \brief PrintJob::signalNetworkReplyError
 * \param statusCode
 */
void PrintJob::signalNetworkReplyError(PrintManager::StatusCode statusCode)
{
    if (!networkReplyErrorSignalled) {
        // Something weird going on here, this function gets called twice, as the on_networkReply_error slot gets invoked
        // during the on_networkReply_readyRead slot for http errors (i.e. status codes >= 400). This kludge fixes this
        // function getting called multiple times and emitting multiple printResult signals.
        networkReplyErrorSignalled = true;

        printProgressWidget->setText(PrintManager::generalStatusMsg(statusCode)
            #ifdef DEBUG_MODE
            + " - " + PrintManager::statusCodeToString(statusCode)
            #endif
        );

        emitSignal_printResult(statusCode, lastResponseErrorMessage);

        // Disconnect so server can release resources (e.g. printer)
        netReply->disconnect();

        printMgr->removePrintJob(this); // Will destroy this object
    }
}


/*!
 * \brief PrintJob::emitSignal_printResult
 * \param statusCode
 * \param statusMsg
 */
void PrintJob::emitSignal_printResult(PrintManager::StatusCode statusCode, QString statusMsg)
{
    PrintResultData printResultData = prepareEmitSignal_printResult(statusCode, &statusMsg);
    emit printResult(statusCode, statusMsg, printResultData);
}


/*!
 * \brief PrintJob::deferredEmitSignal_printResult
 * \param statusCode
 * \param statusMsg
 */
void PrintJob::deferredEmitSignal_printResult(PrintManager::StatusCode statusCode, QString statusMsg)
{
    PrintResultData printResultData = prepareEmitSignal_printResult(statusCode, &statusMsg);
    bool result = QMetaObject::invokeMethod(this, "printResult", Qt::QueuedConnection,
        Q_ARG(PrintManager::StatusCode, statusCode), Q_ARG(QString, statusMsg), Q_ARG(PrintResultData, printResultData));
    Q_ASSERT(result);
}


/*!
 * \brief PrintJob::prepareEmitSignal_printResult
 * \param statusCode
 * \param statusMsg Pointer to string to be set if no status message is provided
 */
PrintResultData PrintJob::prepareEmitSignal_printResult(PrintManager::StatusCode statusCode, QString *statusMsg)
{
    if (statusMsg->isEmpty())
        *statusMsg = PrintManager::generalStatusMsg(statusCode); // Fallback error message
    PrintResultData printResultData;
    printResultData.insert("itemsPrinted", PtrToVariant(&printItems));
    printResultData.insert("numPagesPrinted", totalPages);
    printResultData.insert("userData", userData);
    return printResultData;
}

/*!
 * \brief PrintJob::on_networkReply_error
 */
void PrintJob::on_networkReply_error(QNetworkReply::NetworkError)
{
    readResponse();
}


/*!
 * \brief PrintJob::on_networkReply_sslErrors
 */
void PrintJob::on_networkReply_sslErrors(QNetworkReply::NetworkError)
{
    readResponse();
}


/*!
 * \brief PrintProgressWidget::PrintProgressWidget
 * \param min
 * \param max
 * \param parent
 */
PrintProgressWidget::PrintProgressWidget(int min, int max, QWidget *parent) :
//    QDockWidget(tr("Print"))//, parent, winFlags)
    QProgressBar(parent)
{
    // Used if using QProgressDialog
//  printProgressWidget->setAutoReset(false);

    // Note widgets using a layout automatically have their parent set to the widget of which the layout is installed
//    progressBar = new QProgressBar(this);
    progressBar = this;
    progressBar->setMinimum(min);
    if (max > min)
        progressBar->setMaximum(max);
    progressBar->setMinimumWidth(500);
/*
    cancelButton = 0;
//    cancelButton = new QPushButton(tr("Cancel print"), this);
//    cancelButton->setFixedSize(32, 32);

    layout = new QHBoxLayout;
    layout->addWidget(progressBar);
//    layout->addWidget(cancelButton);
    setLayout(layout);

//    parent->layout()->addWidget(this);
    parent->layout()->addWidget(progressBar);

//  setTitleBarWidget(new QWidget); // Causes title bar to be suppressed
*/
    // Set initial progression text
    update(0);

    // Needed to make initial text appear?
    setValue(0);
}


/*!
 * \brief PrintProgressWidget::~PrintProgressWidget
 */
PrintProgressWidget::~PrintProgressWidget()
{
//  delete layout;
}


/*!
 * \brief PrintProgressWidget::update
 * \param currentPage
 */
void PrintProgressWidget::update(int currentPage)
{
    if (currentPage)
        setText(tr("Printing tickets (%v de %m)..."));  // "Impression de billets (%v de %m) en cours..."
    else
        setText(tr("Printing tickets..."));             // "Impression de billets en cours..."
}


QVariant PtrToVariant(void *ptr)
{
    #ifdef QT_POINTER_SIZE
    #if QT_POINTER_SIZE == 4
    return QVariant(reinterpret_cast<qint32>(ptr));
    #elif QT_POINTER_SIZE == 8
    return QVariant(reinterpret_cast<qint64>(ptr));
    #endif
    #else
    #error Invalid QT_POINTER_SIZE value stored!
    #endif
}
