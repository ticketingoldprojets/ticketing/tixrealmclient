#ifndef SUBLEVELDATAWIDGETMAPPER_H
#define SUBLEVELDATAWIDGETMAPPER_H
#include <QDataWidgetMapper>

class SubLevelDataWidgetMapper : public QDataWidgetMapper
{
public:
    SubLevelDataWidgetMapper(QObject * parent = 0);

public slots:
    void setCurrentSublevelRootIndex(int index);
};

#endif // SUBLEVELDATAWIDGETMAPPER_H
