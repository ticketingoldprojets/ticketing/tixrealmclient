#include <QtWidgets/QApplication>
#include "tixrcore.h"
#include "../welcomedialog.h"
#include "../mainwindow.h"
#include "../TixrClientEditorCommon/site2dview.h"
#include "jsonqstandarditemfactory.h"
#include "customstandarditem.h"

#ifdef DEBUG_MODE
#include "../debug/modelinspectorform.h"
#endif

#define HttpUserAgent "tixrAgent"

TixRealmCore::TixRealmCore() : curLocal(QLocale::system()), cLocal(QLocale::c())
{
    QLocale::setDefault(curLocal);

    QFileInfo fInfo(qApp->arguments()[0]);
    //curPath = QDir::tempPath();//"/sdcard/";//fInfo.absolutePath() + "/"; // directorio de trabajo
    curPath = fInfo.absolutePath() + QDir::separator();

    //qDebug(QString("curPath: " + curPath).toUtf8().constData());

    // DEBO ADAPTAR ESTO PARA OTROS SISTEMAS Y PLATAFORMAS (ANDROID...TABLETTE...)
    /*QDir dir(curPath);
    if(!((dir.mkdir("TixRealmClient") || dir.exists("TixRealmClient")) && dir.cd("TixRealmClient") &&
         (dir.mkdir("images") || dir.exists("images"))))
    {
        lastError = "Can't write to local storage.";
        return;
    }

    curPath += "/TixRealmClient/";*/

    qRegisterMetaType<QModelIndex>("QModelIndex");
    qRegisterMetaType<QWidget*>("QWidget*");
    qRegisterMetaType<QStandardItemModel*>("QStandardItemModel*");

    stdModel = new QStandardItemModel(this);

    inHandlerIndex = outHandlerIndex = 0;
    inHandlerList << &JsonQStandardItemFactory::jsonToModel;
    inHandlerList << &JsonQStandardItemFactory::jsonToTariffModel;
    inHandlerList << &JsonQStandardItemFactory::jsonToPlanModel;
    inHandlerList << &JsonQStandardItemFactory::jsonToQJsonDocument;
    outHandlerList << &JsonQStandardItemFactory::modelToJson;
    outHandlerList << &JsonQStandardItemFactory::tariffModelToJson;

    partnerId = -1;

    stateIconsCache = new QList< QMap<int, QMap<QString, QPair<QImage, QImage> > > >;

    objMainWindow = 0;

    //create and configure the network access manager
    netManager = new QNetworkAccessManager(this);
    //QNetworkProxyFactory::setUseSystemConfiguration(true);

    requestId = 1;

    uiLoaded = false;

    syncMsgHandlersMap.insert("Produit", &TixRealmCore::eatSyncMsgForPlanSalle);

    fillLevelToHeaderModelMap();
}

TixRealmCore::~TixRealmCore()
{
    delete stdModel;
    delete stateIconsCache;
#ifdef DEBUG_MODE
    delete modelInspectorForm;
#endif
}

int TixRealmCore::start()
{
    a = (QApplication*)QApplication::instance();

    WelcomeDialog w(this);
    w.show();

    if(a->exec() == QDialog::Accepted)
    {
        if(!loadSkin())
        {
            QMessageBox::critical(0, qApp->applicationName(), tr("Can not register graphical user interface."));
            return 1;
        }

        MainWindow mW(this);
        mW.showMaximized();

        objMainWindow = &mW;

#ifdef DEBUG_MODE
        modelInspectorForm = new ModelInspectorForm(stdModel);
        modelInspectorForm->show();
#endif

        return a->exec();
    }

    return 0;
}

QString TixRealmCore::getLastError()
{
    QString tmp = lastError;
    lastError.clear();
    return tmp;
}

QStandardItem *TixRealmCore::widgetToModel(QWidget *w)
{
    QStandardItem *root = new QStandardItem;

    QMap<QString,QStandardItem*> mapStrModelItem;

    QList<QWidget*> wList = w->findChildren<QWidget*>();
    for(int i=0, n=wList.size(); i<n; i++)
    {
        QWidget *child = wList.at(i);
        QString elem = child->accessibleName();
        if(elem.isEmpty())
            continue;
        QString attValue, attImgFileName, attImg, imgFileName, imgFileName2,
                attName = child->accessibleDescription();
        if(attName.isEmpty())
            continue;
        if(child->inherits("QLineEdit"))//! QLineEdit et QPlainTextEdit produisent des attributs de type chaine de caractères
            attValue = ((QLineEdit*)child)->text(); else
        if(child->inherits("QPlainTextEdit"))
            attValue = ((QPlainTextEdit*)child)->toPlainText(); else
        if(child->inherits("QComboBox"))
        {
            QComboBox *cb = static_cast<QComboBox*>(child);
            /*
             *En el caso del combo existe dos casos de uso:
             * 1. Se desea enviar el label del item (role DisplayRole).
             * 2. Se desea enviar otro role, normalmente la llave en la base de datos para este item.
             *Para el segundo caso de uso el role a enviar se espera definido en el tixrRelevantRole del combo.
            */
            /*!
             * Dans le cas d'un QComboBox il y a deux cas d'utilisation:
             *  - On souhaite envoyer le text de l'élément choisi de la liste (rôle Qt::DisplayRole).
             *  - On souhaite envoyer la valeu d'autre rôle quiconque, d'habitude la clef dans la basse de données pour l'élément choisi de la liste.
             * Pour activer le deuxième cas d'usage il faut indiquer dans la propriété \b tixrRelevantRole du widget QComboBox le numéro de rôle dont la valeur il faudra prendre.
            */
            QString tixrRelevantRole = child->property("tixrRelevantRole").toString();
            if(tixrRelevantRole.isEmpty())//caso de uso #1
                attValue = cb->currentText();
            else//caso de uso #2
            {
                attValue = cb->itemData(cb->currentIndex(), DataRole::FromName(tixrRelevantRole)).toString();
            }
        }else
        if(child->inherits("QCheckBox"))//! QCheckBox produit des attributs de type booléen.
            attValue = QString::number(((QCheckBox*)child)->isChecked() ? 1 : 0); else
        if(child->inherits("QSpinBox"))//! QSpinBox produit des attributs de type entier.
            attValue = QString::number(((QSpinBox*)child)->value()); else
        if(child->inherits("QDoubleSpinBox"))//! QDoubleSpinBox produit des attributs de type réel.
            attValue = QString::number(((QDoubleSpinBox*)child)->value()); else
        if(child->inherits("QDateTimeEdit"))//! QCheckBox produit des attributs de type date.
            attValue = ((QDateTimeEdit*)child)->dateTime().toString(Qt::ISODate); else
        if(child->inherits("QPushButton"))//! QCheckBox produit des attributs de type chaine de caractères représentant une couleur. Voir la documentation QColor::name.
            attValue = ((QPushButton*)child)->palette().color(QPalette::Background).name(); else
        if(child->inherits("QListView"))//! QListView permet d'envoyer une image.
        {
            //QListView *gView = (QListView*)child;
            QStringList sl = attName.split("|");
            if(sl.size()==4)
            {
                attImgFileName = sl.at(0);
                attImg = sl.at(1);
                imgFileName = sl.at(2);
                attName = attImg;
                imgFileName2 = sl.at(3);
            }
        }else
        if(child->inherits("QToolButton"))
        {
            QStringList sl = attName.split("|");
            if(sl.size()==2)
            {
                attName = sl.at(0);
                attValue = sl.at(1);
            }
        }

        QStandardItem *modelItem = root, *modelSubItem;
        if(mapStrModelItem.contains(elem))
            modelSubItem = mapStrModelItem.value(elem);
        else
        {
            modelSubItem = new QStandardItem;
            modelSubItem->setData(elem, DataRole::jsonProperty());
            modelSubItem->setData(JsonObject, DataRole::jsonType());
            mapStrModelItem.insert(elem,modelSubItem);
            modelItem->appendRow(modelSubItem);
        }

        modelItem = modelSubItem;
        modelSubItem = new QStandardItem;
        modelItem->appendRow(modelSubItem);
        modelSubItem->setData(attName, DataRole::jsonProperty());
        modelSubItem->setData(JsonSimple, DataRole::jsonType());

        if(attImgFileName.isEmpty())
        {
            modelSubItem->setText(attValue);
        }
        else
        {
            modelSubItem->setData(QImage(imgFileName2), Qt::DecorationRole);
            QStandardItem *modelSubItem2 = new QStandardItem(imgFileName);
            modelItem->appendRow(modelSubItem2);
            modelSubItem2->setData(attImgFileName, DataRole::jsonProperty());
            modelSubItem2->setData(JsonSimple, DataRole::jsonType());
        }
    }

    return root;
}

QMap<QGroupBox *, QList<QWidget *> > TixRealmCore::getSubLevelsContainerMap(const QWidget *w)
{
    QList<QGroupBox*> subWlist = w->findChildren<QGroupBox*>();
    QMap<QGroupBox *, QList<QWidget *> > result;

    for(int i=0, n=subWlist.size(); i<n; i++)
    {
        QGroupBox *gb = subWlist[i];
        if(gb->property("tixrJsonKey").isValid())
        {
            result.insert(gb, gb->findChildren<QWidget*>());
        }
    }

    return result;
}

bool TixRealmCore::subLevelsContains(const QMap<QGroupBox *, QList<QWidget *> > &subLevelsMap, QWidget *w)
{
    foreach(const QList<QWidget *> &wList, subLevelsMap.values())
    {
        if(wList.contains(w))
            return true;
    }
    return false;
}

QJsonValue TixRealmCore::jsonValueFromWidget(const QWidget *w)
{
    QJsonValue attValue;

    if(w->inherits("QLineEdit"))//! QLineEdit, QTextEdit et QPlainTextEdit produisent des attributs de type chaine de caractères
        attValue = QJsonValue(((QLineEdit*)w)->text()); else
    if(w->inherits("QPlainTextEdit"))
        attValue = QJsonValue(((QPlainTextEdit*)w)->toPlainText()); else
    if(w->inherits("QTextEdit"))
        attValue = QJsonValue(((QTextEdit*)w)->toPlainText());//QString(((QTextEdit*)w)->toHtml().toUtf8().toBase64())); else
    if(w->inherits("QComboBox"))
    {
        QComboBox *cb = (QComboBox*)(w);
        /*
         *En el caso del combo existe dos casos de uso:
         * 1. Se desea enviar el label del item (role DisplayRole).
         * 2. Se desea enviar otro role, normalmente la llave en la base de datos para este item.
         *Para el segundo caso de uso el role a enviar se espera definido en el tixrRelevantRole del combo.
        */
        /*!
         * Dans le cas d'un QComboBox il y a deux cas d'utilisation:
         *  - On souhaite envoyer le text de l'élément choisi de la liste (rôle Qt::DisplayRole).
         *  - On souhaite envoyer la valeu d'autre rôle quiconque, d'habitude la clef dans la basse de données pour l'élément choisi de la liste.
         * Pour activer le deuxième cas d'usage il faut indiquer dans la propriété \b tixrRelevantRole du widget QComboBox le numéro de rôle dont la valeur il faudra prendre.
        */
        QString tixrRelevantRole = w->property("tixrRelevantRole").toString();
        if(tixrRelevantRole.isEmpty())//caso de uso #1
            attValue = QJsonValue(cb->currentText());
        else//#2
        {
            if(cb->currentIndex() > -1)
            {
                QVariant v = cb->itemData(cb->currentIndex(), DataRole::FromName(tixrRelevantRole));
                if(v.isValid())
                    attValue = QJsonValue(v.toString());
            }
        }
    }else
    if(w->inherits("QCheckBox"))//! QCheckBox produit des attributs de type booléen.
        attValue = QJsonValue(((QCheckBox*)w)->isChecked()); else
    if(w->inherits("QRadioButton"))
        attValue = QJsonValue(((QRadioButton*)w)->isChecked()); else
    if(w->inherits("QSpinBox"))//! QSpinBox produit des attributs de type entier.
        attValue = QJsonValue(((QSpinBox*)w)->value()); else
    if(w->inherits("QDoubleSpinBox"))//! QDoubleSpinBox produit des attributs de type réel.
        attValue = QJsonValue(((QDoubleSpinBox*)w)->value()); else
    if(w->inherits("QDateTimeEdit"))//! QCheckBox produit des attributs de type date.
        attValue = QJsonValue(((QDateTimeEdit*)w)->dateTime().toString(Qt::ISODate)); else
    if(w->inherits("QPushButton"))//! QPushButton produit des attributs de type chaine de caractères représentant une couleur. Voir la documentation QColor::name.
        attValue = QJsonValue(((QPushButton*)w)->palette().color(QPalette::Background).name()); else
    if(w->inherits("QLabel"))//! QLabel permet d'envoyer une image.
    {
        QLabel *imgViewer = (QLabel*)(w);
        QPicture *pict = const_cast<QPicture*>(imgViewer->picture());

        if(pict)
        {
            QByteArray ba;
            QBuffer buffer(&ba);
            buffer.open(QIODevice::WriteOnly);
            pict->save(&buffer);
            buffer.close();

            attValue = QJsonValue(QString(ba.toBase64()));
        }
        else
        {
            QPixmap *pxm = const_cast<QPixmap*>(imgViewer->pixmap());

            if(pxm)
            {
                QByteArray ba;
                QBuffer buffer(&ba);
                buffer.open(QIODevice::WriteOnly);
                pxm->save(&buffer, "PNG");
                buffer.close();

                attValue = QJsonValue(QString(ba.toBase64()));
            }
        }
    }/*else
    if(w->inherits("QToolButton"))
    {
        QStringList sl = attName.split("|");
        if(sl.size()==2)
        {
            attName = sl.at(0);
            attValue = QJsonValue(sl.at(1));
        }
    }*/

    return attValue;
}

QJsonObject *TixRealmCore::widgetToJson(const QWidget *w)
{
    QJsonObject *mainObj = new QJsonObject;

    QMap<QGroupBox *, QList<QWidget *> > subLevelsMap = getSubLevelsContainerMap(w);

    QList<QWidget*> wList = w->findChildren<QWidget*>();
    for(int i=0, n=wList.size(); i<n; i++)
    {
        QWidget *child = wList.at(i);

        QString attName = child->property("tixrJsonKey").toString();

        if(attName.isEmpty())
            continue;

        if(subLevelsContains(subLevelsMap, child))
            continue;

        QJsonValue attValue;

        if(child->inherits("QGroupBox"))
        {
            QJsonObject *subLevelsJsonObj = widgetToJson(child);
            attValue = QJsonValue(*subLevelsJsonObj);
            delete subLevelsJsonObj;
        }
        else
            if(child->inherits("QTableView"))
            {
                QJsonObject sizesObj;
                QTableView *tv = qobject_cast<QTableView*>(child);
                QStandardItem *rItm = stdModel->itemFromIndex(tv->rootIndex());
                const QStandardItemModel *hModel = qobject_cast<const QStandardItemModel*>(tv->horizontalHeader()->model());

                if(rItm && hModel)
                {
                    for(int j=0, n=rItm->columnCount(); j<n; j++)
                    {
                        QStandardItem *itm = rItm->child(0,j);
                        QStandardItem *hdr = hModel->horizontalHeaderItem(j);

                        sizesObj.insert(hdr->text(), QJsonValue(itm->data(Qt::EditRole).toInt()));
                    }

                    attValue = QJsonValue(sizesObj);
                }
            }
            else
                attValue = jsonValueFromWidget(child);

        if(!attValue.isNull())
            mainObj->insert(attName, attValue);
    }

    return mainObj;
}

void TixRealmCore::addToModel(QStandardItem *item, const QJsonObject *jsonObj)
{
    if (item == NULL || jsonObj == NULL || jsonObj->isEmpty()){
        return;
    }

    QStandardItem *target;

    for(QJsonObject::const_iterator it=jsonObj->constBegin(), end=jsonObj->constEnd(); it!=end; it++)
    {
        QString pName = it.key();

        target = JsonQStandardItemFactory::getTargetItem(pName, item);

        int qtRole = DataRole::FromName(pName);

        Q_ASSERT_X(!target->data(qtRole).isValid() || target->data(qtRole).toString().isEmpty(), "addToModel", QString("duplicate role %1 in row %2, col %3, data %4").arg(pName,QString::number(target->row()), QString::number(target->column()), target->data(qtRole).toString()).toUtf8().constData());
        target->setData((*it).toVariant(), qtRole);
    }
}

QStandardItem *TixRealmCore::itemByNameFromModel(QStandardItem *modelItem, QString itemName)
{
    if(!modelItem)
        return 0;

    for(int i=0,n=modelItem->rowCount(); i<n; i++)
    {
        QStandardItem *subModelItem = modelItem->child(i);
        QString name = subModelItem->accessibleText();
        if(name==itemName)
            return subModelItem;

    }

    return 0;
}

QStandardItem *TixRealmCore::itemByValuefullscanFromModel(QStandardItem *startRootItem, int qtrole, QVariant value)
{
    if(!startRootItem)
        return 0;

    if(!value.isValid())
        return 0;

    if(startRootItem->data(qtrole) == value)
        return startRootItem;

    for(int i=0,n=startRootItem->rowCount(); i<n; i++)
    {
        QStandardItem *stdItm = itemByValuefullscanFromModel(startRootItem->child(i), qtrole, value);
        if(stdItm)
            return stdItm;
    }

    return 0;
}

QStandardItem *TixRealmCore::modelEntryFromProductType(int prodType)
{
    switch(prodType)
    {
        case 0: return keyToItemMap.value("stock");
        case 1: return keyToItemMap.value("shopstock");
        case 2: return keyToItemMap.value("transtock");
        case 3: return keyToItemMap.value("services");
    }
    return 0;
}

void TixRealmCore::setConnParam(QString serverHost, int serverPort, QString proxyHost,
                  int proxyPort, QString proxyUsr, QString proxyPwd)
{
    host = serverHost;
    port = serverPort;
    proxy = proxyHost;
    proxyPort = proxyPort;
    proxyUser = proxyUsr;
    proxyPassword = proxyPwd;
    inHandlerIndex = 0;

    //netManager->connectToHostEncrypted(serverHost, serverPort);
}

PendingRequest *TixRealmCore::sendMsg(QStandardItem *requestItem, HTTP_METHOD_ID method, QString path, QObject *sender, const char *senderSlot, const QJsonObject *jsonData)
{
    PendingRequest *result = sendMsg(requestItem, method, path, sender, senderSlot, QJsonDocument(*jsonData).toJson(/*QJsonDocument::Compact*/));
    delete jsonData;
    return result;
}

PendingRequest *TixRealmCore::sendMsg(QStandardItem *requestItem, HTTP_METHOD_ID method, QString path, QObject *sender, const char *senderSlot, const QByteArray &data)
{
    qDebug("executing sendMsg");

    PendingRequest *pReq = new PendingRequest(method);
    pReq->id = requestId++;
    /**
      * Quien será el padre de la respuesta depende de tres parámetros: el tipo de envío (GET o POST), el requestItem y el bufer de datos de la solicitud.
      * Si se trata de un GET el padre corresponde a requestItem.
      * Si es un POST y hay un requestItem y no hay buffer y el requestItem está en el stdModel entonces el padre es el padre del requestItem.
      * Si es un POST y hay un requestItem y no hay buffer y el requestItem NO está en el stdModel entonces no hay un padre.
      * Si es un POST y hay un requestItem y HAY un buffer entonces el padre es el requestItem.
      * Si es un POST y no hay un requestItem entonces no hay padre.
      */
    pReq->responseItemParent = method==HTTP_METHOD_ID::POST?(requestItem?(data.isEmpty()?(requestItem->model()?(requestItem->parent()?requestItem->parent():stdModel->invisibleRootItem()):0):requestItem):0):requestItem;
    pReq->requestItem = requestItem = method==HTTP_METHOD_ID::GET?0:(data.isEmpty()?requestItem:0);
    pReq->inHandlerIndex = inHandlerIndex;
    pReq->pgFunction = pgFunction;
    pReq->sender = sender;
    pReq->senderSlot = senderSlot;

#ifdef DEBUG_MODE
    if(requestItem)
    {
        requestItem->setText("REQUEST");//debug purpose
    }
#endif

    QString url = QString("https://%1:%2%3").arg(host, QString::number(port), path);
    QNetworkRequest qNetReq = QNetworkRequest(QUrl(url));

    //set headears
    qNetReq.setHeader(QNetworkRequest::KnownHeaders::UserAgentHeader, HttpUserAgent);
    qNetReq.setRawHeader("Connection", "Keep-Alive");
    if(!credentials.isEmpty())
    {
        qNetReq.setRawHeader("Authorization", QByteArray("Basic ")+credentials);
    }
    if(!sessionId.isEmpty())
    {
        qNetReq.setRawHeader("Session-Id", sessionId);
    }
    qNetReq.setRawHeader("pgFunction", pgFunction.toUtf8());
    if(partnerId>-1)
        qNetReq.setRawHeader("partnerId", QString::number(partnerId).toUtf8());
    if(!partnerRespHandler.isEmpty())
        qNetReq.setRawHeader("partnerRespHandler", partnerRespHandler.toUtf8());
    if(!partnerRespHandlerPath.isEmpty())
        qNetReq.setRawHeader("partnerRespHandlerPath", partnerRespHandlerPath.toUtf8());

    //send request
    QNetworkReply *reply;
    if(method == HTTP_METHOD_ID::POST)
    {
        qNetReq.setHeader(QNetworkRequest::KnownHeaders::ContentTypeHeader, "application/json");
        QByteArray postedData = data;
        if(postedData.isEmpty())
            postedData = outHandlerList[outHandlerIndex](requestItem);
        //qDebug() << "request: " << postedData;
        pReq->setData(postedData);
        reply = netManager->post(qNetReq, postedData);
        //qDebug("post data: %s", data.constData());
    }
    else
    {
        reply = netManager->get(qNetReq);
    }

    connect(reply, SIGNAL(finished()), this, SLOT(handleResponse()));
    connect(reply, SIGNAL(sslErrors(QList<QSslError>)), this, SLOT(handleSslErrors(QList<QSslError>)));

    //register pending request
    pendingRequestMap.insert(reply, pReq);

    //restore default parameters values
    credentials.clear();//En lo adelante lo que se envía es el id de la sesión, las credenciales sólo sirven para la apertura de la sesión.
    pgFunction.clear();
    inHandlerIndex = 0;
    outHandlerIndex = 0;
    partnerId = -1;
    partnerRespHandler.clear();
    partnerRespHandlerPath.clear();

    return pReq;
}

void TixRealmCore::handleResponse()
{
    QNetworkReply *reply = qobject_cast<QNetworkReply *>(sender());

    if(reply->hasRawHeader("Session-Id"))//renovar el id de sesión
        sessionId = reply->rawHeader("Session-Id");

    PendingRequest *pReq = pendingRequestMap.contains(reply) ? pendingRequestMap.take(reply) : 0;
    Q_ASSERT(pReq);

    QStandardItem *responseItem = 0;
    QString error;

    QVariant codeAtt = reply->attribute(QNetworkRequest::HttpStatusCodeAttribute);
    if(reply->error() != QNetworkReply::NoError || !codeAtt.isValid() || codeAtt.toInt() >= 400)//reply->error() != QNetworkReply::NoError)
    {
        if(reply->error() == QNetworkReply::NoError)
            error = reply->attribute(QNetworkRequest::HttpReasonPhraseAttribute).toString();
        else
            error = reply->errorString();
        //QByteArray ba = reply->readAll();
    }
    else if(pReq->isResponseExpected)
    {
        //read response and translate to a QStandardItem hierarchy.
        responseItem = inHandlerList[pReq->inHandlerIndex](reply, error, pReq);

        QStandardItemModel *hModel = JsonQStandardItemFactory::getHeaders();
        if(hModel)
        {
            itemToHeaderModelMap.insert(responseItem, hModel);
        }
    }

    pReq->responseItem = responseItem;
    pReq->respError = error;

    reply->deleteLater();

    QObject *curSender = pReq->sender;
    const char *curSenderSlot = pReq->senderSlot;

    QStandardItem *parent = pReq->getResponseItemParent();

    if(parent && responseItem)
        parent->appendRow(responseItem);

    if(error.isEmpty() && responseItem && !pReq->respDataMapKey.isEmpty())
        keyToItemMap.insert(pReq->respDataMapKey, responseItem);

    QMetaObject::invokeMethod(curSender, curSenderSlot, Q_ARG(QString,error), Q_ARG(PendingRequest*,pReq));

    parent = pReq->getResponseItemParent();

    if(!parent)//en varios casos hago un responseItemParent := 0 desde el handle del widget para eliminar el item respuesta
    {
        Q_ASSERT_X(pReq->respDataMapKey.isEmpty(), "pReq->respDataMapKey.isEmpty", pReq->pgFunction.toUtf8().constData());
        delete responseItem;//no quiero guardar la respuesta
    }

    delete pReq;//también libera el requestItem
}

void TixRealmCore::handleSslErrors(const QList<QSslError> &errors)
{
    QNetworkReply *reply = qobject_cast<QNetworkReply*>(sender());
#ifdef DEBUG_MODE
    foreach(QSslError sslError, errors)
    {
        qDebug(sslError.errorString().toUtf8().constData());
    }
#else
    Q_UNUSED(errors)
#endif
    reply->ignoreSslErrors();
}

void TixRealmCore::readSyncMsg(QStandardItem *syncData)
{
    QString cmd = syncData->data(DataRole::ikey()).toString();
    QStringList coord = syncData->data(DataRole::iuid()).toString().split(',', QString::SkipEmptyParts);

    for(int i=0, n=syncData->rowCount(); i<n; i++)
    {
        QStandardItem *stdItem = syncData->child(i);

        QString itemName = stdItem->accessibleText(), ikey;

        if(syncMsgHandlersMap.contains(itemName))
            (this->*syncMsgHandlersMap.value(itemName))(cmd, &coord, syncData);
        else
        {   //default processing
            ikey = stdItem->data(DataRole::ikey()).toString();
            QStandardItem *targetStdItem = findStdItem(&coord);
            if(!targetStdItem)
                return;

            if(cmd=="I")//insert
            {
                targetStdItem->appendRow(stdItem);
            }else

            if(cmd=="D")//delete
            {
                targetStdItem->removeRow(stdItem->row());
            }else

            {//update
                CustomStandardItem *scStdItem = static_cast<CustomStandardItem*>(stdItem);
                for(int i=0, n=scStdItem->customRolesList.size(); i<n; i++)
                {
                    int r = scStdItem->customRolesList.at(i);
                    QVariant v = stdItem->data(r);
                    targetStdItem->setData(v, r);
                }
            }
        }
    }

    delete syncData;
}


QString TixRealmCore::getPgFunction() const
{
    return pgFunction;
}

void TixRealmCore::setPgFunction(const QString &value)
{
    pgFunction = value;
}

/**
 * @brief TixRealmCore::getItemAtLevelFromChild retorna el ancestro en el nivel \b level. Esta función ignora los elementos contenedores y asume que \b child no es un contenedor.
 * @param child Elemento del model del cual se requiere encontrar el ancestro.
 * @param level Nivel deseado del ancestro.
 * @return El ancestro de \b child en el nivel \b level.
 */
QStandardItem *TixRealmCore::getItemAtLevelFromChild(QStandardItem * const child, int level)
{
    Q_ASSERT(child);
    Q_ASSERT(level >= 0);

    QStandardItem *aux = child;
    QList<QStandardItem*> parentList;
    bool isContainer = false;

    do
    {
        if(!isContainer)
            parentList.prepend(aux);

        aux = aux->parent();

        isContainer = !isContainer;
    }
    while(aux);

    return parentList.at(level);
}

bool TixRealmCore::getUiLoaded() const
{
    return uiLoaded;
}

QStandardItem *TixRealmCore::findStdItem(QStringList *coordList)
{
    QStandardItem *root = keyToItemMap.value(coordList->takeFirst());
    return root?(coordList->isEmpty()?root:stdItemFinder(coordList, root)):0;
}

QStandardItem *TixRealmCore::stdItemFinder(QStringList *coordList, QStandardItem *root)
{
    QString ikey = coordList->takeFirst();

    for(int i=0, n=root->rowCount(); i<n; i++)
    {
        QStandardItem *stdItem = root->child(i);
        if(stdItem->data(DataRole::ikey()).toString()==ikey)
        {
            return coordList->isEmpty()?stdItem:((root=stdItem->child(0))?stdItemFinder(coordList, root):0);
        }
    }

    return 0;
}

QGraphicsObject *TixRealmCore::findPlaceItem(QStandardItem *root, QString svgId)
{
    QVariant v = root->data(DataRole::site2dviewptr());
    if(!v.isValid())
        return 0;

    Site2dView *site2dView = v.value<Site2dView*>();
    QList<Place*> pList = site2dView->getPlaceItemsList();

    for(int i=0, n=pList.size(); i<n; i++)
    {
        Place *place = pList.at(i);
        if(place->id==svgId)
            return place;
    }

    return 0;
}

void TixRealmCore::eatSyncMsgForPlanSalle(QString action, QStringList *coord, QStandardItem *stdItem)
{
    QStandardItem *targetStdItem = findStdItem(coord);
    if(!targetStdItem)
        return;

    QString placeSvgId = stdItem->data(DataRole::psvgid()).toString();
    QGraphicsObject *obj = findPlaceItem(targetStdItem, placeSvgId);
    if(!obj)
        return;
    Place *place = qobject_cast<Place*>(obj);

    int etat = stdItem->data(DataRole::petat()).toInt();
    place->setEtat(etat);
}

void TixRealmCore::fillLevelToHeaderModelMap()
{
    QStandardItemModel *model = new QStandardItemModel;
    model->setHorizontalHeaderLabels(QStringList() << tr("Image") << tr("Title") << tr("Theme") << tr("Comment") << tr("Creation Date"));
    levelToHeaderModelMap.insert(0, model);

    model = new QStandardItemModel;
    model->setHorizontalHeaderLabels(QStringList() << tr("Image") << tr("Title") << tr("Date") << tr("Begin Selling Date") << tr("End Selling Date")
                                                   << tr("Description") << tr("Venue") << tr("VAT") << tr("VAT over Fee") << tr("Fee") << tr("Map") << tr("Auto Seating") << tr("Have Season Ticket") << tr("Printer ID")
                                                   << tr("Ticket Model ID") << tr("User Profile") << tr("Zone Tariff Map") << tr("Tariff") << tr("Is a Flow"));
    levelToHeaderModelMap.insert(1, model);

    model = new QStandardItemModel;
    model->setHorizontalHeaderLabels(QStringList() << QString::null << tr("Name") << tr("Total") << tr("Availability"));
    levelToHeaderModelMap.insert(2, model);
}

bool TixRealmCore::loadSkin(QString skinName)
{
    /*!
     * \todo Adapter l'interface actuelle au nouveau \a skin, par example, si une fenêtre n'existe plus dans le nouveau \a skin il faut la fermer; mais dans ce cas, quoi faire de la information y contenue? il faudra demander à l'utilisateur de la enregistrer.
     * \todo Implémenter une méthode de validation de \a skin, une façon de vérifier qu'il s'agit d'un \a skin auhentique.
     */
    if(skinName.isNull())
    {
        QString fn;
        skinName = QFile::exists((fn=curPath + "skins/default.skin"))?fn:":/skin/default.skin";
    }

    //registrar el nuevo recurso
    if(!QResource::registerResource(skinName))
        return false;

    //liberar el recurso actual
    if(!currentSkinName.isEmpty())
    {
        QResource::unregisterResource(currentSkinName);
    }

    currentSkinName = skinName;

    //cargar el Style
    QFile file(":/styles.qss");
    if(!file.open(QFile::ReadOnly))
        return false;
    a->setStyleSheet(QLatin1String(file.readAll()));
    file.close();

    return uiLoaded = true;
}


//-TixRealmCore-----------------------------------------------------------------------------------------------------------------------
