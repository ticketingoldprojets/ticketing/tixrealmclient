#ifndef SCMETAQENUM_H
#define SCMETAQENUM_H
//#include <functional>
//#include <set>
//#include <map>
#include <QtCore>
//using namespace std;
template <class T>
class EnumClass : public QObject
{
//private:
    // Predicate for finding the corresponding instance
    /*struct Enum_Predicate_Corresponds: public std::unary_function<const TixrEnum<T>*, bool>
    {
        Enum_Predicate_Corresponds(int Value) : m_value(Value){}
        bool operator()(const TixrEnum<T>* E){ return E->Role() == m_value;}

    private:
        const int m_value;
    };*/

    // Comparison functor for the set of instances
    /*class Enum_Ptr_Less: public std::binary_function<const TixrEnum<T>*, const TixrEnum<T>*, bool>
    {
        bool operator()(const TixrEnum<T>* E_1, const TixrEnum<T>* E_2){ return E_1->Role() < E_2->Role(); }
    };*/

public:
    bool operator<(const EnumClass<T>* E){ return this->Role() < E->Role(); }

    // Access to int value
    inline int operator()(void) const { return m_value; }

    static bool exists(const QString & ptyName)
    {
        return map_instancesName.contains(ptyName);
    }

    static int FromName(const QString & ptyName)
    {
        Q_ASSERT_X(map_instancesName.contains(ptyName), "EnumClass", QString("Invalid TixRealm's data property name: %1").arg(ptyName).toUtf8().constData());
        return (*map_instancesName.value(ptyName))();
    }

    //static const TixrEnum<T>* RoleByName(const std::string & roleName) { return this->map_instancesName.value(roleName);  }

    /*static int Min(void) { return (*s_instances.begin())->m_value; }
    static int Max(void) { return (*s_instances.rbegin())->m_value; }
    static const TixrEnum<T>* Corresponding_Enum(int Value)
    {
        const_iterator it = find_if(s_instances.begin(), s_instances.end(),
                                    Enum_Predicate_Corresponds(Value));
        return (it != s_instances.end()) ? *it : NULL;
    }
    static bool Is_Valid_Value(int Value) { return Corresponding_Enum(Value) != NULL; }

    // Number of elements
    static typename instances_list::size_type size(void) { return s_instances.size(); }

    // Iteration
    static const_iterator begin(void) { return s_instances.begin(); }
    static const_iterator end(void) { return s_instances.end(); }*/

protected:
    explicit EnumClass(int Value, QString rolename) { m_value = Value; instances_list.insert(this); map_instancesName.insert(rolename, this); }

    int m_value;
    static QSet<const EnumClass<T>*> instances_list;
    static QMap<QString, const EnumClass<T>*> map_instancesName;
};

#endif // SCQMETAQENUM_H
