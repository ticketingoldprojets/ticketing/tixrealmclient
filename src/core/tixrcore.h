#ifndef TIXRCORE_H
#define TIXRCORE_H
#include <QtCore/QXmlStreamWriter>
#if (QT_VERSION < QT_VERSION_CHECK(5, 0, 0))
#include <QtGui/QGraphicsObject>
#else
#include <QtWidgets/QGraphicsObject>
#endif
#include <QtWidgets/QGroupBox>
#include <QNetworkProxyFactory>
#include <QNetworkReply>
#include "jsonqstandarditemfactory.h"
#include "pendingrequest.h"

#ifdef DEBUG_MODE
class ModelInspectorForm;
#endif

/*!
 *  \class TixRealmCore tixrcore.h
 *  \brief TixRealmCore contient le point d'entré à l'application, chargé de la création de
 *          l'interface graphique. C'est aussi la classe principale de la couche Modèle dans le patron de développement Modèle-vue préconisé par Qt.
 *
 *  TixRealmCore contient le point d'entré à l'application, chargé de la création de l'interface graphique.
 *  C'est aussi la classe principale de la couche Modèle dans le patron de développement Modèle-Vue préconisé par Qt.
*/
class TixRealmCore : public QObject
{
    Q_OBJECT

    QApplication *a;
    QString lastError;
    QString host;
    int port;
    QString proxy;
    int proxyPort;//Proxy's port
    QString proxyUser;
    QString proxyPassword;
    int inHandlerIndex;
    int outHandlerIndex;
    int partnerId;
    QString partnerRespHandler;
    QString partnerRespHandlerPath;
    QNetworkAccessManager *netManager;
    QNetworkProxyFactory *proxyFactory;
    int requestId;
    QString pgFunction;

    QMap<QNetworkReply*, PendingRequest*> pendingRequestMap;

    QByteArray sessionId;
    QByteArray credentials;

    //skins management
    bool uiLoaded;

    QList<QStandardItem *(*)(QIODevice*, QString&, PendingRequest*)> inHandlerList;//métodos de conversión JSON -> MODELO
    QList<QByteArray (*)(QStandardItem*)> outHandlerList;//métodos de conversión MODELO -> JSON

#ifdef DEBUG_MODE
    ModelInspectorForm *modelInspectorForm;
#endif

    QStandardItem *findStdItem(QStringList *coordList);
    QStandardItem *stdItemFinder(QStringList *coordList, QStandardItem *root);
    QGraphicsObject *findPlaceItem(QStandardItem *root, QString svgId);
    void eatSyncMsgForPlanSalle(QString action, QStringList *coord, QStandardItem *stdItem);
    void fillLevelToHeaderModelMap();
    QJsonValue jsonValueFromWidget(const QWidget *w);

private slots:
    /*!
     * \brief readSyncMsg Handle of synchronisation messages.
     * \param syncData The message as a QStandardItem hierarchy.
     */
    void readSyncMsg(QStandardItem *syncData);
    void handleResponse();
    void handleSslErrors(const QList<QSslError> &errors);

public:
    TixRealmCore();
    ~TixRealmCore();
    int start(); //!< Start UI

    QString getLastError();
    QStandardItem *widgetToModel(QWidget *w);//!< Convertit un objet QWidget en une hiérarchie d'objets de la classe QStandardItem, il s'agit d'une représentation d'un message adressé au serveur. Voir <A HREF="index.html">la page initial</A>.
    QJsonObject *widgetToJson(const QWidget *w);
    QMap<QGroupBox*, QList<QWidget*> > getSubLevelsContainerMap(const QWidget *w);
    bool subLevelsContains(const QMap<QGroupBox *, QList<QWidget *> > &subLevelsMap, QWidget *w);

    /*!
     * \brief itemByNameFromModel renvoie l'élément QStandardItem qui représente l'élément XML de la réponse du serveur avec le nom <I>itemName</I>.
     * \param modelItem élément racine de la hiérarchie de QStandardItem répresentant la réponse du serveur.
     * \param itemName nom de l'élément réponse.
     * \return QStandardItem représentant l'élément XML de la réponse.
     *
     *  itemByNameFromModel renvoie l'élément QStandardItem qui représente l'élément XML de la réponse du serveur avec le nom <I>itemName</I>.
     *  Le nom est stocké sous le rôle Qt::AccessibleTextRole.
     *
     *  \attention Cette fonction n'atteint que les descendants directes de l'élément racine. Pour atteindre un élément à tout autre niveau utilisé la fonction itemByValuefullscanFromModel(QStandardItem *, int, QVariant).
     */
    QStandardItem *itemByNameFromModel(QStandardItem *modelItem, QString itemName);

    /*!
     * \brief itemByValuefullscanFromModel renvoie l'élément QStandardItem qui représente l'élément XML de la réponse du serveur avec la valeur <I>value</I> sous le rôle <I>qtrole</I>.
     * \param startRootItem élément racine de la hiérarchie de QStandardItem répresentant la réponse du serveur.
     * \param qtrole le rôle de la donnée.
     * \param value la valeur de la donnée.
     * \return QStandardItem représentant l'élément XML de la réponse.
     *
     *  itemByValuefullscanFromModel renvoie l'élément QStandardItem qui représente l'élément XML de la réponse du serveur avec la valeur <I>value</I> sous le rôle <I>qtrole</I>.
     *  Cette fonction explore tous les niveaux de la hiérarchie.
     */
    QStandardItem *itemByValuefullscanFromModel(QStandardItem *startRootItem, int qtrole, QVariant value);

    /*!
     * \brief setConnParam établit les paramètres de connection au serveur <B>TixRealmserver</B>.
     * \param serverHost Le nom ou l'adresse du serveur
     * \param serverPort Le port
     * \param proxyHost Le nom ou l'adresse du serveur proxy, le cas échéant(<I>lce</I>).
     * \param proxyPort Le port du serveur proxy, <I>lce</I>.
     * \param proxyUsr Utilisateur du serveur proxy, <I>lce</I>.
     * \param proxyPwd Mot de passe pour l'utilisateur du serveur proxy, <I>lce</I>.
     *
     *  setConnParam établit les paramètres de connection au serveur. Elle est appellée une seule fois, avant l'ouverture d'une séssion.
     */
    void setConnParam(QString serverHost, int serverPort, QString proxyHost = QString::null,
                      int proxyPort = -1, QString proxyUsr = QString::null, QString proxyPwd = QString::null);

    /*!
     * \brief sendMsg envoie un message au serveur <B>TixRealmserver</B>.
     * \param requestItem élément racine de la hiérarchie représentant le message à envoyer.
     * \param newMethod La méthode HTTP de la requête (GET ou POST).
     * \param newPath Chemin dans le serveur. Voir <A HREF="../TixRealmserver/TixRealmserver.pdf">TixRealmserver.pdf</A>.
     * \param sender Objet qui envoie la requête.
     * \param senderSlot slot dans l'objet <I>sender</I> charger du traitement de la réponse.
     * \param isMain
     * \return
     *
     *  sendMsg envoie un message au serveur <B>TixRealmserver</B>. Les paramètres de connection doivent être préalablement établis à travers setConnParam(QString,int,QString,int,QString,QString).
     *  Plusieurs fonctions servent à modifier la façon dont on génère la requête, traite la réponse ou son contenu:
     *  setInHandlerIndex(int), setOutHandlerIndex(int), setTarget(int,QString,QString), setSessionId(QByteArray).
     *
     *  Pour plus d'information concernant l'envoyer de message, sérialisation XML et le format de requêtes voir <A HREF="../TixRealmClient.pdf">TixRealmClient.pdf</A>.
     */
    PendingRequest *sendMsg(QStandardItem *requestItem, HTTP_METHOD_ID method, QString path, QObject *sender, const char *senderSlot, const QByteArray &data = QByteArray());

    PendingRequest *sendMsg(QStandardItem *requestItem, HTTP_METHOD_ID method, QString path, QObject *sender, const char *senderSlot, const QJsonObject *jsonData);

    /*!
     * \brief setInHandlerIndex établit la procédure utilisée pour sérialiser la requête.
     * \param funcIdx indice de la procédure de sérialisation.
     *
     *  setInHandlerIndex établit la procédure utiliser pour sérialiser la hiérarchie QStandardItem qui représente la requête.
     *  Jusqu'à présent deux valeurs sont possibles pour <I>funcIdx</I>:
     *  - 0: ConnDetail::modelToXml(QStandardItem*) pour le traitement de tout type de requête sauf celles concernant les grilles de tarifs.
     *  - 1: ConnDetail::grilleModelToXml(QStandardItem*) pour le traitement des requêtes concernant les grilles de tarifs.
     */
    void setInHandlerIndex(int idx){inHandlerIndex = idx;}

    /*!
     * \brief setOutHandlerIndex établit la procédure utilisée pour convertir la reponse, en format <I>XML</I>, en une hiérarchie.
     * \param funcIdx indice de la procédure de convertion.
     *
     *  setOutHandlerIndex établit la procédure utiliser pour convertir la reponse, en format <I>XML</I>, en une hiérarchie QStandardItem. Pour plus d'information concernant la methode de convertion voir <A HREF="../TixRealmClient.pdf">TixRealmClient.pdf</A>.
     *  Jusqu'à présent deux valeurs sont possibles pour <I>funcIdx</I>:
     *  - 0: ConnDetail::xmlToModel(QString&) pour le traitement de tout type de reponses sauf celles concernant les grilles de tarifs.
     *  - 1: ConnDetail::xmlToGrilleModel(QString&) pour le traitement des reponses concernant les grilles de tarifs.
     */
    void setOutHandlerIndex(int idx){outHandlerIndex = idx;}

    /*!
     * \brief setTarget fixe le partenaire auquel est dirigé cette requête.
     * \param partner identifiant du partenaire.
     * \param handler Procédure, du serveur, chargée du traitement de la reponse du partenaire.
     * \param handlerPath Chemin du serveur pour le traitement de la reponse du partenaire.
     *
     *  setTarget fixe le partenaire auquel est dirigé cette requête. Pour plus de détails sur le système de communication avec les partenaires voir <A HREF="../TixRealmserver/TixRealmserver.pdf">TixRealmserver.pdf</A>.
     */
    void setTarget(int partner, QString handler = QString::null, QString handlerPath = QString::null){partnerId = partner; partnerRespHandler = handler; partnerRespHandlerPath = handlerPath;}

    /*!
     * \brief setSessionId fixe l'identifiant de la session en cours.
     * \param si identifiant de la session.
     *
     *  setSessionId fixe l'identifiant de la session en cours dans le serveur <I>TixRealmserver</I>, à envoyer avec chaque requête.
     */
    void setSessionId(QByteArray si){sessionId = si;}

    void setCredentials(QByteArray cred){credentials = cred;}

    /*!
     * \brief applyStyle charge un module d'interface graphique (appellé <I>skin</I>)
     * \param skinName nom du <I>skin</I>.
     * \return
     *
     *  applyStyle charge un module d'interface graphique (appellé <I>skin</I>) dont le nom complet du fichier conteneur est <I>skinName</I>.
     *  Si <I>skinName</I> est vide la procédure cherche le <I>skin</I> <B>../skins/default.skin</B>, s'il n'existe pas le <I>skin</I> <B>default.skin</B> intégré dans l'exécutable sera chargé.
     */
    bool loadSkin(QString skinName = QString::null);

    /*!
     * \brief modelEntryFromProductType renvoie l'élément du model <I>QStandardItemModel</I> qui correspond à la racine pour les produits du type <I>prodType</I>.
     * \param prodType le type de produit.
     * \return le <I>QStandardItem</I> sous lequel sont stockés les produits du type <I>prodType</I>.
     *
     *  modelEntryFromProductType renvoie l'élément du model <I>QStandardItemModel</I> qui correspond à la racine pour les produits du type <I>prodType</I>.
     *  <I>prodType</I> peut avoir une des valeurs suivantes:
     *      - 0: Billetterie.
     *      - 1: Boutique.
     *      - 2: Transport.
     *      - 3: Service.
     *  Pour plus d'information concernant la structuration des données voir <A HREF="../TixRealmClient.pdf">TixRealmClient.pdf</A>.
     */
    QStandardItem *modelEntryFromProductType(int prodType);

    QLocale curLocal;//!< QLocale utilisée actuellement.
    QLocale cLocal;//!< QLocale du serveur.

    QByteArray getSessionId(){return sessionId;}
    QString curPath;//!< Chemin d'installation.
    QString currentSkinName;//!< Fichier contenant le <I>skin</I> en cours.
    QStandardItemModel *stdModel;//!<Conteneur de toutes, ou presque, les données gérés para le client.
    QMap<QString,QStandardItem*> keyToItemMap;//!< Correspondance entre le nom et l'objet QStandardItem racine dans le modèle stdModel.
    QMap<QStandardItem*,QStandardItemModel*> itemToHeaderModelMap;//!< Correspondance entre une hiérarchie QStandardItem et l'objet QStandardItemModel contenant les en-têtes pour cette hiérarchie.
    QMap<int, QStandardItemModel*> levelToHeaderModelMap;//!< Correspondecia entre entre un nivel del modelo de datos y un modelo de headers.
    QMap< QString, void (TixRealmCore::*)(QString,QStringList*,QStandardItem*) > syncMsgHandlersMap;

    /*
    Caché de iconos de los asientos. En la lista cada posición corresponde a uno de los estados posibles: libre, bloqueada, vendida,...
    El contenido de cada posición corresponde a las versiones de los iconos primero por nivel de escala y por cada nivel están las versiones por color para ambos sub-estados (normal, seleccionado).
    La caché es creada es un nivel más alto: en mainwindow en el modo edición y en tixrCore en modo venta. Esto permite compartir la caché a nivel de aplicación entre todos los planos abiertos.
    */
    QList< QMap<int, QMap<QString, QPair<QImage, QImage> > > > *stateIconsCache;//0 - Libre; 1 - Bloquée; 2 - Payée

    QObject *objMainWindow;

    QString getPgFunction() const;
    void setPgFunction(const QString &value);

    /**
     * @brief getItemAtLevelFromChild Devuelve le padre que se encuentra en el nivel level dado un descendiente directo o indirecto.
     * @param child Descendiente directo o indirecto del elemento buscado.
     * @return Elemento padre a nivel level en la gerarquía.
     */
    QStandardItem *getItemAtLevelFromChild(QStandardItem * const child, int level);

    bool getUiLoaded() const;

    /*!
     * \brief addToModel Add each jsonvalue to <I>item</I>.
     * \param item Item for add values.
     *
     *  addToModel Util method for add a set of values to a QStandarItem with the format used on stdModel for store data.
     */
    static void addToModel(QStandardItem *item, const QJsonObject *jsonObj);

signals:
    // gestión del carro de compras
    /*!
     * \brief addCart signale le déclenchement de la procédure pour ajouter un caddie.
     */
    void addCart(QStandardItem*);
    /*!
     * \brief findCart signale le déclenchement d'une recherche de caddie.
     */
    void findCart(int, QStandardItem*);
    /*!
     * \brief cartRequestResult signale la disponibilité d'une reponse de recherche d'un caddie.
     */
    void cartRequestResult(bool, QStandardItem*);

// for unit testing purpose
friend class LoginTest;
friend class JsonModelFactoryTest;
friend class NewSeanceTest;
friend class NewSpectWidgetTest;
};

#endif // TIXRCORE_H

