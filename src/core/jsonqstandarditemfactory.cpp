#include "jsonqstandarditemfactory.h"
#include "../../lib/cJSON/cJSON.h"
#include "customstandarditem.h"
#include "../../../TixrClientEditorCommon/customgraphicsscene.h"
#include "../../../TixrClientEditorCommon/site2dview.h"
#include "pendingrequest.h"
#include <QIODevice>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>

QStandardItemModel *JsonQStandardItemFactory::hModel = 0;
bool JsonQStandardItemFactory::isSyncMsg = false;

Q_DECLARE_METATYPE( QStandardItem* )

JsonQStandardItemFactory::JsonQStandardItemFactory()
{
}

QByteArray JsonQStandardItemFactory::modelToJson(QStandardItem *rootItem)
{
    Q_ASSERT(rootItem);

    JsonElemType curJsonType = (JsonElemType)rootItem->data(DataRole::jsonType()).toInt();

    Q_ASSERT(curJsonType==JsonObject || curJsonType==JsonArray);

    QList<QStandardItem*> stdItmList;
    QList<cJSON*> jsonList;

    cJSON *jsonDoc = curJsonType==JsonObject?cJSON_CreateObject():cJSON_CreateArray();

    stdItmList.append(rootItem);
    jsonList.append(jsonDoc);

    do
    {
        rootItem = stdItmList.takeFirst();
        cJSON *rootJson = jsonList.takeFirst();

        curJsonType = (JsonElemType)rootItem->data(DataRole::jsonType()).toInt();

        int N = rootItem->columnCount(),
            M = rootItem->rowCount();

        for(int j=0; j<N; j++)
            for(int i=0; i<M; i++)
            {
                QStandardItem *childItm = rootItem->child(i,j);

                JsonElemType childJsonType = (JsonElemType)childItm->data(DataRole::jsonType()).toInt();

                QString name = childItm->data(DataRole::jsonProperty()).toString();

                switch(childJsonType)
                {
                    case JsonObject:
                    case JsonArray:
                    {
                        cJSON *newRoot = childJsonType==JsonObject?cJSON_CreateObject():cJSON_CreateArray();

                        if(curJsonType==JsonObject)
                            cJSON_AddItemToObject(rootJson, name.toUtf8().constData(), newRoot);
                        else
                            cJSON_AddItemToArray(rootJson, newRoot);

                        stdItmList.append(childItm);
                        jsonList.append(newRoot);
                        break;
                    }

                    case JsonSimple:
                    {
                        QString value;
                        if(childItm->data(Qt::DisplayRole).isValid())
                            value = childItm->data(Qt::DisplayRole).toString();
                        else
                        {
                            QImage img = rootItem->data(Qt::DecorationRole).value<QImage>();
                            QByteArray ba;
                            QBuffer buffer(&ba);
                            buffer.open(QIODevice::WriteOnly);
                            img.save(&buffer, "PNG");
                            buffer.close();
                            value = ba.toBase64();
                        }
                        if(curJsonType==JsonObject)
                            cJSON_AddStringToObject(rootJson, name.toUtf8().constData(), value.toUtf8().constData());
                        else
                            cJSON_AddItemToArray(rootJson, cJSON_CreateString(value.toUtf8().constData()));
                    }
                }
            }
    }
    while(!stdItmList.isEmpty());

    char *json = cJSON_Print(jsonDoc);

    QByteArray ba(json);

    cJSON_Delete(jsonDoc);
    free(json);

    return ba;
}

QByteArray JsonQStandardItemFactory::tariffModelToJson(QStandardItem *tarItem)
{
    QStandardItemModel *griModel = static_cast<QStandardItemModel*>(tarItem->model());

    cJSON *paramObj = cJSON_CreateObject();

    int M = tarItem->rowCount(), N = tarItem->columnCount();

    cJSON *catCliArr = cJSON_CreateArray();
    for(int i=0; i<M; i++)
    {
        QStandardItem *hItem = griModel->verticalHeaderItem(i);
        cJSON_AddItemToArray(catCliArr, cJSON_CreateString(hItem->text().toUtf8().constData()));
    }

    cJSON_AddStringToObject(paramObj, "griname", tarItem->text().toUtf8().constData());
    cJSON_AddItemToObject(paramObj, "catscli", catCliArr);

    QVariant v = tarItem->data(DataRole::ikey());
    if(v.isValid())
        cJSON_AddNumberToObject(paramObj, "idgri", v.toInt());

    cJSON *childParams;
    cJSON_AddItemToObject(paramObj, "tariffs", childParams=cJSON_CreateArray());

    for(int j=0; j<N; j++)
    {
        QStandardItem *stdItem = griModel->horizontalHeaderItem(j);

        cJSON *tarif = cJSON_CreateObject();

        cJSON_AddItemToArray(childParams, tarif);

        cJSON *pricesArr = cJSON_CreateArray();
        for(int i=0; i<M; i++)
        {
            cJSON_AddItemToArray(pricesArr, cJSON_CreateNumber(tarItem->child(i,j)->data(Qt::DisplayRole).toFloat()));
        }

        cJSON_AddStringToObject(tarif, "tname", stdItem->text().toUtf8().constData());
        cJSON_AddItemToObject(tarif, "prices", pricesArr);
    }

    char *json = cJSON_Print(paramObj);

    QByteArray ba(json);

    cJSON_Delete(paramObj);
    free(json);

    return ba;
}

QStandardItem *JsonQStandardItemFactory::getTargetItem(const QString &propertyName, QStandardItem *curItem)
{
    QString tmp(propertyName);
    return getTargetItem(tmp, curItem);
}

QStandardItem *JsonQStandardItemFactory::getTargetItem(QString &propertyName, QStandardItem *curItem)
{
    QStringList sList = propertyName.split("_", QString::SkipEmptyParts);
    Q_ASSERT(sList.size()>0 && sList.size()<3);

    QString columnName;

    if(sList.size()==2)//format ColumName_RoleName
    {
        propertyName = sList.at(1);
        columnName = sList.at(0);
    }
    else // format ColumnName_Qt::DisplayRole or 0_RoleName
    {
        if(DataColumn::exists(propertyName))
        {
            columnName = propertyName;
            propertyName = QStringLiteral("qtdisplayrole");
        }
        else
        {
            columnName = QStringLiteral("imgcol");
        }
    }

    Q_ASSERT_X(DataRole::exists(propertyName), "getTargetItem", QStringLiteral("propertyName = %1").arg(propertyName).toUtf8().constData());
    Q_ASSERT_X(columnName.isEmpty() || DataColumn::exists(columnName), "getTargetItem", QStringLiteral("columnName = %1").arg(columnName).toUtf8().constData());

    QStandardItem *parent = curItem->parent();
    Q_ASSERT(parent);//el elemento actual ya debe tener un padre

    int row = curItem->row();
    int col = columnName.isEmpty() ? 0 : DataColumn::FromName(columnName);//los datos serán representados en más de una columna
    QStandardItem *currentStdItem = parent->child(row,col);
    if(!currentStdItem)
    {
        currentStdItem = createItem();
        parent->setChild(row, col, currentStdItem);
    }

    return currentStdItem;
}

QStandardItem *JsonQStandardItemFactory::createItem(const QString &text)
{
    //QStandardItem *newStdItem = isSyncMsg ? new CustomStandardItem(text) : new QStandardItem(text);
    QStandardItem *newStdItem = new CustomStandardItem(text);
    newStdItem->setFlags(newStdItem->flags() ^ (Qt::ItemIsDropEnabled));
    return newStdItem;
}

QStandardItem *JsonQStandardItemFactory::jsonToModel(QIODevice *ioDev, QString &error, PendingRequest *pReq)
{
    /**
     * Si el padre es un array los hijos serán hijos QStandardItem del padre.
     * Si el padre es un objeto las propiedades serán sea valores de roles o roles de QStandardItem situados en columnas según el nombre de la propiedad esté en DataRole o en DataColumn.
     * */
    QByteArray ba = ioDev->readAll();
    qDebug() << "response: " << ba;

    QJsonParseError pError;
    QJsonDocument jsonDoc = QJsonDocument::fromJson(ba, &pError);
    if(pError.error != QJsonParseError::NoError)
    {
        error = QObject::tr("Invalid server response");
        qDebug() << "QJsonParseError: " << pError.errorString();
        return 0;
    }

    if(!jsonDoc.isArray())
    {
        error = QObject::tr("Invalid server response");
        return 0;
    }

    QList<QJsonValue> jsonList;//cola de json's items por procesar

    if(pReq->isJsonSingleValueFunc)
    {
        if(jsonDoc.array().count() != 1 || !jsonDoc.array().at(0).isObject())
        {
            error = QObject::tr("Invalid server response");
            return 0;
        }

        QJsonObject obj = jsonDoc.array().at(0).toObject();

        QString pgFunc = pReq->getPgFunction().section('.', 1);//quitar el nombre del esquema pgsql
        if(obj.size() != 1 || !obj.contains(pgFunc))
        {
            error = QObject::tr("Invalid server response");
            return 0;
        }

        jsonList << obj.value(pgFunc);
    }
    else
        jsonList << QJsonValue(jsonDoc.array());

    QStandardItem *rootItem = createItem();
    QStandardItem *result = rootItem;

    QList<QStandardItem*> mItmList;//cola de model's items por procesar
    mItmList << rootItem;

    do
    {
        QJsonValue jsonValue = jsonList.takeFirst();
        rootItem = mItmList.takeFirst();

        QStandardItem *modelItem, *target;
        if(jsonValue.isArray())
        {
            QJsonArray jsonArray = jsonValue.toArray();
            for(QJsonArray::const_iterator it=jsonArray.constBegin(), end=jsonArray.constEnd(); it<end; it++)
            {
                modelItem = createItem();
                rootItem->appendRow(modelItem);

                if((*it).isArray() || (*it).isObject())
                {
                    mItmList << modelItem;
                    jsonList << *it;
                }
                else
                {
                    Q_ASSERT_X(!modelItem->data(Qt::DisplayRole).isValid() || modelItem->data(Qt::DisplayRole).toString().isEmpty(), "jsonToModel", QString("duplicate role %1 in funct %2").arg(QStringLiteral("Qt::DisplayRole"),pReq->getPgFunction()).toUtf8().constData());//controlar que no estoy sobrescribiendo un valor de role existente, el problema viene probablemente porque estoy duplicando los roles o columnas en la definición de columnas de la función pgsql
                    modelItem->setData((*it).toVariant(), Qt::DisplayRole);
                }
            }
        }
        else if(jsonValue.isObject())
        {
            QStandardItem * stdItemWithPendingImgGenTask = 0;
            QJsonObject jsonObj = jsonValue.toObject();
            for(QJsonObject::const_iterator it=jsonObj.constBegin(), end=jsonObj.constEnd(); it!=end; it++)
            {
                QString pName = it.key();

                target = getTargetItem(pName, rootItem);

                int qtRole = DataRole::FromName(pName);

                if((*it).isArray() || (*it).isObject())
                {
                    modelItem = createItem();
                    mItmList << modelItem;
                    jsonList << *it;
                    target->appendRow(modelItem);
                }
                else
                {
                    Q_ASSERT_X(!target->data(qtRole).isValid() || target->data(qtRole).toString().isEmpty(), "jsonToModel", QString("duplicate role %1 in funct %2, row %3, col %4, data %5").arg(pName,pReq->getPgFunction(), QString::number(target->row()), QString::number(target->column()), target->data(qtRole).toString()).toUtf8().constData());//controlar que no estoy sobrescribiendo un valor de role existente, el problema viene probablemente porque estoy duplucando los roles o columnas en la definición de columnas de la función pgsql
                    if(qtRole == Qt::DecorationRole)
                    {
                        //image generation
                        QByteArray ba = (*it).toVariant().toByteArray();
                        ba = QByteArray::fromBase64(ba);
                        if(ba.isEmpty())
                        {
                            stdItemWithPendingImgGenTask = target;
                        }
                        else
                        {
                            QColor c;
                            if(ba.length() < 15)//el formato de color más largo tiene 13 caracteres
                            {
                                c = QColor(ba.constData());
                            }

                            if(c.isValid())
                                target->setData(c, Qt::DecorationRole);
                            else
                            {
                                QPixmap p;
                                p.loadFromData(ba);
                                target->setData(p, Qt::DecorationRole);
                            }
                        }
                    }
                    else
                        target->setData((*it).toVariant(), qtRole);
                }
            }
            if(stdItemWithPendingImgGenTask)
                setItemImage(stdItemWithPendingImgGenTask);
        }
        else if(!jsonValue.isNull())
        {
            Q_ASSERT(false);
            error = QObject::tr("Invalid response, there is not place for a single value here");
            delete result;
            return 0;
        }
    }
    while(!jsonList.isEmpty());

    return result;
}

QStandardItem *JsonQStandardItemFactory::jsonToTariffModel(QIODevice *ioDev, QString &error, PendingRequest *pReq)
{
    Q_UNUSED(pReq)

    QByteArray ba = ioDev->readAll();

    //qDebug() << "response: " << ba;

    cJSON *jsonRoot = cJSON_Parse(ba.constData());
    if(!jsonRoot)
    {
        error = QObject::tr(cJSON_GetErrorPtr());
        cJSON_Delete(jsonRoot);
        return 0;
    }

    QString pTableName;//Pricing table name
    int itKey;//database's item key
    cJSON *catCliArray;
    QDateTime creationDate;
    QList<QPair<QString,int> > nameIdPairList;
    QList<cJSON*> priceColList;
    int M;

    QStandardItem *currentCatProd = pReq->respHandlerParams.contains("currentCatProd") ? pReq->respHandlerParams.value("currentCatProd").value<QStandardItem*>() : 0;

    cJSON *subitem = cJSON_GetArrayItem(jsonRoot, 0);//se espera sólo un elemento, la tabla de precios
    if(subitem->type==cJSON_Object)
    {
        itKey = cJSON_GetObjectItem(subitem, "ikey")->valueint;
        pTableName = cJSON_GetObjectItem(subitem, "nomgri")->valuestring;
        catCliArray = cJSON_GetObjectItem(subitem, "catscli");
        creationDate = QDateTime::fromString(QString(cJSON_GetObjectItem(subitem, "createdate")->valuestring), Qt::ISODate);
        M = cJSON_GetArraySize(catCliArray);
        cJSON *childs = cJSON_GetObjectItem(subitem, "tariffs");

        for(int i=0, n=cJSON_GetArraySize(childs); i<n; i++)
        {
            cJSON *tarItm = cJSON_GetArrayItem(childs, i);
            Q_ASSERT(tarItm);
            if(tarItm->type==cJSON_Object)
            {
                cJSON *nameItm = cJSON_GetObjectItem(tarItm, "nom");
                cJSON *idItm = cJSON_GetObjectItem(tarItm, "idzonetarif");
                nameIdPairList << QPair<QString,int>(nameItm->valuestring, idItm->valueint);

                if(currentCatProd)
                {
                    cJSON *quoteItm = cJSON_GetObjectItem(tarItm, "quote");
                    Q_ASSERT(quoteItm);
                    if(!quoteItm)
                    {
                        currentCatProd->removeRow(0);
                        error = QObject::tr("Quotes not found for this tariff");
                        cJSON_Delete(jsonRoot);
                        return 0;
                    }
                    currentCatProd->appendColumn(QList<QStandardItem*>()<<createItem(QString::number(quoteItm->valueint)));
                }

                cJSON *priceArray = cJSON_GetObjectItem(tarItm, "lesprix");
                int d = cJSON_GetArraySize(priceArray);
                if(M != d)
                {
                    Q_ASSERT(M == d);
                    error = QObject::tr("Invalid tariff structure");
                    cJSON_Delete(jsonRoot);
                    if(currentCatProd)
                        currentCatProd->removeRow(0);
                    return 0;
                }
                priceColList << priceArray;
            }
        }
    }
    else
    {
        error = QObject::tr("Invalid response for a tariff");
        cJSON_Delete(jsonRoot);
        return 0;
    }

    QStandardItem *modelItem = createItem(pTableName);
    int N = nameIdPairList.size();
    modelItem->setData(itKey, DataRole::ikey());//La llave
    modelItem->setRowCount(M);
    modelItem->setColumnCount(N);
    hModel = new QStandardItemModel(M, N);

    for(int i=0; i<M; i++)//Crear los encabezados en el modelo de headers
    {
        cJSON *catCliObj = cJSON_GetArrayItem(catCliArray, i);
        hModel->setVerticalHeaderItem(i, new QStandardItem(catCliObj->valuestring));
    }

    for(int j=0; j<N; j++)
    {
        QPair<QString,int> colData = nameIdPairList.at(j);
        QStandardItem *hItem = new QStandardItem(colData.first);
        hModel->setHorizontalHeaderItem(j, hItem);
        hItem->setData(colData.second, DataRole::ikey());//La llave
        cJSON *priceArray = priceColList.at(j);
        for(int i=0; i<M; i++)
        {
            QStandardItem *child = createItem();
            cJSON *priceItm = cJSON_GetArrayItem(priceArray, i);
            child->setData(QVariant(priceItm->valuedouble), Qt::DisplayRole);
            child->setEditable(false);
            modelItem->setChild(i, j, child);
        }
    }

    cJSON_Delete(jsonRoot);

    return modelItem;
}

QStandardItem *JsonQStandardItemFactory::jsonToPlanModel(QIODevice *ioDev, QString &error, PendingRequest *pReq)
{
    QStandardItem *requestItem = pReq->getRequestItem();

    Site2dView *site2dView = requestItem->parent()->data(DataRole::site2dviewptr()).value<Site2dView*>();
    site2dView->loadPlan(ioDev);

    error = site2dView->errorMsg();

    return 0;//el responseItem no tiene utilidad en este caso
}

QStandardItem *JsonQStandardItemFactory::jsonToQJsonDocument(QIODevice *ioDev, QString &error, PendingRequest *pReq)
{
    QJsonParseError parserError;
    QByteArray ba = ioDev->readAll();
    pReq->rawResp = QJsonDocument::fromJson(ba, &parserError);
    if(parserError.error != QJsonParseError::NoError)
        error = parserError.errorString();

    QFile f("D:/out.txt");
    f.open(QIODevice::WriteOnly);
    f.write(ba);
    f.close();

    return 0;//el responseItem no tiene utilidad en este caso
}

#include <QtXml/QDomDocument>
#include <QtSvg/QSvgRenderer>
#include <QtGui/QPainter>
QImage JsonQStandardItemFactory::genCalendarImg(QDate d)
{
    QFile f(":/images/calendar.svg");
    f.open(QIODevice::ReadOnly);
    QDomDocument domDoc;
    domDoc.setContent(&f);
    f.close();

    QString dia = d.toString("dd");
    QString mes = d.toString("MMM");
    QString ano = d.toString("yyyy");
    mes = mes.left(1).toUpper()+mes.right(mes.size()-1); //!<Al menos en Fr. el primer caracter no está en mayúscula según dice la doc. Qt

    QDomNodeList nodeList = domDoc.elementsByTagName("text");
    for(int i=0,n=nodeList.size(); i<n; i++)
    {
        QDomElement elem = nodeList.at(i).toElement();

        if(elem.attribute("id") == "text3329")
        {
            QDomNode node = elem.firstChild();
            Q_ASSERT(!node.isNull());
            node.setNodeValue(dia);
        }else

        if(elem.attribute("id") == "text3327")
        {
            QDomNode node = elem.firstChild();
            Q_ASSERT(!node.isNull());
            node.setNodeValue(mes);
        }else

        if(elem.attribute("id") == "text4005")
        {
            QDomNode node = elem.firstChild();
            Q_ASSERT(!node.isNull());
            node.setNodeValue(ano);
        }
    }

    QSvgRenderer renderer(domDoc.toByteArray());
    QSize s2 = renderer.viewBox().size();
    QImage img(s2, QImage::Format_ARGB32);
    img.fill(0);
    QPainter painter(&img);
    renderer.render(&painter);
    painter.end();

    return img;
}

QImage JsonQStandardItemFactory::genZoneTarImg(QString zname, QString dispo, QString total)
{
    QFile f(":/images/zoneTarif.svg");
    f.open(QIODevice::ReadOnly);
    QDomDocument domDoc;
    domDoc.setContent(&f);
    f.close();

    QDomNodeList nodeList = domDoc.elementsByTagName("text");
    for(int i=0,n=nodeList.size(); i<n; i++)
    {
        QDomElement elem = nodeList.at(i).toElement();
        if(elem.attribute("id") == "text3327-7")//Nom de la zone
        {
            QDomNode node = elem.firstChild().firstChild();
            Q_ASSERT(!node.isNull());
            node.setNodeValue(zname);
        }else
        if(elem.attribute("id") == "text3329")//Máximo disponible
        {
            QDomNode node = elem.firstChild();
            Q_ASSERT(!node.isNull());
            node.setNodeValue(dispo);
        }else
        if(elem.attribute("id") == "text3327")//Cantidad vendida
        {
            QDomNode node = elem.firstChild();
            Q_ASSERT(!node.isNull());
            node.setNodeValue(total.toInt()==-1?"∞":total);
        }
    }

    QSvgRenderer renderer(domDoc.toByteArray());
    QSize s2 = renderer.viewBox().size();
    QImage img(s2, QImage::Format_ARGB32);
    img.fill(0);
    QPainter painter(&img);
    renderer.render(&painter);
    painter.end();

    return img;
}

QImage JsonQStandardItemFactory::genPriceImg(qreal price)
{
    QFile f(":/images/money.svg");
    f.open(QIODevice::ReadOnly);
    QDomDocument domDoc;
    domDoc.setContent(&f);
    f.close();

    QString priceStr = QLocale().toString(price);

    QDomNodeList nodeList = domDoc.elementsByTagName("text");
    for(int i=0,n=nodeList.size(); i<n; i++)
    {
        QDomElement elem = nodeList.at(i).toElement();
        if(elem.attribute("id") == "text3019")//Nom de la zone
        {
            QDomNode node = elem.firstChild().firstChild();
            Q_ASSERT(!node.isNull());
            node.setNodeValue(priceStr);
        }
    }

    QSvgRenderer renderer(domDoc.toByteArray());
    QSize s2 = renderer.viewBox().size();
    QImage img(s2, QImage::Format_ARGB32);
    img.fill(0);
    QPainter painter(&img);
    renderer.render(&painter);
    painter.end();

    return img;
}

QImage JsonQStandardItemFactory::genBlankImg(int width, int height)
{
    QImage blankSurface(width, height, QImage::Format_RGB16);

    QPainter painter(&blankSurface);
    QPoint p1(width*4/10, 0);
    QPoint p2(width*6/10, height);
    QLinearGradient linearGrad(p1, p2);
    linearGrad.setColorAt(0, Qt::black);
    linearGrad.setColorAt(1, Qt::white);
    painter.setBrush(linearGrad);
    painter.fillRect(0, 0, width, height, QBrush(linearGrad));

    painter.setPen(QPen(QColor(64,64,64), 4));
    painter.setBrush(QBrush());
    painter.drawRect(2, 2, width-3, height-3);
    painter.end();

    return blankSurface;
}


bool JsonQStandardItemFactory::getIsSyncMsg()
{
    return isSyncMsg;
}

void JsonQStandardItemFactory::setIsSyncMsg(bool value)
{
    isSyncMsg = value;
}
void JsonQStandardItemFactory::setItemImage(QStandardItem *stdItem)
{
    QImage img;
    QVariant v, v2, v3;
    //QString elemName = stdItem->accessibleText();

    //if(elemName=="CategorieProduits" || elemName=="TransportProdCateg")
    QStandardItem *p = stdItem->parent();
    Q_ASSERT(p);
    QStandardItem *f = p->child(stdItem->row(), DataColumn::FromName(QStringLiteral("cpdat")));
    if(f && (v=f->data(Qt::DisplayRole)).isValid())
    {
        img = genCalendarImg(v.toDate());
    }
    else

    //if(elemName=="ZoneAvecPrix")
    if((v=stdItem->data(Qt::DisplayRole)).isValid() &&
       (v2=stdItem->data(DataRole::zapquant())).isValid() &&
       (v3=stdItem->data(DataRole::zapdispo())).isValid())
    {
        QString zname = v.toString();
        QString zsize = v2.toString();
        QString zdispo = v3.toString();
        img = genZoneTarImg(zname, zsize, zdispo);
    }
    else

    //if(elemName=="CategCli")
    if((v=stdItem->data(DataRole::ccprice())).isValid())
    {
        qreal price = v.toDouble();
        img = genPriceImg(price);
    }
    else

    img = genBlankImg(150,200);

    stdItem->setData(img, Qt::DecorationRole);
}
