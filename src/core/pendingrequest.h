#ifndef PENDINGREQUEST_H
#define PENDINGREQUEST_H

#include <QtGui/QStandardItemModel>
#include <QJsonDocument>

enum class HTTP_METHOD_ID : char {NONE, GET, POST};

class PendingRequest
{
    QByteArray postedData;
    QStandardItemModel *hModel;
    int id;
    QStandardItem *requestItem;
    QStandardItem *responseItem;
    QStandardItem *responseItemParent;
    int inHandlerIndex;
    HTTP_METHOD_ID type;
    QString pgFunction;//función pgsql que se mandó a ejecutar, o una cadena vacía
    const char *senderSlot;//manipulador de la respuesta, es un slot en el sender
    QString respDataMapKey;//llave en tixrCore->keyToItemMap

public:
    QMap<QString,QVariant> respHandlerParams;//Permite pasar parámetros desde donde se envía la solicitud hasta dónde se procesa la respuesta.
    QJsonDocument rawResp;//En algunos casos es preferible tratar la respuesta como json sin transformarla en una gerarquía de QStandardItems.
    bool isResponseExpected;///<!Se espera una respuesta, valdrá FALSE en el caso de ejecución de función VOID en el servidor.
    bool isJsonSingleValueFunc;///<!La función llamada es del tipo: RETURNS json.
    const char *senderSlot2;//en el caso de las clases que heredan de TixrWidget, permite tener un tratamiento más específico de la respuesta
    QObject *sender;//emisor de la request
    QString respError;

    PendingRequest(HTTP_METHOD_ID reqType);
    ~PendingRequest();
    void setData(QByteArray data);
    QStandardItemModel *headersModel();
    HTTP_METHOD_ID getType() const;
    QStandardItem *getRequestItem();
    void setRequestItem(QStandardItem *reqItm);
    QStandardItem *getResponseItem();
    QStandardItem *getResponseItemParent();
    void setResponseItem(QStandardItem *respItm);///<!Util en los casos en que se invoca el slot directamente, para visualizar datos que ya están en el cliente
    void setResponseItemParent(QStandardItem *parent);
    QString getPgFunction() const;
    void setPgFunction(const QString &pgfn){ pgFunction = pgfn; }
    QString getRespDataMapKey() const;
    void setRespDataMapKey(const QString &value);

friend class TixRealmCore;
};

#endif // PENDINGREQUEST_H
