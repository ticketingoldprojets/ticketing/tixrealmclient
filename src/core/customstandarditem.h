#ifndef CUSTOMSTANDARDITEM_H
#define CUSTOMSTANDARDITEM_H

#include <QStandardItem>

class CustomStandardItem : public QStandardItem
{
public:
    CustomStandardItem(const QString & text);
    void setData(const QVariant &value, int role);
    QList<int> customRolesList;
};

#endif // CUSTOMSTANDARDITEM_H
