#include "datacolumn.h"

template<> QSet<const EnumClass<DataColumn>*> EnumClass<DataColumn>::instances_list = QSet<const EnumClass<DataColumn>*>();
template<> QMap<QString, const EnumClass<DataColumn>*> EnumClass<DataColumn>::map_instancesName = QMap<QString, const EnumClass<DataColumn>*>();

/**
 * Commom columns
 */
const DataColumn DataColumn::imgcol(0, "imgcol");//la primera columna será la imagen para aquellos elementos que lo requieren

/**
 * Cmd: gettotalscommandepayments
 */
const DataColumn DataColumn::tcpmodepaycol(1, "tcpmodepaycol");
const DataColumn DataColumn::tcpmontantcol(2, "tcpmontantcol");
const DataColumn DataColumn::tcpdevisecol(3, "tcpdevisecol");

/**
 * Cmd: getcloturescaisse
 */
const DataColumn DataColumn::ccdatecloturercol(1, "ccdatecloturercol");

/**
 * Cmd: gettransactionsbydate
 */
const DataColumn DataColumn::tacmontantcol(1, "tacmontantcol");
const DataColumn DataColumn::tacsoldecol(2, "tacsoldecol");

/**
 * Cmd: getencaissementsbydate
 */
const DataColumn DataColumn::eacmodepaycol(1, "eacmodepaycol");
const DataColumn DataColumn::eacmontantcol(2, "eacmontantcol");

/**
 * Cmd: gettierscommandepayments
 */
const DataColumn DataColumn::tcptotalcol(1, "tcptotalcol");
const DataColumn DataColumn::tcpequilibrercol(2, "tcpequilibrercol");

/**
 * Cmd: getpiecescaisse_1
 */
const DataColumn DataColumn::pcnombrecol(1, "pcnombrecol");
const DataColumn DataColumn::pcmontant(2, "pcmontant");

/**
 * Cmd: getpaymentbymode
 */
const DataColumn DataColumn::pbmnombrecol(1, "pbmnombrecol");
const DataColumn DataColumn::pbmmontantcol(2, "pbmmontantcol");
const DataColumn DataColumn::pbmnombre2col(3, "pbmnombre2col");
const DataColumn DataColumn::pbmmontant2col(4, "pbmmontant2col");

/**
 * Cmd: gettiers
 */
const DataColumn DataColumn::tnomcol(1, "tnomcol");
const DataColumn DataColumn::tdatenaissancecol(2, "tdatenaissancecol");
const DataColumn DataColumn::tcommentairecol(3, "tcommentairecol");
const DataColumn DataColumn::tidentifiantcol(4, "tidentifiantcol");
const DataColumn DataColumn::tttierscol(5, "tttierscol");
const DataColumn DataColumn::tcivilitecol(6, "tcivilitecol");
const DataColumn DataColumn::taccountcol(7, "taccountcol");
const DataColumn DataColumn::tinfoentreprisecol(8, "tinfoentreprisecol");
const DataColumn DataColumn::tdatecreationcol(9, "tdatecreationcol");
const DataColumn DataColumn::tsex(10, "tsex");
const DataColumn DataColumn::topermakercol(11, "topermakercol");
const DataColumn DataColumn::tprenomcol(12, "tprenomcol");
const DataColumn DataColumn::tpwd(13, "tpwd");

/**
 * Cmd: getoperators
 */
const DataColumn DataColumn::tonomcol(1, "tonomcol");
const DataColumn DataColumn::todatenaissancecol(2, "todatenaissancecol");
const DataColumn DataColumn::tocommentairecol(3, "tocommentairecol");
const DataColumn DataColumn::toidentifiantcol(4, "toidentifiantcol");
const DataColumn DataColumn::tottierscol(5, "tottierscol");
const DataColumn DataColumn::tocivilitecol(6, "tocivilitecol");
const DataColumn DataColumn::todatecreationcol(7, "todatecreationcol");
const DataColumn DataColumn::tosexcol(8, "tosexcol");
const DataColumn DataColumn::toopermakercol(9, "toopermakercol");

/**
 * Cmd: getallmodepaiem
 */
const DataColumn DataColumn::mpouvtiroircol(1, "mpouvtiroircol");
const DataColumn DataColumn::mpanonymcol(2, "mpanonymcol");

/**
 * Cmd: getalldevises
 */
const DataColumn DataColumn::dabrcol(1, "dabrcol");
const DataColumn DataColumn::dtauxcol(2, "dtauxcol");

/**
 * Cmd: getallbanques
 */
const DataColumn DataColumn::bpayscol(1, "bpayscol");

/**
 * Cmd: getallcatsforfamprods, getalltranspcatsforfamprods
 */
const DataColumn DataColumn::cptitre(1, "cptitre");
const DataColumn DataColumn::cpdat(2, "cpdat");
const DataColumn DataColumn::cpddvente(3, "cpddvente");
const DataColumn DataColumn::cpdfvente(4, "cpdfvente");
const DataColumn DataColumn::cpdescript(5, "cpdescript");
const DataColumn DataColumn::cpaddress(6, "cpaddress");
const DataColumn DataColumn::cpiva(7, "cpiva");
const DataColumn DataColumn::cpivalcosto(8, "cpivalcosto");
const DataColumn DataColumn::cpcosto(9, "cpcosto");
const DataColumn DataColumn::cpplano(10, "cpplano");
const DataColumn DataColumn::cpnoautofill(11, "cpnoautofill");
const DataColumn DataColumn::cpnoseasonticket(12, "cpnoseasonticket");
const DataColumn DataColumn::cpprinterid(13, "cpprinterid");
const DataColumn DataColumn::cpticketmodelid(14, "cpticketmodelid");
const DataColumn DataColumn::cpuserprof(15, "cpuserprof");
const DataColumn DataColumn::cpztarmap(16, "cpztarmap");
const DataColumn DataColumn::cpgritar(17, "cpgritar");
const DataColumn DataColumn::cpisaflow(18, "cpisaflow");
const DataColumn DataColumn::cpsizes(19, "cpsizes");
//const DataColumn DataColumn::tpcsubcatcol(1, "tpcsubcatcol");//para los productos transporte, por el momento no los trataré, para simplificar (Yovanis, 30092014)

/**
 * Cmd: getallfamcatprods
 */
const DataColumn DataColumn::fptitlecol(1, "fptitlecol");
const DataColumn DataColumn::fpthemecol(2, "fpthemecol");
const DataColumn DataColumn::fpdescripcol(3, "fpdescripcol");
const DataColumn DataColumn::fpdatecol(4, "fpdatecol");

/**
 * Cmd: getdetailscommande
 */
const DataColumn DataColumn::dcpayedcol(1, "dcpayedcol");
const DataColumn DataColumn::dcticketscol(2, "dcticketscol");

/**
 * Cmd: gettierspayments
 */
const DataColumn DataColumn::pdtdevcol(1, "pdtdevcol");
const DataColumn DataColumn::pdtamountcol(2, "pdtamountcol");
const DataColumn DataColumn::pdtmodpaycol(3, "pdtmodpaycol");
const DataColumn DataColumn::pdtpayercol(4, "pdtpayercol");

/**
 * Cmd: getcommandepayments
 */
const DataColumn DataColumn::cpmodepaycol(1, "cpmodepaycol");
const DataColumn DataColumn::cpmontantcol(2, "cpmontantcol");
const DataColumn DataColumn::cpdevisecol(3, "cpdevisecol");
const DataColumn DataColumn::cpdatepaymcol(4, "cpdatepaymcol");

/**
 * Cmd: getports
 */
const DataColumn DataColumn::gpportnamecol(0, "gpportnamecol");

/**
 * Tiers Child Model Index
 */
const DataColumn DataColumn::tcicontact(0, "tcicontact");
const DataColumn DataColumn::tciaddress(1, "tciaddress");
const DataColumn DataColumn::tcihistoric(2, "tcihistoric");
const DataColumn DataColumn::tcipayments(3, "tcipayments");

/**
 * Cmd: getpartners
 */
const DataColumn DataColumn::cpartnerutilisateur(1, "cpartnerutilisateur");
const DataColumn DataColumn::cpartnerventedirecte(2, "cpartnerventedirecte");
const DataColumn DataColumn::cpartnerenvoyercontactes(3, "cpartnerenvoyercontactes");
const DataColumn DataColumn::cpartneradressesrv(4, "cpartneradressesrv");
const DataColumn DataColumn::cpartnerportsrv(5, "cpartnerportsrv");
const DataColumn DataColumn::cpartnerdatecreation(6, "cpartnerdatecreation");

/*! address item */
const DataColumn DataColumn::address1(0, "address1");
const DataColumn DataColumn::address2(1, "address2");
const DataColumn DataColumn::city(2, "city");
const DataColumn DataColumn::zipcode(3, "zipcode");
const DataColumn DataColumn::country(4, "country");
