#ifndef JSONQSTANDARDITEMFACTORY_H
#define JSONQSTANDARDITEMFACTORY_H
#include <QtGui/QStandardItem>
#include <QJsonDocument>
#include "datarole.h"
#include "datacolumn.h"

/*
 Representación de un fichero JSON de comandos del servidor en forma de gerarquía de QStandardItem.
 */
/*!
 * \brief JsonlElemType représente le type d'entité \a JSON dans une hiérarchie contenant la requête au serveur.
 *
 *  - JsonSimple: Json string, numeric or boolean.
 */
enum JsonElemType{JsonObject, JsonArray, JsonSimple};

enum TixrProductType{TicketingProductType, ShopProductType, TravelProductType};

// Dimention of Decoration ListView Image
enum DdlSize{ddlwidth=160, ddlheight=60};

#ifdef QT_POINTER_SIZE
#if QT_POINTER_SIZE == 4
typedef qint32 tixrqptr;
#elif QT_POINTER_SIZE == 8
typedef qint64 tixrqptr;
#else
#error Invalid QT_POINTER_SIZE value stored!
#endif
#else
#error No QT_POINTER_SIZE macro defined!
#endif

inline tixrqptr tixrqptrFromVariant(const QVariant &v)
{
#ifdef QT_POINTER_SIZE
#if QT_POINTER_SIZE == 4
    return (tixrqptr)v.toUInt();
#elif QT_POINTER_SIZE == 8
    return (tixrqptr)v.toLongLong();
#else
#error Invalid QT_POINTER_SIZE value stored!
#endif
#else
#error No QT_POINTER_SIZE macro defined!
#endif
}

#define QPAIRITEMSTRINT(str, key) QPair<QString, int>(str, key)
#define QLISTITEMQPAIRSTRINT QList<QPair<QString, int> >()
#define ADDPARAMTOMODEL(modelItem, strParamName, strParamValue) {QStandardItem * paramItem = new QStandardItem(strParamValue); modelItem->child(0)->appendRow(paramItem); paramItem->setData(strParamName, DataRole::jsonProperty()); paramItem->setData(JsonSimple, DataRole::jsonType());}

class PendingRequest;
/*!
 * \brief The JsonQStandardItemFactory class translate a json obj to QStandardItem and vice-versa.
 */
class JsonQStandardItemFactory
{
    static QStandardItemModel *hModel;//headers model
    static bool isSyncMsg;//se trata de un mensaje de sincronización
    static void setItemImage(QStandardItem *stdItem);

public:
    JsonQStandardItemFactory();

    /*!
     * \brief Convert a QStandardItem hierarchy to a JSON document.
     * \param rootItem Root of the QStandardItem hierarchy.
     */
    static QByteArray modelToJson(QStandardItem *rootItem);
    static QByteArray tariffModelToJson(QStandardItem *tarItem);

    static QStandardItem *jsonToModel(QIODevice *ioDev, QString &error, PendingRequest *pReq);
    static QStandardItem *jsonToTariffModel(QIODevice *ioDev, QString &error, PendingRequest *pReq);
    static QStandardItem *jsonToPlanModel(QIODevice *ioDev, QString &error, PendingRequest *pReq);
    static QStandardItem *jsonToQJsonDocument(QIODevice *ioDev, QString &error, PendingRequest *pReq);

    static QImage genCalendarImg(QDate d/*, QSize s*/);
    static QImage genBlankImg(int width, int height);
    static QImage genZoneTarImg(QString zname, QString dispo, QString total);
    static QImage genPriceImg(qreal price);

    static QStandardItemModel *getHeaders(){QStandardItemModel *aux=hModel; hModel=0; return aux;}

    static bool getIsSyncMsg();
    static void setIsSyncMsg(bool value);

    static QStandardItem *createItem(const QString &text = QString::null);
    static QStandardItem *getTargetItem(QString &propertyName, QStandardItem *curItem);
    static QStandardItem *getTargetItem(const QString &propertyName, QStandardItem *curItem);

// for unit testing purpose
friend class JsonModelFactoryTest;
};

#endif // JSONQSTANDARDITEMFACTORY_H
