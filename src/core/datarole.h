#ifndef DATAROLE_H
#define DATAROLE_H
#include "metaqenum.h"

class DataRole: public EnumClass<DataRole>
{
private:
    explicit DataRole(int Value, QString Name): EnumClass<DataRole>(Value, Name) { }

public:
    /*! Qt roles */
    static const DataRole qtdisplayrole;
    static const DataRole qtdisplayrole2;
    static const DataRole qtdecorole;

    /*! commom roles */
    static const DataRole msg;
    static const DataRole ikey;
    static const DataRole ikey2;
    static const DataRole ikey3;
    static const DataRole scvar;
    static const DataRole jsonType;
    static const DataRole jsonProperty;
    static const DataRole targetkey;
    static const DataRole iuid;
    static const DataRole tweak;
    static const DataRole mediafile;
    //static const DataRole img;
    static const DataRole auxptr1;
    static const DataRole auxptr2;
    static const DataRole indicator;
    static const DataRole errmsg;

    /*! login, dbCP */
    static const DataRole slogin;
    static const DataRole passw;
    static const DataRole host;//database server
    static const DataRole port;//database server's port
    static const DataRole database;
    static const DataRole snom;
    static const DataRole sprenom;
    static const DataRole starttime;
    static const DataRole operatid;
    static const DataRole stoken;//session token, session's id;

    /*! getamounts */
    static const DataRole fondcaisse;
    static const DataRole totalbillet;
    static const DataRole totalcommande;
    static const DataRole totalpaym;
    static const DataRole totalespece;
    static const DataRole derniercloture;

    /*! gettotalscommandepayments */
    static const DataRole tcpiddevise;
    static const DataRole tcpidmodepay;

    /*! getcloturescaisse */
    static const DataRole ccidcloturer;
    static const DataRole ccidoperateur;
    static const DataRole ccdatecloturer;
    static const DataRole cctotalsales;
    static const DataRole cctotalpays;
    static const DataRole cctotalbills;
    static const DataRole ccdeviseinfo;

    /*! gettransactionsbydate */
    static const DataRole tacidcommande;
    static const DataRole tacidclient;
    static const DataRole tacidoperateur;
    static const DataRole tacadrlivraison;
    static const DataRole tacadrfacturation;
    static const DataRole tacdatecreation;
    static const DataRole tacdatevalidation;
    static const DataRole tactotal;

    /*! getencaissementsbydate */
    static const DataRole eacidpayment;
    static const DataRole eacidtiers;
    static const DataRole eacmodepay;
    static const DataRole eacdevise;
    static const DataRole eacdatepayment;
    static const DataRole eacdateintegration;
    static const DataRole eacmontant;

    /*! gettierscommandepayments */
    static const DataRole tcpidtiers;

    /*! getpiecescaisse_1 */
    static const DataRole pctaux;
    static const DataRole pciddevise;

    /*! gettiers */
    static const DataRole tnom;
    static const DataRole tmodifprivileges;
    static const DataRole tttiers;
    static const DataRole tcivilite;
    static const DataRole tsex;
    static const DataRole topermaker;
    static const DataRole tprioritycomment;
    static const DataRole topercomment;
    static const DataRole topercomment2;
    static const DataRole tpwd;

    /*! getcivilites */
    static const DataRole ctypecivilite;

    /*! gettypetiers */
    static const DataRole ttpgrole;

    /*! getallmodepaiem */
    static const DataRole mptypepaiem;

    /*! getallfamcatprods */
    static const DataRole fpidprod;//producteur
    static const DataRole fpidetab;//etablisement
    static const DataRole fpidgri;//tariff
    static const DataRole fpidpart;//partner

    /*! getallcatsforfamprods, getalltranspcatsforfamprods */
    static const DataRole tpcissubcat;//para los productos transporte, por el momento no los trataré, para simplificar (Yovanis, 30092014)
    static const DataRole cptcolor;

    /*! getallzonetaravecprixincat */
    static const DataRole zapquant;
    static const DataRole zapidgricol;
    static const DataRole zapdispo;

    /*! getallcatcliandprixinztar */
    static const DataRole ccprice;

    /*!  gettierscommandes */
    static const DataRole ccomment;
    static const DataRole cadressliv;
    static const DataRole cadressefac;
    static const DataRole ctotal;
    static const DataRole cnomoperat;
    static const DataRole cprenomoperat;

    /*! Shopping's cart and customer shop's history related roles */
    static const DataRole stockitemptr;
    static const DataRole catcliptr;
    static const DataRole scorderid;
    static const DataRole scorderdate;
    static const DataRole payorptr;/*! pointer to the order's payor */
    static const DataRole tiersptr;/*! pointer to the order's owner */
    static const DataRole printstatuscode;
    static const DataRole nticketsprinted;
    static const DataRole auxnticketsprinted;

    /*! addnonlabeledprodtocart */
    static const DataRole psvgid;
    static const DataRole pcatprodid;
    static const DataRole petat;
    static const DataRole pdescrip;

    /*!  getdetailscommande */
    static const DataRole dcquantite;
    static const DataRole dcprix;
    static const DataRole dccatcli;
    static const DataRole dccoord;
    static const DataRole dciddetails;
    static const DataRole dclabels;
    static const DataRole dcborrowed;/*! total dû pour ce produit */
    static const DataRole dcproducttype;

    /*! gettierspayments */
    static const DataRole pdtbankid;
    static const DataRole pdtnumcheque;
    static const DataRole pdtdateinteg;
    static const DataRole pdtdateannul;
    static const DataRole pdtcanceled;
    static const DataRole pdtexternal;/*! dans le cas des remboursement */

    /*! Stock management */
    static const DataRole site2dviewptr;

    /*! getcommandepayments */
    static const DataRole cpidmodepay;
    static const DataRole cplibelle;
    static const DataRole cpmontant;
    static const DataRole cpiddevise;
    static const DataRole cpdatepaym;
    static const DataRole cpdateinteg;

    /*!  getcommandeproduits */
    static const DataRole cpdateseanse;
    static const DataRole cpidzonetar;
    static const DataRole cpzonetar;
    static const DataRole cpcatcli;
    static const DataRole cpquantite;
    static const DataRole cpprix;
    static const DataRole cpcoord;

    /*!  gettierscontacts */
    static const DataRole gtclibelle;
    static const DataRole gtcvaleur;
    static const DataRole gtcpriorite;
    static const DataRole gtctypevaleur;

    /*!  gettierscoordonnees */
    static const DataRole dridville;
    static const DataRole dradresse1;
    static const DataRole dradresse2;
    static const DataRole drcodepostal;
    static const DataRole drusagehabituel;
    static const DataRole drdecoration;
    static const DataRole dridpays;
    static const DataRole dridtiers;

    /*! Skins management */
    static const DataRole skinapercu;
    static const DataRole skinfname;

    /*!  Admin. de grilles de tarifs */
    static const DataRole griadmincmd;

    /*!  Gestion des plans de salles */
    static const DataRole placesinset;
    static const DataRole ztarnom;
    static const DataRole nomsitenumbitem;
    static const DataRole seriesitenumbitem;
    static const DataRole levelsitenumbitem;

    /*!  getpartners */
    static const DataRole partnernom;
    static const DataRole partnerutilisateur;
    static const DataRole partnermdp;
    static const DataRole partneridsession;
    static const DataRole partnerventedirecte;
    static const DataRole partnerenvoyercontactes;
    static const DataRole partneradressesrv;
    static const DataRole partnerportsrv;
    static const DataRole partnerdatecreation;

    /*!  getallbanques */
    static const DataRole gabidp;

    /* Headers metadata */
    static const DataRole dataTypeID;

    /* getalldevices */
    static const DataRole newseanceprinterid;

    /* getalluserprofiles */
    static const DataRole userprofileid;
};

#endif // DATAROLE_H
