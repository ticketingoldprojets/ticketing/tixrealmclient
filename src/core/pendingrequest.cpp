#include "pendingrequest.h"

PendingRequest::PendingRequest(HTTP_METHOD_ID reqType) : requestItem(0),
    hModel(0), type(reqType),
    responseItemParent(0),
    isResponseExpected(true),
    isJsonSingleValueFunc(false),
    senderSlot2(0)
{
}

PendingRequest::~PendingRequest()
{
    if(requestItem)
    {
        if(requestItem->model())
        {
            QStandardItem *p;
            if(p = requestItem->parent())
                p->removeRow(requestItem->row());
            else
                requestItem->model()->invisibleRootItem()->removeRow(requestItem->row());
        }
        else
            delete requestItem;
    }
}

QString PendingRequest::getRespDataMapKey() const
{
    return respDataMapKey;
}

void PendingRequest::setRespDataMapKey(const QString &value)
{
    respDataMapKey = value;
}

void PendingRequest::setData(QByteArray data)
{
    postedData = data;
}

QStandardItemModel *PendingRequest::headersModel()
{
    return hModel;
}

HTTP_METHOD_ID PendingRequest::getType() const
{
    return type;
}

QStandardItem *PendingRequest::getRequestItem()
{
    return requestItem;
}

void PendingRequest::setRequestItem(QStandardItem *reqItm)
{
    requestItem = reqItm;
}

QStandardItem *PendingRequest::getResponseItem()
{
    return responseItem;
}

QStandardItem *PendingRequest::getResponseItemParent()
{
    return responseItemParent;
}

void PendingRequest::setResponseItem(QStandardItem *respItm)
{
    responseItem = respItm;
}

void PendingRequest::setResponseItemParent(QStandardItem *parent)
{
    responseItemParent = parent;
}

QString PendingRequest::getPgFunction() const
{
    return pgFunction;
}
