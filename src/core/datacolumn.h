#ifndef DATACOLUMN_H
#define DATACOLUMN_H
#include "metaqenum.h"

class DataColumn: public EnumClass<DataColumn>
{
private:
    explicit DataColumn(int Value, QString Name): EnumClass<DataColumn>(Value, Name) { }

public:
    /*! Commons columns */
    static const DataColumn imgcol;

    /*! gettotalscommandepayments */
    static const DataColumn tcpmodepaycol;
    static const DataColumn tcpmontantcol;
    static const DataColumn tcpdevisecol;

    /*! getcloturescaisse */
    static const DataColumn ccdatecloturercol;

    /*! gettransactionsbydate */
    static const DataColumn tacmontantcol;
    static const DataColumn tacsoldecol;

    /*! getencaissementsbydate */
    static const DataColumn eacmodepaycol;
    static const DataColumn eacmontantcol;

    /*! gettierscommandepayments */
    static const DataColumn tcptotalcol;
    static const DataColumn tcpequilibrercol;

    /*! getpiecescaisse_1 */
    static const DataColumn pcnombrecol;
    static const DataColumn pcmontant;

    /*! getpaymentbymode */
    static const DataColumn pbmnombrecol;
    static const DataColumn pbmmontantcol;
    static const DataColumn pbmnombre2col;
    static const DataColumn pbmmontant2col;

    /*! gettiers */
    static const DataColumn tnomcol;
    static const DataColumn tdatenaissancecol;
    static const DataColumn tcommentairecol;
    static const DataColumn tidentifiantcol;
    static const DataColumn tttierscol;
    static const DataColumn tcivilitecol;
    static const DataColumn taccountcol;
    static const DataColumn tinfoentreprisecol;
    static const DataColumn tdatecreationcol;
    static const DataColumn tsex;
    static const DataColumn topermakercol;
    static const DataColumn tprenomcol;
    static const DataColumn tpwd;

    /*! getoperators */
    static const DataColumn tonomcol;
    static const DataColumn todatenaissancecol;
    static const DataColumn tocommentairecol;
    static const DataColumn toidentifiantcol;
    static const DataColumn tottierscol;
    static const DataColumn tocivilitecol;
    static const DataColumn todatecreationcol;
    static const DataColumn tosexcol;
    static const DataColumn toopermakercol;

    /*! getallmodepaiem */
    static const DataColumn mpouvtiroircol;
    static const DataColumn mpanonymcol;

    /*! getalldevises */
    static const DataColumn dabrcol;
    static const DataColumn dtauxcol;

    /*! getallbanques */
    static const DataColumn bpayscol;

    /*! getallfamcatprods */
    static const DataColumn fptitlecol;
    static const DataColumn fpthemecol;
    static const DataColumn fpdescripcol;
    static const DataColumn fpdatecol;

    /*! getallcatsforfamprods, getalltranspcatsforfamprods */
    static const DataColumn cptitre;
    static const DataColumn cpdat;
    static const DataColumn cpddvente;
    static const DataColumn cpdfvente;
    static const DataColumn cpdescript;
    static const DataColumn cpaddress;
    static const DataColumn cpiva;
    static const DataColumn cpivalcosto;
    static const DataColumn cpcosto;
    static const DataColumn cpplano;
    //static const DataColumn tpcsubcat; //para los productos transporte, por el momento no los trataré, para simplificar (Yovanis, 30092014)
    static const DataColumn cpnoautofill;
    static const DataColumn cpnoseasonticket;
    static const DataColumn cpprinterid;
    static const DataColumn cpticketmodelid;
    static const DataColumn cpuserprof;
    static const DataColumn cpztarmap;
    static const DataColumn cpgritar;
    static const DataColumn cpisaflow;
    static const DataColumn cpsizes;

    /*! getcommandepayments */
    static const DataColumn cpmodepaycol;
    static const DataColumn cpmontantcol;
    static const DataColumn cpdevisecol;
    static const DataColumn cpdatepaymcol;

    /*!  getports */
    static const DataColumn gpportnamecol;

    /*!  Tiers Child Model Index */
    static const DataColumn tcicontact;
    static const DataColumn tciaddress;
    static const DataColumn tcihistoric;
    static const DataColumn tcipayments;

    /*!  getdetailscommande */
    static const DataColumn dcpayedcol;
    static const DataColumn dcticketscol;

    /*!  gettierspayments */
    static const DataColumn pdtdevcol;
    static const DataColumn pdtamountcol;
    static const DataColumn pdtmodpaycol;
    static const DataColumn pdtpayercol;

    /*!  getpartners */
    static const DataColumn cpartnerutilisateur;
    static const DataColumn cpartnerventedirecte;
    static const DataColumn cpartnerenvoyercontactes;
    static const DataColumn cpartneradressesrv;
    static const DataColumn cpartnerportsrv;
    static const DataColumn cpartnerdatecreation;

    /*! address item */
    static const DataColumn address1;
    static const DataColumn address2;
    static const DataColumn city;
    static const DataColumn zipcode;
    static const DataColumn country;
};

#endif // DATACOLUMN_H
