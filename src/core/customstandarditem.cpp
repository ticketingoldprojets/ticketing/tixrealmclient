#include "customstandarditem.h"

CustomStandardItem::CustomStandardItem(const QString &text) : QStandardItem(text)
{

}

void CustomStandardItem::setData(const QVariant &value, int role)
{
    if(value.isValid())
    {
        if(!customRolesList.contains(role))
            customRolesList.append(role);
    }
    else
        customRolesList.removeOne(role);

    QStandardItem::setData(value, role);
}
