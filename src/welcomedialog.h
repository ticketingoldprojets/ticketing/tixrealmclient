#ifndef WELCOMEDIALOG_H
#define WELCOMEDIALOG_H

#include "core/tixrcore.h"
#if (QT_VERSION < QT_VERSION_CHECK(5, 0, 0))
#include <QtGui>
#else
#include <QtWidgets/QtWidgets>
#endif

/*! \class WelcomeDialog welcomedialog.h
 *  \brief Fenêtre d'authentification auprès du serveur TixRealmserver.
 *
 * WelcomeDialog permet d'introduire les paramètres de connexion au serveur et d'envoyer la requête d'ouverture de session.
 * En cas de réussite on stocke dans QStandardItemModel les paramètres de connexion au serveur et l'identifiant de la session.
 */
class WelcomeDialog : public QDialog
{
    Q_OBJECT

public:
    WelcomeDialog(TixRealmCore *mainObj);
    ~WelcomeDialog();

private slots:
    void enableLoginButton(QString newText);
    void enableProxyInput(int state);
    void on_pbEntrer_clicked();
    void on_pbSortir_clicked();

    /*!
     * \brief readResponse traite la reponse à une requête d'ouverture de session.
     * \param respError est le message d'erreur.
     * \param requestItem hiérarchie QStandardItem représentant la requête.
     * \param responseItem hiérarchie QStandardItem représentant la reponse.
     * \param curReq est le fils d'exécution dans lequel on a traité la requête.
     */
    void readResponse(QString respError, PendingRequest *curReq);

private:
    QCheckBox *checkBox;
    QLineEdit *proxyLineEdit;
    QSpinBox *proxyPortSpinBox;
    QLineEdit *proxyUsrLineEdit;
    QLineEdit *proxyMdpLineEdit;
    QLabel *label_5;
    QLabel *label_6;
    QLabel *label_7;

    QWidget *welUI;

    QLineEdit *hostLineEdit;
    QSpinBox *portSpinBox;

    QLineEdit *opLineEdit;
    QLineEdit *mdpLineEdit;

    QPushButton *pbEntrer;
    QPushButton *pbSortir;

    TixRealmCore *tixrCore;
};

#endif // WELCOMEDIALOG_H

