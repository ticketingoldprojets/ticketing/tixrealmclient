#include "newpaymentdialog.h"
#include "mainwindow.h"

TixrNewPaymentDialog::TixrNewPaymentDialog(TixRealmCore *mo, QWidget *blockedWidget, QWidget *parent) :
    TixrDialog(":/formconfig/newPayment.ui", blockedWidget, parent)
{
    tixrCore = mo;

    fvModePaiem = (PictureFlowView*)findChild<QAbstractItemView*>("fvModePaiem");
    cbDevises = findChild<QComboBox*>("cbDevises");
    dsbAmount = findChild<QDoubleSpinBox*>("dsbAmount");
    dsbRestantDu = findChild<QDoubleSpinBox*>("dsbRestantDu");
    dsbRestantDuStyleSheet = dsbRestantDu->styleSheet();
    dsbRestantDu->setStyleSheet(dsbRestantDuStyleSheet + "color:black;");
    paymTView = findChild<QTableView*>("paymTView");
    cbPayeur = findChild<QComboBox*>("cbPayeur");
    pbRecordPay = findChild<QPushButton*>("pbRecordPay");
    pbErasePay = findChild<QPushButton*>("pbErasePay");
    pbErasePayStyleSheet = pbErasePay->styleSheet();
    pbErasePay->setStyleSheet(pbErasePayStyleSheet + "QPushButton{color:black;}");
    tbReset = findChild<QToolButton*>("tbReset");

    QStandardItemModel *paymTViewHModel = new QStandardItemModel;
    paymTViewHModel->setHorizontalHeaderLabels(QStringList()<<tr("Date")<<tr("Currency")<<tr("Amount")<<tr("Type")<<tr("Payor")<<tr("Comment"));
    paymTView->horizontalHeader()->setModel(paymTViewHModel);
    connect(paymTView->selectionModel(), SIGNAL(selectionChanged(QItemSelection,QItemSelection)), this, SLOT(paymTViewSelectionChanged(QItemSelection,QItemSelection)));

    tiersFinderDialog = new TixrTiersFinderDialog(this, parent);
    errorStr = tiersFinderDialog->getErrorMsg();
    if(!errorStr.isEmpty())
        return;

    connect(tiersFinderDialog, SIGNAL(accepted()), this, SLOT(setPayor()));

    checkDetailsDialog = new TixrCheckDialog(this, parent);
    errorStr = checkDetailsDialog->getErrorMsg();
    if(!errorStr.isEmpty())
        return;

    checkDetailsDialog->cbBank->setModel(tixrCore->stdModel);
    QStandardItem *banksRoot = tixrCore->keyToItemMap.value("banques");
    checkDetailsDialog->cbBank->setRootModelIndex(banksRoot->index());
    if(banksRoot->hasChildren())
        checkDetailsDialog->cbBank->setCurrentIndex(0);

    connect(checkDetailsDialog, SIGNAL(accepted()), this, SLOT(checkDialogAccepted()));

    cbDevises->setModel(tixrCore->stdModel);
    QStandardItem *devisesRoot = tixrCore->keyToItemMap.value("devises");
    cbDevises->setRootModelIndex(devisesRoot->index());
    if(devisesRoot->hasChildren())
        cbDevises->setCurrentIndex(0);

    MainWindow::tixrConnectSlotsByName(this);
}

void TixrNewPaymentDialog::recordPay(bool dette, QStandardItem *modeItem)
{
    QStandardItem *paymRoot = tixrCore->stdModel->itemFromIndex(paymTView->rootIndex());

    QStandardItem *dateItem = new QStandardItem("<en cours>");
    if(dette)
        dateItem->setData(QColor(Qt::red), Qt::DecorationRole);

    QStandardItem *deviseItem = new QStandardItem;
    if(dette)
        deviseItem->setData("-1", DataRole::ikey());
    else
    {
        QString idDev = cbDevises->itemData(cbDevises->currentIndex(), DataRole::ikey()).toString();
        deviseItem->setData(idDev, DataRole::ikey());
        deviseItem->setText(cbDevises->currentText());
    }

    QStandardItem *montantItem = new QStandardItem(tixrCore->curLocal.toString(dsbAmount->value()));

    QVariant pIdV = cbPayeur->itemData(cbPayeur->currentIndex(), DataRole::ikey());
    if(!pIdV.isValid())
        pIdV = QVariant(-1);
    QStandardItem *payeurItem = new QStandardItem(cbPayeur->currentText());
    payeurItem->setData(pIdV, DataRole::ikey());

    paymRoot->appendRow(QList<QStandardItem*>()<<dateItem<<deviseItem<<montantItem<<modeItem<<payeurItem);

    qreal resto = dsbRestantDu->value() - dsbAmount->value();

    dsbRestantDu->setStyleSheet(dsbRestantDuStyleSheet + (resto>0?"color:red;":(resto<0?"color:green;":"color:black;")));
    dsbRestantDu->setValue(resto);

    dsbAmount->setMaximum(resto);
    dsbAmount->setValue(resto);

    pbRecordPay->setEnabled(resto>0);

    cbPayeur->clear();
    cbPayeur->setEditable(true);
    tbReset->setEnabled(false);
}

void TixrNewPaymentDialog::checkDialogAccepted()
{
    if(checkDetailsDialog->modeItem)
        recordPay(false, checkDetailsDialog->checkPayItem);
}

void TixrNewPaymentDialog::on_pbRecordPay_clicked()
{
    if(!dsbAmount->value())
    {
        return;
    }

    QStandardItem *mpItm = tixrCore->stdModel->itemFromIndex(fvModePaiem->rootIndex().child(fvModePaiem->currentSlide(), 0));
    if(mpItm->data(DataRole::mptypepaiem()).toString() == "Check")
    {
        if(cbPayeur->currentText().isEmpty())
            QMessageBox::information(this, qApp->applicationName(), tr("You should choice a valid payor for this payment type."));
        else
        {
            checkDetailsDialog->move(rect().center());
            checkDetailsDialog->modeItem = mpItm;
            checkDetailsDialog->checkPayItem = 0;
            checkDetailsDialog->show();
        }
        return;
    }

    QStandardItem *modeItem = new QStandardItem;
    modeItem->setText(mpItm->text());
    modeItem->setData(mpItm->data(DataRole::ikey()), DataRole::ikey());
    modeItem->setData(mpItm->data(DataRole::mptypepaiem()), DataRole::mptypepaiem());
    modeItem->setData(QVariant(-1), DataRole::pdtbankid());//bank id.

    recordPay(false, modeItem);
}

void TixrNewPaymentDialog::on_pbErasePay_clicked()
{
    QModelIndexList indexList = paymTView->selectionModel()->selectedRows();
    if(indexList.isEmpty())
    {
        return;
    }

    QModelIndex idx = indexList.first();

    QStandardItem *amountItem = tixrCore->stdModel->itemFromIndex(idx.sibling(idx.row(), 2));

    qreal amount = amountItem->text().toDouble();
    tixrCore->stdModel->removeRow(idx.row(), idx.parent());

    dsbRestantDu->setValue(dsbRestantDu->value() + amount);
    qreal v = dsbRestantDu->value();
    dsbAmount->setMaximum(v);
    dsbAmount->setValue(v);

    pbRecordPay->setEnabled(v>0);
    pbAjourner->setEnabled(v>0);

    QStandardItem *payRoot = tixrCore->stdModel->itemFromIndex(paymTView->rootIndex());
    pbErasePay->setEnabled(payRoot->rowCount() == 1);
}

void TixrNewPaymentDialog::on_tbFind_clicked()
{
    tiersFinderDialog->move(rect().center());

    QStandardItem *modelItem = tixrCore->keyToItemMap.value("crm");
    Q_ASSERT(modelItem);

    tiersFinderDialog->setTiersRoot(modelItem);
    tiersFinderDialog->show();
}

void TixrNewPaymentDialog::setPayor()
{
    QStandardItem *selTiers = tiersFinderDialog->selectedTiers();
    if(selTiers)
    {
        cbPayeur->clear();
        cbPayeur->addItem(selTiers->text() + " " + selTiers->parent()->child(selTiers->row(), 1)->text().toUpper());
        cbPayeur->setEditable(false);
        cbPayeur->setCurrentIndex(0);
        cbPayeur->setItemData(0, selTiers->data(DataRole::ikey()), DataRole::ikey());

        cbPayeur->setItemData(0, QVariant(reinterpret_cast<tixrqptr>(selTiers)), DataRole::payorptr());

        tbReset->setEnabled(true);
    }
}

void TixrNewPaymentDialog::on_tbReset_clicked()
{
    cbPayeur->clear();
    cbPayeur->setEditable(true);
    tbReset->setEnabled(false);
}

void TixrNewPaymentDialog::on_paymTView_doubleClicked(const QModelIndex & index)
{
    QStandardItem *modeItem = tixrCore->stdModel->itemFromIndex(index.sibling(index.row(), 3));
    if(modeItem->data(DataRole::mptypepaiem()).toString() == "Check")
    {
        checkDetailsDialog->move(rect().center());
        checkDetailsDialog->modeItem = 0;
        checkDetailsDialog->checkPayItem = modeItem;
        checkDetailsDialog->show();
        return;
    }
}

void TixrNewPaymentDialog::paymTViewSelectionChanged(const QItemSelection &, const QItemSelection &)
{
    QModelIndexList indexList = paymTView->selectionModel()->selectedRows();
    QStandardItem *payItem = 0;

    if(!indexList.isEmpty())
        payItem = tixrCore->stdModel->itemFromIndex(indexList.first());

    pbErasePay->setEnabled(payItem && !payItem->data(DataRole::pdtcanceled()).isValid());
    pbErasePay->setStyleSheet(pbErasePayStyleSheet + (payItem?(payItem->data(DataRole::ikey()).isValid()?"QPushButton{color:red;}":"QPushButton{color:black;}"):"QPushButton{color:black;}"));
}
