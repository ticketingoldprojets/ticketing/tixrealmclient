#ifndef SCREMBOURSEMENTDIALOG_H
#define SCREMBOURSEMENTDIALOG_H
#include "dialog.h"
#include "../TixRealmClientPlugins/PictureFlowViewPlugin/pictureflow.h"

class TixrRemboursementDialog : public TixrDialog
{
    Q_OBJECT
public:
    explicit TixrRemboursementDialog(TixRealmCore *mo, QWidget *blockedWidget, QStandardItem *cItm, QWidget *parent = 0);
    void setAmount();

private:
    PictureFlowView *fvModePaiem;
    QDoubleSpinBox *dsbAmount;
    QLineEdit *leComent;
    QRadioButton *rbInternal;
    QStandardItem *cliItem;

private slots:
    void on_pbOk_clicked();
};

#endif // SCREMBOURSEMENTDIALOG_H
