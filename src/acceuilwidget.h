//-----------------------------------------------------------------------------
#ifndef SCACCEUILWIDGET_H
#define SCACCEUILWIDGET_H
//-----------------------------------------------------------------------------
#include "widget.h"
//-----------------------------------------------------------------------------
class TixrAcceuilWidget : public TixrWidget
{
    Q_OBJECT

public:
    explicit TixrAcceuilWidget(TixRealmCore *mObj, QWidget *parent = 0);

protected:
    QStandardItem * currOper;
    QDoubleSpinBox * dsbTotalEspece;
    QDoubleSpinBox * dsbFondCaisse;
    QDateTimeEdit * deDernierCloture;
    QDateTimeEdit * deArretCloture;
    QPushButton * pbEquilibrage;
    QTableView * tvPayments;
    QTableView * tvClotures;
    QTableView * tvTiersTrans;
    QTabWidget * twPieceDevise;
    QTableView * tvTransactions;
    QTableView * tvEncaissements;
    QPushButton * pbApercu;
    QPushButton * pbCloturer;

private:
#ifndef QT_NO_FILESYSTEMWATCHER
    QFileSystemWatcher *dirWatcher;
#endif
    QLabel *lblApercu;
    QGroupBox *gbThemes;
    QListWidget *lwSkins;
    QPushButton *pbRestorer;

    void timerEvent(QTimerEvent * event);
    QLabel *lbConnTime;//Yovanis
    QString operName;//Yovanis
    QDateTime startDT;//fecha y hora de inicio de la conexión

    QLabel *lbOpName;//Yovanis

private slots:
    void on_tvTransactions_doubleClicked(const QModelIndex & index);
    void on_tvEncaissements_doubleClicked(const QModelIndex & index);
    void on_tvTiersTrans_doubleClicked(const QModelIndex & index);
    void on_pbApercu_clicked(bool checked = false);
    void on_pbCloturer_clicked(bool checked = false);

    QStandardItem * fillNewCloture(QStandardItem * reqItem, QStandardItem * respItem);
    void reloadSkins(const QString & path);
    void on_lwSkins_itemClicked(QListWidgetItem *item);
    void on_pbAppliquer_clicked();
    void on_pbRestorer_clicked();

    void responseGetAmounts(QString respError, PendingRequest *curReq);
    void responseGetCivilites(QString respError, PendingRequest *curReq);
    void responseGetTypeTiers(QString respError, PendingRequest *curReq);
    void responseGetPays(QString respError, PendingRequest *curReq);
    void responseGetEncaissementsByDate(QString respError, PendingRequest *curReq);
    void responseGetTiersCommandePayments(QString respError, PendingRequest *curReq);
    void responseGetCloturesCaisse(QString respError, PendingRequest *curReq);
    void responseGetTransactionsByDate(QString respError, PendingRequest *curReq);
    void responseGetTotalsCommandePayments(QString respError, PendingRequest *curReq);
    void responseGetTiers(QString respError, PendingRequest *curReq);
    void responseInsClotureCaisse(QString respError, PendingRequest *curReq);
    void responseGetPiecesCaisse_1(QString respError, PendingRequest *curReq);
    void responseGetPaymentByMode(QString respError, PendingRequest *curReq);

    // YOVANIS
    void respDbCP(QString respError, QStandardItem *, QStandardItem *responseItem, PendingRequest *curReq);
};
//-----------------------------------------------------------------------------
#endif // SCACCEUILWIDGET_H
//-----------------------------------------------------------------------------
