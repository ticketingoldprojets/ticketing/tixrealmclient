#include "cartwidget.h"
#include "mainwindow.h"
#include <QtUiTools/QUiLoader>
#include "crm/newtierswidget.h"
#include "stock/imggeneratoritemdelegate.h"

TixrCartWidget::TixrCartWidget(TixRealmCore *mObj, QWidget *parent) :
    QDockWidget(parent), tixrCore(mObj), prevCartIndex(-1)
{
    QUiLoader loader;
    QFile f(":/formconfig/cartWidget.ui");
    bool r = f.open(QFile::ReadOnly);
    Q_ASSERT_X(r, "TixrCartWidget::TixrCartWidget", "Cannot open :/formconfig/cartWidget.ui");
    QWidget *widget =  loader.load(&f, this);
    f.close();

    if(!widget)
    {
        errorStr = "Cannot load the graphical user interface.";
        return;
    }

    setWidget(widget);

    widget->setObjectName("dynWidget");
    cartsSW = widget->findChild<QStackedWidget*>("cartsSW");

    setFeatures(QDockWidget::DockWidgetMovable | QDockWidget::DockWidgetClosable);
    setAllowedAreas(Qt::LeftDockWidgetArea | Qt::RightDockWidgetArea);

    waitLabel = widget->findChild<QLabel*>("waitLabel");
    waitMovie = new QMovie(":/ajax-loader.gif");
    connect(waitMovie, SIGNAL(updated(QRect)), this, SLOT(animateIcon(QRect)));

    f.setFileName(":/formconfig/cartWidgetContent.ui");
    f.open(QFile::ReadOnly);
    widget =  loader.load(&f);
    f.close();

    cartsSW->addWidget(widget);

    cartHeaderModel = new QStandardItemModel(this);
    cartHeaderModel->setHorizontalHeaderLabels(QStringList()<<tr("Items")<<tr("Payment status")<<tr("Ticket status"));

    cartView = widget->findChild<QTreeView*>("cartView");
    totalDSB = widget->findChild<QDoubleSpinBox*>("totalDSB");
    sousTotalDSB = widget->findChild<QDoubleSpinBox*>("sousTotalDSB");
    fvModePaiem = (PictureFlowView*)findChild<QAbstractItemView*>("fvModePaiem");
    cbDevises = widget->findChild<QComboBox*>("cbDevises");
    dsbAmount = widget->findChild<QDoubleSpinBox*>("dsbAmount");
    dsbRestantDu = widget->findChild<QDoubleSpinBox*>("dsbRestantDu");
    dsbRestantDuStyleSheet = dsbRestantDu->styleSheet();
    dsbRestantDu->setStyleSheet(dsbRestantDuStyleSheet + "color:black;");
    paymTView = widget->findChild<QTableView*>("paymTView");
    cbPayeur = widget->findChild<QComboBox*>("cbPayeur");
    pbRecordPay = widget->findChild<QPushButton*>("pbRecordPay");
    pbAjourner = widget->findChild<QPushButton*>("pbAjourner");
    connect(pbAjourner, SIGNAL(clicked()), this, SLOT(on_pbRecordPay_clicked()), Qt::UniqueConnection);
    pbPayer = widget->findChild<QPushButton*>("pbPayer");
    pbErasePay = widget->findChild<QPushButton*>("pbErasePay");
    pbErasePayStyleSheet = pbErasePay->styleSheet();
    pbErasePay->setStyleSheet(pbErasePayStyleSheet + "QPushButton{color:black;}");
    tbReset = widget->findChild<QToolButton*>("tbReset");
    gbBalance = widget->findChild<QGroupBox*>("gbBalance");
    dsbCurBalance = gbBalance->findChild<QDoubleSpinBox*>("dsbCurBalance");
    dsbFinBalance = gbBalance->findChild<QDoubleSpinBox*>("dsbFinBalance");

    cartsItem = new QStandardItem;
    tixrCore->stdModel->appendRow(cartsItem);
    tixrCore->keyToItemMap.insert("carts", cartsItem);//raiz de los carros de compras

    //crear el carro de compras por defecto
    QStandardItem *currentCartItem = new QStandardItem;
    cartsItem->appendRow(currentCartItem);
    currentCartItem->setText(tr("Unknown Client"));

    curTasksList << QList<PendingRequest*>();

    //El carro de compras tiene un primer nodo para los productos y un segundo para los pagos
    QStandardItem *stdItem = new QStandardItem;
    currentCartItem->appendRow(stdItem);

    cartView->setModel(tixrCore->stdModel);
    cartView->setRootIndex(stdItem->index());
    connect(cartView->selectionModel(), SIGNAL(selectionChanged(QItemSelection,QItemSelection)), this, SLOT(cartViewSelectionChanged(QItemSelection,QItemSelection)));
    cartView->header()->setModel(cartHeaderModel);

    stdItem = new QStandardItem;
    currentCartItem->appendRow(stdItem);
    paymTView->setModel(tixrCore->stdModel);
    paymTView->setRootIndex(stdItem->index());
    paymTViewHModel = new QStandardItemModel;
    paymTViewHModel->setHorizontalHeaderLabels(QStringList()<<tr("Date")<<tr("Currency")<<tr("Amount")<<tr("Type")<<tr("Payor")<<tr("Comment"));
    paymTView->horizontalHeader()->setModel(paymTViewHModel);
    connect(paymTView->selectionModel(), SIGNAL(selectionChanged(QItemSelection,QItemSelection)), this, SLOT(paymTViewSelectionChanged(QItemSelection,QItemSelection)));

    cartsFView = (PictureFlowView*)findChild<QAbstractItemView*>("cartsFView");
    cartsFView->setModel(tixrCore->stdModel);
    cartsFView->setRootIndex(cartsItem->index());

    connect(cartsFView, SIGNAL(clicked(QModelIndex)), this, SLOT(changeCurrentCart(QModelIndex)));
    connect(cartsFView, SIGNAL(newDropEvent(QDropEvent*)), this, SLOT(handleCartDropEvent(QDropEvent*)));

    setObjectName("cartWidget");

    //cargar las formas de pago existentes para este operador
    tixrCore->setPgFunction("main.getallmodepaiem");
    PendingRequest *pReq = tixrCore->sendMsg(tixrCore->stdModel->invisibleRootItem(), HTTP_METHOD_ID::GET, "/api", this, "responseGetModePaiem");
    pReq->setRespDataMapKey("modpaym");
    curTasksList[cartsSW->currentIndex()].append(pReq);

    //cargar las divisas existentes
    tixrCore->setPgFunction("main.getalldevises");
    pReq = tixrCore->sendMsg(tixrCore->stdModel->invisibleRootItem(), HTTP_METHOD_ID::GET, "/api", this, "responseGetDevises");
    pReq->setRespDataMapKey("devises");
    curTasksList[cartsSW->currentIndex()].append(pReq);

    printMgr = new PrintManager(tixrCore);
    pbPrint = findChild<QPushButton*>("pbPrint");
    updatePrintButton();

    tiersFinderDialog = new TixrTiersFinderDialog(this, parent);
    errorStr = tiersFinderDialog->getErrorMsg();
    if(!errorStr.isEmpty())
        return;

    connect(tiersFinderDialog, SIGNAL(accepted()), this, SLOT(setPayor()));

    checkDetailsDialog = new TixrCheckDialog(this, parent);
    errorStr = checkDetailsDialog->getErrorMsg();
    if(!errorStr.isEmpty())
        return;

    connect(checkDetailsDialog, SIGNAL(accepted()), this, SLOT(checkDialogAccepted()));

    //cargar los bancos
    tixrCore->setPgFunction("main.getallbanques");
    pReq = tixrCore->sendMsg(tixrCore->stdModel->invisibleRootItem(), HTTP_METHOD_ID::GET, "/api", this, "responseGetBanques");
    pReq->setRespDataMapKey("banques");
    curTasksList[cartsSW->currentIndex()].append(pReq);

    waitMovie->start();
    MainWindow::tixrConnectSlotsByName(this);

    //gestión de la edición de transacciones desde la interfaz del cliente
    connect(this, SIGNAL(cartRequestResult(bool, QStandardItem*)), tixrCore, SIGNAL(cartRequestResult(bool, QStandardItem*)));
    connect(tixrCore, SIGNAL(findCart(int, QStandardItem*)), this, SLOT(handleFindCart(int, QStandardItem*)));
    connect(tixrCore, SIGNAL(addCart(QStandardItem*)), this, SLOT(handleAddCart(QStandardItem*)));
}

TixrCartWidget::~TixrCartWidget()
{
    delete waitMovie;
    delete printMgr;
    delete paymTViewHModel;
    delete cartHeaderModel;
}

void TixrCartWidget::on_tbFind_clicked()
{
    tiersFinderDialog->move(rect().center());

    QStandardItem *modelItem = tixrCore->keyToItemMap.value("crm");
    if(modelItem)
    {
        tiersFinderDialog->setTiersRoot(modelItem);
        tiersFinderDialog->show();
    }
    else
    {
        //cargar los clientes
        tixrCore->setPgFunction("main.gettiers");
        PendingRequest *pReq = tixrCore->sendMsg(tixrCore->stdModel->invisibleRootItem(), HTTP_METHOD_ID::GET, "/api", this, "responseGetTiers");
        pReq->setRespDataMapKey("crm");
        curTasksList[cartsSW->currentIndex()].append(pReq);
    }
}

void TixrCartWidget::setPayor()
{
    QStandardItem *selTiers = tiersFinderDialog->selectedTiers();
    if(selTiers)
    {
        cbPayeur->clear();
        cbPayeur->addItem(selTiers->text() + " " + selTiers->parent()->child(selTiers->row(), 1)->text().toUpper());
        cbPayeur->setEditable(false);
        cbPayeur->setCurrentIndex(0);
        cbPayeur->setItemData(0, selTiers->data(DataRole::ikey()), DataRole::ikey());

        cbPayeur->setItemData(0, QVariant(reinterpret_cast<tixrqptr>(selTiers)), DataRole::payorptr());

        tbReset->setEnabled(true);
    }
}

void TixrCartWidget::on_tbReset_clicked()
{
    cbPayeur->clear();
    cbPayeur->setEditable(true);
    tbReset->setEnabled(false);
}

void TixrCartWidget::showEvent(QShowEvent *event)
{
    if(checkDetailsDialog->wasVisible())
        checkDetailsDialog->show();

    if(tiersFinderDialog->wasVisible())
        tiersFinderDialog->show();

    event->accept();
}

void TixrCartWidget::hideEvent(QHideEvent *event)
{
    checkDetailsDialog->hide();
    tiersFinderDialog->hide();
    event->accept();
}

void TixrCartWidget::setAlwaysOnTopDialogVisibility(bool visible)
{
    checkDetailsDialog->setVisible(visible && checkDetailsDialog->wasVisible() && isVisible());
    tiersFinderDialog->setVisible(visible && tiersFinderDialog->wasVisible() && isVisible());
}

void TixrCartWidget::updatePrintButton()
{
    bool enable = !getPrintableProducts().isEmpty();
    pbPrint->setEnabled(enable);

    ///HS TEMP SO I CAN TEST THE PRINTER CODE
    //pbPrint->setEnabled(true);

    // Show active print job widget (if current cart has changed or it's the initial default one)
    int currCartIndex = cartsSW->currentIndex();
    if (currCartIndex != prevCartIndex) {
        prevCartIndex = currCartIndex;

        PrintJob *currCartPrintJob = cartsPrintJobs.contains(currCartIndex) ? cartsPrintJobs.value(currCartIndex) : 0;
        printMgr->showPrintJob(currCartPrintJob);

        // Set print button style (active state) for current cart
        if (currCartPrintJob) {
            //HS TEMP! CHANGE BUTTON STYLE
            pbPrint->setStyleSheet("QPushButton#pbPrint { background-color: #0c0; border: 3px solid #f00; background-position: center; background-repeat: none; background-image: url(:images/btn/Print-icon.png); } ");
        }
        else {
            //HS TEMP! CHANGE BUTTON STYLE
            pbPrint->setStyleSheet("QPushButton#pbPrint { background-color: transparent; border: none; background-position: center; background-repeat: none; background-image: url(:images/btn/Print-icon.png); } ");
        }
    }
}

void TixrCartWidget::animateIcon(const QRect & rect)
{
    Q_UNUSED(rect)

    if(curTasksList.at(cartsSW->currentIndex()).isEmpty())
    {
        waitLabel->hide();
        return;
    }

    waitLabel->setPixmap(waitMovie->currentPixmap());
    waitLabel->show();
}

bool TixrCartWidget::beginHandleResponse(QString respError, PendingRequest *curReq)
{
    for(int i=0, n=curTasksList.size(); i<n; i++)
        if(curTasksList.at(i).contains(curReq))
        {
            curTasksList[i].removeOne(curReq);
            break;
        }

    if(respError.isEmpty())
        return true;


    QMessageBox::information(this, qApp->applicationName(), tr(respError.toUtf8().constData()));

    return false;
}

void TixrCartWidget::endHandleResponse(PendingRequest *curReq)
{
    //
}

void TixrCartWidget::responseGetModePaiem(QString respError, PendingRequest *curReq)
{
    if(!beginHandleResponse(respError, curReq))
        return;

    fvModePaiem->setModel(tixrCore->stdModel);
    fvModePaiem->setRootIndex(curReq->getResponseItem()->index());
}

void TixrCartWidget::responseGetDevises(QString respError, PendingRequest *curReq)
{
    if(!beginHandleResponse(respError, curReq))
        return;

    QStandardItem *responseItem = curReq->getResponseItem();

    cbDevises->setModel(tixrCore->stdModel);
    cbDevises->setRootModelIndex(responseItem->index());
    if(responseItem->hasChildren())
        cbDevises->setCurrentIndex(0);
}

void TixrCartWidget::responseGetBanques(QString respError, PendingRequest *curReq)
{
    if(!beginHandleResponse(respError, curReq))
        return;

    QStandardItem *responseItem = curReq->getResponseItem();

    checkDetailsDialog->cbBank->setModel(tixrCore->stdModel);
    checkDetailsDialog->cbBank->setRootModelIndex(responseItem->index());
    if(responseItem->hasChildren())
        checkDetailsDialog->cbBank->setCurrentIndex(0);
}

void TixrCartWidget::responseGetTiers(QString respError, PendingRequest *curReq)
{
    if(!beginHandleResponse(respError, curReq))
        return;

    tiersFinderDialog->setTiersRoot(curReq->getResponseItem());
    tiersFinderDialog->show();
}

void TixrCartWidget::changeCurrentCart(const QModelIndex &index)
{
    if(cartsSW->currentIndex() == index.row())
        return;

    cartsSW->setCurrentIndex(index.row());

    updateCurrentCartWidgets();

    updatePrintButton();
}

void TixrCartWidget::updateCurrentCartWidgets()
{
    QWidget *widget = cartsSW->currentWidget();
    cartView = widget->findChild<QTreeView*>("cartView");
    totalDSB = widget->findChild<QDoubleSpinBox*>("totalDSB");
    sousTotalDSB = widget->findChild<QDoubleSpinBox*>("sousTotalDSB");
    fvModePaiem = (PictureFlowView*)widget->findChild<QAbstractItemView*>("fvModePaiem");
    cbDevises = widget->findChild<QComboBox*>("cbDevises");
    dsbAmount = widget->findChild<QDoubleSpinBox*>("dsbAmount");
    dsbRestantDu = widget->findChild<QDoubleSpinBox*>("dsbRestantDu");
    paymTView = widget->findChild<QTableView*>("paymTView");
    cbPayeur = widget->findChild<QComboBox*>("cbPayeur");
    pbRecordPay = widget->findChild<QPushButton*>("pbRecordPay");
    pbAjourner = widget->findChild<QPushButton*>("pbAjourner");
    pbPayer = widget->findChild<QPushButton*>("pbPayer");
    pbErasePay = widget->findChild<QPushButton*>("pbErasePay");
    pbErasePayStyleSheet = pbErasePay->styleSheet();
    tbReset = widget->findChild<QToolButton*>("tbReset");
    gbBalance = widget->findChild<QGroupBox*>("gbBalance");
    dsbCurBalance = gbBalance->findChild<QDoubleSpinBox*>("dsbCurBalance");
    dsbFinBalance = gbBalance->findChild<QDoubleSpinBox*>("dsbFinBalance");
}

void TixrCartWidget::on_pbRecordPay_clicked()
{
    if(!dsbAmount->value())
    {
        return;
    }

    QString bn = sender()->objectName();
    bool dette = bn=="pbAjourner";

    QStandardItem *cartItem = tixrCore->stdModel->itemFromIndex(cartView->rootIndex());

    if(dette && !cartItem->parent()->data(DataRole::ikey()).isValid())
    {
        QMessageBox::information(this, qApp->applicationName(), tr("Delayed payment is not accepted for anonymous orders."));
        return;
    }

    QStandardItem *mpItm = tixrCore->stdModel->itemFromIndex(fvModePaiem->rootIndex().child(fvModePaiem->currentSlide(), 0));
    if(!dette && mpItm->data(DataRole::mptypepaiem()).toString() == "Check")
    {
        if(cbPayeur->currentText().isEmpty())
            QMessageBox::information(this, qApp->applicationName(), tr("You should choice a valid payor for this payment type."));
        else
        {
            checkDetailsDialog->move(rect().center());
            checkDetailsDialog->modeItem = mpItm;
            checkDetailsDialog->checkPayItem = 0;
            checkDetailsDialog->show();
        }
        return;
    }

    QStandardItem *modeItem = new QStandardItem;
    modeItem->setText(mpItm->text());
    if(!dette)
        modeItem->setData(mpItm->data(DataRole::ikey()), DataRole::ikey());
    modeItem->setData(mpItm->data(DataRole::mptypepaiem()), DataRole::mptypepaiem());

    recordPay(dette, modeItem);
}

void TixrCartWidget::on_paymTView_doubleClicked(const QModelIndex & index)
{
    QStandardItem *modeItem = tixrCore->stdModel->itemFromIndex(index.sibling(index.row(), 3));
    if(modeItem->data(DataRole::mptypepaiem()).toString() == "Check")
    {
        checkDetailsDialog->move(rect().center());
        checkDetailsDialog->modeItem = 0;
        checkDetailsDialog->checkPayItem = modeItem;
        checkDetailsDialog->show();
    }
}

void TixrCartWidget::recordPay(bool dette, QStandardItem *modeItem)
{
    QStandardItem *paymRoot = tixrCore->stdModel->itemFromIndex(paymTView->rootIndex());

    QStandardItem *dateItem = new QStandardItem(QString("<%1>").arg(tr("in progress")));
    if(dette)
        dateItem->setData(QColor(Qt::red), Qt::DecorationRole);

    QStandardItem *deviseItem = new QStandardItem;
    if(!dette)
    {
        QString idDev = cbDevises->itemData(cbDevises->currentIndex(), DataRole::ikey()).toString();
        deviseItem->setData(idDev, DataRole::ikey());
        deviseItem->setText(cbDevises->currentText());
    }

    QStandardItem *montantItem = new QStandardItem;
    montantItem->setData(QVariant(dsbAmount->value()), Qt::DisplayRole);

    QVariant pIdV = cbPayeur->itemData(cbPayeur->currentIndex(), DataRole::ikey());
    QStandardItem *payeurItem = new QStandardItem(cbPayeur->currentText());
    payeurItem->setData(pIdV, DataRole::ikey());

    paymRoot->appendRow(QList<QStandardItem*>()<<dateItem<<deviseItem<<montantItem<<modeItem<<payeurItem);

    qreal newpay = dsbAmount->value();
    qreal resto = dsbRestantDu->value() - newpay;

    dsbRestantDu->setStyleSheet(dsbRestantDuStyleSheet + (resto>0?"color:red;":(resto<0?"color:green;":"color:black;")));
    dsbRestantDu->setValue(resto);

    dsbAmount->setMaximum(qMax(resto,0.));
    dsbAmount->setValue(resto);

    pbRecordPay->setEnabled(resto>0);
    pbAjourner->setEnabled(resto>0);

    if(!dette)
    {
        dsbFinBalance->setValue(dsbFinBalance->value() + newpay);
    }

    cbPayeur->clear();
    cbPayeur->setEditable(true);
    tbReset->setEnabled(false);
}

void TixrCartWidget::checkDialogAccepted()
{
    if(checkDetailsDialog->modeItem)
        recordPay(false, checkDetailsDialog->checkPayItem);
}

void TixrCartWidget::on_pbErasePay_clicked()
{
    QModelIndexList indexList = paymTView->selectionModel()->selectedRows();
    if(indexList.isEmpty())
    {
        return;
    }

    QModelIndex idx = indexList.first();

    QStandardItem *devItem = tixrCore->stdModel->itemFromIndex(idx.sibling(idx.row(), DataColumn::pdtdevcol()));
    QStandardItem *modpayItem = tixrCore->stdModel->itemFromIndex(idx.sibling(idx.row(), DataColumn::pdtmodpaycol()));

    bool noDebt = devItem->data(DataRole::ikey()).isValid() && modpayItem->data(DataRole::ikey()).isValid();

    QStandardItem *payItem = tixrCore->stdModel->itemFromIndex(idx);
    if(payItem->data(DataRole::ikey()).isValid())
    {
        if(QMessageBox::question(this, qApp->applicationName(), tr("Do you want to cancel this %1?").arg(tr(noDebt?"payment":"DEBT")), QMessageBox::Yes|QMessageBox::No) == QMessageBox::No)
            return;

        QStandardItem *modelSubItem = new QStandardItem(1,1);
        modelSubItem->setData("ItemId", DataRole::jsonProperty());
        modelSubItem->setData(JsonObject, DataRole::jsonType());

        QStandardItem *modelSubItem2 = new QStandardItem(payItem->data(DataRole::ikey()).toString());
        modelSubItem2->setData("srvikey", DataRole::jsonProperty());
        modelSubItem2->setData(JsonSimple, DataRole::jsonType());
        modelSubItem->setChild(0, 0, modelSubItem2);

        payItem->appendRow(modelSubItem);

        tixrCore->setPgFunction("main.delpaym");
        PendingRequest *pReq = tixrCore->sendMsg(modelSubItem, HTTP_METHOD_ID::POST, "/api", this, "responseAnnulPaiem");
        curTasksList[cartsSW->currentIndex()].append(pReq);

        return;
    }

    QStandardItem *amountItem = tixrCore->stdModel->itemFromIndex(idx.sibling(idx.row(), DataColumn::pdtamountcol()));

    qreal amount = amountItem->text().toDouble();

    dsbRestantDu->setValue(dsbRestantDu->value() + amount);
    qreal v = dsbRestantDu->value();
    dsbAmount->setMaximum(qMax(v,0.));
    dsbAmount->setValue(v);

    pbRecordPay->setEnabled(v>0);
    pbAjourner->setEnabled(v>0);

    if(noDebt)//no es una deuda
    {
        dsbFinBalance->setValue(dsbFinBalance->value() - amount);
    }

    tixrCore->stdModel->removeRow(idx.row(), idx.parent());

    if(paymTView->selectionModel()->hasSelection())//actualizar el estado del botón
    {
        payItem = tixrCore->stdModel->itemFromIndex(paymTView->selectionModel()->selectedRows().first());
        pbErasePay->setEnabled(payItem && !payItem->data(DataRole::pdtcanceled()).isValid());
    }
}

void TixrCartWidget::responseAnnulPaiem(QString respError, PendingRequest *curReq)
{
    curReq->setResponseItemParent(0);//para liberar el responseItem en TixRealmCore::handleResponse

    if(!beginHandleResponse(respError, curReq))
    {
        return;
    }

    QStandardItem *requestItem = curReq->getRequestItem();
    QStandardItem *responseItem = curReq->getResponseItem();

    QStandardItem *p = requestItem->parent();

    p->setData(QVariant(QColor(Qt::gray)), Qt::DecorationRole);
    p->setData(QVariant(true), DataRole::pdtcanceled());//pago anulado

    //actualizar las cantidades pagadas por cada producto
    QList<QStandardItem*> prodsLis = getProds(true);

    QModelIndex idx = p->index();
    QStandardItem *devItem = tixrCore->stdModel->itemFromIndex(idx.sibling(idx.row(), DataColumn::pdtdevcol()));
    QStandardItem *modpayItem = tixrCore->stdModel->itemFromIndex(idx.sibling(idx.row(), DataColumn::pdtmodpaycol()));

    bool noDebt = devItem->data(DataRole::ikey()).isValid() && modpayItem->data(DataRole::ikey()).isValid();

    for(int i=0, n=responseItem->rowCount(); i<n; i++)
    {
        QStandardItem *respItm = responseItem->child(i);

        QStandardItem *pItem = 0;
        for(int j=0, m=prodsLis.size(); j<m; j++)
        {
            QStandardItem *aux = prodsLis.at(j);
            if(aux->data(DataRole::dciddetails()) == respItm->data(DataRole::ikey()))
            {
                pItem = aux;
                break;
            }
        }

        Q_ASSERT(pItem);

        qreal v = respItm->data(DataRole::scvar()).toDouble();

        QStandardItem *payedItm = pItem->parent()->child(pItem->row(), DataColumn::dcpayedcol());
        Q_ASSERT(payedItm);

        if(noDebt)
        {
            qreal old = payedItm->data(Qt::DisplayRole).toDouble();
            qreal tp = old - v;
            payedItm->setData(tp, Qt::DisplayRole);

            QColor c = tp?(tp<(pItem->data(DataRole::dcprix()).toDouble()*pItem->data(DataRole::dcquantite()).toInt())?Qt::yellow:Qt::green):Qt::red;
            payedItm->setData(c, Qt::DecorationRole);
        }
        else
        {
            qreal old = payedItm->data(DataRole::dcborrowed()).toDouble();
            payedItm->setData(old - v, DataRole::dcborrowed());
        }
    }

    //eliminar las referencias a los productos pagados con este pago
    while(p->hasChildren())
        p->removeRow(0);

    //actualizar lo que resta por pagar y los botones de la interfaz
    QStandardItem *cItm = p->parent()->parent(),
                  *tiersItem = reinterpret_cast<QStandardItem*>(tixrqptrFromVariant(cItm->data(DataRole::tiersptr())));

    QWidget *w = cartsSW->widget(cItm->row());
    QDoubleSpinBox *dsb;
    QStandardItem *montantItem = p->parent()->child(p->row(), DataColumn::pdtamountcol());
    qreal v = montantItem->text().toDouble();
    qreal resto;

    if(noDebt)
    {
        dsb = w->findChild<QDoubleSpinBox*>("dsbFinBalance");
        Q_ASSERT(dsb);

        resto = dsb->value() - v;
        dsb->setValue(resto);

        dsb = w->findChild<QDoubleSpinBox*>("dsbCurBalance");
        Q_ASSERT(dsb);
        dsb->setValue(dsb->value() - v);

        if(cItm->data(DataRole::ikey()).isValid())
        {
            Q_ASSERT(tiersItem);
            tiersItem->parent()->child(tiersItem->row(), DataColumn::taccountcol())->setData(dsb->value(), Qt::DisplayRole);
        }
    }

    dsb = w->findChild<QDoubleSpinBox*>("dsbRestantDu");
    Q_ASSERT(dsb);
    resto = dsb->value() + v;
    dsb->setStyleSheet(dsbRestantDuStyleSheet + (resto>0?"color:red;":(resto<0?"color:green;":"color:black;")));
    dsb->setValue(resto);

    dsb = w->findChild<QDoubleSpinBox*>("dsbAmount");
    Q_ASSERT(dsb);
    dsb->setMaximum(qMax(resto,0.));
    dsb->setValue(resto);

    QPushButton *pb = w->findChild<QPushButton*>("pbRecordPay");
    Q_ASSERT(pb);
    pb->setEnabled(resto>0);
    pb = w->findChild<QPushButton*>("pbAjourner");
    Q_ASSERT(pb);
    pb->setEnabled(resto>0);
}

void TixrCartWidget::paymTViewSelectionChanged(const QItemSelection &, const QItemSelection &)
{
    QModelIndexList indexList = paymTView->selectionModel()->selectedIndexes();

    if(indexList.isEmpty())
        return;

    QModelIndex idx = indexList.first();
    QStandardItem *payItem = tixrCore->stdModel->itemFromIndex(idx.sibling(idx.row(), 0));

    pbErasePay->setEnabled(payItem && !payItem->data(DataRole::pdtcanceled()).isValid());
    pbErasePay->setStyleSheet(pbErasePayStyleSheet + (payItem?(payItem->data(DataRole::ikey()).isValid()?"QPushButton{color:red;}":"QPushButton{color:black;}"):"QPushButton{color:black;}"));
}

inline void TixrCartWidget::filterProducts(QList<QStandardItem*> *prodsList)
{
    for(int i=0; i<prodsList->size();)
    {
        QStandardItem *prodItm = prodsList->at(i);
        QStandardItem *payedItm = prodItm->parent()->child(prodItm->row(), 1);
        if(payedItm->data(Qt::DisplayRole).toDouble()>=prodItm->data(DataRole::dcprix()).toDouble()*prodItm->data(DataRole::dcquantite()).toInt())
            prodsList->removeAt(i);
        else
            i++;
    }
}

void TixrCartWidget::on_pbPayer_clicked()
{
    QStandardItem *cartItem = tixrCore->stdModel->itemFromIndex(cartView->rootIndex());

    QStandardItem *rootPaymsItem = tixrCore->stdModel->itemFromIndex(paymTView->rootIndex());

    //lista de productos a pagar
    QList<QStandardItem*> prodsList = getProds(true);
    filterProducts(&prodsList);
    if(prodsList.isEmpty())
    {
        QMessageBox::information(this, qApp->applicationName(), tr("Nothing to pay off."));
        return;
    }

    QStandardItem *orderItm = cartItem->parent();

    QJsonObject paymReq;

    QVariant v = orderItm->data(DataRole::ikey());
    if(v.isValid())
        paymReq.insert("clientid", QJsonValue(v.toInt()));

    v = orderItm->data(DataRole::scorderid());
    if(v.isValid())
        paymReq.insert("orderid", QJsonValue(v.toInt()));

    QJsonArray itemsArray;

    //productos
    for(int i=0, n=prodsList.size(); i<n; i++)
    {
        QStandardItem *prodItem = prodsList.at(i);

        QJsonObject prodObj;

        v = prodItem->data(DataRole::dciddetails());
        if(v.isValid())
            prodObj.insert("iddc", QJsonValue(v.toInt()));

        prodObj.insert("ik", QJsonValue(prodItem->data(DataRole::ikey()).toInt()));
        prodObj.insert("cc", QJsonValue(prodItem->data(DataRole::dccatcli()).toString()));
        prodObj.insert("p", QJsonValue(prodItem->data(DataRole::dcprix()).toDouble()));
        prodObj.insert("n", QJsonValue(prodItem->data(DataRole::dcquantite()).toInt()));

        itemsArray.append(prodObj);
    }

    paymReq.insert("items", itemsArray);

    //pagos
    QJsonArray paymsArray;

    QList<QStandardItem*> *paymItmList = new QList<QStandardItem*>;
    for(int i=0, n=rootPaymsItem->rowCount(); i<n; i++)
    {
        QStandardItem *paymItm = rootPaymsItem->child(i);

        if(paymItm->data(DataRole::ikey()).isValid())//pago ya efectuado
            continue;

        *paymItmList << paymItm;

        QJsonObject paymObj;

        v = rootPaymsItem->child(i,1)->data(DataRole::ikey());
        if(v.isValid())
            paymObj.insert("iddev", v.toInt());

        paymObj.insert("amount", rootPaymsItem->child(i,2)->data(Qt::DisplayRole).toDouble());

        QStandardItem *modPaymItm = rootPaymsItem->child(i,3);
        v = modPaymItm->data(DataRole::ikey());
        if(v.isValid())
            paymObj.insert("idmodpay", v.toInt());

        QStandardItem *payorItm = rootPaymsItem->child(i,4);

        v = payorItm->data(DataRole::ikey());
        if(v.isValid())
            paymObj.insert("payerid", v.toInt());

        v = payorItm->data(Qt::DisplayRole);
        if(v.isValid())
            paymObj.insert("payername", v.toString());

        v = modPaymItm->data(DataRole::pdtbankid());
        if(v.isValid())
            paymObj.insert("bankid", v.toString());

        v = modPaymItm->data(DataRole::pdtnumcheque());
        if(v.isValid())
            paymObj.insert("numcheque", v.toString());

        paymsArray.append(paymObj);
    }

    if(paymItmList->isEmpty() && !dsbCurBalance->value())
    {

        QMessageBox::information(this, qApp->applicationName(), QObject::tr("There is no enough payments or funds!"));
        return;
    }

    paymReq.insert("payms", paymsArray);

    tixrCore->setInHandlerIndex(3);//la respuesta como un QJsonDocument
    tixrCore->setPgFunction("main.payment");
    PendingRequest *pReq = tixrCore->sendMsg(0, HTTP_METHOD_ID::POST, "/api", this, "responseDoPayment", QJsonDocument(paymReq).toJson());

    pReq->respHandlerParams.insert("prodsList", QVariant(reinterpret_cast<tixrqptr>(new QList<QStandardItem*>(prodsList))));
    pReq->respHandlerParams.insert("paymItmList", QVariant(reinterpret_cast<tixrqptr>(paymItmList)));
    pReq->respHandlerParams.insert("orderItm", QVariant(reinterpret_cast<tixrqptr>(orderItm)));

    curTasksList[cartsSW->currentIndex()].append(pReq);
}

void TixrCartWidget::updatePaymentItem(QStandardItem *payItem, const QJsonObject &rawPaym, int orderId)
{
    QStandardItem *orderItm = 0;
    QJsonValue jVal = rawPaym.value("details");
    if(jVal.isUndefined())
        return;
    QJsonArray paymDetails = jVal.toArray();
    for(int j=0, m=paymDetails.size(); j<m; j++)
    {
        jVal = paymDetails.at(j);
        Q_ASSERT(!jVal.isUndefined());
        QJsonObject detPaymObj = jVal.toObject();

        if(detPaymObj.contains("orderid"))
            orderId = detPaymObj.value("orderid").toInt();

        //verificar si ya existen otros pagos para esta misma transacción
        for(int i=0, n=payItem->rowCount(); i<n; i++)
        {
            orderItm = payItem->child(i);
            if(orderItm->data(DataRole::ikey()).toInt() == orderId)
                break;
            orderItm = 0;
        }

        if(!orderItm)
        {
            orderItm = new QStandardItem;
            orderItm->setData("Commande", DataRole::jsonProperty());
            orderItm->setData(orderId, DataRole::ikey());
            payItem->appendRow(orderItm);
        }

        QStandardItem *detItm = 0;
        int id = detPaymObj.value("prodid").toInt();
        qreal a = detPaymObj.value("montant").toDouble();

        //verificar si ya existen otros pagos para este mismo producto
        for(int i=0, n=orderItm->rowCount(); i<n; i++)
        {
            detItm = orderItm->child(i);
            if(detItm->data(DataRole::ikey()).toInt() == id)
                break;
            detItm = 0;
        }

        if(detItm)
        {
            qreal t = detItm->data(Qt::DisplayRole).toDouble();
            t += a;
            detItm->setData(t, Qt::DisplayRole);
        }
        else
        {
            detItm = new QStandardItem;
            detItm->setData("DetPaym", DataRole::jsonProperty());
            detItm->setData(id, DataRole::ikey());
            detItm->setData(a, Qt::DisplayRole);

            orderItm->appendRow(detItm);
        }
    }
}

inline void mergePaymInfoImp(QStandardItem *ordReg1, QStandardItem *ordReg2)
{
    while(ordReg2->hasChildren())
    {
        QStandardItem *info1 = 0, *info2 = ordReg2->takeRow(0).first();
        for(int j=0, m=ordReg1->rowCount(); j<m; j++)
        {
            info1 = ordReg1->child(j);
            if(info1->data(DataRole::ikey()) == info2->data(DataRole::ikey()))
                break;
            info1 = 0;
        }
        if(info1)
            delete info2;
        else
            ordReg1->appendRow(info2);
    }
}

void TixrCartWidget::mergePaymInfo(QStandardItem *P, QStandardItem *Q)
{
    while(Q->hasChildren())
    {
        QStandardItem *pInf = 0, *qInf = Q->takeRow(0).first();
        for(int j=0, m=P->rowCount(); j<m; j++)
        {
            pInf = P->child(j);
            if(pInf->data(DataRole::ikey()) == qInf->data(DataRole::ikey()))
                break;
            pInf = 0;
        }
        if(pInf)
        {
            mergePaymInfoImp(pInf, qInf);
            delete qInf;
        }
        else
            P->appendRow(qInf);
    }
}

void TixrCartWidget::handlePayWithFund(const QStandardItem *orderItem, const QJsonObject &rawResp)
{
    QJsonValue jVal = rawResp.value("old_payms");

    if(jVal.isUndefined())
        return;

    QJsonArray oldPaymsArray = jVal.toArray();

    QStandardItem *cartPaymRoot = orderItem->child(1);
    QStandardItem *tiersPaymRoot = 0, *tiersItm = reinterpret_cast<QStandardItem*>(tixrqptrFromVariant(orderItem->data(DataRole::tiersptr())));
    if(tiersItm)
        tiersPaymRoot = tiersItm->child(DataColumn::tcipayments());

    for(int i=0, n=oldPaymsArray.size(); i<n; i++)
    {
        /*paymResp = paymsRespPaymList.takeFirst();
        QStandardItem *paymItm = 0;

        //comprobar si este pago está en el historial del cliente
        for(int j=0, m=tiersPaymRoot?tiersPaymRoot->rowCount():0; j<m; j++)
        {
            paymItm = tiersPaymRoot->child(j);
            if(paymItm->data(DataRole::ikey())==paymResp->data(DataRole::ikey()))
            {
                tiersPaymRoot->takeRow(j);
                cartPaymRoot->appendRow(paymItm);
                break;
            }
            paymItm = 0;
        }

        if(!paymItm)
        {
            //comprobar si está en el carro
            for(int j=0, m=cartPaymRoot->rowCount(); j<m; j++)
            {
                paymItm = cartPaymRoot->child(j);
                if(paymItm->data(DataRole::ikey())==paymResp->data(DataRole::ikey()))
                {
                    break;
                }
                paymItm = 0;
            }
        }

        if(!paymItm)
        {*/
            /**
             11*qtdisplayrole timestamp,
             0*pdtdevcol_ikey integer,
             10*pdtdevcol_qtdisplayrole text
             1*pdtamountcol_qtdisplayrole real,
             2*pdtmodpaycol_ikey integer,
             5*pdtmodpaycol_pdtbankid integer,
             6*pdtmodpaycol_pdtnumcheque text,
             9*pdtmodpaycol_qtdisplayrole text,
             3*pdtpayercol_ikey integer,
             4*pdtpayercol_qtdisplayrole text,
             7*pdtdateinteg timestamp without time zone,
             8*pdtdateannul timestamp without time zone,
             */

            /*QString paymAttsStr = paymResp->data(DataRole::scvar()).toString();
            QStringList paymAtts = paymAttsStr.split('|');

            Q_ASSERT(paymAtts.size() == 12);

            QList<QStandardItem*> itmList;
            itmList << (paymItm = new QStandardItem(paymAtts.at(11)));
            paymItm->setData(paymResp->data(DataRole::ikey()), DataRole::ikey());
            QDateTime dt = QDateTime::fromString(paymAtts.at(7), Qt::ISODate);
            paymItm->setData(dt, DataRole::pdtdateinteg());
            dt = QDateTime::fromString(paymAtts.at(8), Qt::ISODate);
            paymItm->setData(dt, DataRole::pdtdateannul());

            QStandardItem *detPaymItm = new QStandardItem(paymAtts.at(10));
            detPaymItm->setData(paymAtts.at(0), DataRole::ikey());
            itmList << detPaymItm;

            detPaymItm = new QStandardItem;
            detPaymItm->setData(QLocale::c().toDouble(paymAtts.at(1)), Qt::DisplayRole);
            itmList << detPaymItm;

            detPaymItm = new QStandardItem(paymAtts.at(9));
            detPaymItm->setData(paymAtts.at(2), DataRole::ikey());
            detPaymItm->setData(paymAtts.at(5), DataRole::pdtbankid());
            detPaymItm->setData(paymAtts.at(6), DataRole::pdtnumcheque());
            itmList << detPaymItm;

            detPaymItm = new QStandardItem(paymAtts.at(4));
            detPaymItm->setData(paymAtts.at(3), DataRole::ikey());
            itmList << detPaymItm;

            cartPaymRoot->appendRow(itmList);

            updatePaymentItem(paymItm, paymsRespPaymList, orderId);
        }

        delete paymResp;*/
    }
}

void handleDebtPayOff()
{
    /*
    // actualizar el historial del cliente con los productos cuya deuda se ha saldado
    QStandardItem *tiersOrdersRoot = tiersItm?tiersItm->child(DataColumn::tcihistoric()):0;
    if(tiersOrdersRoot)
    {
        QMap<QStandardItem*, QList<QStandardItem*>&> ;
        QList<QStandardItem*> prodsList = getProds(true, tiersOrdersRoot);
        QStandardItem *prodResp;
        while((prodResp = responseItem->child(0)) && prodResp->accessibleText() != "Item4Id")
        {
            responseItem->removeRow(0);
            QVariant pIdV = prodResp->data(DataRole::ikey());
            QStringList sList = prodResp->data(DataRole::scvar()).toString().split('|');
            Q_ASSERT(sList.size() == 2);
            qreal q = sList.first().toDouble();
            orderId = sList.at(1).toInt();

            QStandardItem *prodItm = 0;
            for(int i=0, n=prodsList.size(); i<n; i++)
            {
                prodItm = prodsList.at(i);
                if(prodItm->data(DataRole::ikey())==pIdV)
                    break;
                prodItm = 0;
            }

            if(prodItm)
            {
                QStandardItem *payedItm = prodItm->parent()->child(prodItm->row(), DataColumn::dcpayedcol());
                Q_ASSERT(payedItm);
                qreal tp = payedItm->data(Qt::DisplayRole).toDouble() + q;
                payedItm->setData(tp, Qt::DisplayRole);
                QColor c = tp?(tp<(prodItm->data(DataRole::dcprix()).toDouble()*prodItm->data(DataRole::dcquantite()).toInt())?Qt::yellow:Qt::green):Qt::red;
                payedItm->setData(c, Qt::DecorationRole);
                //actualizar la cantidad debida
                q = payedItm->data(DataRole::dcborrowed()).toDouble() - q;
                q = q<0?0:q;
                payedItm->setData(q, DataRole::dcborrowed());
            }
        }
    }
    else
    {
        while((paymResp = responseItem->child(0)) && paymResp->accessibleText() != "Item4Id")
            responseItem->removeRow(0);
    }

    //Actualizar los registros de deudas
    while(responseItem->hasChildren())
    {
        QStandardItem *respDebtItm = responseItem->takeRow(0).first();

        Q_ASSERT(respDebtItm->accessibleText() == "Item4Id");

        QStringList respList = respDebtItm->data(DataRole::scvar()).toString().split('|', QString::SkipEmptyParts);
        Q_ASSERT(respList.size() == 3);

        QStandardItem *root;
        QList<QStandardItem*> paymRcdList = findPaymentsForItem(cartPaymRoot, respList.at(1).toInt(), respDebtItm->data(DataRole::ikey()).toInt(), respList.at(0).toInt());
        if(paymRcdList.isEmpty())
        {
            if(tiersPaymRoot)
            {
                paymRcdList = findPaymentsForItem(tiersPaymRoot, respList.at(1).toInt(), respDebtItm->data(DataRole::ikey()).toInt(), respList.at(0).toInt());
                if(!paymRcdList.isEmpty())
                    root = tiersPaymRoot;
            }
        }
        else
            root = cartPaymRoot;

        if(!paymRcdList.isEmpty())
        {
            QStandardItem *detPaymItm = paymRcdList.first();
            QStandardItem *cmdItm = detPaymItm->parent();
            Q_ASSERT(cmdItm);
            QStandardItem *debtItm = cmdItm->parent();
            Q_ASSERT(debtItm);
            qreal p = QLocale::c().toDouble(respList.at(2)), q;
            if(p)
            {
                q = detPaymItm->data(Qt::DisplayRole).toDouble() - p;
                Q_ASSERT(q > 0);
                detPaymItm->setData(p, Qt::DisplayRole);
            }
            else
            {
                q = detPaymItm->data(Qt::DisplayRole).toDouble();
                cmdItm->removeRow(detPaymItm->row());
                if(!cmdItm->hasChildren())
                    debtItm->removeRow(cmdItm->row());
            }

            QStandardItem *amountItm = debtItm->parent()->child(debtItm->row(), DataColumn::pdtamountcol());
            q = amountItm->data(Qt::DisplayRole).toDouble() - q;
            if(q)
                amountItm->setData(q, Qt::DisplayRole);
            else
                root->removeRow(debtItm->row());
        }
    }
    */
}

void TixrCartWidget::responseDoPayment(QString respError, PendingRequest *curReq)
{
    QList<QStandardItem*> *ptrProdsList = reinterpret_cast<QList<QStandardItem*>*>( tixrqptrFromVariant(curReq->respHandlerParams["prodsList"]) );
    QList<QStandardItem*> *ptrPaymsList = reinterpret_cast<QList<QStandardItem*>*>( tixrqptrFromVariant(curReq->respHandlerParams["paymItmList"]) );

    if(!beginHandleResponse(respError, curReq))
    {
        delete ptrPaymsList;
        delete ptrProdsList;
        return;
    }

    int M = ptrProdsList->size();
    int N = ptrPaymsList->size();

    QStandardItem *orderItem = reinterpret_cast<QStandardItem*>( tixrqptrFromVariant(curReq->respHandlerParams["orderItm"]) );

    QVariant v = orderItem->data(DataRole::scorderid());
    int orderId = v.toInt();

    QJsonObject rawResp = curReq->rawResp.array().at(0).toObject().value("payment").toObject();

    QJsonArray paymsArray = rawResp.value("payms").toArray();
    QJsonArray prodsArray = rawResp.value("prods").toArray();

    int orderId2 = rawResp.value("orderid").toInt();
    QString orderdate = rawResp.value("orderdate").toString();

    int n = paymsArray.count();

    if(n != N || prodsArray.count() != M)
    {
        QMessageBox::information(this, qApp->applicationName(), tr("Invalid server response for a payment."));
        delete ptrPaymsList;
        delete ptrProdsList;
        return;
    }

    if(!v.isValid())
    {
        orderId = rawResp.value("orderid").toInt();
        QVariant orderDate = rawResp.value("orderdate").toVariant();
        orderItem->setData(orderId, DataRole::scorderid());
        orderItem->setData(orderDate, DataRole::scorderdate());
    }

    //tratar los pagos hechos con el saldo de la cuenta
    handlePayWithFund(orderItem, rawResp);

    //establecer el id. de cada pago
    qreal tp = 0;
    for(int i=0; i<ptrPaymsList->size(); i++)
    {
        QStandardItem *payItm = ptrPaymsList->at(i);
        QStandardItem *root = payItm->parent();
        QStandardItem *devItm = root->child(payItm->row(), DataColumn::pdtdevcol());
        QStandardItem *modItm = root->child(payItm->row(), DataColumn::pdtmodpaycol());

        tp += root->child(payItm->row(), DataColumn::pdtamountcol())->data(Qt::DisplayRole).toDouble();

        if(!devItm->data(DataRole::ikey()).isValid() || !modItm->data(DataRole::ikey()).isValid())
        {
            //buscar si ya existe otra deuda
            QStandardItem *paymsItm = tixrCore->stdModel->itemFromIndex(paymTView->rootIndex());
            int j, S;
            for(j=0, S=paymsItm->rowCount(); j<S; j++)
            {
                QStandardItem *payItm2 = paymsItm->child(j);
                devItm = paymsItm->child(j, DataColumn::pdtdevcol());
                modItm = paymsItm->child(j, DataColumn::pdtmodpaycol());
                if((!devItm->data(DataRole::ikey()).isValid() || !modItm->data(DataRole::ikey()).isValid()) && payItm2 != payItm && !payItm2->data(DataRole::pdtcanceled()).isValid())
                {
                    QStandardItem *vItm = root->child(payItm->row(), DataColumn::pdtamountcol());
                    QStandardItem *vItm2 = paymsItm->child(j, DataColumn::pdtamountcol());
                    vItm2->setData(QVariant(vItm2->data(Qt::DisplayRole).toDouble() + vItm->data(Qt::DisplayRole).toDouble()), Qt::DisplayRole);
                    root->removeRow(payItm->row());
                    break;
                }
            }
            if(j<S)
            {
                ptrPaymsList->removeAt(i);
                continue;
            }
        }

        QJsonObject rawPaym = paymsArray.at(i).toObject();
        int paymid = rawPaym.value("paymid").toInt();
        QVariant amount = rawPaym.value("amount");

        payItm->setData(amount, Qt::DisplayRole);
        payItm->setData(paymid, DataRole::ikey());

        updatePaymentItem(payItm, rawPaym, orderId);
    }

    //Actualizar el historial del cliente con los productos cuya deuda se ha saldado
    //Actualizar los registros de deudas
    handleDebtPayOff();

    //actualizar las cantidades pagadas por cada producto
    for(int i=0; i<M; i++)
    {
        const QJsonValue &jVal = prodsArray.at(i);
        Q_ASSERT(!jVal.isUndefined());

        QJsonObject prodObj = jVal.toObject();

        qreal p = prodObj.value("payed").toDouble();//tixrCore->cLocal.toDouble(sl.first());
        QStandardItem *prodItm = ptrProdsList->at(i);

        int iddet = prodObj.value("iddet").toInt();
        prodItm->setData(iddet, DataRole::dciddetails());//id. en main.detailscommande

        QStandardItem *payedItm = prodItm->parent()->child(prodItm->row(), DataColumn::dcpayedcol());
        Q_ASSERT(payedItm);
        qreal q = payedItm->data(Qt::DisplayRole).toDouble() + p;
        payedItm->setData(q, Qt::DisplayRole);
        QColor c = q?(q<(prodItm->data(DataRole::dcprix()).toDouble()*prodItm->data(DataRole::dcquantite()).toInt())?Qt::yellow:Qt::green):Qt::red;
        payedItm->setData(c, Qt::DecorationRole);
        //actualizar la cantidad debida
        p = prodObj.value("borrowed").toDouble();
        payedItm->setData(payedItm->data(DataRole::dcborrowed()).toDouble() + p, DataRole::dcborrowed());
    }

    if(orderItem->data(DataRole::ikey()).isValid())
    {
        //Actualizar el saldo del cliente
        QWidget *w = cartsSW->widget(orderItem->row());
        QDoubleSpinBox *dsb = w->findChild<QDoubleSpinBox*>("dsbCurBalance");
        QDoubleSpinBox *dsb2 = w->findChild<QDoubleSpinBox*>("dsbFinBalance");
        dsb->setValue(dsb2->value());
        QStandardItem *tiersItem = reinterpret_cast<QStandardItem*>(tixrqptrFromVariant(orderItem->data(DataRole::tiersptr())));
        Q_ASSERT(tiersItem);
        tiersItem->parent()->child(tiersItem->row(), DataColumn::taccountcol())->setData(dsb->value(), Qt::DisplayRole);
    }

    delete ptrPaymsList;
    delete ptrProdsList;

    QItemSelection is;
    paymTViewSelectionChanged(is, is);
}

void TixrCartWidget::on_pbNew_clicked()
{
    createCart();
}

void TixrCartWidget::createCart(QStandardItem *currentCartItem)
{
    QUiLoader loader;
    QFile f(":/formconfig/cartWidgetContent.ui");
    f.open(QFile::ReadOnly);
    QWidget *w =  loader.load(&f);
    Q_ASSERT(w);
    f.close();
    cartsSW->addWidget(w);

    if(currentCartItem)
    {
        QGroupBox *gbb = w->findChild<QGroupBox*>("gbBalance");
        Q_ASSERT(gbb);
        gbb->setVisible(true);
        QStandardItem *tiersItem = reinterpret_cast<QStandardItem*>(tixrqptrFromVariant(currentCartItem->data(DataRole::tiersptr())));
        qreal b = tiersItem->parent()->child(tiersItem->row(), DataColumn::taccountcol())->data(Qt::DisplayRole).toDouble();
        QDoubleSpinBox *dsbcb = gbb->findChild<QDoubleSpinBox*>("dsbCurBalance");
        Q_ASSERT(dsbcb);
        dsbcb->setValue(b);
        QDoubleSpinBox *dsbfb = gbb->findChild<QDoubleSpinBox*>("dsbFinBalance");
        Q_ASSERT(dsbfb);
        dsbfb->setValue(b);
    }
    else
    {
        currentCartItem = new QStandardItem;
        currentCartItem->setText(tr("Unknown Client"));
    }

    cartsItem->appendRow(currentCartItem);

    //El carro de compras tiene un primer nodo para los productos y un segundo para los pagos
    QStandardItem *stdItem = currentCartItem->child(0);
    if(stdItem)
    {

        QList<QStandardItem*> leafsList = getProds(true, stdItem);

        qreal t = 0;
        for(int i=0, n=leafsList.size(); i<n; i++)
        {
            QStandardItem *pItm = leafsList.at(i);
            QVariant v = pItm->data(DataRole::dcquantite());
            int N = v.isValid() ? v.toInt() : 1;
            t += N * pItm->data(DataRole::dcprix()).toDouble();
        }
        QDoubleSpinBox *totalDSB = w->findChild<QDoubleSpinBox*>("totalDSB");
        totalDSB->setValue(t);
    }
    else
    {
        stdItem = new QStandardItem;
        currentCartItem->setChild(0, stdItem);
    }

    QTreeView *trv = w->findChild<QTreeView*>("cartView");
    trv->setModel(tixrCore->stdModel);
    trv->setRootIndex(stdItem->index());
    connect(trv->selectionModel(), SIGNAL(selectionChanged(QItemSelection,QItemSelection)), this, SLOT(cartViewSelectionChanged(QItemSelection,QItemSelection)));
    trv->header()->setModel(cartHeaderModel);

    stdItem = tixrCore->keyToItemMap.value("modpaym");

    PictureFlowView *fv = (PictureFlowView*)w->findChild<QAbstractItemView*>("fvModePaiem");
    fv->setModel(tixrCore->stdModel);
    fv->setRootIndex(stdItem->index());

    stdItem = tixrCore->keyToItemMap.value("devises");
    QComboBox *cb = w->findChild<QComboBox*>("cbDevises");
    cb->setModel(tixrCore->stdModel);
    cb->setRootModelIndex(stdItem->index());
    if(stdItem->hasChildren())
        cb->setCurrentIndex(0);

    QTableView *tv = w->findChild<QTableView*>("paymTView");

    stdItem = currentCartItem->child(1);
    if(!stdItem)
    {
        stdItem = new QStandardItem;
        currentCartItem->setChild(1, stdItem);
    }

    tv->setModel(tixrCore->stdModel);
    tv->setRootIndex(stdItem->index());
    tv->horizontalHeader()->setModel(paymTViewHModel);
    connect(tv->selectionModel(), SIGNAL(selectionChanged(QItemSelection,QItemSelection)), this, SLOT(paymTViewSelectionChanged(QItemSelection,QItemSelection)));
    //connect(tv, SIGNAL(doubleClicked(QModelIndex)), this, SLOT(on_paymTView_doubleClicked(QModelIndex)));

    QPushButton *pbAj = w->findChild<QPushButton*>("pbAjourner");
    connect(pbAj, SIGNAL(clicked()), this, SLOT(on_pbRecordPay_clicked()), Qt::UniqueConnection);

    curTasksList << QList<PendingRequest*>();

    MainWindow::tixrConnectSlotsByName(this, w);
}

void TixrCartWidget::assignCart(QStandardItem *currentCartItem, QStandardItem *&newCartItem)
{
    currentCartItem->setData(newCartItem->data(Qt::DisplayRole), Qt::DisplayRole);
    currentCartItem->setData(newCartItem->data(DataRole::ikey()), DataRole::ikey());
    currentCartItem->setData(newCartItem->data(DataRole::scorderid()), DataRole::scorderid());
    currentCartItem->setData(newCartItem->data(DataRole::scorderdate()), DataRole::scorderdate());
    QVariant v;
    currentCartItem->setData(v = newCartItem->data(DataRole::tiersptr()), DataRole::tiersptr());

    QStandardItem *dstRootItem = currentCartItem->child(1);
    QStandardItem *srcRootItem = newCartItem->child(1);
    Q_ASSERT(dstRootItem);
    Q_ASSERT(srcRootItem);
    while(srcRootItem->hasChildren())
    {
        dstRootItem->appendRow(srcRootItem->takeRow(0));
    }

    dstRootItem = currentCartItem->child(0);
    srcRootItem = newCartItem->child(0);
    Q_ASSERT(dstRootItem);
    Q_ASSERT(srcRootItem);
    while(srcRootItem->hasChildren())
    {
        dstRootItem->appendRow(srcRootItem->takeRow(0));
    }

    QList<QStandardItem*> leafsList = getProds(true, dstRootItem);

    qreal t = 0;
    for(int i=0, n=leafsList.size(); i<n; i++)
    {
        QStandardItem *pItm = leafsList.at(i);
        QVariant v = pItm->data(DataRole::dcquantite());
        int N = v.isValid() ? v.toInt() : 1;
        t += N * pItm->data(DataRole::dcprix()).toDouble();
    }

    QGroupBox *gb;
    QDoubleSpinBox *dsbCB, *dsbFB, *dsbRDu, *dsbT, *dsbA, *dsbST;

    QModelIndex idx = dstRootItem->index();
    if(idx == cartView->rootIndex())
    {
        gb = gbBalance;
        dsbT = totalDSB;
        dsbCB = dsbCurBalance;
        dsbFB = dsbFinBalance;
        dsbRDu = dsbRestantDu;
        dsbA = dsbAmount;
        dsbST = sousTotalDSB;
    }
    else
    {
        QWidget *w = cartsSW->widget(idx.row());

        gb = w->findChild<QGroupBox*>("gbBalance");
        dsbT = w->findChild<QDoubleSpinBox*>("totalDSB");
        dsbCB = w->findChild<QDoubleSpinBox*>("dsbCurBalance");
        dsbFB = w->findChild<QDoubleSpinBox*>("dsbFinBalance");
        dsbRDu = w->findChild<QDoubleSpinBox*>("dsbRestantDu");
        dsbA = w->findChild<QDoubleSpinBox*>("dsbAmount");
        dsbST = w->findChild<QDoubleSpinBox*>("sousTotalDSB");
    }

    dsbT->setValue(t);
    gb->setVisible(true);

    QStandardItem *tiersItem = reinterpret_cast<QStandardItem*>(tixrqptrFromVariant(v));
    qreal b = tiersItem->parent()->child(tiersItem->row(), DataColumn::taccountcol())->data(Qt::DisplayRole).toDouble();

    dsbCB->setValue(b);
    dsbFB->setValue(b);
    dsbRDu->setValue(0);
    dsbA->setValue(0);
    dsbST->setValue(0);

    delete newCartItem;
    newCartItem = currentCartItem;
}

void TixrCartWidget::on_pbDel_clicked()
{
    QList<QStandardItem*> prodsList = getProds(true);
    for(int i=0; i<prodsList.size();)//eliminar de la lista los productos que ya fueron pagados
    {
        QStandardItem *pItm = prodsList.at(i);
        QStandardItem *payedItm = pItm->parent()->child(pItm->row(), 1);
        qreal t = payedItm->data(Qt::DisplayRole).toDouble() + payedItm->data(DataRole::dcborrowed()).toDouble();
        int n = qCeil( t / pItm->data(DataRole::dcprix()).toDouble() );//pagados o reservados, incluso parcialmente
        int N = pItm->data(DataRole::dcquantite()).toInt();
        if(n >= N)
            prodsList.removeAt(i);
        else
        {
            i++;
            pItm->setData(N-n, DataRole::auxptr1());
        }
    }

    if(prodsList.isEmpty())
    {
        delCurCart();
        return;
    }

    if(QMessageBox::question(this, tr("Shopping cart"), tr("All unreserved or unpaid items will be removed. Are you sure?"),
                                     QMessageBox::Yes|QMessageBox::No) == QMessageBox::No)
        return;

    enleverProds(prodsList);
}

void TixrCartWidget::delCurCart()
{
    QStandardItem *cartItm = cartsItem->child(cartsFView->currentSlide());

    if(!isEmpty())
        updateHistory(cartItm);//actualizar el historial del cliente o de las transacciones anónimas

    if(cartsItem->rowCount() == 1)//siempre se dejará al menos un carrito
    {
        //limpiar
        cartItm->setText("Unknown Client");
        cartItm->setData(QVariant(), DataRole::scorderdate());
        cartItm->setData(QVariant(), DataRole::tiersptr());

        cartsFView->render();

        dsbCurBalance->setValue(0);
        dsbFinBalance->setValue(0);

        totalDSB->setValue(0);
        dsbRestantDu->setValue(0);
        dsbRestantDu->setStyleSheet(dsbRestantDuStyleSheet + "color:black;");

        dsbAmount->setMaximum(0);
        dsbAmount->setValue(0);

        pbRecordPay->setEnabled(false);
        pbAjourner->setEnabled(false);
    }
    else
        destroyCurrentCart();

    updatePrintButton();
}

bool TixrCartWidget::isEmpty(QStandardItem *cItm)
{
    QStandardItem *prodsRootItm = cItm?cItm->child(0):tixrCore->stdModel->itemFromIndex(cartView->rootIndex());
    if(prodsRootItm && prodsRootItm->hasChildren())
        return false;

    QStandardItem *paymsRootItm = cItm?cItm->child(1):tixrCore->stdModel->itemFromIndex(paymTView->rootIndex());
    if(paymsRootItm && paymsRootItm->hasChildren())
        return false;

    return true;
}

void TixrCartWidget::destroyCurrentCart()
{
    int idx = cartsFView->currentSlide();

    cartsItem->removeRow(idx);
    curTasksList.removeAt(idx);

    if(idx >= cartsItem->rowCount())
        idx--;

    cartsFView->clearCaches();
    cartsFView->setCurrentSlide(idx);

    QWidget *w = cartsSW->currentWidget();
    cartsSW->removeWidget(w);
    w->setParent(0);
    delete w;

    updateCurrentCartWidgets();

    updatePrintButton();
}

void TixrCartWidget::updateHistory(QStandardItem *cartItm)
{
    QStandardItem *cartItmClone = cartItm->clone();
    cartItmClone->setData(cartItm->data(DataRole::scorderdate()), Qt::DisplayRole);

    if(cartItm->data(DataRole::ikey()).isValid())
    {
        QStandardItem *tiersItm = reinterpret_cast<QStandardItem*>( tixrqptrFromVariant(cartItm->data(DataRole::tiersptr())) );

        QStandardItem *histRoot = tiersItm->child(DataColumn::tcihistoric());
        if(!histRoot)
        {
            QStandardItem *aux = cartItm->child(0);
            while(aux->hasChildren())
                aux->removeRow(0);

            aux = cartItm->child(1);
            while(aux->hasChildren())
                aux->removeRow(0);

            return;
        }

        QStandardItem *paymRoot = tiersItm->child(DataColumn::tcipayments());
        Q_ASSERT(paymRoot);

        //productos
        QStandardItem *orderItem = 0;
        for(int i=0, n=histRoot->rowCount(); i<n; i++)
        {
            orderItem = histRoot->child(i);
            if(orderItem->data(DataRole::scorderid()) == cartItm->data(DataRole::scorderid()))
                break;
            orderItem = 0;
        }
        if(orderItem)
        {
            QStandardItem *dstRootItem = orderItem->child(0);
            if(!dstRootItem)
            {
                orderItem->setChild(0, dstRootItem = new QStandardItem);
            }

            QStandardItem *srcRootItem = cartItm->child(0);
            Q_ASSERT(srcRootItem);

            while(dstRootItem->hasChildren())
            {
                dstRootItem->removeRow(0);
            }

            while(srcRootItem->hasChildren())
            {
                dstRootItem->appendRow(srcRootItem->takeRow(0));
            }

            delete cartItmClone;
        }
        else
        {
            cartItmClone->appendRow(new QStandardItem);
            QStandardItem *dstRootItem = cartItmClone->child(0);
            QStandardItem *srcRootItem = cartItm->child(0);
            Q_ASSERT(dstRootItem);
            Q_ASSERT(srcRootItem);
            while(srcRootItem->hasChildren())
            {
                dstRootItem->appendRow(srcRootItem->takeRow(0));
            }

            histRoot->appendRow(cartItmClone);
        }

        //pagos
        QStandardItem *srcRootItem = cartItm->child(1);
        Q_ASSERT(srcRootItem);
        while(srcRootItem->hasChildren())
        {
            QStandardItem *srcPaymItem = srcRootItem->child(0);
            if(srcPaymItem->data(DataRole::ikey()).isValid())
            {
                QStandardItem *paymItem = 0;
                for(int i=0, n=paymRoot->rowCount(); i<n; i++)
                {
                    paymItem = paymRoot->child(i);
                    if(paymItem->data(DataRole::ikey()) == srcPaymItem->data(DataRole::ikey()))
                        break;
                    paymItem = 0;
                }

                if(paymItem)
                {
                    /*int r = paymItem->row();
                    paymRoot->removeRow(r);
                    paymRoot->insertRow(r, srcRootItem->takeRow(0));*/

                    mergePaymInfo(paymItem, srcPaymItem);
                    srcRootItem->removeRow(0);
                }
                else
                    paymRoot->appendRow(srcRootItem->takeRow(0));
            }
            else
                srcRootItem->removeRow(0);//los pagos no efectuados son eliminados
        }
    }
    else
    {
        QStandardItem *anonymRoot = tixrCore->keyToItemMap.value("anonym_transac");
        if(!anonymRoot)
        {
            QStandardItem *aux = cartItm->child(0);
            while(aux->hasChildren())
                aux->removeRow(0);

            aux = cartItm->child(1);
            while(aux->hasChildren())
                aux->removeRow(0);

            return;
        }

        //Actualizar el historial de las transacciones anónimas
        QStandardItem *dstRootItem;
        cartItmClone->appendRow(dstRootItem = new QStandardItem);
        QStandardItem *srcRootItem = cartItm->child(0);
        Q_ASSERT(dstRootItem);
        Q_ASSERT(srcRootItem);
        while(srcRootItem->hasChildren())
        {
            dstRootItem->appendRow(srcRootItem->takeRow(0));
        }

        cartItmClone->appendRow(dstRootItem = new QStandardItem);
        srcRootItem = cartItm->child(1);
        Q_ASSERT(dstRootItem);
        Q_ASSERT(srcRootItem);
        while(srcRootItem->hasChildren())
        {
            dstRootItem->appendRow(srcRootItem->takeRow(0));
        }

        anonymRoot->appendRow(cartItmClone);
    }
}

void TixrCartWidget::on_pbEnleverProd_clicked()
{
    QList<QStandardItem*> prodsList = getProds();

    if(prodsList.isEmpty())
        return;

    QStringList list;
    for(int i=0, n=prodsList.size(); i<n; i++)
    {
        list << prodsList.at(i)->text();
    }

    int amount = 0;
    QVariant v;
    if(prodsList.size()==1 && (v=prodsList.first()->data(DataRole::dcquantite())).isValid() && v.toInt() > 1)
    {
        // Permitir liberar una cierta cantidad de este producto
        QString itLbl = prodsList.first()->text();
        itLbl = itLbl.left(itLbl.lastIndexOf(" ["));
        bool ok;
        int cv = v.toInt();
        amount = QInputDialog::getInt(this, tr("There are %n item(s) \"%1\" in the cart", 0, v.toInt()).arg(itLbl),
                                  tr("How many do you want to remove?"), cv, 1, cv, 1, &ok);
        if(!ok)
            return;
    }

    if(!amount && QMessageBox::question(this, qApp->applicationName(), tr("You are trying to remove %n item(s) from the cart:\n\n%2\n\nAre you sure?", 0, list.size()).arg(list.join("\n")), QMessageBox::Yes|QMessageBox::No) == QMessageBox::No)
        return;

    enleverProds(prodsList, true, amount);
}

void TixrCartWidget::enleverProds(QList<QStandardItem*> &prodsList, bool refreshCartWidgets, int amount)
{
    QStandardItem *modelSubItem = new QStandardItem(1, prodsList.size());
    modelSubItem->setData("FreeProdParams", DataRole::jsonProperty());
    modelSubItem->setData(JsonObject, DataRole::jsonType());

    for(int i=0, n=prodsList.size(); i<n; i++)
    {
        QStandardItem *pItm = prodsList.at(i);
        QVariant v = pItm->data(DataRole::dcquantite());

        int a = amount?amount:(v.isValid()?v.toInt():1);

        QStandardItem *modelSubItem3 = new QStandardItem(1, 3);
        modelSubItem3->setData("Produit", DataRole::jsonProperty());
        modelSubItem3->setData(JsonObject, DataRole::jsonType());
        modelSubItem->setChild(0, i, modelSubItem3);

        modelSubItem3->setData(QVariant(reinterpret_cast<tixrqptr>(pItm)), DataRole::auxptr1());

        QStandardItem *modelSubItem2 = new QStandardItem(pItm->data(DataRole::ikey()).toString());
        modelSubItem2->setData("itemsid", DataRole::jsonProperty());
        modelSubItem2->setData(JsonSimple, DataRole::jsonType());
        modelSubItem3->setChild(0, 0, modelSubItem2);

        v = pItm->data(DataRole::dciddetails());
        if(v.isValid())
        {
            modelSubItem2 = new QStandardItem(v.toString());
            modelSubItem2->setData("detailsid", DataRole::jsonProperty());
            modelSubItem2->setData(JsonSimple, DataRole::jsonType());
            modelSubItem3->setChild(0, 1, modelSubItem2);
        }

        modelSubItem2 = new QStandardItem(QString::number(a));
        modelSubItem2->setData("howmany", DataRole::jsonProperty());
        modelSubItem2->setData(JsonSimple, DataRole::jsonType());
        modelSubItem3->setChild(0, 2, modelSubItem2);
    }

    modelSubItem->setData(QVariant(reinterpret_cast<tixrqptr>(new QList<QStandardItem*>(prodsList))), DataRole::auxptr1());
    modelSubItem->setData(QVariant(refreshCartWidgets), DataRole::auxptr2());

    cartsItem->child(cartsFView->currentSlide())->appendRow(modelSubItem);

    tixrCore->setPgFunction("main.freeprod");
    PendingRequest *pReq = tixrCore->sendMsg(modelSubItem, HTTP_METHOD_ID::POST, "/api", this, "responseEnleverProduits");
    curTasksList[cartsSW->currentIndex()].append(pReq);

    cartView->selectionModel()->clearSelection();
    sousTotalDSB->setValue(0.);
}

QList<QStandardItem*> TixrCartWidget::findPaymentsForItem(QStandardItem *paymsRoot, int ordId, int itId, QVariant paymId)
{
    QList<QStandardItem*> rlist;

    for(int i=0, n=paymsRoot->rowCount(); i<n; i++)
    {
        QStandardItem *pItm = paymsRoot->child(i);
        if(paymId.isValid() && pItm->data(DataRole::ikey()).toInt() != paymId.toInt())
            continue;

        for(int j=0, m=pItm->rowCount(); j<m; j++)
        {
            QStandardItem *ordItm = pItm->child(j);
            if(ordItm->data(DataRole::ikey()).toInt() == ordId)
            {
                for(int k=0, N=ordItm->rowCount(); k<N; k++)
                {
                    QStandardItem *detItm = ordItm->child(k);
                    if(detItm->data(DataRole::ikey()).toInt() == itId)
                    {
                        rlist << detItm;
                        break;
                    }
                }
                break;
            }
        }

        if(paymId.isValid())
            break;
    }

    return rlist;
}

void TixrCartWidget::responseEnleverProduits(QString respError, PendingRequest *curReq)
{
    curReq->setResponseItemParent(0);//para liberar el responseItem en TixRealmCore::handleResponse

    QStandardItem *requestItem = curReq->getRequestItem();
    QStandardItem *responseItem = curReq->getResponseItem();

    QList<QStandardItem*> *ptrProdsList = reinterpret_cast<QList<QStandardItem*>*>( tixrqptrFromVariant(requestItem->data(DataRole::auxptr1())) );

    if(!beginHandleResponse(respError, curReq))
    {
        delete ptrProdsList;
        return;
    }

    if(responseItem->rowCount() < ptrProdsList->size())
    {
        delete ptrProdsList;
        QMessageBox::warning(this, qApp->applicationName(), tr("Invalid response for this request"));
        return;
    }

    QStringList keyList, msgList, scvarList;
    QMap<int, QList< QPair<int, qreal> > > itemToPaymDetMap;

    QStandardItem *reqParamsItm = requestItem->child(0,1);

    for(int i=0, n=responseItem->rowCount(); i<n;)
    {
        QStandardItem *c = responseItem->child(i);
        QString msg = c->text();
        msgList << msg;
        QString idStr = c->data(DataRole::ikey()).toString();
        keyList << idStr;
        scvarList << c->data(DataRole::scvar()).toString();

        QVariant v = reqParamsItm->child(0,i++)->child(0,1)->data(Qt::DisplayRole);

        if(v.isValid() && msg.isEmpty())
        {
            //Obtener los detalles de los pagos
            QStandardItem *detPaym;
            QList< QPair<int, qreal> > paymAmountPairList;
            while(i<responseItem->rowCount() && (detPaym = responseItem->child(i++))->accessibleText() == "ItemId")
            {
                paymAmountPairList << QPair<int, qreal>(detPaym->data(DataRole::ikey()).toInt(), detPaym->data(DataRole::scvar()).toDouble());
            }

            itemToPaymDetMap.insert(v.toInt(), paymAmountPairList);
        }
    }

    QStandardItem *ordItem = requestItem->parent();
    QStandardItem *prodsRoot = ordItem->child(0);
    QStandardItem *paymsRoot = ordItem->child(1);
    int orderId = ordItem->data(DataRole::scorderid()).toInt();
    qreal dif = 0;//precio total de todos los productos liberados
    qreal pDif = 0;//total de dinero a favor del cliente
    qreal npDif = 0;//total anulado en productos que aún no se habían pagado
    for(int i=0; i<ptrProdsList->size();)
    {
        QStandardItem *pItem = ptrProdsList->at(i);

        QString itId = pItem->data(DataRole::ikey()).toString();
        int k = keyList.indexOf(itId);
        if(k<0 || !msgList.at(k).isEmpty())
        {
            i++;
            continue;
        }

        ptrProdsList->removeAt(i);
        keyList.removeAt(k);
        msgList.removeAt(k);
        QString iddet = scvarList.takeAt(k);//id. del nuevo registro de detalles de la transacción, los registros no son eliminados sino remplazados por otros tras cada actualización.

        //tomar la cantidad de productos que se solicitó eliminar
        int amount = reqParamsItm->child(0,i)->child(0,2)->data(Qt::DisplayRole).toInt();

        int curTotal = pItem->data(DataRole::dcquantite()).toInt();
        qreal prix = pItem->data(DataRole::dcprix()).toDouble();
        qreal d = amount * prix;//importe total de lo que se liberó
        dif += d;

        QStandardItem *payedItm = pItem->parent()->child(pItem->row(), DataColumn::dcpayedcol());
        qreal P = payedItm->data(Qt::DisplayRole).toDouble();
        qreal Q = payedItm->data(DataRole::dcborrowed()).toDouble();
        qreal S = curTotal * prix - P - Q;

        //actualizar la cantidad disponible en la zona
        QStandardItem *catCliItem = reinterpret_cast<QStandardItem*>( tixrqptrFromVariant(pItem->data(DataRole::catcliptr())) );
        if(!catCliItem)
        {
            QVariant v = pItem->data(DataRole::dcproducttype());
            if(v.isValid())
            {
                QStandardItem *prodTypeStockEntry = tixrCore->modelEntryFromProductType(v.toInt());

                v = pItem->parent()->data(DataRole::iuid());
                catCliItem = tixrCore->itemByValuefullscanFromModel(prodTypeStockEntry, DataRole::iuid(), v);
                if(catCliItem)
                    pItem->setData(QVariant(reinterpret_cast<tixrqptr>(catCliItem)), DataRole::catcliptr());
            }
        }

        if(catCliItem)
        {
            QStandardItem *padre = catCliItem->parent();
            QStandardItem *zItem = padre->parent();
            int quant = zItem->data(DataRole::zapquant()).toInt() - amount;
            zItem->setData(QVariant(quant), DataRole::zapquant());
            QImage newImg = JsonQStandardItemFactory::genZoneTarImg(zItem->text(), QString::number(quant), zItem->data(DataRole::zapdispo()).toString());
            zItem->setData(QVariant(newImg), Qt::DecorationRole);
        }

        QString oldIdDet = reqParamsItm->child(0,i++)->child(0,1)->text();

        QList<QStandardItem*> paymRcdList = findPaymentsForItem(paymsRoot, orderId, oldIdDet.toInt());

        if(amount < curTotal)
        {
            //actualizar la cantidad restante
            curTotal -= amount;
            pItem->setData(curTotal, DataRole::dcquantite());

            //actualizar el decorado
            QString itLbl = pItem->text();
            itLbl = itLbl.left(itLbl.lastIndexOf(" ["));//quitar la deco. precedente
            qreal costoTotal = curTotal * prix;
            itLbl += QString(" [%1€ x %2 -> %3]").arg(tixrCore->curLocal.toString(prix), QString::number(curTotal), tixrCore->curLocal.toString(costoTotal));
            pItem->setText(itLbl);

            //actualizar lo que se ha pagado/reservado
            qreal nV = qMax(0., S-d);
            d = qMax(0., d-S);
            S = nV;

            nV = qMax(0., Q-d);
            d = qMax(0., d-Q);
            Q = nV;
            payedItm->setData(Q, DataRole::dcborrowed());

            P = qMax(0., P-d);
            payedItm->setData(P, Qt::DisplayRole);

            pDif += d;//dinero a favor del cliente
            npDif += S;

            if(oldIdDet != "-1")
            {
                Q_ASSERT(!iddet.isEmpty());
                pItem->setData(iddet, DataRole::dciddetails());

                //actualizar los registros de pagos
                QList< QPair<int, qreal> > &paymAmountPairList = itemToPaymDetMap[oldIdDet.toInt()];
                for(int j=0, n=paymRcdList.size(); j<n; j++)
                {
                    QStandardItem *detItm = paymRcdList.at(j);
                    detItm->setData(iddet, DataRole::ikey());
                    int payId = detItm->parent()->parent()->data(DataRole::ikey()).toInt();
                    for(int k=0, m=paymAmountPairList.size(); k<m; k++)
                    {
                        const QPair<int,qreal> &pair = paymAmountPairList.at(k);
                        if(pair.first == payId)
                        {
                            if(pair.second)
                                detItm->setData(pair.second, Qt::DisplayRole);
                            else
                            {
                                QStandardItem *oItm = detItm->parent();
                                oItm->removeRow(detItm->row());
                                if(!oItm->hasChildren())
                                    oItm->parent()->removeRow(oItm->row());
                            }
                            break;
                        }
                    }
                }
            }

            continue;
        }

        pDif += P;

        QStandardItem *p = pItem->parent();
        p->removeRow(pItem->row());

        while(!p->hasChildren() && p!=prodsRoot)//eliminar todos los nodos que no tienen más productos
        {
            pItem = p;
            p = p->parent();
            p->removeRow(pItem->row());
        }

        //borrar todos los registros de pagos de este producto
        for(int j=0, n=paymRcdList.size(); j<n; j++)
        {
            QStandardItem *detItm = paymRcdList.at(j);
            QStandardItem *oItm = detItm->parent();
            oItm->removeRow(detItm->row());
            if(!oItm->hasChildren())
                oItm->parent()->removeRow(oItm->row());
        }
    }

    QWidget *w = cartsSW->widget(ordItem->row());
    QDoubleSpinBox *dsbCurB = w->findChild<QDoubleSpinBox*>("dsbCurBalance");
    Q_ASSERT(dsbCurB);

    if(requestItem->data(DataRole::auxptr2()).toBool())
    {
        //actualizar el total a pagar
        QDoubleSpinBox *dsb = w->findChild<QDoubleSpinBox*>("totalDSB");
        Q_ASSERT(dsb);
        dsb->setValue(dsb->value() - dif);

        dsbCurB->setValue(dsbCurB->value() + pDif);
        qreal resto = dsbCurB->value();

        dsb = w->findChild<QDoubleSpinBox*>("dsbFinBalance");
        Q_ASSERT(dsb);
        dsb->setValue(resto - npDif);

        resto = -dsb->value();

        dsb = w->findChild<QDoubleSpinBox*>("dsbRestantDu");
        Q_ASSERT(dsb);
        dsb->setValue(resto);
        dsb->setStyleSheet(dsbRestantDuStyleSheet + (resto>0?"color:red;":(resto<0?"color:green;":"color:black;")));

        dsb = w->findChild<QDoubleSpinBox*>("dsbAmount");
        Q_ASSERT(dsb);
        dsb->setMaximum(qMax(resto,0.));
        dsb->setValue(resto);

        QPushButton *pb = w->findChild<QPushButton*>("pbRecordPay");
        Q_ASSERT(pb);
        pb->setEnabled(resto>0);
        pb = w->findChild<QPushButton*>("pbAjourner");
        Q_ASSERT(pb);
        pb->setEnabled(resto>0);

        updatePrintButton();
    }
    else
        delCurCart();

    //Actualizar el saldo del cliente, en el caso pertinente
    QStandardItem *tiersItem = reinterpret_cast<QStandardItem*>(tixrqptrFromVariant(ordItem->data(DataRole::tiersptr())));
    if(tiersItem)
    {
        tiersItem->parent()->child(tiersItem->row(), DataColumn::taccountcol())->setData(dsbCurB->value(), Qt::DisplayRole);
    }

    //mostrar los errores para los elementos que no se pudieron eliminar
    QStringList list;
    while(!ptrProdsList->isEmpty())
    {
        QStandardItem *pItem = ptrProdsList->first();
        int k = keyList.indexOf(pItem->data(DataRole::ikey()).toString());
        if(k<0)
            k = 0;//esto nunca debería ocurrir, el servidor siempre envía un mensaje para cada producto identificado por el id de dicho elemento. Pero si no fuera así cojo el primer mensaje que está en la lista :(, a partir de ahí...es la mierda.
        list << pItem->text() + ": " + tr(msgList.at(k).toUtf8().constData());
        ptrProdsList->removeFirst();
        keyList.removeAt(k);
        msgList.removeAt(k);
    }

    delete ptrProdsList;

    if(!list.isEmpty())
        QMessageBox::warning(this, qApp->applicationName(), tr("Error trying to remove the following item(s)\n\n%1", 0, list.size()).arg(list.join("\n")));
}

void TixrCartWidget::on_pbPrint_clicked()
{
    PrintJob *printJob;
    int currCartIndex = cartsSW->currentIndex();
    if (cartsPrintJobs.contains(currCartIndex)) {
        printJob = cartsPrintJobs.value(currCartIndex);
        printJob->confirmAbortPrinting();
    }
    else {
        PrintItems printItems = getPrintableProducts();

        // Save number of tickets already printed as this will be needed as a reference as the printing advances and changes the original value
        foreach (PrintItem printItem, printItems) {
            QStandardItem *printStateItem = printItem.second;
            printStateItem->setData(printStateItem->data(DataRole::nticketsprinted()), DataRole::auxnticketsprinted());
        }

        QString reportFileName = tixrCore->curPath % "xml/grussNouvelleCreation2012.xml";
//      QWidget *cartSW_currWidget = cartsSW->currentWidget();
//      QWidget *widgetContents = findChild<QWidget*>("scrollAreaWidgetContents");
//      PrintJob *printJob = printMgr->print(reportFileName, idList, this, widgetContents);
        QStatusBar *statusBar = tixrCore->objMainWindow->findChild<QStatusBar*>("statusbar");
        printJob = printMgr->print(reportFileName, printItems, this, statusBar, currCartIndex);
        cartsPrintJobs.insert(currCartIndex, printJob);
    }
}

void TixrCartWidget::printResult(PrintManager::StatusCode statusCode, QString statusMsg, PrintResultData printResultData)
{
    int cartIndex;
    PrintItems itemsPrinted = *VariantToPtr<PrintItems>(printResultData["itemsPrinted"]);
    int numPagesPrinted = printResultData["numPagesPrinted"].toInt();
    int printImgNum = -1;
///    QString printToolTip;

    switch (statusCode) {
    case PrintManager::PrintStarted:
        printImgNum = 2;
///        printToolTip = tr("Printing in progress");
        //...

    case PrintManager::PrintResumed:
        //HS TEMP! CHANGE BUTTON STYLE
        pbPrint->setStyleSheet("QPushButton#pbPrint { background-color: #0c0; border: 3px solid #f00; background-position: center; background-repeat: none; background-image: url(:images/btn/Print-icon.png); } ");
        break;

    case PrintManager::PagePrinted:
        break;

    case PrintManager::PrintAborted:
        printImgNum = 2;
///        printToolTip = tr("Print aborted");
        //...

    case PrintManager::NothingToPrint:
/*        if (statusCode == PrintManager::NothingToPrint) {
            printImgNum = 1;
            printToolTip = tr("Print completed");
        }
*/
        //HS TEMP! CHANGE BUTTON STYLE
        pbPrint->setStyleSheet("QPushButton#pbPrint { background-color: #f00; border: 3px solid #fc0; background-position: center; background-repeat: none; background-image: url(:images/btn/Print-icon.png); } ");
        //...

    case PrintManager::PrintFinished:
        printImgNum = 1;
///        printToolTip = tr("Print completed");
        //...

    default:
        cartIndex = printResultData["appData"].toInt();
        QMessageBox::information(this, PrintManager::generalStatusMsg(statusCode),
            #ifdef DEBUG_MODE
//          PrintManager::statusCodeToString(statusCode) + "\n" +
            #endif
            statusMsg);

        // Remove the print job for the cart that this slot pertains to
        cartsPrintJobs.remove(cartIndex);

        // Reactivate print button
        //HS TEMP! CHANGE BUTTON STYLE
        pbPrint->setStyleSheet("QPushButton#pbPrint { background-color: transparent; border: none; background-position: center; background-repeat: none; background-image: url(:images/btn/Print-icon.png); } ");
        break;

    case PrintManager::PrintPaused:
        //HS TEMP! CHANGE BUTTON STYLE
        pbPrint->setStyleSheet("QPushButton#pbPrint { background-color: #fc0; border: 3px solid #f00; background-position: center; background-repeat: none; background-image: url(:images/btn/Print-icon.png); } ");
        break;
    }

    // Change print state images & tooltip status text if need be
    if (printImgNum != -1) {
        // For each (panier) print item, work out how many current pages (i.e. tickets) have been printed from the total
        // number of all pages for all print items printed - this can be deduced as the items are printed in order
        int total = 0;
        foreach (PrintItem printItem, itemsPrinted) {
            QStandardItem *printStateItem = printItem.second;
            int qty = printStateItem->data(DataRole::dcquantite()).toInt();
            int numOrigAlreadyPrinted = printStateItem->data(DataRole::nticketsprinted()).toInt();
            int origNumToPrint = qty - numOrigAlreadyPrinted;
            int numPagesPrintedForItem = numPagesPrinted - total;
            numPagesPrintedForItem = qBound(0, numPagesPrintedForItem, origNumToPrint);
            updatePrintStateItem(printStateItem, printImgNum, statusCode, numOrigAlreadyPrinted + numPagesPrintedForItem);
            total += origNumToPrint;
        }
    }

//  qDebug() << __FUNCTION__ " status code =" << statusCode << " status msg =" << statusMsg;
}

void TixrCartWidget::responseAddToCar(QString respError, PendingRequest *curReq)
{
    if(curReq)
    {
        emit notifyTaskDone(curReq);
    }

    if(!beginHandleResponse(respError, curReq))
    {
        curReq->setResponseItemParent(0);//para liberar el responseItem en TixRealmCore::handleResponse
        return;
    }

    //Agregar los elementos al carro de compras
    QStandardItem *cartItem = tixrCore->stdModel->itemFromIndex(cartView->rootIndex());//El carro de compras activo es el visualizado en el cartView

    QStandardItem *catCliItem = curReq->getResponseItemParent();
    curReq->setResponseItemParent(0);//para liberar el responseItem en TixRealmCore::handleResponse

    QVariant v = catCliItem->data(DataRole::ccprice());
    QVariant v2 = catCliItem->data(Qt::DisplayRole);//Categoría de cliente (libellé de prix)
    int n = 0;//total de productos reservados
    QStandardItem *responseItem = curReq->getResponseItem();
    while(responseItem->hasChildren())
    {
        QStandardItem *pItem = responseItem->takeRow(0).first();
        if(!pItem->data(DataRole::dcquantite()).isValid())// Cantidad de productos, sólo es obligatorio en la respuesta del servidor si n > 1
        {
            pItem->setData(1, DataRole::dcquantite());
            n++;
        }
        else
            n += pItem->data(DataRole::dcquantite()).toInt();
        pItem->setData(v, DataRole::dcprix());//precio del producto en el carro
        pItem->setData(v2, DataRole::dccatcli());//categoría de cliente

        embedProdItem(cartItem, catCliItem, pItem);
    }

    //actualizar la cantidad disponible en la zona
    QStandardItem *zItem = catCliItem->parent()->parent();
    int quant = zItem->data(DataRole::zapquant()).toInt() + n;
    zItem->setData(QVariant(quant), DataRole::zapquant());
    QImage newImg = JsonQStandardItemFactory::genZoneTarImg(zItem->text(), QString::number(quant), zItem->data(DataRole::zapdispo()).toString());
    zItem->setData(QVariant(newImg), Qt::DecorationRole);

    show();
}

void TixrCartWidget::addToCarFromPlan(QList<QStandardItem*> itemsList)
{
    QStandardItem *cartItem = tixrCore->stdModel->itemFromIndex(cartView->rootIndex());//El carro de compras activo es el visualizado en el cartView

    for(int i=0, n=itemsList.size(); i<n; i++)
    {
        cartItem->appendRow( itemsList.at(i) );
    }

    //show();
}

void TixrCartWidget::decodeDataRecursive(QDataStream &stream, QStandardItem *item)
{
    int colCount, childCount;
    stream >> *item;
    stream >> colCount >> childCount;
    item->setColumnCount(colCount);

    int childPos = childCount;

    while(childPos > 0)
    {
        childPos--;
        QStandardItem *child = new QStandardItem;
        decodeDataRecursive(stream, child);
        item->setChild( childPos / colCount, childPos % colCount, child);
    }
}

void TixrCartWidget::handleCartDropEvent(QDropEvent *event)
{
    QWidget *w = static_cast<QWidget*>(event->source());
    QString cn = w->metaObject()->className();
    QString on = w->objectName();
    if(!w || cn!="QTableView" || on!="tvTiers")// Arrastrar un cliente hacia el carrito de compras actual
        return;

    // check if the format is supported (QStandardItemModel::dropMimeData)
    QString format = QLatin1String("application/x-qstandarditemmodeldatalist");
    if(!event->mimeData()->hasFormat(format))//los datos se esperan desde un QStandardItemModel
        return;

    // decode and insert
    QByteArray encoded = event->mimeData()->data(format);
    QDataStream stream(&encoded, QIODevice::ReadOnly);

    QVector<QStandardItem*> items;
    QTableView *tv = static_cast<QTableView*>(w);

    while(!stream.atEnd())
    {
        int r, c;
        QStandardItem *item = new QStandardItem;
        stream >> r >> c;
        decodeDataRecursive(stream, item);

        if(c)
            delete item;
        else
        {
            QStandardItem *tiersItem = tixrCore->stdModel->itemFromIndex(tv->rootIndex().child(r, c));

            item->setData(QVariant(reinterpret_cast<tixrqptr>(tiersItem)), DataRole::tiersptr());
            items.append(item);
        }
    }

    QStandardItem *rootItem = tixrCore->stdModel->itemFromIndex(cartsFView->rootIndex());
    QStandardItem *srcItem = items.at(0);
    items.remove(0);

    // Verificar si ya existe un carro de compras para este cliente
    for(int i=0, n=rootItem->rowCount(); i<n; i++)
    {
        QStandardItem *targetItem = rootItem->child(i);
        if(srcItem->data(DataRole::ikey()) == targetItem->data(DataRole::ikey()))
        {
            cartsFView->showSlide(i);
            event->accept();
            QMessageBox::information(this, tr("Shopping cart"), QObject::tr("A shopping cart already exist for this client."));
            return;
        }
    }

    int idx = cartsFView->indexAt(event->pos()).row();
    if(idx >= 0)//si se suelta el elemento arrastrado sobre uno existente se sobrescribirá el elemento existente
    {
        QStandardItem *targetItem = tixrCore->stdModel->itemFromIndex(cartsFView->rootIndex().child(idx,0));

        QVariant v = srcItem->data(DataRole::tiersptr());

        if(targetItem->data(DataRole::scorderid()).isValid())
        {
            //adjudicar el carro al cliente
            QStandardItem *modelSubItem = new QStandardItem(1,2);
            modelSubItem->setData("AssignCartParams", DataRole::jsonProperty());
            modelSubItem->setData(JsonObject, DataRole::jsonType());

            QStandardItem *modelSubItem2 = new QStandardItem(srcItem->data(DataRole::ikey()).toString());
            modelSubItem2->setData("clientid", DataRole::jsonProperty());
            modelSubItem2->setData(JsonSimple, DataRole::jsonType());
            modelSubItem->setChild(0, 0, modelSubItem2);

            modelSubItem2 = new QStandardItem(targetItem->data(DataRole::scorderid()).toString());
            modelSubItem2->setData("orderid", DataRole::jsonProperty());
            modelSubItem2->setData(JsonSimple, DataRole::jsonType());
            modelSubItem->setChild(0, 1, modelSubItem2);

            targetItem->appendRow(modelSubItem);

            modelSubItem->setData(v, DataRole::auxptr1());

            tixrCore->setPgFunction("main.assigncart");
            PendingRequest *pReq = tixrCore->sendMsg(modelSubItem, HTTP_METHOD_ID::POST, "/api", this, "responseAssignCart");
            curTasksList[cartsSW->currentIndex()].append(pReq);

            event->accept();
            return;
        }

        targetItem->setText(srcItem->data(DataRole::qtdisplayrole()).toString() + " " + srcItem->data(DataRole::tnom()).toString());
        targetItem->setData(srcItem->data(DataRole::ikey()), DataRole::ikey());
        targetItem->setData(v, DataRole::tiersptr());
        delete srcItem;

        QStandardItem *tiersItem = reinterpret_cast<QStandardItem*>(tixrqptrFromVariant(v));
        qreal b = tiersItem->parent()->child(tiersItem->row(), DataColumn::taccountcol())->data(Qt::DisplayRole).toDouble();
        dsbCurBalance->setValue(b);
        dsbFinBalance->setValue(b - dsbRestantDu->value());

        if(b>0)
            dsbRestantDu->setValue(dsbRestantDu->value() - b);

        for(int i=0, n=items.size(); i<n; i++)
        {
            createCart(items.at(i));
        }
    }
    else
        for(int i=0, n=items.size(); i<n; i++)
        {
            createCart(items.at(i));
        }

    cartsFView->render();
    event->accept();
}

void TixrCartWidget::responseAssignCart(QString respError, PendingRequest *curReq)
{
    curReq->setResponseItemParent(0);//para liberar el responseItem en TixRealmCore::handleResponse

    if(!beginHandleResponse(respError, curReq))
    {
        return;
    }

    QStandardItem *requestItem = curReq->getRequestItem();

    QVariant v;
    QStandardItem *tItm = reinterpret_cast<QStandardItem*>( tixrqptrFromVariant(v = requestItem->data(DataRole::auxptr1())) ),
                  *cartItm = requestItem->parent();

    cartItm->setText(tItm->data(DataRole::qtdisplayrole()).toString() + " " + tItm->data(DataRole::tnom()).toString());
    cartItm->setData(tItm->data(DataRole::ikey()), DataRole::ikey());
    QStandardItem *oldTiersItm = reinterpret_cast<QStandardItem*>(tixrqptrFromVariant(cartItm->data(DataRole::tiersptr())));
    cartItm->setData(v, DataRole::tiersptr());

    //actualizar el saldo del cliente
    /*QList<QStandardItem*> pList = getProds(true);
    qreal C = 0.;
    for(int i=0, n=pList.size(); i<n; i++)
    {
        QStandardItem *pItm = pList.at(i);
        C += pItm->data(TixrQDataRole::dcprix()).toDouble() * pItm->data(TixrQDataRole::dcquantite()).toInt();
    }

    QStandardItem *paymsRoot = cartItm->child(1);
    qreal P = 0;
    for(int i=0, n=paymsRoot->rowCount(); i<n; i++)
    {
        QVariant v;
        if(!((v = paymsRoot->child(i)->data(TixrQDataRole::pdtcanceled())).isValid() && v.toBool()))
            P += paymsRoot->child(i,TixrDataCol::pdtamountcol())->data(Qt::DisplayRole).toDouble();
    }*/

    QWidget *w = cartsSW->widget(cartItm->row());
    QDoubleSpinBox *dsb = w->findChild<QDoubleSpinBox*>("dsbCurBalance");

    if(oldTiersItm)
    {
        //actualizar el saldo del antiguo dueño del carrito
        QStandardItem *accountItm = oldTiersItm->parent()->child(oldTiersItm->row(), DataColumn::taccountcol());
        qreal b = accountItm->data(Qt::DisplayRole).toDouble();
        accountItm->setData(b - dsb->value(), Qt::DisplayRole);
    }

    QStandardItem *accountItm = tItm->parent()->child(tItm->row(), DataColumn::taccountcol());
    qreal b = accountItm->data(Qt::DisplayRole).toDouble();
    dsb->setValue(dsb->value()+b);

    accountItm->setData(dsb->value(), Qt::DisplayRole);

    QDoubleSpinBox *dsb2 = w->findChild<QDoubleSpinBox*>("dsbFinBalance"),
                   *dsb3 = w->findChild<QDoubleSpinBox*>("dsbRestantDu");
    dsb2->setValue(dsb2->value() - dsb3->value());
    if(b>0)
    {
        dsb3->setValue(dsb3->value() - b);
    }

    cartsFView->render();
}

void TixrCartWidget::on_fvModePaiem_clicked(const QModelIndex &index)
{

    qDebug() << "mode paym = " << tixrCore->stdModel->data(index).toString();
}

void TixrCartWidget::handleAddCart(QStandardItem *newCartItm)
{
    //localizar el primer carrito libre para reutilizarlo, si no existe se crea uno nuevo
    QStandardItem *cartItm;
    int i, n;
    for(i=0, n=cartsItem->rowCount(); i<n; i++)
    {
        if(isEmpty(cartItm = cartsItem->child(i)) && (!cartItm->data(DataRole::ikey()).isValid() ||\
                                                      !cartItm->data(DataRole::ikey()).isValid()) &&\
          (!cartItm->data(DataRole::scorderid()).isValid() || !cartItm->data(DataRole::scorderid()).isValid()))
            break;
    }

    if(i<n)
        assignCart(cartItm, newCartItm);
    else
        createCart(newCartItm);

    cartsFView->setCurrentIndex(newCartItm->index());
    changeCurrentCart(newCartItm->index());

    show();
}

void TixrCartWidget::handleFindCart(int clientid, QStandardItem *cartRoot)
{
    for(int i=0, n=cartsItem->rowCount(); i<n; i++)
    {
        QStandardItem *targetItem = cartsItem->child(i);
        if(clientid == targetItem->data(DataRole::ikey()).toInt())
        {
            emit cartRequestResult(true, cartRoot);
            return;
        }
    }

    emit cartRequestResult(false, cartRoot);
}

void TixrCartWidget::embedProdItem(QStandardItem *cartItm, QStandardItem *catCliItm, QStandardItem *pItm)
{
    if(!catCliItm)
        return;

    QList<QStandardItem*> parentsList;

    QStandardItem *padre = catCliItm;

    //formar la lista de ancestros hasta catCliItm
    while(padre)
    {
        parentsList.insert(0, padre);

        padre = padre->parent();
        if(padre)
            padre = padre->parent();
    }

    if(parentsList.isEmpty())//esto nunca debe ocurrir
    {
        cartItm->appendRow(pItm);
        return;
    }

    QStandardItem *r = cartItm;
    int i = 0, n, dn = pItm->data(DataRole::dcquantite()).toInt();

    while(!(i>=r->rowCount() || parentsList.isEmpty()))
    {
        QStandardItem *child = r->child(i);
        QStandardItem *stockItm = reinterpret_cast<QStandardItem*>( tixrqptrFromVariant(child->data(DataRole::stockitemptr())) );
        QStandardItem *prodClassItm = parentsList.first();
        if(stockItm == prodClassItm || child->data(DataRole::iuid()) == prodClassItm->data(DataRole::iuid()))
        {
            r = child;
            parentsList.removeFirst();
            i = 0;

            if(!stockItm)
            {
                child->setData(QVariant(reinterpret_cast<tixrqptr>(prodClassItm)), DataRole::stockitemptr());
            }
        }
        else
            i++;
    }

    if(parentsList.isEmpty())//ya existía en el carrito el camino completo de los padres del nuevo elemento
    {
        QStandardItem *pItm2 = 0;
        for(i=0, n=r->rowCount(); i<n; i++)
        {
            QStandardItem *tmp = r->child(i);
            int id1 = tmp->data(DataRole::ikey()).toInt();
            int id2 = pItm->data(DataRole::ikey()).toInt();
            qreal p1 = tmp->data(DataRole::dcprix()).toDouble();
            qreal p2 = pItm->data(DataRole::dcprix()).toDouble();
            QString s1 = tmp->data(DataRole::pdescrip()).toString();//tmp->text();
            QString s2 = pItm->text();
            if((id1==id2) && (p1==p2) && (s1==s2))
            {
                pItm2 = tmp;
                break;
            }
        }

        QStandardItem *ticketItm;
        PrintManager::StatusCode printStatus;
        int printImgNum, numPrinted, qty;

        if(pItm2)
        {
            pItm2->setData(pItm2->data(DataRole::dcquantite()).toInt()+dn, DataRole::dcquantite());//incrementar la cantidad
            delete pItm;
            pItm = pItm2;
            QStandardItem *payedItm = r->child(pItm->row(), DataColumn::dcpayedcol());
            qreal tp = payedItm->data(Qt::DisplayRole).toDouble();
            QColor c = tp?(tp<(pItm->data(DataRole::dcprix()).toDouble()*pItm->data(DataRole::dcquantite()).toInt())?Qt::yellow:Qt::green):Qt::red;
            payedItm->setData(c, Qt::DecorationRole);

            // Set print status to not finished if it wasn't set as not printed now that another item has been appended
            ticketItm = r->child(pItm->row(), DataColumn::dcticketscol());
            printStatus = static_cast<PrintManager::StatusCode>(ticketItm->data(DataRole::printstatuscode()).toInt());
            numPrinted = -1;    // Signifies don't change current setting
            if(printStatus != PrintManager::PrintNotStarted) {
                printImgNum = 2;
                printStatus = PrintManager::PrintNotFinished;
            }
            else
                printImgNum = -1; // Signifies don't change current setting
        }
        else
        {
            QStandardItem *payedItm = new QStandardItem;
            payedItm->setData(0, Qt::DisplayRole);
            payedItm->setData(QColor(Qt::red), Qt::DecorationRole);
            ticketItm = new QStandardItem;
            printImgNum = 3;
            printStatus = PrintManager::PrintNotStarted;
            numPrinted = 0;
            r->appendRow(QList<QStandardItem*>()<<pItm<<payedItm<<ticketItm);
        }
        qty = pItm->data(DataRole::dcquantite()).toInt();
        updatePrintStateItem(ticketItm, printImgNum, printStatus, numPrinted, qty);
    }
    else
    {
        for(i=0, n=parentsList.size(); i<n; i++)
        {
            QStandardItem *stockItm = parentsList.at(i);

            QStandardItem *newItm = new QStandardItem(stockItm->text());
            newItm->setData(QVariant(reinterpret_cast<tixrqptr>(stockItm)), DataRole::stockitemptr());
            newItm->setData(stockItm->data(DataRole::iuid()), DataRole::iuid());
            r->appendRow(newItm);
            r = newItm;
        }

        QStandardItem *payedItm = new QStandardItem;
        payedItm->setData(0, Qt::DisplayRole);
        payedItm->setData(QColor(Qt::red), Qt::DecorationRole);
        QStandardItem *ticketItm = new QStandardItem;
        int qty = pItm->data(DataRole::dcquantite()).toInt();
        updatePrintStateItem(ticketItm, 3, PrintManager::PrintNotStarted, 0, qty);
        r->appendRow(QList<QStandardItem*>()<<pItm<<payedItm<<ticketItm);
    }

    //decorar
    qreal prix = pItm->data(DataRole::dcprix()).toDouble();
    n = pItm->data(DataRole::dcquantite()).toInt();
    QString itLbl = pItm->data(DataRole::pdescrip()).toString() + QString(" [%1€ x %2 -> %3]").arg(tixrCore->curLocal.toString(prix), QString::number(n), tixrCore->curLocal.toString(n*prix));
    pItm->setText(itLbl);

    pItm->setData(QVariant(reinterpret_cast<tixrqptr>(catCliItm)), DataRole::catcliptr());

    //actualizar los totales
    qreal total = dn * prix;
    totalDSB->setValue(totalDSB->value() + total);

    dsbFinBalance->setValue(dsbFinBalance->value() - total);

    dsbRestantDu->setStyleSheet(dsbRestantDuStyleSheet + (total?"color:red;":"color:black;"));
    dsbRestantDu->setValue(-dsbFinBalance->value());

    total = dsbRestantDu->value();

    dsbAmount->setMaximum(qMax(total,0.));
    dsbAmount->setValue(total);

    pbRecordPay->setEnabled(total);
    pbAjourner->setEnabled(total);
}

/**
 * @brief TixrCartWidget::updatePrintStateItem
 * @param item
 * @param printImgNum -1 signifies don't change current setting
 * @param statusCode
 * @param numPrinted -1 signifies don't change current setting
 * @param qty -1 signifies don't change current setting
 */
void TixrCartWidget::updatePrintStateItem(QStandardItem *item, int printImgNum, PrintManager::StatusCode statusCode, int numPrinted, int qty)
{
    QString toolTip = PrintManager::generalStatusMsg(statusCode);
    item->setToolTip(toolTip);
    if (qty != -1)
        item->setData(qty, DataRole::dcquantite());         // Number of tickets for this details commande entry in the panier
    else
        qty = item->data(DataRole::dcquantite()).toInt();
    if (numPrinted != -1)
        item->setData(numPrinted, DataRole::nticketsprinted());  // Number of tickets already printed
    else
        numPrinted = item->data(DataRole::nticketsprinted()).toInt();
    if (printImgNum != -1) {
        // Force print image to be 'partially printed' if more tickets remain to be printed. This could happen if items
        // are added to a details command entry in the panier that has already been paid for subsequently get printed.
        if (printImgNum == 1 && numPrinted < qty)
            printImgNum = 2;
        QString printImg = ":/images/btn/etatbillet" % QString::number(printImgNum) % ".png";
        item->setData(QImage(printImg), Qt::DecorationRole);
    }
    item->setText(QString("%1 / %2").arg(numPrinted).arg(qty));
    item->setData(statusCode, DataRole::printstatuscode());      // Print state
}

void TixrCartWidget::cartViewSelectionChanged(const QItemSelection & /*selected*/, const QItemSelection & /*deselected*/)
{
    QList<QStandardItem*> prodsLis = getProds();

    qreal total = 0;
    for(int i=0, n=prodsLis.size(); i<n; i++)
    {
        QStandardItem *pItem = prodsLis.at(i);
        QVariant v = pItem->data(DataRole::dcquantite());
        int N = v.isValid() ? v.toInt() : 1;
        total += pItem->data(DataRole::dcprix()).toDouble() * N;
    }

    sousTotalDSB->setValue(total);
}

QList<QStandardItem*> TixrCartWidget::getProds(bool forgetSelection, QStandardItem *cItm)
{
    QList<QStandardItem*> leafsList;
    if(cItm)
        forgetSelection = true;

    if(cartView->selectionModel()->hasSelection() && !forgetSelection)
    {
        QModelIndexList mIdxList = cartView->selectionModel()->selectedIndexes();
        for(int i=0, n=mIdxList.size(); i<n; i++)
        {
            QModelIndex idx = mIdxList.at(i);
            QStandardItem *pItm = tixrCore->stdModel->itemFromIndex(idx.sibling(idx.row(), 0));
            if(!leafsList.contains(pItm))
                leafsList << pItm;
        }
    }
    else
    {
        QStandardItem *r = cItm?cItm:tixrCore->stdModel->itemFromIndex(cartView->rootIndex());
        for(int i=0, n=r?r->rowCount():0; i<n; i++)
        {
            leafsList << r->child(i);
        }
    }

    for(int idx=0; idx<leafsList.size();)
    {
        QStandardItem *item = leafsList.at(idx);
        if(!item->hasChildren())
        {
            idx++;
            continue;
        }

        item = leafsList.takeAt(idx);
        int i = 0;
        for(int j=0, m=item->rowCount(); j<m; j++)
        {
            QStandardItem *pItm = item->child(j);
            if(!leafsList.contains(pItm))
                leafsList.insert(idx+(i++), pItm);
        }
    }

    return leafsList;
}

PrintItems TixrCartWidget::getPrintableProducts()
{
    // Checks panier ids not empty (ids are empty for products that have not been paid or had their payment validated)
    PrintItems printItems;
    QList<QStandardItem*> cartItems = getProds();
    foreach (QStandardItem* cartItem, cartItems) {
        QString id = cartItem->data(DataRole::dciddetails()).toString(); // main.detailscommande.iddetailscommande primary key
        if (!id.isEmpty()) {
            QModelIndex cartItemIdx = cartItem->index();
            QModelIndex printStateItemIdx = cartItemIdx.sibling(0, 2);
            QStandardItem *printStateItem = tixrCore->stdModel->itemFromIndex(printStateItemIdx);
            printItems.append(PrintItem(id, printStateItem));
        }
    }
    return printItems;
}
//---------------------------------------------------------------------------------------------------------
