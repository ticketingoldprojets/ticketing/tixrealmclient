/*! \mainpage Description
 * <I>TixRealmClient</I> est une application cliente pour l'application serveur <A HREF="../../SimpleCLICserver/SimpleCLICserver.pdf">SimpleCLICserver</A> capable de gérer plusieurs fils d'exécution et charger dynamiquement son interface graphique à partir d'un fichier de définition de \a skin.
 *
 * \section struct_sec Structure de données
 *
 * La plupart des données sont stockées dans un QStandardItemModel en mode de hiérarchie, ce model correspond à l'object TixRealmCore::stdModel. Chaque type de donnée est stockée sous une branche de l'arbre et chaque branche est identifiée par un nom unique. La correspondande entre le nom et la branche se trouve dans TixRealmCore::keyToItemMap.
 * L'image suivante montre la structure:
 * \image html structure_donnee.jpeg
 * \image latex structure_donnee.jpeg
 * C'est à dire que pour avoir un pointer à la racine de produits billetterie, par exemple, il faudrait faire:
 * \code{.cpp}
 *      QStandardItem *r = keyToItemMap->value("stock");
 * \endcode
 *
 * \section req_sec Envoie de requêtes
 * L'envoie de requêtes au serveur \a SimpleCLICserver se fait via la fonction TixRealmCore::sendMsg. Le contenu du message est représenté comme une hiérarchie de QStandardItem. Il existe 3 façons de générer cette structure, à savoir: \ref gen_man_sub, via \ref wTm_sub ou par \ref mix_sub.
 * \subsection gen_man_sub Génération manuelle de la hiérarchie
 * Dans une hiérarchie QStandardItem qui représente un message \a XML la relation père-fils émule la subordination des entités \a XML. Le type d'entité est contenue dans le rôle TixrQDataRole::xmlType et correspond à une des valeurs ::CmdXmlElemType. Le nom de l'entité \a XML est contenue dans le rôle Qt::AccessibleTextRole et la valeur dans Qt::DisplayRole.
 * Par exemple, les lignes de codes suivantes:
 * \code{.cpp}
 * QStandardItem *modelItem = new QStandardItem(1, 2);
 * modelItem->setAccessibleText("Command");
 * modelItem->setData(CmdXmlElem, TixrQDataRole::xmlType());
 *
 * QStandardItem *modelSubItem = new QStandardItem("main.getallprodsforcat");
 * modelSubItem->setAccessibleText("name");
 * modelSubItem->setData(CmdXmlAtt, TixrQDataRole::xmlType());
 * modelItem->setChild(0, 0, modelSubItem);
 *
 * modelSubItem = new QStandardItem(1,1);
 * modelSubItem->setAccessibleText("ItemId");
 * modelSubItem->setData(CmdXmlElem, TixrQDataRole::xmlType());
 * modelItem->setChild(0, 1, modelSubItem);
 *
 * QString idCatProdStr = tixrCore->stdModel->data(index, TixrQDataRole::ikey()).toString();
 * QStandardItem *modelSubItem2 = new QStandardItem(idCatProdStr);
 * modelSubItem2->setAccessibleText("srvikey");
 * modelSubItem2->setData(CmdXmlAtt, TixrQDataRole::xmlType());
 * modelSubItem->setChild(0, 0, modelSubItem2);
 * \endcode
 * Correspondent à l'envoie du fichier \a XML suivant:
 * \code{.xml}
 * <?xml version="1.0" encoding="UTF8"?>
 * <Command name="main.getallprodsforcat">
 *   <ItemId srvikey="1"/>
 * </Command>
 * \endcode
 * Cette façon de générer la hiérarchie est plus efficiente quand il y a des instructions de boucle.
 *
 * \subsection wTm_sub Usage de TixRealmCore::widgetToModel
 * Il est possible aussi de coder le message dans un fichier d'interface graphique Qt, dans ce cas la relation conteneur-contenu entre les composants visuels émule la subordination des entités \a XML. Dans le widget racine, pour le nom de la commande on utilise la propriété \b accessibleDescription et pour la methode \a HTTP et le chemin du serveur la propriété \b accessibleName.
 * Dans les widgets fils le nom des éléments \a XML se trouve dans \b accessibleName et le nom de l'attribut dans \b accessibleDescription. Le type de la valeur de l'attribut est inféré par le type de widget. Pour plus de reinseigement voir TixRealmCore::widgetToModel. Pour des exemple voir les définitions d'interface graphiques D:/SkinsBuilder/Skin0/formconfig/newSpect.ui.
 *
 * \subsection mix_sub Modification du resultat de TixRealmCore::widgetToModel
 * Il y a de cas particuliers où la structure générée par TixRealmCore::widgetToModel ne contient pas toute l'information nécessaire. Le fragment de code suivant montre l'utilisation des deux méthode précédants pour créer la hiérarchie.
 * \code{.cpp}
 *  QStandardItem *newModelItem = tixrCore->widgetToModel(scDynWidget);
 *
 *  QString idFam = rootItem->parent()->data(TixrQDataRole::ikey()).toString();
 *  QStandardItem *idFamItem = new QStandardItem(idFam);
 *  idFamItem->setAccessibleText("fam");
 *  idFamItem->setData(CmdXmlAtt, TixrQDataRole::xmlType());
 *
 *  QStandardItem *childItem = newModelItem->child(0)->data(TixrQDataRole::xmlType())==CmdXmlAtt?newModelItem->child(1):newModelItem->child(0);
 *  childItem->appendRow(idFamItem);
 * \endcode
 *
 * \section rep_sec Traitement des réponses
 * Les réponses du serveur sont réçues en forme de fichiers \a XML. Dans la <I>couche modèle</I> les fonctions ConnDetail::xmlToModel et ConnDetail::xmlToGrilleModel sont chargées de faire la transformation en hiérarchie QStandardItem.
 * Chaque élément \a XML donnera lieu à un rang d'objets QStandardItem, combien d'objets et sa disposition dépend du nom des attributs de l'élément \a XML.
 * Le nom des attributs suit le schéma suivant: <B>[\<COLONNE\>_]\<ROLE\></B> où
 * - \b ROLE est le nom d'une valeur de la classe TixrQDataRole.
 * - \b COLONNE est optionnelle et correspond au nom d'une valeur de la classe TixrDataCol.
 *
 * Le nom de l'attribut indique où il faut placer la valeur qu'il porte: la colonne de l'objet QStandardItem à générer est calculer comme <B>TixrDataCol::FromName(COLONNE)</B> si la valeur \b COLONNE n'existe pas la valeur 0 est utilisée. Le rôle de la valeur se calcule comme <B>TixrQDataRole::FromName(ROLE)</B>.
 * Voici un exemple d'une réponse valide du serveur \a SimpleCLICserver:
 * \code{.xml}
 * <?xml version="1.0" encoding="UTF-8"?>
 * <Response info="cmd=getallbanques; method=POST; path=/">
 *   <Banque ikey="1" gabidp="1" qtdisplayrole="La Banque postale" bpayscol_qtdisplayrole="France"/>
 *   <Banque ikey="2" gabidp="1" qtdisplayrole="Groupe AXA" bpayscol_qtdisplayrole="France"/>
 *   <Banque ikey="4" gabidp="1" qtdisplayrole="Crédit agricole" bpayscol_qtdisplayrole="France"/>
 *   <Banque ikey="3" gabidp="1" qtdisplayrole="BNP Paribas" bpayscol_qtdisplayrole="France"/>
 *   <Banque ikey="5" gabidp="1" qtdisplayrole="Société générale" bpayscol_qtdisplayrole="France"/>
 *   <Banque ikey="6" gabidp="1" qtdisplayrole="Groupe BPCE" bpayscol_qtdisplayrole="France"/>
 *   <Banque ikey="7" gabidp="1" qtdisplayrole="Groupama" bpayscol_qtdisplayrole="France"/>
 *   <Banque ikey="8" gabidp="1" qtdisplayrole="Crédit mutuel-CIC" bpayscol_qtdisplayrole="France"/>
 *   <Header colindex="1" collabel="Nom"/>
 *   <Header colindex="2" collabel="Pays"/>
 * </Response>
 * \endcode
 * Avec cette réponse on génère 8 rang de QStandardItem de 2 colonne chacune. La première colonne aura la position 0 et la deuxième la position TixrDataCol::FromName("bpayscol"). L'objet QStandardItem de la première colonne aura une valeur définie dans les rôle TixrQDataRole::FromName("ikey"), TixrQDataRole::FromName("gabidp") et Qt::DisplayRole; l'objet QStandardItem de la deuxième colonne n'aura de valeur définie que dans le rôle Qt::DisplayRole. TixrQDataRole::FromName("qtdisplayrole") c'est une alias pour Qt::DisplayRole.
 * Dans cet exemple les élément <B>\<Header/\></B> contiennent l'information des en-têtes, voir la documentation du serveur pour plus de détails sur la génération des en-têtes. La sous-section suivante explique comment utiliser les en-têtes.
 *
 * \subsection head_sub Gestion des en-têtes
 * Quand les réponses du serveur contiennent des élément <B>\<Header/\></B>, comme dans l'exemple précédant, la <I>couche modèle</I> construit un object de type QStandardItemModel contenant les en-têtes de la réponse. Cet objet est accessible avec la fonction ClientConnThread::headersModel, l'exemple suivant montre son utilisation.
 * \code{.cpp}
 * QStandardItemModel * hModel = curThread ? curThread->headersModel() : tixrCore->itemToHeaderModelMap.value(responseItem);
 * if(hModel)
 * {
 *   this->tixrCore->itemToHeaderModelMap.insert(responseItem, hModel);
 *   this->tvPayments->horizontalHeader()->setModel(hModel);
 * }
 * \endcode
 *
 * \section gui_sec Interface graphique
 * \a TixRealmClient gère son interface graphique dans un fichier à part appelé \a skin contenant les widgets, images et une feuille de style pour la mise en page et la décoration.
 * Le \a skin est chargé au démarrage par TixRealmCore::applyStyle, une fois en exécution il est possible de changer l'interface graphique en changeant de \a skin.
 * \todo Le système de gestion de \a skin n'est pas encore fini, actuellement les widgets déjà visibles ne sont pas affectés par le changement du \a skin.
 * \subsection skin_sub Edition et création de nouveau \a skins
 * Les fichiers sources pour les \a skin se trouvent dans le répertoire <B>D:\\SkinsBuilder</B>. Le sous-répertoire \b Skin0 contient le sources pour le \a skin par défaut, celui fourni dans l'exécutable <I>TixRealmClient.exe</I>.
 * \b Skin0 est le \a skin de référence, des nouveaux \a skins peuvent être crées à partant de celui-ci
 *
 * \section dyn_propty Index of Qt Dynamic Properties in use:
 * - \b tixrJsonKey defined in a .ui files mark the value of this widget as serializable by widgetToJson function and value of this property is the key in the resulting Json Object.
 * - \b tixrNSeat used in Site2dView to save number of selected seats, or products.
 * - \b tixrPageIdx is the tabwidget or stackedwidget page index. This property is normally set in a pushbutton or toolbutton to enable navigation.
 * - \b tixrPageHolder name of a tabwidget or stackedwidget widget.
 * - \b tixrStockViewLevel in a hierarchical view of stock this is the level of product's category.
 * - \b tixrStockViewType type of stock view, type of widget (list, table, coverflow...)
 * - \b tixrHeaderOrientation Orientation of a QHeader. Used in GrillesAdminPage to edit header's properties.
 * - \b tixrHeaderIndex Index of horizontal or vertical header.
 * - \b tixrRelevantRole used to declare the to serialize to the request data insted of the normal DisplayRole.
 * /
