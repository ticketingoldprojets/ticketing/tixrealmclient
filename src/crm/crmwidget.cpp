#include "crmwidget.h"
#include "newtierswidget.h"
#include "../mainwindow.h"
//-----------------------------------------------------------------------------
TixrCrmWidget::TixrCrmWidget(TixRealmCore *mObj, QWidget *parent) :
    TixrWidget("crmPage", mObj, "crmbtn.png", parent)
{
    this->tixrWidgetType = TIXR_CRM;
    if(!this->getErrorMsg().isEmpty())
        return;

    this->parentTabWidget->addTab(this, windowIcon(), tr("crm"));

    this->tvTiers = this->findChild<QTableView *>("tvTiers");
    this->tvTiersSearch = this->findChild<QTableView *>("tvTiersSearch");
    this->leTiersSearch = this->findChild<QLineEdit *>("leTiersSearch");
    this->tiersRoot = NULL;

    if(!this->tixrCore->keyToItemMap.contains("civilites"))
    {
        this->tixrCore->setPgFunction("main.getcivilites");
        PendingRequest *pReq = this->tixrCore->sendMsg(this->tixrCore->stdModel->invisibleRootItem(), HTTP_METHOD_ID::GET, "/api", this, "handleSimpleResponse");
        pReq->setRespDataMapKey("civilites");
        this->currentTaskList << pReq;
    }

    if(!this->tixrCore->keyToItemMap.contains("typetiers"))
    {
        this->tixrCore->setPgFunction("main.gettypetiers");
        PendingRequest *pReq = this->tixrCore->sendMsg(this->tixrCore->stdModel->invisibleRootItem(), HTTP_METHOD_ID::GET, "/api", this, "handleSimpleResponse");
        pReq->setRespDataMapKey("typetiers");
        this->currentTaskList << pReq;
    }

    if(!this->tixrCore->keyToItemMap.contains("pays"))
    {
        this->tixrCore->setPgFunction("main.getpays");
        PendingRequest *pReq = this->tixrCore->sendMsg(this->tixrCore->stdModel->invisibleRootItem(), HTTP_METHOD_ID::GET, "/api", this, "handleSimpleResponse");
        pReq->setRespDataMapKey("pays");
        this->currentTaskList << pReq;
    }

    if(this->tixrCore->keyToItemMap.contains("crm"))
    {
        QStandardItem *crmItem = this->tixrCore->keyToItemMap["crm"];
        this->tvTiers->setModel(this->tixrCore->stdModel);
        this->tvTiers->setRootIndex(crmItem->index());

        this->setTiersRoot(crmItem);
    }
    else
    {
        this->tixrCore->setPgFunction("main.gettiers");
        PendingRequest *pReq = this->tixrCore->sendMsg(this->tixrCore->stdModel->invisibleRootItem(), HTTP_METHOD_ID::GET, "/api", this, "responseGetCustomers");
        pReq->setRespDataMapKey("crm");
        this->currentTaskList << pReq;
    }

    MainWindow::tixrConnectSlotsByName(this);
}

void TixrCrmWidget::handleSimpleResponse(QString respError, PendingRequest *curReq)
{
    if(!beginHandleResponse(respError, curReq))
        return;

    endHandleResponse(curReq);
}

void TixrCrmWidget::responseDelCustomer(QString respError, PendingRequest *curReq)
{
    if(!beginHandleResponse(respError, curReq))
        return;

    QStandardItem *requestItem = curReq->getRequestItem();
    int row = requestItem->data(DataRole::auxptr1()).toInt();
    QStandardItem *currItem = this->tixrCore->keyToItemMap.value("crm")->child(row);
    if(currItem->data(DataRole::indicator()).isValid())
    {
        TixrNewTiersWidget *tabWidget = reinterpret_cast<TixrNewTiersWidget*>( tixrqptrFromVariant(currItem->data(DataRole::indicator())) );
        this->parentTabWidget->removeTab(this->parentTabWidget->indexOf(tabWidget));
    }

    this->tixrCore->keyToItemMap.value("crm")->removeRow(row);
    this->tvTiers->setModel(this->tixrCore->stdModel);
    this->tvTiers->setRootIndex(this->tixrCore->keyToItemMap.value("crm")->index());

    endHandleResponse(curReq);
}

void TixrCrmWidget::responseGetCustomers(QString respError, PendingRequest *curReq)
{
    if(!beginHandleResponse(respError, curReq))
        return;

    QStandardItem *responseItem = curReq->getResponseItem();

    /*if(requestItem && curReq) //incorporar la respuesta al modelo, sólo si es una respuesta del servidor
    {
        for(int i = 0, countTiers = responseItem->rowCount(); i < countTiers; i++)
        {
            QStandardItem *commentItm = responseItem->child(i, DataColumn::tcommentairecol());
            if(commentItm)
                commentItm->setForeground(responseItem->child(i)->data(DataRole::tprioritycomment()).toInt() ? Qt::red : Qt::white);
        }
    }*/

    this->tvTiers->setModel(this->tixrCore->stdModel);
    this->tvTiers->setRootIndex(responseItem->index());

    this->setTiersRoot(responseItem);

    endHandleResponse(curReq);
}

void TixrCrmWidget::on_pbNouveauTiers_clicked()
{
    TixrNewTiersWidget *newTiers = new TixrNewTiersWidget(this->tixrCore, NULL, this->parentTabWidget);
    QString e = newTiers->getErrorMsg();
    if(!e.isEmpty())
    {
        delete newTiers;
        QMessageBox::information(NULL, qApp->applicationName(), tr(e.toUtf8().constData()));
    }
}

void TixrCrmWidget::on_pbDelTiers_clicked()
{
    QModelIndex index = this->tvTiers->currentIndex();
    if(!index.isValid())
        return;

    if(QMessageBox::question(this, QStringLiteral("Remove client"), QStringLiteral("Do you really want to remove this client record")) == QMessageBox::No)
        return;

    QStandardItem *modelSubItem = new QStandardItem(1, 1);
    modelSubItem->setData("ItemId", DataRole::jsonProperty());
    modelSubItem->setData(JsonObject, DataRole::jsonType());

    modelSubItem->setData(index.row(), DataRole::auxptr1());

    QStandardItem *modelSubItem2 = new QStandardItem(index.sibling(index.row(), 0).data(DataRole::ikey()).toString());
    modelSubItem2->setData("srvikey", DataRole::jsonProperty());
    modelSubItem2->setData(JsonSimple, DataRole::jsonType());
    modelSubItem->setChild(0, 0, modelSubItem2);

    this->tixrCore->setPgFunction("main.deltiers");
    PendingRequest *pReq = this->tixrCore->sendMsg(modelSubItem, HTTP_METHOD_ID::POST, "/api", this, "responseDelCustomer");
    this->currentTaskList << pReq;
    pReq->isResponseExpected = false;// main.deltiers is a VOID PostgreSQL function
}

void TixrCrmWidget::on_tvTiers_doubleClicked(const QModelIndex & index)
{
    TixrNewTiersWidget * tabWidget = NULL;
    QStandardItem * currItem = this->tixrCore->stdModel->itemFromIndex(index.sibling(index.row(), 0));
    if(currItem->data(DataRole::indicator()).isValid())
    {
        tabWidget = reinterpret_cast<TixrNewTiersWidget*>( tixrqptrFromVariant(currItem->data(DataRole::indicator())) );
    }
    if(this->parentTabWidget->indexOf(tabWidget) != -1)
    {
        this->parentTabWidget->setCurrentWidget(tabWidget);
        return;
    }

    TixrNewTiersWidget *newTiers = new TixrNewTiersWidget(this->tixrCore, this->tixrCore->stdModel->itemFromIndex(index), this->parentTabWidget, currItem);
    QString e = newTiers->getErrorMsg();
    if(!e.isEmpty())
    {
        delete newTiers;
        QMessageBox::information(NULL, qApp->applicationName(), tr(e.toUtf8().constData()));
    }
}

void TixrCrmWidget::setTiersRoot(QStandardItem *rootItem)
{
    if(this->tiersRoot = rootItem)
    {
        this->proxyModel = new QSortFilterProxyModel();
        this->proxyModel->setDynamicSortFilter(true);
        this->proxyModel->setFilterKeyColumn(1);
        this->proxyModel->setSourceModel(this->tiersRoot->model());

        this->tvTiersSearch->setModel(this->proxyModel);
        this->tvTiersSearch->setRootIndex(proxyModel->mapFromSource(this->tiersRoot->index()));
    }
}

void TixrCrmWidget::on_leTiersSearch_textChanged(const QString &)
{
    this->proxyModel->setFilterRegExp(QRegExp(this->leTiersSearch->text(), Qt::CaseInsensitive));
}
