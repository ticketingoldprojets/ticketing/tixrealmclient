#include "tiersfinderdialog.h"
#include "../cartwidget.h"
#include "../mainwindow.h"
#include "../newpaymentdialog.h"

TixrTiersFinderDialog::TixrTiersFinderDialog(QWidget *blockedWidget, QWidget *parent) :
    TixrDialog(":/formconfig/tiersfinder.ui", blockedWidget, parent)
{
    tiersRoot = 0;
    tiersTV = findChild<QTableView*>("tiersTV");
    lineEdit = findChild<QLineEdit*>("lineEdit");
    MainWindow::tixrConnectSlotsByName(this);
}

void TixrTiersFinderDialog::setTiersRoot(QStandardItem *rootItem)
{
    tiersRoot = rootItem;
    if(tiersRoot)
    {
        proxyModel = new TixrSortFilterProxyModel(this);
        proxyModel->setFilterKeyColumn(-1);
        proxyModel->setRootItem(tiersRoot);
        proxyModel->setSourceModel(tiersRoot->model());

        tiersTV->setModel(proxyModel);
        tiersTV->setRootIndex(proxyModel->mapFromSource(tiersRoot->index()));
    }
}

QStandardItem *TixrTiersFinderDialog::selectedTiers()
{
    QModelIndexList idxList = tiersTV->selectionModel()->selectedIndexes();
    return idxList.isEmpty()?0:\
            static_cast<QStandardItemModel*>(tiersRoot->model())->itemFromIndex(proxyModel->mapToSource(idxList.first()));
}

void TixrTiersFinderDialog::on_lineEdit_textChanged(const QString &)
{
    proxyModel->setFilterRegExp(QRegExp(lineEdit->text(), Qt::CaseInsensitive));
}
