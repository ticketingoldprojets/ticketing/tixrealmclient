//-----------------------------------------------------------------------------
#include "newoperatorwidget.h"
#include "../mainwindow.h"
//-----------------------------------------------------------------------------
#define TIERSCOLSVIEW   12
//-----------------------------------------------------------------------------
TixrNewOperatorWidget::TixrNewOperatorWidget(TixRealmCore * mObj, const QModelIndex & tiersItemIndex, QStandardItem * rootItem, QWidget * parent) :
    Person("newOperateur", mObj, "crmbtn.png", parent, rootItem)
{
    this->tixrWidgetType = TIXR_NEWOPERATOR;
}

void TixrNewOperatorWidget::on_pbValider_clicked()
{
    QJsonObject* jsonObj = this->tixrCore->widgetToJson(this);
    if(updateMode)
    {
        jsonObj->insert("ikey", this->currentItem->data(DataRole::ikey()).toInt());
    }

    this->tixrCore->setPgFunction("main.instiers");
    PendingRequest* pReq = this->tixrCore->sendMsg(0, HTTP_METHOD_ID::POST, "/api", this, "responseUpsertPerson", jsonObj);
    if(!updateMode)
        pReq->setRespDataMapKey("crm");
    this->currentTaskList << pReq;
}

void TixrNewOperatorWidget::responseUpsertPerson(QString respError, PendingRequest *curReq)
{
    if(!beginHandleResponse(respError, curReq))
        return;

    currentItem->setData(QVariant::fromValue<TixrNewOperatorWidget *>(this), DataRole::indicator());

    int wIndex;
    if((wIndex = endHandleResponse(curReq)) > -1)
        parentTabWidget->setTabText(wIndex, getName());
}
