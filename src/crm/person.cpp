#include "person.h"
#include "../mainwindow.h"

Person::Person(QString uiName, TixRealmCore *mObj, QString icon, QWidget *parent, QStandardItem *r, QStandardItem *cItem) :
    TixrWidget(uiName, mObj, icon, parent, r, cItem)
{
    if(!this->getErrorMsg().isEmpty())
        return;

    tiersName = getName();

    if(this->parentTabWidget)
    {
        this->parentTabWidget->addTab(this, windowIcon(), tiersName);
        this->parentTabWidget->setCurrentWidget(this);
    }

    setupUI();

    this->fillComboBox(this->cbUsageHabituel, QLISTITEMQPAIRSTRINT << QPAIRITEMSTRINT("", -1) << QPAIRITEMSTRINT("TOUS LES USAGES", 0) << QPAIRITEMSTRINT("Facturation", 1) << QPAIRITEMSTRINT("Livraisson", 2));
    this->fillComboBox(this->cbTypeValeur, QLISTITEMQPAIRSTRINT << QPAIRITEMSTRINT("TELFIXE", 0) << QPAIRITEMSTRINT("PORTABLE", 1) << QPAIRITEMSTRINT("FAX", 2) << QPAIRITEMSTRINT("EMAIL", 3) << QPAIRITEMSTRINT("WEB", 4));
    this->fillComboBox(this->cbNature, QLISTITEMQPAIRSTRINT << QPAIRITEMSTRINT("Personne Physique", 0) << QPAIRITEMSTRINT("Personne Morale", 1));
    this->fillComboBox(this->cbSex, QLISTITEMQPAIRSTRINT << QPAIRITEMSTRINT("Masculin", 0) << QPAIRITEMSTRINT("Feminin", 1));

    if(cbCivilite)
    {
        this->cbCivilite->setModel(this->tixrCore->stdModel);
        this->cbCivilite->setRootModelIndex(this->tixrCore->keyToItemMap["civilites"]->index());
    }

    // this->cbTTiers->setModel(this->tixrCore->stdModel);
    // this->cbTTiers->setRootModelIndex(this->tixrCore->keyToItemMap["typetiers"]->index());

    if(this->cbPays)
    {
        this->cbPays->setModel(this->tixrCore->stdModel);
        this->cbPays->setRootModelIndex(this->tixrCore->keyToItemMap["pays"]->index());
    }

    this->cbEtat->setVisible(updateMode);
    this->lbEtat->setVisible(updateMode);

    if(!updateMode)
    {
        if(this->tixrCore->keyToItemMap["civilites"]->hasChildren() && this->cbCivilite)
            this->cbCivilite->setCurrentIndex(0);
        // this->cbTTiers->setCurrentIndex(1);
        if(this->cbPays)
        {
            if(this->tixrCore->keyToItemMap["pays"]->hasChildren())
                this->cbPays->setCurrentIndex(0);
            this->on_cbPays_currentIndexChanged(0);
        }
        // this->leOperator->setText(this->tixrCore->keyToItemMap["login"]->data(TixrQDataRole::snom()).toString());
        // this->leOperatorComment->setText(this->tixrCore->keyToItemMap["login"]->data(TixrQDataRole::snom()).toString());
    }

    this->leMotPasseNew->setVisible(updateMode);
    this->lbNewPassword->setVisible(updateMode);
}

Person::~Person()
{
    if(updateMode)
        this->currentItem->setData(QVariant(), DataRole::indicator());
}

QString Person::getName()
{
    if(updateMode)
        return currentItem->data(DataRole::qtdisplayrole()).toString() + " " + this->currentItem->data(DataRole::tnom()).toString();
    else
        return uiName == "newTiers" ? tr("New Customer") : tr("New Cashier");
}

void Person::setupUI()
{
    this->leNom             = this->findChild<QLineEdit *>("leNom");
    this->lePrenom          = this->findChild<QLineEdit *>("lePrenom");
    this->leIdentifiant     = this->findChild<QLineEdit *>("leIdentifiant");
    this->leMotPasse        = this->findChild<QLineEdit *>("leMotPasse");
    this->leMotPasseNew     = this->findChild<QLineEdit *>("leMotPasseNew");
    this->lbNewPassword     = this->findChild<QLabel *>("lbNewPassword");
    this->deDateNaissance   = this->findChild<QDateEdit *>("deDateNaissance");
    // this->cbTTiers          = this->findChild<QComboBox *>("cbTTiers");
    this->cbCivilite        = this->findChild<QComboBox *>("cbCivilite");
    this->leCoordLiv        = this->findChild<QLineEdit *>("leCoordLiv");
    this->leCoordFact       = this->findChild<QLineEdit *>("leCoordFact");
    this->cbNature          = this->findChild<QComboBox *>("cbNature");
    this->leCommentaire     = this->findChild<QPlainTextEdit *>("leCommentaire");
    this->leModifPrivileges = this->findChild<QSpinBox *>("leModifPrivileges");
    this->cbSex             = this->findChild<QComboBox *>("cbSex");
    this->leDateCreation    = this->findChild<QLineEdit *>("leDateCreation");
    this->leOperator        = this->findChild<QLineEdit *>("leOperator");
    this->leOperatorComment = this->findChild<QLineEdit *>("leOperatorComment");
    this->rbHigh            = this->findChild<QRadioButton *>("rbHigh");
    this->rbNormal          = this->findChild<QRadioButton *>("rbNormal");
    this->cbEtat            = this->findChild<QComboBox *>("cbEtat");
    this->lbEtat            = this->findChild<QLabel *>("lbEtat");

    this->leValeur          = this->findChild<QLineEdit *>("leValeur");
    this->leLibelle         = this->findChild<QLineEdit *>("leLibelle");
    this->cbTypeValeur      = this->findChild<QComboBox *>("cbTypeValeur");
    this->lePriorite        = this->findChild<QLineEdit *>("lePriorite");

    this->cbPays            = this->findChild<QComboBox *>("cbPays");
    this->cbDepartement     = this->findChild<QComboBox *>("cbDepartement");
    this->leAdreese1        = this->findChild<QLineEdit *>("leAdreese1");
    this->leAdreese2        = this->findChild<QLineEdit *>("leAdreese2");
    this->cbVille           = this->findChild<QComboBox *>("cbVille");
    this->leCodePostal      = this->findChild<QLineEdit *>("leCodePostal");
    this->cbUsageHabituel   = this->findChild<QComboBox *>("cbUsageHabituel");

    this->groupBox_3        = this->findChild<QGroupBox *>("groupBox_3");
}

QString Person::validate()
{
    if(this->leNom->text().isEmpty() && this->lePrenom->text().isEmpty())
    {
        return QObject::trUtf8("The Name and Surname can not be empty");
    }

    if(this->groupBox_3->isChecked() && !this->leIdentifiant->text().isEmpty() && this->leMotPasse->text().isEmpty())
    {
        return QObject::trUtf8("The password can not be empty");
    }

    return QString::null;
}

void Person::on_cbPays_currentIndexChanged(int index)
{
    QStandardItem * itemPays = this->tixrCore->keyToItemMap["pays"]->child(index);
    if(!itemPays->hasChildren())
    {
        QStandardItem *modelSubItem = new QStandardItem(1, 1);
        modelSubItem->setData("ItemId", DataRole::jsonProperty());
        modelSubItem->setData(JsonObject, DataRole::jsonType());

        itemPays->appendRow(modelSubItem);

        QStandardItem *modelSubItem2 = new QStandardItem(itemPays->data(DataRole::ikey()).toString());
        modelSubItem2->setData("srvikey", DataRole::jsonProperty());
        modelSubItem2->setData(JsonSimple, DataRole::jsonType());
        modelSubItem->setChild(0, 0, modelSubItem2);

        this->tixrCore->setPgFunction("main.getvilles");
        PendingRequest *pReq = this->tixrCore->sendMsg(modelSubItem, HTTP_METHOD_ID::POST, "/api", this, "commonResponseHandler");
        pReq->senderSlot2 = "responseGetVilles";
        this->currentTaskList << pReq;
    }
    else if(itemPays->child(0)->child(0)->text() != "main.getvilles")  // para que no salga el nombre de la funcion en el comboBox
    {
        this->cbVille->setModel(this->tixrCore->stdModel);
        this->cbVille->setRootModelIndex(itemPays->child(0)->index());
        this->cbVille->setCurrentIndex(0);
    }
}

void Person::fillComboBox(QComboBox * comboBox, const QList<QPair<QString, int> > & pairList)
{
    if(!comboBox)
        return;

    QStandardItemModel * roleModel = new QStandardItemModel();
    QStandardItem * roleItem;
    for(int i = 0, countList = pairList.count(); i < countList; i++)
    {
        roleModel->appendRow(roleItem = new QStandardItem(pairList[i].first));
        roleItem->setData(pairList[i].second, DataRole::ikey());
    }
    comboBox->setModel(roleModel);
    comboBox->setCurrentIndex(0);
}

void Person::responseGetVilles(PendingRequest *curReq)
{
    this->cbVille->setModel(this->tixrCore->stdModel);
    this->cbVille->setRootModelIndex(curReq->getResponseItem()->index());
    this->cbVille->setCurrentIndex(0);
}
