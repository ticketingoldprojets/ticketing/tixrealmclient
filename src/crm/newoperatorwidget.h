//-----------------------------------------------------------------------------
#ifndef SCNEWOPERATORWIDGET_H
#define SCNEWOPERATORWIDGET_H
//-----------------------------------------------------------------------------
#include <QMetaType>
#include "person.h"

class TixrNewOperatorWidget : public Person
{
    Q_OBJECT

public:
    explicit TixrNewOperatorWidget(TixRealmCore *mObj, const QModelIndex &tiersItemIndex, QStandardItem *rootItem = NULL, QWidget *parent = NULL);

private slots:
    void on_pbValider_clicked();
    void responseUpsertPerson(QString respError, PendingRequest *curReq);
};
//-----------------------------------------------------------------------------
Q_DECLARE_METATYPE(TixrNewOperatorWidget *)
//-----------------------------------------------------------------------------
#endif // TixrNewOperatorWidget_H
//-----------------------------------------------------------------------------

