//-----------------------------------------------------------------------------
#ifndef SCCRMWIDGET_H
#define SCCRMWIDGET_H
//-----------------------------------------------------------------------------
#include "../widget.h"
//-----------------------------------------------------------------------------
class TixrCrmWidget : public TixrWidget
{
    Q_OBJECT

    public:
        explicit TixrCrmWidget(TixRealmCore *mObj, QWidget *parent = 0);
        void setTiersRoot(QStandardItem *rootItem);

    protected:
        QTableView * tvTiers;
        QTableView * tvTiersSearch;
        QLineEdit * leTiersSearch;
        QSortFilterProxyModel * proxyModel;
        QStandardItem * tiersRoot;

    private slots:        
        void responseGetCustomers(QString respError, PendingRequest *curReq);
        void responseDelCustomer(QString respError, PendingRequest *curReq);

        void handleSimpleResponse(QString respError, PendingRequest *curReq);

        void on_pbNouveauTiers_clicked();
        void on_pbDelTiers_clicked();
        void on_tvTiers_doubleClicked(const QModelIndex & index);
        void on_leTiersSearch_textChanged(const QString &);
};
//-----------------------------------------------------------------------------
#endif // SCCRMWIDGET_H
//-----------------------------------------------------------------------------

