//-----------------------------------------------------------------------------
#ifndef SCNEWTIERSWIDGET_H
#define SCNEWTIERSWIDGET_H
//-----------------------------------------------------------------------------
#include <QMetaType>
#include "../newpaymentdialog.h"
#include "../remboursementdialog.h"
#include "person.h"
//-----------------------------------------------------------------------------
class TixrNewTiersWidget : public Person
{
    Q_OBJECT

public:
    explicit TixrNewTiersWidget(TixRealmCore *mObj, QStandardItem *r, QWidget *parent = 0, QStandardItem *cItem = 0);
    ~TixrNewTiersWidget();
    void setOnTopWidgetsVisibility(bool visible);

signals:
    void addCart(QStandardItem*);
    void findCart(int, QStandardItem*);

public slots:
    void responseDoPayment(QString respError, PendingRequest *curReq);
    void responseRemboursement(QString respError, PendingRequest *curReq);

protected:
    QLineEdit * leFormeJuridique;
    QLineEdit * leDenomination;
    QLineEdit * leRCS;
    QLineEdit * leNAF;
    QLineEdit * leSIRET;
    QComboBox * cbEffectif;
    QLineEdit * leCodeClient;
    QLineEdit * leCodeFournisseur;
    QLabel    * lbFormeJuridique;
    QLabel    * lbDenomination;
    QLabel    * lbRCS;
    QLabel    * lbNAF;
    QLabel    * lbSIRET;
    QLabel    * lbEffectif;
    QLabel    * lbCodeClient;
    QLabel    * lbCodeFournisseur;

    QPushButton * pageSwitcher___1;
    QPushButton * pageSwitcher___3;
    QPushButton * pageSwitcher___5;

    QWidget * pageNewTiers;

    void showEvent(QShowEvent * event);
    void hideEvent(QHideEvent *event);

private:
    TixrNewPaymentDialog *newPaymentDialog;
    TixrRemboursementDialog *rembDialog;
    QWidget *histoPage;
    QTreeView *cartView;
    QTableView *paymTView;
    QTableView *tbvHistorique;
    QStandardItemModel *cartHeaderModel;
    QStandardItemModel *paymTViewHModel;
    QStandardItemModel *histoHModel;
    TixrPaymFilterProxyModel *proxyModel;
    QDoubleSpinBox *sousTotalDSB;
    QDoubleSpinBox *totalDSB;
    QDoubleSpinBox *dsbCurBalance;
    QStandardItem *curOrderItem;
    QPushButton *pbEdit;
    void embedProdItem(QStandardItem *rootItm, QStringList *idList, QStringList *lblList, QList<QStandardItem *> *pItmRow);
    QList<QStandardItem*> getProds(bool forgetSelection = false);

private slots:
    void on_pbValider_clicked();
    void on_tbvHistorique_doubleClicked(const QModelIndex & index);
    void on_cbNature_currentIndexChanged(int index);
    void on_cbTTiers_currentIndexChanged(int index);
    void on_rbHigh_clicked(bool checked = false);
    void on_rbNormal_clicked(bool checked = false);
    void on_pbPayer_clicked();
    void on_stackedWidget_currentChanged(int index);
    void on_pbEdit_clicked();

    void responseGetClientOrder(QString respError, PendingRequest *curReq);
    void responseGetClientPayments(QString respError, PendingRequest *curReq);
    void responseUpsertPerson(QString respError, PendingRequest *curReq);
    void responseGetOrders(PendingRequest *curReq);

    void cartViewSelectionChanged(const QItemSelection &, const QItemSelection &);
    void tbvHistoriqueSelectionChanged(const QItemSelection &, const QItemSelection &);
    //void paymTViewSelectionChanged(const QItemSelection &, const QItemSelection &);
    void handleCartRequestResult(bool, QStandardItem*);
    void on_pbRefund_clicked();
};
//-----------------------------------------------------------------------------
Q_DECLARE_METATYPE(TixrNewTiersWidget *)
//-----------------------------------------------------------------------------
#endif // SCNEWTIERSWIDGET_H
//-----------------------------------------------------------------------------

