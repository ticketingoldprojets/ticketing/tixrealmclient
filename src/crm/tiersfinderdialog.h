#ifndef SCTIERSFINDERDIALOG_H
#define SCTIERSFINDERDIALOG_H
#include "../dialog.h"
#include "../customproxymodel.h"

class TixrTiersFinderDialog : public TixrDialog
{
    Q_OBJECT
public:
    explicit TixrTiersFinderDialog(QWidget *blockedWidget, QWidget *parent = 0);
    QStandardItem *selectedTiers();
    void setTiersRoot(QStandardItem *rootItem);
    
private:
    QTableView *tiersTV;
    QStandardItem *tiersRoot;
    TixrSortFilterProxyModel *proxyModel;
    QLineEdit *lineEdit;

private slots:
    void on_lineEdit_textChanged(const QString &);
};

#endif // SCTIERSFINDERDIALOG_H
