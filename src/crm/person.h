#ifndef PERSON_H
#define PERSON_H

#include "../widget.h"

class Person : public TixrWidget
{
    Q_OBJECT
public:
    explicit Person(QString uiName, TixRealmCore *mObj, QString icon = QString::null, QWidget *parent = 0, QStandardItem *r = 0, QStandardItem *cItem = 0);
    ~Person();

protected:
    QLineEdit * leNom;
    QLineEdit * lePrenom;
    QLineEdit * leIdentifiant;
    QLineEdit * leMotPasse;
    QLineEdit * leMotPasseNew;
    QDateEdit * deDateNaissance;
    QLabel    * lbNewPassword;
    // QComboBox * cbTTiers;
    QComboBox * cbCivilite;
    QLineEdit * leCoordLiv;
    QLineEdit * leCoordFact;
    QComboBox * cbNature;
    QPlainTextEdit * leCommentaire;
    QSpinBox * leModifPrivileges;
    QComboBox * cbSex;
    QLineEdit * leDateCreation;
    QLineEdit * leOperator;
    QLineEdit * leOperatorComment;
    QRadioButton * rbHigh;
    QRadioButton * rbNormal;

    QLineEdit * leValeur;
    QLineEdit * leLibelle;
    QComboBox * cbTypeValeur;
    QLineEdit * lePriorite;

    QComboBox * cbPays;
    QComboBox * cbDepartement;
    QLineEdit * leAdreese1;
    QLineEdit * leAdreese2;
    QComboBox * cbVille;
    QLineEdit * leCodePostal;
    QComboBox * cbUsageHabituel;

    QComboBox * cbEtat;
    QLabel    * lbEtat;

    QGroupBox * groupBox_3;

    QString tiersName;

    QString validate();
    void fillComboBox(QComboBox *, const QList<QPair<QString, int> >&);
    QString getName();

private:
    void setupUI();

private slots:
    void on_cbPays_currentIndexChanged(int index);
    void responseGetVilles(PendingRequest *curReq);
};

#endif // PERSON_H
