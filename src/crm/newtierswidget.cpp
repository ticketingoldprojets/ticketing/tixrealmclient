#include "newtierswidget.h"
#include "../mainwindow.h"

#define TIERSCOLSVIEW   12

TixrNewTiersWidget::TixrNewTiersWidget(TixRealmCore * mObj, QStandardItem * r, QWidget * parent, QStandardItem *cItem) :
    Person("newTiers", mObj, "crmbtn.png", parent, r, cItem), newPaymentDialog(0), curOrderItem(0), rembDialog(0)
{
    this->tixrWidgetType = TIXR_NEWTIERS;

    if(!this->getErrorMsg().isEmpty())
        return;

    this->pageSwitcher___1  = this->findChild<QPushButton *>("pageSwitcher___1");
    this->pageSwitcher___3  = this->findChild<QPushButton *>("pageSwitcher___3");
    this->pageSwitcher___5  = this->findChild<QPushButton *>("pageSwitcher___5");

    this->leFormeJuridique  = this->findChild<QLineEdit *>("leFormeJuridique");
    this->leDenomination    = this->findChild<QLineEdit *>("leDenomination");
    this->leRCS             = this->findChild<QLineEdit *>("leRCS");
    this->leNAF             = this->findChild<QLineEdit *>("leNAF");
    this->leSIRET           = this->findChild<QLineEdit *>("leSIRET");
    this->cbEffectif        = this->findChild<QComboBox *>("cbEffectif");
    this->leCodeClient      = this->findChild<QLineEdit *>("leCodeClient");
    this->leCodeFournisseur = this->findChild<QLineEdit *>("leCodeFournisseur");
    this->lbFormeJuridique  = this->findChild<QLabel *>("lbFormeJuridique");
    this->lbDenomination    = this->findChild<QLabel *>("lbDenomination");
    this->lbRCS             = this->findChild<QLabel *>("lbRCS");
    this->lbNAF             = this->findChild<QLabel *>("lbNAF");
    this->lbSIRET           = this->findChild<QLabel *>("lbSIRET");
    this->lbEffectif        = this->findChild<QLabel *>("lbEffectif");
    this->lbCodeClient      = this->findChild<QLabel *>("lbCodeClient");
    this->lbCodeFournisseur = this->findChild<QLabel *>("lbCodeFournisseur");

    this->histoPage         = this->findChild<QWidget *>("histoPage");

    this->pbEdit            = this->histoPage->findChild<QPushButton*>("pbEdit");

    this->pageNewTiers      = this->findChild<QWidget *>("pageNewTiers");

    this->dsbCurBalance     = histoPage->findChild<QDoubleSpinBox *>("dsbCurBalance");
    /*if(this->currentItem)//Ahora lo hago con un QDataWidgetMapper
    {
        qreal b = this->currentItem->parent()->child(this->currentItem->row(), TixrDataCol::taccountcol())->data(Qt::DisplayRole).toDouble();
        this->dsbCurBalance->setValue(b);
    }*/

    cartHeaderModel = new QStandardItemModel(this);
    cartHeaderModel->setHorizontalHeaderLabels(QStringList()<<tr("Items")<<tr("Payment status")<<tr("Ticket status"));
    this->cartView = histoPage->findChild<QTreeView *>("cartView");
    this->cartView->setModel(cartHeaderModel);
    this->cartView->header()->setModel(cartHeaderModel);
    this->cartView->header()->show();//aún no sé por qué (quizás un Qt Bug), si no hago esto y que la página inicial no es la que corresponde al cartView(por defecto no lo es) entonces el header no se muestra; de lo contrario, si la pag. del cartView es la actual el header se muestra sin problemas.

    this->paymTView         = histoPage->findChild<QTableView *>("paymTView");
    paymTViewHModel = new QStandardItemModel(this);
    paymTViewHModel->setHorizontalHeaderLabels(QStringList()<<tr("Date")<<tr("Currency")<<tr("Amount")<<tr("Type")<<tr("Payor")<<tr("Comment"));
    this->paymTView->setModel(paymTViewHModel);
    this->paymTView->horizontalHeader()->setModel(paymTViewHModel);

    this->tbvHistorique     = histoPage->findChild<QTableView *>("tbvHistorique");
    histoHModel = new QStandardItemModel(this);
    histoHModel->setHorizontalHeaderLabels(QStringList()<<tr("Date"));
    this->tbvHistorique->setModel(histoHModel);
    this->tbvHistorique->horizontalHeader()->setModel(histoHModel);

    this->totalDSB          = histoPage->findChild<QDoubleSpinBox*>("totalDSB");
    this->sousTotalDSB      = histoPage->findChild<QDoubleSpinBox*>("sousTotalDSB");

    this->pageSwitcher___1->setEnabled(updateMode);
    this->pageSwitcher___3->setEnabled(updateMode);
    this->pageSwitcher___5->setEnabled(updateMode);

    if(updateMode)
    {
        if(!this->currentItem->child(DataColumn::tcihistoric()))
        {
            QStandardItem *modelSubItem = new QStandardItem(1, 1);
            modelSubItem->setData("ItemId", DataRole::jsonProperty());
            modelSubItem->setData(JsonObject, DataRole::jsonType());

            this->currentItem->setChild(DataColumn::tcihistoric(), 0, modelSubItem);

            QStandardItem *modelSubItem2 = new QStandardItem(this->currentItem->data(DataRole::ikey()).toString());
            modelSubItem2->setData("srvikey", DataRole::jsonProperty());
            modelSubItem2->setData(JsonSimple, DataRole::jsonType());
            modelSubItem->setChild(0, 0, modelSubItem2);

            this->tixrCore->setPgFunction("main.gettierscommandes");
            PendingRequest *pReq = this->tixrCore->sendMsg(modelSubItem, HTTP_METHOD_ID::POST, "/api", this, "commonResponseHandler");
            pReq->senderSlot2 = "responseGetOrders";
            this->currentTaskList << pReq;
        }
        else
        {
            this->tbvHistorique->setModel(this->tixrCore->stdModel);
            this->tbvHistorique->setRootIndex(this->currentItem->child(DataColumn::tcihistoric())->index());
            this->tbvHistorique->horizontalHeader()->setModel(histoHModel);
            connect(tbvHistorique->selectionModel(), SIGNAL(selectionChanged(QItemSelection,QItemSelection)), this, SLOT(tbvHistoriqueSelectionChanged(QItemSelection,QItemSelection)));
        }

        if(this->currentItem->child(DataColumn::tcipayments()))
        {
            proxyModel = new TixrPaymFilterProxyModel(this);
            proxyModel->setSourceModel(this->tixrCore->stdModel);
            proxyModel->setFilterRole(DataRole::ikey());

            this->paymTView->setModel(proxyModel);
            this->paymTView->setRootIndex(proxyModel->mapFromSource(this->currentItem->child(DataColumn::tcipayments())->index()));
            this->paymTView->horizontalHeader()->setModel(paymTViewHModel);
        }
        else
        {
            QStandardItem *modelSubItem = new QStandardItem(1,1);
            modelSubItem->setData("ItemId", DataRole::jsonProperty());
            modelSubItem->setData(JsonObject, DataRole::jsonType());

            QStandardItem *modelSubItem2 = new QStandardItem(this->currentItem->data(DataRole::ikey()).toString());
            modelSubItem2->setData("srvikey", DataRole::jsonProperty());
            modelSubItem2->setData(JsonSimple, DataRole::jsonType());
            modelSubItem->setChild(0, 0, modelSubItem2);

            this->currentItem->setChild(DataColumn::tcipayments(), modelSubItem);

            tixrCore->setPgFunction("main.gettierspayments");
            PendingRequest *pReq = tixrCore->sendMsg(modelSubItem, HTTP_METHOD_ID::POST, "/api", this, "responseGetClientPayments");
            pReq->isJsonSingleValueFunc = true;
            currentTaskList << pReq;
        }
    }

    this->leMotPasseNew->setVisible(updateMode);
    this->lbNewPassword->setVisible(updateMode);

    this->on_cbNature_currentIndexChanged(0);

    newPaymentDialog = new TixrNewPaymentDialog(tixrCore, histoPage, this);
    errorStr = newPaymentDialog->getErrorMsg();
    if(!errorStr.isEmpty())
        return;

    rembDialog = new TixrRemboursementDialog(tixrCore, histoPage, currentItem, this);
    errorStr = rembDialog->getErrorMsg();
    if(!errorStr.isEmpty())
        return;

    //gestión de la edición de transacciones desde la interfaz del cliente
    connect(this, SIGNAL(findCart(int, QStandardItem*)), tixrCore, SIGNAL(findCart(int, QStandardItem*)));
    connect(this, SIGNAL(addCart(QStandardItem*)), tixrCore, SIGNAL(addCart(QStandardItem*)));

    MainWindow::tixrConnectSlotsByName(this);
}

TixrNewTiersWidget::~TixrNewTiersWidget()
{

}

void TixrNewTiersWidget::on_pbValider_clicked()
{
    QString err = validate();

    if(!err.isEmpty())
    {
        QMessageBox::information(this, qApp->applicationName(), err);
        return;
    }

    QJsonObject* jsonObj = this->tixrCore->widgetToJson(this->pageNewTiers); // para capturar el componente activo del tabWidget (ventana activa);

    QStringList infoEntrepriseList;
    if(this->leFormeJuridique->text() != "" )
        infoEntrepriseList.append("{FormeJuridique," + this->leFormeJuridique->text() + "}");
    if(this->leDenomination->text() != "" )
        infoEntrepriseList.append("{Denomination," + this->leDenomination->text() + "}");
    if(this->leRCS->text() != "" )
        infoEntrepriseList.append("{RCS," + this->leRCS->text() + "}");
    if(this->leNAF->text() != "" )
        infoEntrepriseList.append("{NAF," + this->leNAF->text() + "}");
    if(this->leSIRET->text() != "" )
        infoEntrepriseList.append("{SIRET," + this->leSIRET->text() + "}");

    QString infoEntreprise = infoEntrepriseList.empty() ? "" : infoEntrepriseList.join(QString(","));
    infoEntreprise = infoEntreprise.prepend("{").append("}");
    // for(int i = 1, count = infoEntrepriseList.count(); i < count ; i++) infoEntreprise = infoEntreprise + "," + infoEntrepriseList[i];

    jsonObj->insert("tinfoentreprisecol_qtdisplayrole", infoEntreprise);

    jsonObj->insert("tttiers", QJsonValue("customer"));//QJsonValue("cashier")//jsonObj->insert("tttiers", 2);

    jsonObj->insert("topermaker", this->tixrCore->keyToItemMap["login"]->data(DataRole::operatid()).toInt());

    jsonObj->insert("tprioritycomment", this->rbHigh->isChecked() ? 1 : 0);

    jsonObj->insert(QStringLiteral("tcommentairecol_qtdisplayrole"), QJsonValue("2"));

    if(updateMode)
    {
        jsonObj->insert("ikey", this->currentItem->data(DataRole::ikey()).toInt());
    }

    this->tixrCore->setPgFunction("main.instiers");
    PendingRequest* pReq = this->tixrCore->sendMsg(0, HTTP_METHOD_ID::POST, "/api", this, "responseUpsertPerson", jsonObj);
    if(!updateMode)
        pReq->setRespDataMapKey("crm");
    this->currentTaskList << pReq;
}

void TixrNewTiersWidget::on_cbNature_currentIndexChanged(int index)
{
    this->leFormeJuridique->setVisible(index);
    this->leDenomination->setVisible(index);
    this->leRCS->setVisible(index);
    this->leNAF->setVisible(index);
    this->leSIRET->setVisible(index);
    this->cbEffectif->setVisible(index);
    this->lbFormeJuridique->setVisible(index);
    this->lbDenomination->setVisible(index);
    this->lbRCS->setVisible(index);
    this->lbNAF->setVisible(index);
    this->lbSIRET->setVisible(index);
    this->lbEffectif->setVisible(index);
}

void TixrNewTiersWidget::on_cbTTiers_currentIndexChanged(int index)
{
    int idtypetiers = this->tixrCore->keyToItemMap["typetiers"]->child(index)->data(DataRole::ikey()).toInt();
    this->lbCodeFournisseur->setVisible(idtypetiers == 3);
    this->leCodeFournisseur->setVisible(idtypetiers == 3);
    this->lbCodeClient->setVisible(idtypetiers != 3);
    this->leCodeClient->setVisible(idtypetiers != 3);
}

void TixrNewTiersWidget::on_rbHigh_clicked(bool checked)
{
    if(checked)
        this->leCommentaire->setStyleSheet("color:red");
}

void TixrNewTiersWidget::on_rbNormal_clicked(bool checked)
{
    if(checked)
        this->leCommentaire->setStyleSheet("color:black");
}

void TixrNewTiersWidget::responseUpsertPerson(QString respError, PendingRequest *curReq)
{
    if(!beginHandleResponse(respError, curReq))
        return;

    this->currentItem->setData(reinterpret_cast<tixrqptr>(this), DataRole::indicator());

    if(endHandleResponse(curReq) == -1)
        return;

    if(!updateMode)
    {
        dataWidgetMapper->setModel(tixrCore->stdModel);
        dataWidgetMapper->setRootIndex(tixrCore->keyToItemMap["crm"]->index());
        dataWidgetMapper->addMapping(dsbCurBalance, DataColumn::taccountcol());
        dataWidgetMapper->setCurrentIndex(currentItem->row());
    }

    // this->parentTabWidget->setTabText(wIndex, tr((this->currentItem->data(TixrQDataRole::tnom()).toString() + QString(" ") + this->currentItem->data(TixrQDataRole::qtdisplayrole()).toString()).toUtf8().constData()));

    this->pageSwitcher___1->setEnabled(updateMode);
    this->pageSwitcher___3->setEnabled(updateMode);
    this->pageSwitcher___5->setEnabled(updateMode);

    this->leMotPasseNew->setVisible(updateMode);
    this->lbNewPassword->setVisible(updateMode);

    this->cbEtat->setVisible(updateMode);
    this->lbEtat->setVisible(updateMode);
}

void TixrNewTiersWidget::responseGetOrders(PendingRequest *curReq)
{
    QStandardItem* responseItem = curReq->getResponseItem();

    this->tbvHistorique->setModel(this->tixrCore->stdModel);
    this->tbvHistorique->setRootIndex(responseItem->index());

    this->tbvHistorique->horizontalHeader()->setModel(histoHModel);
    connect(tbvHistorique->selectionModel(), SIGNAL(selectionChanged(QItemSelection,QItemSelection)), this, SLOT(tbvHistoriqueSelectionChanged(QItemSelection,QItemSelection)));
}

void TixrNewTiersWidget::on_tbvHistorique_doubleClicked(const QModelIndex & index)
{
    QStandardItem * orderItem = this->tixrCore->stdModel->itemFromIndex(index);
    if(orderItem->hasChildren())
    {
        QStandardItem *detailsItem = orderItem->child(0);
        if(detailsItem->accessibleText() == "ItemId")//aún no se ha recibido la respuesta
            return;

        cartView->setModel(tixrCore->stdModel);
        cartView->setRootIndex(detailsItem->index());
        cartView->header()->setModel(cartHeaderModel);
        connect(cartView->selectionModel(), SIGNAL(selectionChanged(QItemSelection,QItemSelection)), this, SLOT(cartViewSelectionChanged(QItemSelection,QItemSelection)));
        orderItem->setData(QVariant(QColor(Qt::red)), Qt::DecorationRole);
        if(curOrderItem)
        {
            curOrderItem->setData(QVariant(), Qt::DecorationRole);
        }
        curOrderItem = orderItem;
        curOrderItem->setData(QVariant(QColor(Qt::red)), Qt::DecorationRole);

        QList<QStandardItem*> prodsLis = getProds(true);

        qreal total = 0;
        for(int i=0, n=prodsLis.size(); i<n; i++)
        {
            QStandardItem *pItem = prodsLis.at(i);
            QVariant v = pItem->data(DataRole::dcquantite());
            int N = v.isValid() ? v.toInt() : 1;
            total += pItem->data(DataRole::dcprix()).toDouble() * N;
        }

        totalDSB->setValue(total);
        sousTotalDSB->setValue(0);

        pbEdit->setEnabled(true);
    }
    else
    {
        QStandardItem *modelSubItem = new QStandardItem(1,1);
        modelSubItem->setData("ItemId", DataRole::jsonProperty());
        modelSubItem->setData(JsonObject, DataRole::jsonType());

        QStandardItem *modelSubItem2 = new QStandardItem(orderItem->data(DataRole::scorderid()).toString());
        modelSubItem2->setData("srvikey", DataRole::jsonProperty());
        modelSubItem2->setData(JsonSimple, DataRole::jsonType());
        modelSubItem->setChild(0, 0, modelSubItem2);

        orderItem->appendRow(modelSubItem);

        tixrCore->setPgFunction("main.getdetailscommande");
        PendingRequest *pReq = tixrCore->sendMsg(modelSubItem, HTTP_METHOD_ID::POST, "/api", this, "responseGetClientOrder");
        currentTaskList << pReq;
    }
}
//-----------------------------------------------------------------------------
void TixrNewTiersWidget::responseGetClientOrder(QString respError, PendingRequest *curReq)
{
    QStandardItem *requestItem = curReq->getRequestItem();
    QStandardItem *responseItem = curReq->getResponseItem();

    if(!respError.isEmpty())
    {
        requestItem->parent()->removeRow(requestItem->row());
        delete responseItem;

        currentTaskList.removeOne(curReq);
        int wIndex = parentTabWidget->indexOf(this);
        if(currentTaskList.isEmpty() && wIndex>=0)
            parentTabWidget->setTabIcon(wIndex, windowIcon());

        QMessageBox::information(this, qApp->applicationName(), tr(respError.toUtf8().constData()));

        return;
    }

    QStandardItem *orderItem = requestItem->parent(), *histoRoot = new QStandardItem;
    orderItem->setChild(requestItem->row(), histoRoot);
    qreal total = 0;

    while(responseItem->hasChildren())
    {
        QList<QStandardItem*> rowItms = responseItem->takeRow(0);

        QStandardItem *prodItm = rowItms.at(0),
                      *payedItm = rowItms.at(1),
                      *ticketItm = rowItms.at(2);

        qreal tp = payedItm->data(Qt::DisplayRole).toDouble();
        int N = prodItm->data(DataRole::dcquantite()).toInt();
        qreal p = prodItm->data(DataRole::dcprix()).toDouble();
        qreal st = p * N;
        QColor c = tp?(tp<st?Qt::yellow:Qt::green):Qt::red;
        payedItm->setData(c, Qt::DecorationRole);

        total += st;

        int ntickets = ticketItm->text().toInt();
        ticketItm->setText(QString("%1 / %2").arg(QString::number(ntickets), QString::number(N)));
        ticketItm->setData(QImage(":/images/btn/etatbillet" + QString::number(ntickets?(ntickets==N?1:2):3) + ".png"), Qt::DecorationRole);
        ticketItm->setToolTip(tr(ntickets?(ntickets==N?"Print completed":"Printing not finished"):"Not printed"));
        ticketItm->setData(ntickets, DataRole::nticketsprinted());

        QStringList idList = prodItm->data(DataRole::dccoord()).toString().split("||", QString::SkipEmptyParts);
        prodItm->setData(QVariant(), DataRole::dccoord());

        QStringList lblList = prodItm->data(DataRole::dclabels()).toString().split("||", QString::SkipEmptyParts);
        prodItm->setData(QVariant(), DataRole::dclabels());
        prodItm->setText(prodItm->data(DataRole::pdescrip()).toString() + QString(" [%1€ x %2 -> %3]").arg(tixrCore->curLocal.toString(p), QString::number(N), tixrCore->curLocal.toString(st)));

        embedProdItem(histoRoot, &idList, &lblList, &rowItms);
    }

    cartView->setModel(tixrCore->stdModel);
    cartView->setRootIndex(histoRoot->index());
    cartView->header()->setModel(cartHeaderModel);
    connect(cartView->selectionModel(), SIGNAL(selectionChanged(QItemSelection,QItemSelection)), this, SLOT(cartViewSelectionChanged(QItemSelection,QItemSelection)));
    totalDSB->setValue(total);
    sousTotalDSB->setValue(0);

    if(curOrderItem)
    {
        curOrderItem->setData(QVariant(), Qt::DecorationRole);
    }
    curOrderItem = orderItem;
    curOrderItem->setData(QVariant(QColor(Qt::red)), Qt::DecorationRole);

    currentTaskList.removeOne(curReq);
    int wIndex = parentTabWidget->indexOf(this);
    if(currentTaskList.isEmpty() && wIndex>=0)
        parentTabWidget->setTabIcon(wIndex, windowIcon());

    delete responseItem;

    pbEdit->setEnabled(true);
}
//-----------------------------------------------------------------------------
void TixrNewTiersWidget::embedProdItem(QStandardItem *rootItm, QStringList *idList, QStringList *lblList, QList<QStandardItem *> *pItmRow)
{
    QStandardItem *r = rootItm;
    int i = 0, n;

    while(!(i>=r->rowCount() || idList->isEmpty()))
    {
        QStandardItem *child = r->child(i);
        if(child->data(DataRole::iuid()).toString() == idList->last())
        {
            r = child;
            idList->removeLast();
            lblList->removeLast();
            i = 0;
        }
        else
            i++;
    }

    if(idList->isEmpty())
    {
        r->appendRow(*pItmRow);
    }
    else
    {
        for(i=lblList->size()-1; i>=0; i--)
        {
            QStandardItem *newItm = new QStandardItem(lblList->at(i));
            newItm->setData(idList->at(i), DataRole::iuid());
            r->appendRow(newItm);
            r = newItm;
        }

        r->appendRow(*pItmRow);
    }
}
//-----------------------------------------------------------------------------
void TixrNewTiersWidget::cartViewSelectionChanged(const QItemSelection & /*selected*/, const QItemSelection & /*deselected*/)
{
    QList<QStandardItem*> prodsLis = getProds();

    qreal total = 0;
    for(int i=0, n=prodsLis.size(); i<n; i++)
    {
        QStandardItem *pItem = prodsLis.at(i);
        QVariant v = pItem->data(DataRole::dcquantite());
        int N = v.isValid() ? v.toInt() : 1;
        total += pItem->data(DataRole::dcprix()).toDouble() * N;
    }

    sousTotalDSB->setValue(total);
}
//-----------------------------------------------------------------------------
QList<QStandardItem*> TixrNewTiersWidget::getProds(bool forgetSelection)
{
    QList<QStandardItem*> leafsList;

    if(cartView->selectionModel()->hasSelection() && !forgetSelection)
    {
        QModelIndexList mIdxList = cartView->selectionModel()->selectedIndexes();
        for(int i=0, n=mIdxList.size(); i<n; i++)
        {
            QModelIndex idx = mIdxList.at(i);
            QStandardItem *pItm = tixrCore->stdModel->itemFromIndex(idx.sibling(idx.row(), 0));
            if(!leafsList.contains(pItm))
                leafsList << pItm;
        }
    }
    else
    {
        QStandardItem *r = tixrCore->stdModel->itemFromIndex(cartView->rootIndex());
        for(int i=0, n=r?r->rowCount():0; i<n; i++)
        {
            leafsList << r->child(i);
        }
    }

    for(int idx=0; idx<leafsList.size();)
    {
        QStandardItem *item = leafsList.at(idx);
        if(!item->hasChildren())
        {
            idx++;
            continue;
        }

        item = leafsList.takeAt(idx);
        int i = 0;
        for(int j=0, m=item->rowCount(); j<m; j++)
        {
            QStandardItem *pItm = item->child(j);
            if(!leafsList.contains(pItm))
                leafsList.insert(idx+(i++), pItm);
        }
    }

    return leafsList;
}
//-----------------------------------------------------------------------------
void TixrNewTiersWidget::responseGetClientPayments(QString respError, PendingRequest *curReq)
{
    if(!beginHandleResponse(respError, curReq))
        return;

    QStandardItem *responseItem = curReq->getResponseItem();

    proxyModel = new TixrPaymFilterProxyModel(this);
    proxyModel->setRootItem(responseItem);
    proxyModel->setSourceModel(responseItem->model());
    proxyModel->setFilterRole(DataRole::ikey());

    paymTView->setModel(proxyModel);
    paymTView->setRootIndex(proxyModel->mapFromSource(responseItem->index()));

    paymTView->horizontalHeader()->setModel(paymTViewHModel);

    //connect(paymTView->selectionModel(), SIGNAL(selectionChanged(QItemSelection,QItemSelection)), this, SLOT(paymTViewSelectionChanged(QItemSelection,QItemSelection)));

    endHandleResponse(curReq);
}
//-----------------------------------------------------------------------------
void TixrNewTiersWidget::on_pbPayer_clicked()
{
    newPaymentDialog->move(histoPage->rect().center());
    newPaymentDialog->show();
}
//-----------------------------------------------------------------------------
void TixrNewTiersWidget::showEvent(QShowEvent *event)
{
    if(newPaymentDialog && newPaymentDialog->wasVisible())
        newPaymentDialog->show();

    if(rembDialog && rembDialog->wasVisible())
        rembDialog->show();

    event->accept();
}
//-----------------------------------------------------------------------------
void TixrNewTiersWidget::hideEvent(QHideEvent *event)
{
    if(newPaymentDialog)
        newPaymentDialog->hide();

    if(rembDialog)
        rembDialog->hide();

    event->accept();
}
//-----------------------------------------------------------------------------
void TixrNewTiersWidget::setOnTopWidgetsVisibility(bool visible)
{
    if(newPaymentDialog)
        newPaymentDialog->setVisible(visible && newPaymentDialog->wasVisible() && isVisible());

    if(rembDialog)
        rembDialog->setVisible(visible && rembDialog->wasVisible() && isVisible());
}
//-----------------------------------------------------------------------------
void TixrNewTiersWidget::on_stackedWidget_currentChanged(int index)
{
    setOnTopWidgetsVisibility(static_cast<QStackedWidget*>(sender())->widget(index) == histoPage);
}
//-----------------------------------------------------------------------------

void TixrNewTiersWidget::tbvHistoriqueSelectionChanged(const QItemSelection &, const QItemSelection &)
{
    QModelIndexList mIdxList = tbvHistorique->selectionModel()->selectedIndexes();

    QString rxPatt;
    for(int i=0, n=mIdxList.size(); i<n; i++)
    {
        rxPatt += mIdxList.at(i).data(DataRole::scorderid()).toString();
        if(i+1<n)
            rxPatt += "|";
    }

    proxyModel->invalidate();
    proxyModel->setFilterRegExp(rxPatt);
}

/*void TixrNewTiersWidget::paymTViewSelectionChanged(const QItemSelection &, const QItemSelection &)
{
    QModelIndex mIdx = paymTView->selectionModel()->selectedIndexes().first();
    QStandardItem *stdItem = tixrCore->stdModel->itemFromIndex(proxyModel->mapToSource(mIdx));

    stdItem->appendRow(new QStandardItem("test"));

    for(int i=0, n=stdItem->rowCount(); i<n; i++)
    {
        QStandardItem *orderItem = stdItem->child(i);
        QString key = orderItem->data(TixrQDataRole::ikey()).toString();
        key = key.toUpper();
    }
}*/
//-----------------------------------------------------------------------------

void TixrNewTiersWidget::on_pbEdit_clicked()
{
    if(curOrderItem)
    {
        connect(tixrCore, SIGNAL(cartRequestResult(bool, QStandardItem*)), this, SLOT(handleCartRequestResult(bool, QStandardItem*)));
        emit findCart(currentItem->data(DataRole::ikey()).toInt(), curOrderItem);
    }
}

void TixrNewTiersWidget::handleCartRequestResult(bool found, QStandardItem *orderItem)
{
    disconnect(tixrCore, SIGNAL(cartRequestResult(bool, QStandardItem*)), this, SLOT(handleCartRequestResult(bool, QStandardItem*)));
    if(found)
    {
        QMessageBox::information(this, tr("Shopping cart"), tr("A shopping cart already exist for this client."));
        return;
    }

    cartView->setModel(0);

    orderItem->setData(orderItem->data(Qt::DisplayRole), DataRole::scorderdate());//guardar la fecha, en el carrito de compras lo que se visualiza es el nombre del cliente
    orderItem->setText(tiersName);

    orderItem->setData(reinterpret_cast<tixrqptr>(currentItem), DataRole::tiersptr());
    orderItem->setData(currentItem->data(DataRole::ikey()), DataRole::ikey());

    //pagos
    QVariant v = orderItem->data(DataRole::scorderid());
    QStandardItem *paymRoot = currentItem->child(DataColumn::tcipayments()), *orderPayms = new QStandardItem;
    for(int i=0; i<paymRoot->rowCount();)
    {
        QStandardItem *paymItm = paymRoot->child(i);
        int j, n;
        for(j=0, n=paymItm->rowCount(); j<n; j++)
        {
            if(paymItm->child(j)->data(DataRole::ikey())==v)
            {
                orderPayms->appendRow(paymRoot->takeRow(i));
                break;
            }
        }
        if(j==n)
            i++;
    }
    orderItem->appendRow(orderPayms);

    pbEdit->setEnabled(false);
    totalDSB->setValue(0);

    orderItem->parent()->takeRow(orderItem->row());
    emit addCart(orderItem);

    curOrderItem = 0;
}

void TixrNewTiersWidget::on_pbRefund_clicked()
{
    rembDialog->setAmount();
    rembDialog->move(histoPage->rect().center());
    rembDialog->show();
}

void TixrNewTiersWidget::responseDoPayment(QString respError, PendingRequest *curReq)
{
    currentTaskList.removeOne(curReq);
    int wIndex = parentTabWidget->indexOf(this);
    if(currentTaskList.isEmpty() && wIndex>=0)
        parentTabWidget->setTabIcon(wIndex, windowIcon());

    QStandardItem *requestItem = curReq->getRequestItem();
    //QStandardItem *responseItem = curReq->getResponseItem();

    QList<QStandardItem*> *ptrProdsList = reinterpret_cast<QList<QStandardItem*>*>( tixrqptrFromVariant(requestItem->data(DataRole::auxptr1())) );
    QList<QStandardItem*> *ptrPaymsList = reinterpret_cast<QList<QStandardItem*>*>( tixrqptrFromVariant(requestItem->data(DataRole::auxptr2())) );

    if(!respError.isEmpty())
    {
        QMessageBox::information(this, qApp->applicationName(), QObject::tr(respError.toUtf8().constData()));
        delete ptrPaymsList;
        delete ptrProdsList;
        return;
    }

    /*int M = ptrProdsList->size();
    int N = ptrPaymsList->size();

    QStandardItem *orderItem = reinterpret_cast<QStandardItem*>( scqptrFromVariant(requestItem->data(TixrQDataRole::indicator())) );
    int orderId = orderItem->data(TixrQDataRole::scorderid()).toInt();

    if(responseItem->rowCount() < (orderId==-1?(M + N + 1):(M + N)))
    {
        QMessageBox::information(this, qApp->applicationName(), tr("Invalid server response for a payment."));
        delete ptrPaymsList;
        delete ptrProdsList;
        return;
    }

    if(orderId==-1)
    {
        QStandardItem *oidItm = responseItem->takeRow(0).first();
        orderId = oidItm->data(TixrQDataRole::ikey()).toInt();
        orderItem->setData(orderId, TixrQDataRole::scorderid());
        orderItem->setData(oidItm->data(TixrQDataRole::scvar()), TixrQDataRole::scorderdate());
        delete oidItm;
    }

    //extraer los pagos
    QList<QStandardItem*> paymsRespPaymList;
    QStandardItem *paymResp;
    QString iname;
    int n = 0;
    while((paymResp = responseItem->child(0)) && (iname = paymResp->accessibleText()) != "Item3Id")
    {
        paymsRespPaymList << paymResp;
        responseItem->takeRow(0);
        if(iname == "ItemId")
            n++;
    }

    //tratar los pagos hechos con el saldo de la cuenta
    QStandardItem *cartPaymRoot = orderItem->child(1);
    QStandardItem *tiersPaymRoot = 0, *tiersItm = reinterpret_cast<QStandardItem*>(scqptrFromVariant(orderItem->data(TixrQDataRole::tiersptr())));
    if(tiersItm)
        tiersPaymRoot = tiersItm->child(TixrDataCol::tcipayments());

    int i;
    for(i=0, n-=ptrPaymsList->size(); i<n; i++)
    {
        paymResp = paymsRespPaymList.takeFirst();
        QStandardItem *paymItm = 0;

        //comprobar si este pago está en el historial del cliente
        for(int j=0, m=tiersPaymRoot?tiersPaymRoot->rowCount():0; j<m; j++)
        {
            paymItm = tiersPaymRoot->child(j);
            if(paymItm->data(TixrQDataRole::ikey())==paymResp->data(TixrQDataRole::ikey()))
            {
                tiersPaymRoot->takeRow(j);
                cartPaymRoot->appendRow(paymItm);
                break;
            }
            paymItm = 0;
        }

        if(!paymItm)
        {
            //comprobar si está en el carro
            for(int j=0, m=cartPaymRoot->rowCount(); j<m; j++)
            {
                paymItm = cartPaymRoot->child(j);
                if(paymItm->data(TixrQDataRole::ikey())==paymResp->data(TixrQDataRole::ikey()))
                {
                    break;
                }
                paymItm = 0;
            }
        }

        if(!paymItm)
        {
            QString paymAttsStr = paymResp->data(TixrQDataRole::scvar()).toString();
            QStringList paymAtts = paymAttsStr.split('|');

            Q_ASSERT(paymAtts.size() == 12);

            QList<QStandardItem*> itmList;
            itmList << (paymItm = new QStandardItem(paymAtts.at(11)));
            paymItm->setData(paymResp->data(TixrQDataRole::ikey()), TixrQDataRole::ikey());
            QDateTime dt = QDateTime::fromString(paymAtts.at(7), Qt::ISODate);
            paymItm->setData(dt, TixrQDataRole::pdtdateinteg());
            dt = QDateTime::fromString(paymAtts.at(8), Qt::ISODate);
            paymItm->setData(dt, TixrQDataRole::pdtdateannul());

            QStandardItem *detPaymItm = new QStandardItem(paymAtts.at(10));
            detPaymItm->setData(paymAtts.at(0), TixrQDataRole::ikey());
            itmList << detPaymItm;

            detPaymItm = new QStandardItem;
            detPaymItm->setData(QLocale::c().toDouble(paymAtts.at(1)), Qt::DisplayRole);
            itmList << detPaymItm;

            detPaymItm = new QStandardItem(paymAtts.at(9));
            detPaymItm->setData(paymAtts.at(2), TixrQDataRole::ikey());
            detPaymItm->setData(paymAtts.at(5), TixrQDataRole::pdtbankid());
            detPaymItm->setData(paymAtts.at(6), TixrQDataRole::pdtnumcheque());
            itmList << detPaymItm;

            detPaymItm = new QStandardItem(paymAtts.at(4));
            detPaymItm->setData(paymAtts.at(3), TixrQDataRole::ikey());
            itmList << detPaymItm;

            cartPaymRoot->appendRow(itmList);

            updatePaymentItem(paymItm, paymsRespPaymList, orderId);
        }

        delete paymResp;
    }

    //establecer el id. de cada pago
    qreal tp = 0;
    for(int i=0; i<ptrPaymsList->size(); i++)
    {
        QStandardItem *payItm = ptrPaymsList->at(i);
        QStandardItem *root = payItm->parent();
        QStandardItem *devItm = root->child(payItm->row(), TixrDataCol::pdtdevcol());
        QStandardItem *modItm = root->child(payItm->row(), TixrDataCol::pdtmodpaycol());

        tp += root->child(payItm->row(), TixrDataCol::pdtamountcol())->data(Qt::DisplayRole).toDouble();

        if(devItm->data(TixrQDataRole::ikey()).toInt()==-1 || modItm->data(TixrQDataRole::ikey()).toInt()==-1)
        {
            //buscar si ya existe otra deuda
            QStandardItem *paymsItm = tixrCore->stdModel->itemFromIndex(paymTView->rootIndex());
            int j, S;
            for(j=0, S=paymsItm->rowCount(); j<S; j++)
            {
                QStandardItem *payItm2 = paymsItm->child(j);
                devItm = paymsItm->child(j, TixrDataCol::pdtdevcol());
                modItm = paymsItm->child(j, TixrDataCol::pdtmodpaycol());
                if((devItm->data(TixrQDataRole::ikey()).toInt()==-1 || modItm->data(TixrQDataRole::ikey()).toInt()==-1) && payItm2 != payItm && !payItm2->data(TixrQDataRole::pdtcanceled()).isValid())
                {
                    QStandardItem *vItm = root->child(payItm->row(), TixrDataCol::pdtamountcol());
                    QStandardItem *vItm2 = paymsItm->child(j, TixrDataCol::pdtamountcol());
                    vItm2->setData(QVariant(vItm2->data(Qt::DisplayRole).toDouble() + vItm->data(Qt::DisplayRole).toDouble()), Qt::DisplayRole);
                    root->removeRow(payItm->row());
                    break;
                }
            }
            if(j<S)
            {
                ptrPaymsList->removeAt(i);
                continue;
            }
        }

        paymResp = paymsRespPaymList.takeFirst();
        payItm->setData(paymResp->data(TixrQDataRole::scvar()), Qt::DisplayRole);
        payItm->setData(paymResp->data(TixrQDataRole::ikey()), TixrQDataRole::ikey());
        delete paymResp;

        updatePaymentItem(payItm, paymsRespPaymList, orderId);
    }

    //actualizar las cantidades pagadas por cada producto
    for(int i=0; i<M; i++)
    {
        QStandardItem *respItm = responseItem->takeRow(0).first();
        QString payedAndBorrowedStr = respItm->data(TixrQDataRole::scvar()).toString();
        QStringList sl = payedAndBorrowedStr.split("|");
        Q_ASSERT(sl.size()==2);
        qreal v = tixrCore->cLocal.toDouble(sl.first());
        QStandardItem *prodItm = ptrProdsList->at(i);

        int iddet = respItm->data(TixrQDataRole::ikey()).toInt();
        if(iddet > -1)
            prodItm->setData(iddet, TixrQDataRole::dciddetails());//id. en main.detailscommande

        QStandardItem *payedItm = prodItm->parent()->child(prodItm->row(), TixrDataCol::dcpayedcol());
        Q_ASSERT(payedItm);
        qreal q = payedItm->data(Qt::DisplayRole).toDouble() + v;
        payedItm->setData(q, Qt::DisplayRole);
        QColor c = q?(q<(prodItm->data(TixrQDataRole::dcprix()).toDouble()*prodItm->data(TixrQDataRole::dcquantite()).toInt())?Qt::yellow:Qt::green):Qt::red;
        payedItm->setData(c, Qt::DecorationRole);
        //actualizar la cantidad debida
        v = tixrCore->cLocal.toDouble(sl.at(1));
        payedItm->setData(payedItm->data(TixrQDataRole::dcborrowed()).toDouble() + v, TixrQDataRole::dcborrowed());
    }

    // actualizar el historial del cliente con los productos cuya deuda se ha saldado
    QStandardItem *tiersOrdersRoot = tiersItm?tiersItm->child(TixrDataCol::tcihistoric()):0;
    if(tiersOrdersRoot)
    {
        QMap<QStandardItem*, QList<QStandardItem*>&> ;
        QList<QStandardItem*> prodsList = getProds(true, tiersOrdersRoot);
        QStandardItem *prodResp;
        while((prodResp = responseItem->child(0)) && prodResp->accessibleText() != "Item4Id")
        {
            responseItem->removeRow(0);
            QVariant pIdV = prodResp->data(TixrQDataRole::ikey());
            QStringList sList = prodResp->data(TixrQDataRole::scvar()).toString().split('|');
            Q_ASSERT(sList.size() == 2);
            qreal q = sList.first().toDouble();
            orderId = sList.at(1).toInt();

            QStandardItem *prodItm = 0;
            for(int i=0, n=prodsList.size(); i<n; i++)
            {
                prodItm = prodsList.at(i);
                if(prodItm->data(TixrQDataRole::ikey())==pIdV)
                    break;
                prodItm = 0;
            }

            if(prodItm)
            {
                QStandardItem *payedItm = prodItm->parent()->child(prodItm->row(), TixrDataCol::dcpayedcol());
                Q_ASSERT(payedItm);
                qreal tp = payedItm->data(Qt::DisplayRole).toDouble() + q;
                payedItm->setData(tp, Qt::DisplayRole);
                QColor c = tp?(tp<(prodItm->data(TixrQDataRole::dcprix()).toDouble()*prodItm->data(TixrQDataRole::dcquantite()).toInt())?Qt::yellow:Qt::green):Qt::red;
                payedItm->setData(c, Qt::DecorationRole);
                //actualizar la cantidad debida
                q = payedItm->data(TixrQDataRole::dcborrowed()).toDouble() - q;
                q = q<0?0:q;
                payedItm->setData(q, TixrQDataRole::dcborrowed());
            }
        }
    }
    else
    {
        while((paymResp = responseItem->child(0)) && paymResp->accessibleText() != "Item4Id")
            responseItem->removeRow(0);
    }

    //Actualizar los registros de deudas
    while(responseItem->hasChildren())
    {
        QStandardItem *respDebtItm = responseItem->takeRow(0).first();

        Q_ASSERT(respDebtItm->accessibleText() == "Item4Id");

        QStringList respList = respDebtItm->data(TixrQDataRole::scvar()).toString().split('|', QString::SkipEmptyParts);
        Q_ASSERT(respList.size() == 3);

        QStandardItem *root;
        QList<QStandardItem*> paymRcdList = findPaymentsForItem(cartPaymRoot, respList.at(1).toInt(), respDebtItm->data(TixrQDataRole::ikey()).toInt(), respList.at(0).toInt());
        if(paymRcdList.isEmpty())
        {
            if(tiersPaymRoot)
            {
                paymRcdList = findPaymentsForItem(tiersPaymRoot, respList.at(1).toInt(), respDebtItm->data(TixrQDataRole::ikey()).toInt(), respList.at(0).toInt());
                if(!paymRcdList.isEmpty())
                    root = tiersPaymRoot;
            }
        }
        else
            root = cartPaymRoot;

        if(!paymRcdList.isEmpty())
        {
            QStandardItem *detPaymItm = paymRcdList.first();
            QStandardItem *cmdItm = detPaymItm->parent();
            Q_ASSERT(cmdItm);
            QStandardItem *debtItm = cmdItm->parent();
            Q_ASSERT(debtItm);
            qreal p = QLocale::c().toDouble(respList.at(2)), q;
            if(p)
            {
                q = detPaymItm->data(Qt::DisplayRole).toDouble() - p;
                Q_ASSERT(q > 0);
                detPaymItm->setData(p, Qt::DisplayRole);
            }
            else
            {
                q = detPaymItm->data(Qt::DisplayRole).toDouble();
                cmdItm->removeRow(detPaymItm->row());
                if(!cmdItm->hasChildren())
                    debtItm->removeRow(cmdItm->row());
            }

            QStandardItem *amountItm = debtItm->parent()->child(debtItm->row(), TixrDataCol::pdtamountcol());
            q = amountItm->data(Qt::DisplayRole).toDouble() - q;
            if(q)
                amountItm->setData(q, Qt::DisplayRole);
            else
                root->removeRow(debtItm->row());
        }
    }

    if(orderItem->data(TixrQDataRole::ikey()).toInt() != -1)
    {
        //Actualizar el saldo del cliente
        QWidget *w = cartsSW->widget(orderItem->row());
        QDoubleSpinBox *dsb = w->findChild<QDoubleSpinBox*>("dsbCurBalance");
        QDoubleSpinBox *dsb2 = w->findChild<QDoubleSpinBox*>("dsbFinBalance");
        dsb->setValue(dsb2->value());
        QStandardItem *currentItem = reinterpret_cast<QStandardItem*>(scqptrFromVariant(orderItem->data(TixrQDataRole::tiersptr())));
        Q_ASSERT(currentItem);
        currentItem->parent()->child(currentItem->row(), TixrDataCol::taccountcol())->setData(dsb->value(), Qt::DisplayRole);
    }*/

    delete ptrPaymsList;
    delete ptrProdsList;

    /*QItemSelection is;
    paymTViewSelectionChanged(is, is);*/
}

void TixrNewTiersWidget::responseRemboursement(QString respError, PendingRequest *curReq)
{
    currentTaskList.removeOne(curReq);
    int wIndex = parentTabWidget->indexOf(this);
    if(currentTaskList.isEmpty() && wIndex>=0)
        parentTabWidget->setTabIcon(wIndex, windowIcon());

    QStandardItem *requestItem = curReq->getRequestItem();
    QStandardItem *responseItem = curReq->getResponseItem();

    if(!respError.isEmpty())
    {
        QMessageBox::information(this, qApp->applicationName(), QObject::tr(respError.toUtf8().constData()));
    }

    QStandardItem *payItm = responseItem->takeRow(0).first(),
                  *deviseItem = new QStandardItem,
                  *payeurItem = new QStandardItem,
                  *rembParamsItm = requestItem->child(0,1),
                  *montantItem = rembParamsItm->takeChild(0,1),
                  *externItem = rembParamsItm->child(0,3),
                  *commentItem = rembParamsItm->takeChild(0,4);

    payItm->setData(payItm->data(DataRole::scvar()), Qt::DisplayRole);
    payItm->setData(externItem->data(Qt::DisplayRole).toBool(), DataRole::pdtexternal());
    deviseItem->setData(-1, DataRole::ikey());
    payeurItem->setData(-1, DataRole::ikey());
    montantItem->setData(JsonSimple, DataRole::jsonType());
    montantItem->setText("-" + montantItem->text());
    commentItem->setData(JsonSimple, DataRole::jsonType());

    QStandardItem *mpItm = reinterpret_cast<QStandardItem*>( tixrqptrFromVariant(requestItem->data(DataRole::indicator())) );

    QStandardItem *modeItem = new QStandardItem;
    modeItem->setText(mpItm->text());
    modeItem->setData(mpItm->data(DataRole::ikey()), DataRole::ikey());
    modeItem->setData(mpItm->data(DataRole::mptypepaiem()), DataRole::mptypepaiem());
    modeItem->setData(QVariant(-1), DataRole::pdtbankid());

    QStandardItem *paymRoot = currentItem->child(DataColumn::tcipayments());
    Q_ASSERT(paymRoot);
    paymRoot->appendRow(QList<QStandardItem*>()<<payItm<<deviseItem<<montantItem<<modeItem<<payeurItem<<commentItem);

    QStandardItem *saldoItm = currentItem->parent()->child(currentItem->row(), DataColumn::taccountcol());
    qreal b = saldoItm->data(Qt::DisplayRole).toDouble();
    b += montantItem->data(Qt::DisplayRole).toDouble();
    saldoItm->setData(b, Qt::DisplayRole);
}
