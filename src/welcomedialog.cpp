#include "welcomedialog.h"
#include <QtUiTools/QUiLoader>

WelcomeDialog::WelcomeDialog(TixRealmCore *mainObj) : QDialog(0), tixrCore(mainObj)
{
    setWindowFlags(Qt::Dialog /*| Qt::FramelessWindowHint | Qt::WindowStaysOnTopHint*/);
    //setAttribute(Qt::WA_TranslucentBackground, true);

    QUiLoader loader;
    QFile f(":/welcome/welcomedialog.ui");
    f.open(QFile::ReadOnly);
    welUI = loader.load(&f);
    f.close();

    setWindowOpacity(welUI->windowOpacity());
    setStyleSheet(welUI->styleSheet());

    QHBoxLayout *lo = new QHBoxLayout;
    setLayout(lo);
    lo->addWidget(welUI);

    checkBox = welUI->findChild<QCheckBox*>("checkBox");
    proxyLineEdit = welUI->findChild<QLineEdit*>("proxyLineEdit");
    proxyPortSpinBox = welUI->findChild<QSpinBox*>("proxyPortSpinBox");
    proxyUsrLineEdit = welUI->findChild<QLineEdit*>("proxyUsrLineEdit");
    proxyMdpLineEdit = welUI->findChild<QLineEdit*>("proxyMdpLineEdit");
    label_5 = welUI->findChild<QLabel*>("label_5");
    label_6 = welUI->findChild<QLabel*>("label_6");
    label_7 = welUI->findChild<QLabel*>("label_7");

    hostLineEdit = welUI->findChild<QLineEdit*>("hostLineEdit");
    portSpinBox = welUI->findChild<QSpinBox*>("portSpinBox");

    opLineEdit = welUI->findChild<QLineEdit*>("opLineEdit");
    mdpLineEdit = welUI->findChild<QLineEdit*>("mdpLineEdit");

    pbEntrer = welUI->findChild<QPushButton*>("pbEntrer");
    pbSortir = welUI->findChild<QPushButton*>("pbSortir");

    connect(hostLineEdit, SIGNAL(textChanged(QString)), this, SLOT(enableLoginButton(QString)));
    connect(checkBox, SIGNAL(stateChanged(int)), this, SLOT(enableProxyInput(int)));

    QSettings appSettings(QSettings::UserScope, "SantRosSoft", "TicketJobber");
    appSettings.beginGroup("Connections");
    QString host = appSettings.value("host").toString();
    int port = appSettings.value("port").toString().toInt();
    QString login = appSettings.value("login").toString();
    QString pwd = QByteArray::fromBase64(appSettings.value("pwd").toString().toUtf8());

    hostLineEdit->setText(host);
    portSpinBox->setValue(port);
    opLineEdit->setText(login);
    mdpLineEdit->setText(pwd);

    QMetaObject::connectSlotsByName(this);
}

WelcomeDialog::~WelcomeDialog()
{
}

void WelcomeDialog::enableLoginButton(QString newText)
{
    pbEntrer->setEnabled(!newText.isEmpty());
}

void WelcomeDialog::enableProxyInput(int state)
{
    proxyLineEdit->setEnabled(state != Qt::Unchecked);
    proxyPortSpinBox->setEnabled(state != Qt::Unchecked);
    proxyUsrLineEdit->setEnabled(state != Qt::Unchecked);
    proxyMdpLineEdit->setEnabled(state != Qt::Unchecked);
    label_5->setEnabled(state != Qt::Unchecked);
    label_6->setEnabled(state != Qt::Unchecked);
    label_7->setEnabled(state != Qt::Unchecked);
}

void WelcomeDialog::on_pbEntrer_clicked()
{
    pbEntrer->setEnabled(false);
    pbSortir->setEnabled(false);

    // para poner los parametros de conexion al servidor
    tixrCore->setConnParam(hostLineEdit->text(), portSpinBox->value(), proxyLineEdit->text(),
                                  proxyPortSpinBox->value(), proxyUsrLineEdit->text(), proxyMdpLineEdit->text() );

    //tixrCore->setTarget(1, "main.registerpartnersession", "/api/login");

    tixrCore->setCredentials(QString("%1:%2").arg(opLineEdit->text(), mdpLineEdit->text()).toUtf8().toBase64());
    tixrCore->setPgFunction("main.signin");
    tixrCore->sendMsg(0, HTTP_METHOD_ID::GET, "/api", this, "readResponse");
}

void WelcomeDialog::readResponse(QString respError, PendingRequest *curReq)
{
    if(respError.isEmpty())
    {
        QStandardItem *responseItem = curReq->getResponseItem();

        QStandardItem *sessionItem = responseItem->child(0);
        Q_ASSERT(sessionItem);

        if(!sessionItem)
        {
            pbEntrer->setEnabled(true);
            pbSortir->setEnabled(true);
            QMessageBox::information(this, qApp->applicationName(), tr("Invalid server session's response."));
            return;
        }

        int r = DataRole::stoken();
        QVariant v = sessionItem->data(r);
        QStandardItem *userItm = new QStandardItem;
        userItm->setData(v, r);//Session id.

        QString login = opLineEdit->text();
        QString pwd = mdpLineEdit->text();

        userItm->setData(QVariant(login), DataRole::slogin());
        userItm->setData(QVariant(pwd), DataRole::passw());
        r = DataRole::snom();
        userItm->setData(sessionItem->data(r), r);//Nom
        r = DataRole::sprenom();
        userItm->setData(sessionItem->data(r), r);//Prenom
        r = DataRole::starttime();
        userItm->setData(QVariant(QDateTime::currentDateTimeUtc()), r);//fecha y hora de inicio de la conexión
        r = DataRole::operatid();
        userItm->setData(sessionItem->data(r), r);
        tixrCore->stdModel->appendRow(userItm);
        tixrCore->keyToItemMap.insert("login", userItm);
        tixrCore->setSessionId(userItm->data(DataRole::stoken()).toByteArray());//Set the session's id.

        QCheckBox *autoSaveChkBox = welUI->findChild<QCheckBox*>("autoSaveChkBox");
        if(autoSaveChkBox->isChecked())
        {
            QSettings appSettings(QSettings::UserScope, "SantRosSoft", "TicketJobber");
            appSettings.beginGroup("Connections");

            QString host = hostLineEdit->text();
            int port = portSpinBox->value();

            appSettings.setValue("host", host);
            appSettings.setValue("port", port);
            appSettings.setValue("login", login);
            appSettings.setValue("pwd", pwd.toUtf8().toBase64());
        }

        accept();
        qApp->exit(result());
        return;
    }

    pbEntrer->setEnabled(true);
    pbSortir->setEnabled(true);
    QMessageBox::information(this, qApp->applicationName(), tr(respError.toUtf8().constData()));
}

void WelcomeDialog::on_pbSortir_clicked()
{
    reject();
    qApp->exit(result());
}

