#ifndef SCNEWPAYMENTDIALOG_H
#define SCNEWPAYMENTDIALOG_H
#include "dialog.h"
#include "../TixRealmClientPlugins/PictureFlowViewPlugin/pictureflow.h"
#include "checkdialog.h"
#include "crm/tiersfinderdialog.h"

class TixrNewPaymentDialog : public TixrDialog
{
    Q_OBJECT

public:
    explicit TixrNewPaymentDialog(TixRealmCore *mo, QWidget *blockedWidget, QWidget *parent = 0);

private:
    void recordPay(bool dette, QStandardItem *modeItem);

    PictureFlowView *fvModePaiem;
    QComboBox *cbDevises;
    QDoubleSpinBox *dsbAmount;
    QDoubleSpinBox *dsbRestantDu;
    QString dsbRestantDuStyleSheet;
    QTableView *paymTView;
    QComboBox *cbPayeur;
    QPushButton *pbRecordPay;
    QPushButton *pbAjourner;
    QPushButton *pbErasePay;
    QString pbErasePayStyleSheet;
    TixrCheckDialog *checkDetailsDialog;
    TixrTiersFinderDialog *tiersFinderDialog;
    QToolButton *tbReset;

private slots:
    void checkDialogAccepted();
    void on_pbRecordPay_clicked();
    void on_pbErasePay_clicked();
    void on_tbFind_clicked();
    void setPayor();
    void on_tbReset_clicked();
    void on_paymTView_doubleClicked(const QModelIndex & index);
    void paymTViewSelectionChanged(const QItemSelection &, const QItemSelection &);
};

#endif // SCNEWPAYMENTDIALOG_H
