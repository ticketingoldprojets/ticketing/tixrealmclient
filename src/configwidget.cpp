﻿#include "configwidget.h"
#include "mainwindow.h"
#include "tariff/grillesadminpage.h"

TixrConfigWidget::TixrConfigWidget(TixRealmCore *mObj, QStandardItem * respItem, QWidget *parent) :
    TixrWidget("configPage", mObj, "configbtn.png", parent)
{
    this->tixrWidgetType = TIXR_CONFIG;
    if(!getErrorMsg().isEmpty())
        return;

    parentTabWidget->addTab(this, windowIcon(), tr("Configuration"));
    parentTabWidget->setCurrentWidget(this);

    this->loadOperatorInfo(mObj, QModelIndex(), respItem, parent);

    if(!this->tixrCore->keyToItemMap.contains(QStringLiteral("operateurs")))
    {
        this->tixrCore->setPgFunction("main.getoperators");
        PendingRequest *pReq = this->tixrCore->sendMsg(this->tixrCore->stdModel->invisibleRootItem(), HTTP_METHOD_ID::GET, "/api", this, "handleResponse_getoperators");
        this->currentTaskList << pReq;
        pReq->setRespDataMapKey(QStringLiteral("operateurs"));
    }
    else
    {
        this->tvOperators->setModel(this->tixrCore->stdModel);
        this->tvOperators->setRootIndex(this->tixrCore->keyToItemMap["operateurs"]->index());
    }

    this->tvPartners           = this->findChild<QTableView *>("tvPartners");
    this->lePartnerNom         = this->findChild<QLineEdit *>("lePartnerNom");
    this->lePartnerUtilisateur = this->findChild<QLineEdit *>("lePartnerUtilisateur");
    this->lePartnerPassword    = this->findChild<QLineEdit *>("lePartnerPassword");
    this->cbPartnerDirectSales = this->findChild<QCheckBox *>("cbPartnerDirectSales");
    this->cbPartnerSendContact = this->findChild<QCheckBox *>("cbPartnerSendContact");
    this->lePartnerAddress     = this->findChild<QLineEdit *>("lePartnerAddress");
    this->lePartnerPort        = this->findChild<QLineEdit *>("lePartnerPort");
    this->wgtDataPartner       = this->findChild<QWidget *>("wgtDataPartner");
    this->pbConnectParner      = this->findChild<QPushButton *>("pbConnectParner");
    this->partnerItem          = NULL;

    /*if(!this->tixrCore->keyToItemMap.contains("partenaires"))
    {
        this->tixrCore->setPgFunction("main.getpartners");
        this->currentTaskList << this->tixrCore->sendMsg(this->tixrCore->stdModel->invisibleRootItem(), HTTP_METHOD_ID::GET, "/api", this, "handleResponse_getpartners");
    }
    else
    {
        this->tvPartners->setModel(this->tixrCore->stdModel);
        this->tvPartners->setRootIndex(this->tixrCore->keyToItemMap["partenaires"]->index());
    }*/
    getPartnersItemList();

    MainWindow::tixrConnectSlotsByName(this);
}

QStandardItem* TixrConfigWidget::getPartnersItemList(bool justGet){
    if(!this->tixrCore->keyToItemMap.contains("partenaires"))
    {
        this->tixrCore->setPgFunction("main.getpartners");
        PendingRequest *pReq = this->tixrCore->sendMsg(this->tixrCore->stdModel->invisibleRootItem(), HTTP_METHOD_ID::GET, "/api", this, "handleResponse_getpartners");
        this->currentTaskList << pReq;
        pReq->setRespDataMapKey(QStringLiteral("partenaires"));
    }
    else if (!justGet)
    {
        this->tvPartners->setModel(this->tixrCore->stdModel);
        this->tvPartners->setRootIndex(this->tixrCore->keyToItemMap["partenaires"]->index());
    }
    return this->tixrCore->keyToItemMap["partenaires"];
}

void TixrConfigWidget::on_pbAdmGri_clicked()
{
    for(int i=0, n=parentTabWidget->count(); i<n; i++)
    {
        TixrWidget *scW = static_cast<TixrWidget*>(parentTabWidget->widget(i));
        if(scW->tixrWtype() == TIXR_GRILLES)
        {
            parentTabWidget->setCurrentIndex(i);
            return;
        }
    }

    GrillesAdminPage *grillesAdminPage =  new GrillesAdminPage(tixrCore, parentTabWidget);

    QString e = grillesAdminPage->getErrorMsg();

    if(!e.isEmpty())
        QMessageBox::information(0, qApp->applicationName(), tr(e.toUtf8().constData()));
}

void TixrConfigWidget::loadOperatorInfo(TixRealmCore * mObj, const QModelIndex &tiersItemIndex, QStandardItem * respItem, QWidget * parent)
{
    this->tiersItem      = this->tixrCore->stdModel->itemFromIndex(tiersItemIndex.sibling(tiersItemIndex.row(), 0));

    /*
    if(!this->getErrorMsg().isEmpty())
        return;

    this->parentTabWidget->addTab(this, windowIcon(), tr(this->tiersItem ? (this->tiersItem->data(TixrQDataRole::tnom()).toString() + QString(" ") + this->tiersItem->data(TixrQDataRole::qtdisplayrole()).toString()).toUtf8().constData() : QString("Nouveau Operator").toUtf8().constData()));
    this->parentTabWidget->setCurrentWidget(this);
    if(this->tiersItem)
        this->tiersItem->setData(QVariant::fromValue<TixrConfigWidget *>(this), TixrQDataRole::indicator());
    */

    this->leNom             = this->findChild<QLineEdit *>("leNom1");
    this->lePrenom          = this->findChild<QLineEdit *>("lePrenom1");
    this->leIdentifiant     = this->findChild<QLineEdit *>("leIdentifiant1");
    this->leMotPasse        = this->findChild<QLineEdit *>("leMotPasse1");
    this->deDateNaissance   = this->findChild<QDateEdit *>("deDateNaissance1");
//    this->cbTTiers          = this->findChild<QComboBox *>("cbTTiers1");
    this->cbCivilite        = this->findChild<QComboBox *>("cbCivilite1");
    this->cbSex             = this->findChild<QComboBox *>("cbSex1");

//    this->leValeur          = this->findChild<QLineEdit *>("leValeur1");
//    this->leLibelle         = this->findChild<QLineEdit *>("leLibelle1");
//    this->cbTypeValeur      = this->findChild<QComboBox *>("cbTypeValeur1");
//    this->lePriorite        = this->findChild<QLineEdit *>("lePriorite1");
//    this->lvContacts        = this->findChild<QListView *>("lvContacts1");

    this->cbPays            = this->findChild<QComboBox *>("cbPays1");
//    this->cbDepartement     = this->findChild<QComboBox *>("cbDepartement1");
    this->leAdreese1        = this->findChild<QLineEdit *>("leAdreese11");
    this->leAdreese2        = this->findChild<QLineEdit *>("leAdreese21");
    this->cbVille           = this->findChild<QComboBox *>("cbVille1");
    this->leCodePostal      = this->findChild<QLineEdit *>("leCodePostal1");
//    this->cbUsageHabituel   = this->findChild<QComboBox *>("cbUsageHabituel1");
    this->lvAdresse         = this->findChild<QListView *>("lvAdresse1");

//    this->tvHistorique      = this->findChild<QTreeView *>("tvHistorique1");
    this->tvOperators       = this->findChild<QTableView *>("tvOperators");

    this->pageNewTiers      = this->findChild<QWidget *>("pageNewTiers");
    this->pageCarnetAdresse = this->findChild<QWidget *>("pageCarnetAdresse");
//    this->pageContacts      = this->findChild<QWidget *>("pageContacts");

//    this->fillComboBox(this->cbUsageHabituel, QLISTITEMQPAIRSTRINT << QPAIRITEMSTRINT("", -1) << QPAIRITEMSTRINT("TOUS LES USAGES", 0) << QPAIRITEMSTRINT("Facturation", 1) << QPAIRITEMSTRINT("Livraisson", 2));
//    this->fillComboBox(this->cbTypeValeur, QLISTITEMQPAIRSTRINT << QPAIRITEMSTRINT("TELFIXE", 0) << QPAIRITEMSTRINT("PORTABLE", 1) << QPAIRITEMSTRINT("FAX", 2) << QPAIRITEMSTRINT("EMAIL", 3) << QPAIRITEMSTRINT("WEB", 4));
    this->fillComboBox(this->cbSex, QLISTITEMQPAIRSTRINT << QPAIRITEMSTRINT("Masculin", 0) << QPAIRITEMSTRINT("Feminin", 1));

    if(this->tixrCore->keyToItemMap.contains("civilites"))
    {
        this->cbCivilite->setModel(this->tixrCore->stdModel);
        QStandardItem *civRoot = this->tixrCore->keyToItemMap["civilites"];
        this->cbCivilite->setRootModelIndex(civRoot->index());
    }
    else
    {
        this->tixrCore->setPgFunction("main.getcivilites");
        PendingRequest *pReq = this->tixrCore->sendMsg(this->tixrCore->stdModel->invisibleRootItem(), HTTP_METHOD_ID::GET, "/api", this, "responseGetCivilites");
        pReq->setRespDataMapKey("civilites");
        this->currentTaskList << pReq;
    }

//    this->cbTTiers->setModel(this->tixrCore->stdModel);
//    this->cbTTiers->setRootModelIndex(this->tixrCore->keyToItemMap["typetiers"]->index());

    this->cbPays->setModel(this->tixrCore->stdModel);
    this->cbPays->setRootModelIndex(this->tixrCore->keyToItemMap["pays"]->index());

    if(this->tiersItem)
    {
        if(!this->tiersItem->child(DataColumn::tcihistoric()))
        {
            QStandardItem *modelSubItem = new QStandardItem(1, 1);
            modelSubItem->setData("ItemId", DataRole::jsonProperty());
            modelSubItem->setData(JsonObject, DataRole::jsonType());

            this->tiersItem->setChild(DataColumn::tcihistoric(), 0, modelSubItem);

            QStandardItem *modelSubItem2 = new QStandardItem(this->tiersItem->data(DataRole::ikey()).toString());
            modelSubItem2->setData("srvikey", DataRole::jsonProperty());
            modelSubItem2->setData(JsonSimple, DataRole::jsonType());
            modelSubItem->setChild(0, 0, modelSubItem2);

            this->tixrCore->setPgFunction("main.gettierscommandes");
            this->currentTaskList << this->tixrCore->sendMsg(modelSubItem, HTTP_METHOD_ID::POST, "/api", this, "serverResponse");
        }
        else
        {
//            this->tvHistorique->setModel(this->tixrCore->stdModel);
//            this->tvHistorique->setRootIndex(this->tiersItem->child(DataColumn::tcihistoric())->index());
        }
    }
    else
    {
        this->cbCivilite->setCurrentIndex(0);
//        this->cbTTiers->setCurrentIndex(0);
        this->cbPays->setCurrentIndex(0);
        this->on_cbPays_currentIndexChanged(0);
        // this->leOperator->setText(this->tixrCore->keyToItemMap["login"]->data(TixrQDataRole::snom()).toString());
        // this->leOperatorComment->setText(this->tixrCore->keyToItemMap["login"]->data(TixrQDataRole::snom()).toString());
    }
}

void TixrConfigWidget::on_pbNew_clicked()
{
    this->leIdentifiant->clear();
    this->leMotPasse->clear();
    this->lePrenom->clear();
    this->leNom->clear();
    this->leIdentifiant->setFocus();
    this->tiersItem = NULL;
}

void TixrConfigWidget::on_pbDelete_clicked()
{
    QModelIndex index = this->tvOperators->currentIndex();
    if(index.isValid())
    {
        this->tixrCore->setPgFunction("main.deltiers");
        QStandardItem *modelItem = this->tixrCore->keyToItemMap["operateurs"]->child(index.row());

        QJsonObject *jsonObj = new QJsonObject;
        jsonObj->insert(QStringLiteral("srvikey"), QJsonValue(modelItem->data(DataRole::ikey()).toString()) );
        currentTaskList << tixrCore->sendMsg(modelItem, HTTP_METHOD_ID::POST, "/api", this, "handleResponse_deleting", jsonObj);
    }
}

void TixrConfigWidget::on_pbValider_clicked()
{
    if(this->leNom->text().isEmpty() && this->lePrenom->text().isEmpty())
    {
        QMessageBox::information(this, qApp->applicationName(), QObject::trUtf8("The Name and Surname can not be empty"));
        return;
    }

    if(!this->leIdentifiant->text().isEmpty() && this->leMotPasse->text().isEmpty())
    {
        QMessageBox::information(this, qApp->applicationName(), QObject::trUtf8("The password can not be empty"));
        return;
    }

    QJsonObject *jsonObj = tixrCore->widgetToJson(this->pageNewTiers);

    jsonObj->insert(QStringLiteral("tcommentairecol_qtdisplayrole"), QJsonValue(""));
    jsonObj->insert(QStringLiteral("tmodifprivileges"), QJsonValue("0"));
    jsonObj->insert(QStringLiteral("tinfoentreprisecol_qtdisplayrole"), QJsonValue("{}"));

    QString idOperator = this->tixrCore->keyToItemMap["login"]->data(DataRole::operatid()).toString();
    jsonObj->insert(QStringLiteral("topermaker"), QJsonValue(idOperator));
    jsonObj->insert(QStringLiteral("tprioritycomment"), QJsonValue("0"));
    jsonObj->insert(QStringLiteral("tcommentairecol_qtdisplayrole"), QJsonValue("2"));

    // PARAMETROS QUE SON OBLIGATORIOS Y FALTABAN EN EL ANTIGUO CODIGO (20160816)
    jsonObj->insert(QStringLiteral("tttiers"), QJsonValue("cashier"));// persontype ==> 'cashier' *********** (LUEGO HAY QUE VER QUE ELEMENTO VISUAL SE ASOCIA CON EL TIPO DE DATO DE POSTGRESQL "persontype" QUE ES UN ENUM)

    if(this->tiersItem)
    {
        tixrCore->setPgFunction(QStringLiteral("main.updtiers"));
        jsonObj->insert(QStringLiteral("p_idtiers"), QJsonValue(this->tiersItem->data(DataRole::ikey()).toString()) );
        currentTaskList << tixrCore->sendMsg(0, HTTP_METHOD_ID::POST, "/api", this, "handleResponse", jsonObj);
    }
    else {
        this->tiersItem = JsonQStandardItemFactory::createItem();

        if(this->tixrCore->keyToItemMap.contains("operateurs"))
            this->tixrCore->keyToItemMap["operateurs"]->appendRow(this->tiersItem);

        TixRealmCore::addToModel(this->tiersItem, jsonObj);

        tixrCore->setPgFunction(QStringLiteral("main.instiers"));
        currentTaskList << tixrCore->sendMsg(0, HTTP_METHOD_ID::POST, "/api", this, "handleResponse_instiers", jsonObj);
    }
}

void TixrConfigWidget::on_tvOperators_doubleClicked(const QModelIndex & index)
{
    this->tiersItem = this->tixrCore->stdModel->itemFromIndex(index.sibling(index.row(), 0));
}

void TixrConfigWidget::on_cbPays_currentIndexChanged(int index)
{
    QStandardItem * itemPays = this->tixrCore->keyToItemMap["pays"]->child(index);
    if(!itemPays->hasChildren())
    {
        QStandardItem *modelSubItem = new QStandardItem(1, 1);
        modelSubItem->setData("ItemId", DataRole::jsonProperty());
        modelSubItem->setData(JsonObject, DataRole::jsonType());

        itemPays->appendRow(modelSubItem);

        QStandardItem *modelSubItem2 = new QStandardItem(itemPays->data(DataRole::ikey()).toString());
        modelSubItem2->setData("srvikey", DataRole::jsonProperty());
        modelSubItem2->setData(JsonSimple, DataRole::jsonType());
        modelSubItem->setChild(0, 0, modelSubItem2);

        this->tixrCore->setPgFunction("main.getvilles");
        this->currentTaskList << this->tixrCore->sendMsg(modelSubItem, HTTP_METHOD_ID::POST, "/api", this, "handleResponse_getvilles");
    }
    else if(itemPays->child(0)->child(0)->text() != "main.getvilles")  // para que no salga el nombre de la funcion en el comboBox
    {
        this->cbVille->setModel(this->tixrCore->stdModel);
        this->cbVille->setRootModelIndex(itemPays->child(0)->index());
        this->cbVille->setCurrentIndex(0);
    }
}

void TixrConfigWidget::fillComboBox(QComboBox * comboBox, const QList<QPair<QString, int> > & pairList)
{
    if (comboBox == NULL)
        return;

    QStandardItemModel * roleModel = new QStandardItemModel();
    QStandardItem * roleItem;
    for(int i = 0, countList = pairList.count(); i < countList; i++)
    {
        roleModel->appendRow(roleItem = new QStandardItem(pairList[i].first));
        roleItem->setData(pairList[i].second, DataRole::ikey());
    }
    comboBox->setModel(roleModel);
    comboBox->setCurrentIndex(0);
}

void TixrConfigWidget::handleResponse_getpartners(QString respError, PendingRequest *curReq)
{
    if(!beginHandleResponse(respError, curReq))
        return;

    QStandardItem *responseItem = curReq->getResponseItem();

    this->tvPartners->setModel(this->tixrCore->stdModel);
    this->tvPartners->setRootIndex(responseItem->index());

    endHandleResponse(curReq);
}

void TixrConfigWidget::handleResponse_getoperators(QString respError, PendingRequest *curReq)
{
    if(!beginHandleResponse(respError, curReq))
        return;

    QStandardItem *responseItem = curReq->getResponseItem();

    this->tvOperators->setModel(this->tixrCore->stdModel);
    this->tvOperators->setRootIndex(responseItem->index());

    endHandleResponse(curReq);
}

void TixrConfigWidget::handleResponse_instiers(QString respError, PendingRequest *curReq)
{
    if(!beginHandleResponse(respError, curReq))
        return;

    QStandardItem *responseItem = curReq->getResponseItem();

    QStandardItem *ikeyItem = responseItem->child(0);
    Q_ASSERT(ikeyItem);
    QVariant v = ikeyItem->data(DataRole::ikey());

    this->tiersItem->setData(v, DataRole::ikey());

    endHandleResponse(curReq);

    curReq->setResponseItemParent(0);//delete responseItem;
}

void TixrConfigWidget::handleResponse(QString respError, PendingRequest *curReq)
{
    if(!beginHandleResponse(respError, curReq))
        return;

    endHandleResponse(curReq);

    curReq->setResponseItemParent(0);//delete responseItem;
}

void TixrConfigWidget::handleResponse_deleting(QString respError, PendingRequest *curReq)
{
    QStandardItem *targetItem = curReq->getResponseItemParent();
    curReq->setRequestItem(0);

    if(!beginHandleResponse(respError, curReq))
    {
        curReq->setResponseItemParent(0);
        return;
    }

    targetItem->parent()->removeRow(targetItem->row());
//    this->tvOperators->setModel(this->tixrCore->stdModel);
//    this->tvOperators->setRootIndex(this->tixrCore->keyToItemMap.value("operateurs")->index());

    endHandleResponse(curReq);
}

void TixrConfigWidget::handleResponse_getvilles(QString respError, PendingRequest *curReq)
{
    if(!beginHandleResponse(respError, curReq))
        return;

    QStandardItem *responseItem = curReq->getResponseItem();

    this->cbVille->setModel(this->tixrCore->stdModel);
    this->cbVille->setRootModelIndex(responseItem->index());

    endHandleResponse(curReq);
}

void TixrConfigWidget::handleResponse_inspartner(QString respError, PendingRequest *curReq)
{
    if(!beginHandleResponse(respError, curReq))
        return;

    QStandardItem *responseItem = curReq->getResponseItem();

    QStandardItem *ikeyItem = responseItem->child(0);
    Q_ASSERT(ikeyItem);
    QVariant v = ikeyItem->data(DataRole::ikey());

    this->partnerItem->setData(v, DataRole::ikey());

    endHandleResponse(curReq);

    curReq->setResponseItemParent(0);//delete responseItem;
}

/*void TixrConfigWidget::serverResponse(QString respError, PendingRequest *curReq)
{
    QStandardItem *requestItem = curReq->getRequestItem();
    QStandardItem *responseItem = curReq->getResponseItem();

    if(!respError.isEmpty())
    {
        QMessageBox::information(this, qApp->applicationName(), tr(respError.toUtf8().constData()));
        return;
    }

    QString dbfnName = requestItem->child(0)->text();

    if(dbfnName == "main.gettierscommandes")
    {
//        QStandardItem * parentCommandes = requestItem->parent();
//        int row = requestItem->row();
//        parentCommandes->removeRow(row); //ERROR, Yovanis(28/03/2013)
//        parentCommandes->setChild(row, responseItem);
        requestItem->parent()->setChild(requestItem->row(), responseItem);
//        this->tvHistorique->setModel(this->tixrCore->stdModel);
//        this->tvHistorique->setRootIndex(responseItem->index());
    }
    else if(dbfnName == "main.getcommandeproduits")
    {
        QStandardItem * requestParent = requestItem->parent();
        QList<QStandardItem *> itemList = responseItem->takeColumn(0);
        if(itemList.count())
        {
            QStandardItem * tvHistoriqueItem = this->tiersItem->child(DataColumn::tcihistoric())->child(requestParent->data(DataRole::qtdisplayrole()).toInt());
            tvHistoriqueItem->insertColumn(0, itemList);
//            this->tvHistorique->setExpanded(tvHistoriqueItem->index(), true);
        }
        delete requestParent;  // en Qt al borrar el padre se borran sus hijos
        delete responseItem;
    }

    if(curReq)
    {
        this->currentTaskList.removeOne(curReq);
        if(this->currentTaskList.isEmpty())
            this->parentTabWidget->setTabIcon(this->parentTabWidget->indexOf(this), windowIcon());
    }
}*/

void TixrConfigWidget::on_pbValiderParner_clicked()
{
    QJsonObject *jsonObj = tixrCore->widgetToJson(this->wgtDataPartner);

    jsonObj->insert(QStringLiteral("partnerventedirecte"), QJsonValue("true"));

    if(this->partnerItem) // IT IS AN UPDATE
    {
        tixrCore->setPgFunction(QStringLiteral("main.updpartner"));
        jsonObj->insert(QStringLiteral("p_id"), QJsonValue(this->partnerItem->data(DataRole::ikey()).toString()) );
        currentTaskList << tixrCore->sendMsg(0, HTTP_METHOD_ID::POST, "/api", this, "handleResponse", jsonObj);
    }
    else // IT IS AN INSERTION
    {
        this->partnerItem = JsonQStandardItemFactory::createItem();

        QStandardItem *partners = getPartnersItemList(true);

        Q_ASSERT(partners); // Ya debe estar el listado partners en tixrCore->stdModel

        partners->appendRow(this->partnerItem);

        TixRealmCore::addToModel(this->partnerItem, jsonObj);

        tixrCore->setPgFunction(QStringLiteral("main.inspartner"));
        currentTaskList << tixrCore->sendMsg(0, HTTP_METHOD_ID::POST, "/api", this, "handleResponse_inspartner", jsonObj);
    }
}

void TixrConfigWidget::on_pbAnnulerParner_clicked()
{

}

void TixrConfigWidget::on_pbAjouterParner_clicked()
{
    this->lePartnerNom->clear();
    this->lePartnerUtilisateur->clear();
    this->lePartnerPassword->clear();
    this->lePartnerAddress->clear();
    this->lePartnerPort->clear();
    this->cbPartnerDirectSales->setChecked(false);
    this->cbPartnerSendContact->setChecked(false);
    this->partnerItem = NULL;
}

void TixrConfigWidget::on_pbEnleverParner_clicked()
{
    QModelIndex index = this->tvPartners->currentIndex();
    if(index.isValid())
    {
        this->tixrCore->setPgFunction("main.delpartner");

        QStandardItem *partners = getPartnersItemList(true);

        Q_ASSERT(partners); // Ya debe estar el listado partners en tixrCore->stdModel

        QStandardItem *modelItem = partners->child(index.row());

        QJsonObject *jsonObj = new QJsonObject;
        jsonObj->insert(QStringLiteral("srvikey"), QJsonValue(modelItem->data(DataRole::ikey()).toString()) );
        currentTaskList << tixrCore->sendMsg(modelItem, HTTP_METHOD_ID::POST, "/api", this, "handleResponse_deleting", jsonObj);
    }
}

void TixrConfigWidget::on_pbConnectParner_clicked()
{
    /*if(this->partnerItem)
    {
        this->tixrCore->setTarget(this->partnerItem->data(DataRole::ikey()).toInt(), "main.registerpartnersession", "/api");

        this->pbConnectParner->setEnabled(false);

        QStandardItem *modelSubItem = new QStandardItem(1, 1);
        modelSubItem->setData("User", DataRole::jsonProperty());
        modelSubItem->setData(JsonObject, DataRole::jsonType());

        this->tixrCore->stdModel->invisibleRootItem()->appendRow(modelSubItem);

        QStandardItem *modelSubItem2 = new QStandardItem(this->partnerItem->data(DataRole::partnerutilisateur()).toString());
        modelSubItem2->setData("usr", DataRole::jsonProperty());
        modelSubItem2->setData(JsonSimple, DataRole::jsonType());
        modelSubItem->setChild(0, 0, modelSubItem2);

        modelSubItem2 = new QStandardItem(this->partnerItem->data(DataRole::partnermdp()).toString());
        modelSubItem2->setData("pwd", DataRole::jsonProperty());
        modelSubItem2->setData(JsonSimple, DataRole::jsonType());
        modelSubItem->setChild(0, 1, modelSubItem2);

        this->tixrCore->setPgFunction("main.login");
        this->currentTaskList << this->tixrCore->sendMsg(modelSubItem, HTTP_METHOD_ID::POST, "/api/login", this, "readResponse");
    }*/
}

void TixrConfigWidget::on_tvPartners_doubleClicked(const QModelIndex & index)
{
    QStandardItem *partnersItems = this->tixrCore->keyToItemMap["partenaires"];
    if(partnersItems)
    {
        int row = index.row();
        this->partnerItem = partnersItems->child(row);
        this->lePartnerNom->setText(this->partnerItem->data(DataRole::partnernom()).toString());
        this->lePartnerUtilisateur->setText(this->partnerItem->data(DataRole::partnerutilisateur()).toString());
        this->lePartnerPassword->setText(this->partnerItem->data(DataRole::partnermdp()).toString());
        this->lePartnerAddress->setText(this->partnerItem->data(DataRole::partneradressesrv()).toString());
        this->lePartnerPort->setText(this->partnerItem->data(DataRole::partnerportsrv()).toString());

        this->cbPartnerDirectSales->setChecked(JsonQStandardItemFactory::getTargetItem(QStringLiteral("cpartnerenvoyercontactes"), partnerItem)->data(Qt::DisplayRole).toBool());

        this->cbPartnerSendContact->setChecked(JsonQStandardItemFactory::getTargetItem(QStringLiteral("cpartnerenvoyercontactes"), partnerItem)->data(Qt::DisplayRole).toBool());
    }
}

void TixrConfigWidget::responseGetCivilites(QString respError, PendingRequest *curReq)
{
    if(!beginHandleResponse(respError, curReq))
        return;

    this->cbCivilite->setModel(this->tixrCore->stdModel);
    QStandardItem *civRoot = this->tixrCore->keyToItemMap["civilites"];
    this->cbCivilite->setRootModelIndex(civRoot->index());

    endHandleResponse(curReq);
}

void TixrConfigWidget::readResponse(QString respError, PendingRequest *curReq)
{
    /*QStandardItem *responseItem = curReq->getResponseItem();

    if(respError.isEmpty())
    {
        QStandardItem *sessionIdItem = tixrCore->itemByNameFromModel(responseItem, "Session");

        if(!sessionIdItem)
        {
            this->pbConnectParner->setEnabled(true);
            QMessageBox::information(this, qApp->applicationName(), tr("Invalid server session's response."));
            return;
        }

        return;
    }

    this->pbConnectParner->setEnabled(true);
    QMessageBox::information(this, qApp->applicationName(), tr(respError.toUtf8().constData()));*/

}
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
//-----------------------------------------------------------------------------
void TixrConfigWidget::getProducts()
{
    // loadTopLevelProductClasification("main.getallfamcatprods", 0);
}
