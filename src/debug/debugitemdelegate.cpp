#include "debugitemdelegate.h"
#include "../core/customstandarditem.h"
#include <QStandardItemModel>

DebugItemDelegate::DebugItemDelegate()
{

}

DebugItemDelegate::~DebugItemDelegate()
{

}

void DebugItemDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option,
                         const QModelIndex &index) const
{
    QString fullData;

    //standard roles
    QHash<int, QByteArray> roleNamesMap = index.model()->roleNames();

    foreach(int role, roleNamesMap.keys())
    {
        QString str = index.data(role).toString();
        if(!str.isEmpty())
            fullData += QStringLiteral("%1 = %2; ").arg(QString(roleNamesMap[role]), str);
    }

    //custom roles
    const QStandardItemModel *stdModel = static_cast<const QStandardItemModel*>(index.model());
    Q_ASSERT(stdModel);
    CustomStandardItem *itm = dynamic_cast<CustomStandardItem*>(stdModel->itemFromIndex(index));

    if(itm)
    {
        foreach(int role, itm->customRolesList)
        {
            QString str = itm->data(role).toString();
            if(!str.isEmpty())
                fullData += QStringLiteral("%1 = %2; ").arg(QString::number(role), str);
        }
    }

    QStyleOptionViewItem option2(option);

    option2.text = fullData;

    QStyledItemDelegate::paint(painter, option2, index);
}
