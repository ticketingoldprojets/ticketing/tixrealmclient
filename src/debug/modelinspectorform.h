#ifndef MODELINSPECTORFORM_H
#define MODELINSPECTORFORM_H

#include <QWidget>
#include <QStandardItemModel>

namespace Ui {
class ModelInspectorForm;
}

class ModelInspectorForm : public QWidget
{
    Q_OBJECT

public:
    explicit ModelInspectorForm(QStandardItemModel *m, QWidget *parent = 0);
    ~ModelInspectorForm();

private:
    Ui::ModelInspectorForm *ui;
    QStandardItemModel *model;

private slots:
    void on_pbUp_clicked();
    void on_rightTV_doubleClicked(const QModelIndex &index);
};

#endif // MODELINSPECTORFORM_H
