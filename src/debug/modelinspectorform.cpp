#include "modelinspectorform.h"
#include "ui_modelinspectorform.h"
#include "debugitemdelegate.h"

ModelInspectorForm::ModelInspectorForm(QStandardItemModel *m, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ModelInspectorForm), model(m)
{
    ui->setupUi(this);
    DebugItemDelegate *dItmDeleg = new DebugItemDelegate;

    ui->leftTV->setItemDelegate(dItmDeleg);
    ui->rightTV->setItemDelegate(dItmDeleg);

    ui->leftTV->setModel(model);
    ui->rightTV->setModel(model);
}

ModelInspectorForm::~ModelInspectorForm()
{
    delete ui;
}

void ModelInspectorForm::on_pbUp_clicked()
{
    ui->rightTV->setRootIndex(ui->leftTV->rootIndex());
    ui->rightTV->setCurrentIndex(ui->rightTV->currentIndex());

    ui->leftTV->setRootIndex(ui->leftTV->rootIndex().parent());
    ui->leftTV->setCurrentIndex(ui->leftTV->rootIndex().child(0,0));
}

void ModelInspectorForm::on_rightTV_doubleClicked(const QModelIndex &index)
{
    ui->leftTV->setRootIndex(ui->rightTV->rootIndex());
    ui->leftTV->setCurrentIndex(index);

    ui->rightTV->setRootIndex(index);
    ui->rightTV->setCurrentIndex(index.child(0,0));
}
