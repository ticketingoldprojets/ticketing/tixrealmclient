#ifndef DEBUGITEMDELEGATE_H
#define DEBUGITEMDELEGATE_H

#include <QStyledItemDelegate>

class DebugItemDelegate : public QStyledItemDelegate
{
public:
    DebugItemDelegate();
    ~DebugItemDelegate();
    void paint(QPainter *painter, const QStyleOptionViewItem &option,
               const QModelIndex &index) const Q_DECL_OVERRIDE;
};

#endif // DEBUGITEMDELEGATE_H
