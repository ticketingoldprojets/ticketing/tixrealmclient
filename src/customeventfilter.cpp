#include "customeventfilter.h"
#include "core/tixrcore.h"
#include "mainwindow.h"

CustomEventFilter::CustomEventFilter(QObject *parent) :
    QObject(parent)
{
}

bool CustomEventFilter::eventFilter(QObject *obj, QEvent *event)
{
    if(event->type() == QEvent::ApplicationDeactivate)
    {
        TixRealmCore *tixrCore = static_cast<TixRealmCore*>(parent());

        if(tixrCore->objMainWindow)
        {
            MainWindow *mainWindow = static_cast<MainWindow*>(tixrCore->objMainWindow);
            mainWindow->setAlwaysOnTopWidgetsVisibility(false);
        }

        return true;
    }

    if(event->type() == QEvent::ApplicationActivate)
    {
        TixRealmCore *tixrCore = static_cast<TixRealmCore*>(parent());

        if(tixrCore->objMainWindow)
        {
            MainWindow *mainWindow = static_cast<MainWindow*>(tixrCore->objMainWindow);
            mainWindow->setAlwaysOnTopWidgetsVisibility(true);
        }

        return true;
    }

    return QObject::eventFilter(obj, event);
}
