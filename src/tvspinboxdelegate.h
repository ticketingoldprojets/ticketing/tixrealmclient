//-----------------------------------------------------------------------------
#ifndef SCTVSPINBOXDELEGATE_H
#define SCTVSPINBOXDELEGATE_H
//-----------------------------------------------------------------------------
#include <QtCore/QObject> //este include es innecesario, sólo porque de lo contrario la siguiente condición no funciona, aún no sé porqué
#if (QT_VERSION < QT_VERSION_CHECK(5, 0, 0))
#include <QtGui>
#else
#include <QtWidgets>
#endif
#include <QMessageBox>
#include "core/datarole.h"
#include "core/datacolumn.h"
//-----------------------------------------------------------------------------
class TVObject : public QObject
{
    Q_OBJECT

    public:
        TVObject(QObject * parent = NULL);

    public slots:
        void updateCell(QWidget * editor);
};
//-----------------------------------------------------------------------------
class TixrTVSpinBoxDelegate : public QItemDelegate
{
    Q_OBJECT

    private:
         TVObject * tvObj;

    signals:

    public:
        TixrTVSpinBoxDelegate(QObject * parent = NULL, TVObject * tvObj = NULL);
        QWidget * createEditor(QWidget * parent, const QStyleOptionViewItem & option, const QModelIndex & index) const;
        void setEditorData(QWidget * editor, const QModelIndex & index) const;
        void setModelData(QWidget * editor, QAbstractItemModel * model, const QModelIndex & index) const;
        void updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &index) const;
};
//-----------------------------------------------------------------------------
#endif // SCTVSPINBOXDELEGATE_H
//-----------------------------------------------------------------------------



