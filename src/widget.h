#ifndef TIXRWIDGET_H
#define TIXRWIDGET_H

#include "core/tixrcore.h"
#if (QT_VERSION < QT_VERSION_CHECK(5, 0, 0))
#include <QtGui>
#else
#include <QtWidgets>
#endif

/*!
 * \brief Types de fenêtres
 */
#define TIXR_UNDEFINED 0
#define TIXR_ACCEUIL 1 //!< Bienvenue
#define TIXR_STOCK 2 //!< Classe de basse pour la vente
#define TIXR_NEWSEANCE 3 //!< Création d'une nouvelle séance
#define TIXR_NEWSPECT 4 //!< Création d'un nouveau spéctacle
#define TIXR_CONFIG 5 //!< Interface de configuration
#define TIXR_CRM 6 //!< CRM
#define TIXR_NEWTIERS 7 //!< Création de client
#define TIXR_GRILLES 8 //!< Administration de grilles de tarifs
//#define TIXR_CARTS 9 //esta definido en cartwidget.h
#define TIXR_PLAN 10 //!< Interface de plans de salles
#define TIXR_TRANSPORT 11 //!< Création d'une saison pour les batteliers
#define TIXR_TICKETINGSTOCK 12 //!< Vente en billetterie
#define TIXR_BOUTIQUESTOCK 13 //!< Vente de produits boutique
#define TIXR_TRANSPORTSTOCK 14 //!< Voyages
#define TIXR_NEWOPERATOR 15 //!< Création d'operateurs

#define QPAIRITEMSTRSTR(str, key) QPair<QString, QString>(str, key)
#define QLISTITEMQPAIRSTRSTR QList<QPair<QString, QString> >()

class TixrWidget : public QWidget
{
    Q_OBJECT

    QMovie *waitMovie;//!< Animation pendant que l'interface gère une requête.

private slots:
    /*!
     * \brief on_pageSwitcher_clicked active une page du QStackedWidget indiqué.
     *
     *  Le nom du QStackedWidget est transmis dans la propriété <I>tixrPageHolder</I> de l'objet déclencheur de la procédure et
     *  l'indice de la page dans la propriété <I>tixrPageIdx</I>.
     */
    void on_pageSwitcher_clicked();

    /*!
     * \brief on_imgSelector_clicked ouvre une boîte de dialogue de fichier permetant choisir une image.
     *
     *  L'image choisie est montrée dans un QListView nommé <I>imgViewer</I>.
     */
    void on_imgSelector_clicked();

    /*!
     * \brief on_imgSelector_clicked ouvre une boîte de dialogue permetant choisir une couleur.
     *
     *  La couleur choisie est montrée comme la couleur de fond de l'objet déclancheur de la procédure.
     */
    void on_colorSelector_clicked();

    /*!
     * \brief upDateTabIcon actualice l'animation d'attente s'il y a encore de tâches encours pour cette fenêtre.
     * \param rect inutile
     */
    void upDateTabIcon(const QRect & rect);

    QStandardItem *mapData(QWidget *w, QString &roleName, QStandardItem *cI, QDataWidgetMapper *mapper);

    void assignDefaultData(QWidget *w, QStandardItem *targetItem, bool editMode, QString &roleName);

    void assignDefaultAddressData(QWidget *w, QStandardItem *targetItem, bool editMode, QDataWidgetMapper *mapper);

    void assignDefaultLabelData(QLabel *lb, QStandardItem *stdItem, bool editMode);

    void assignDefaultComboBoxData(QComboBox *cb, QStandardItem *stdItem, bool editMode);

    void assignDefaultPushButtonData(QPushButton *pb, QStandardItem *stdItem, bool editMode, const QString attName);

    void assignDefaultWidgetData(QWidget *w, QStandardItem *stdItem, bool editMode);

protected:
    explicit TixrWidget(QString uiName, TixRealmCore *mObj, QString icon = QString::null, QWidget *parent = 0, QStandardItem *r = 0, QStandardItem *cItem = 0);

    void setErrorMsg(QString e){errorStr = e;}

    bool beginHandleResponse(QString respError, PendingRequest *curReq);
    int endHandleResponse(PendingRequest *curReq);

    void mapWidgetsToModel(const QWidget *w = 0);

    /*!
     * \brief rootItem contient, si c'est le cas, la requête et plus tard la reponse qui sont gérés dans cette fenêtre.
     */
    QStandardItem *rootItem;//Elemento padre de la entidad que se edita/visualiza en esta ventana. Pj. para una seance el rootItem será el item del espectáculo correspondiente.
    QStandardItem *currentItem;//Elemento en la primera columna de la entidad que se edita en esta ventana.
    QTabWidget *parentTabWidget;//!< QTabWidget qui contient cette fenêtre.
    TixRealmCore *tixrCore;
    QString errorStr;
    QDataWidgetMapper *dataWidgetMapper;
    QList<QDataWidgetMapper*> subLevelsDataWidgetMapperList;
    int tixrWidgetType;
    QString uiName;

public:
    virtual ~TixrWidget();
    QString getErrorMsg(){QString aux = errorStr; errorStr.clear(); return aux;}
    int tixrWtype(){return tixrWidgetType;}
    bool isClosable(){return currentTaskList.isEmpty();}//no hay solicitudes pendientes de respuesta del servidor
    virtual void setOnTopWidgetsVisibility(bool visible){Q_UNUSED(visible); return;}
    //virtual bool isActivable();//si puede ser la página activa, una pág. no puede activarse básicamente porque la interfaz aún no está lista: se espera una resp. del servidor

    QList<PendingRequest*> currentTaskList;//Lista de hilos con las solicitudes actuales en espera de respuesta del servidor.
                                             //Tengo que refactorizar el código de carga del plano para no tener que declarar esta propiedad pública.

    QWidget *dynWidget;
    bool updateMode;
    bool accepted;

    //unit testing
    friend class WidgetBaseTest;

public slots:
    void commonResponseHandler(QString respError, PendingRequest *curReq);

signals:
    void noPendingRequests();//!<< This widget has no more pending server request
    void responseReady(PendingRequest *curReq);
};

Q_DECLARE_METATYPE(QPixmap)

#endif // TIXRWIDGET_H

