#ifndef CUSTOMEVENTFILTER_H
#define CUSTOMEVENTFILTER_H

#include <QObject>

class CustomEventFilter : public QObject
{
    Q_OBJECT

public:
    explicit CustomEventFilter(QObject *parent = 0);

protected:
    bool eventFilter(QObject *obj, QEvent *event);
};

#endif // CUSTOMEVENTFILTER_H
