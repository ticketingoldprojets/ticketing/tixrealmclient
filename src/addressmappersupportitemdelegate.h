#ifndef ADDRESSMAPPERSUPPORTITEMDELEGATE_H
#define ADDRESSMAPPERSUPPORTITEMDELEGATE_H

#include <QStyledItemDelegate>
#include <QUndoStack>

class AddressMapperSupportItemDelegate : public QStyledItemDelegate
{
    Q_OBJECT

    QString concatAddressFields(const QModelIndex &index) const;

public:
    QUndoStack *undoStack;

    explicit AddressMapperSupportItemDelegate(QObject *parent = 0);
    void setEditorData(QWidget *editor, const QModelIndex &index) const;
    void setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const;
};

#endif // ADDRESSMAPPERSUPPORTITEMDELEGATE_H
