#ifndef PrintManager_H
#define PrintManager_H

#include <QStandardItem>
#include <QObject>
#include <QThread>
#include <QByteArray>
#include <QXmlStreamWriter>
#include <QNetworkReply>

#if (QT_VERSION < QT_VERSION_CHECK(5, 0, 0))
#include <QtGui>
#else
#include <QtWidgets>
#endif

#include "core/pendingrequest.h"

class PrintProgressWidget;
class QTimer;
class QBoxLayout;
class PrintJob;

typedef QHash<QString, QVariant> PrintResultData;
typedef QPair<QString, QStandardItem*> PrintItem;
typedef QList<PrintItem> PrintItems;

QVariant PtrToVariant(void *ptr);

template<typename T> T *VariantToPtr(QVariant v)
{
    #ifdef QT_POINTER_SIZE
    #if QT_POINTER_SIZE == 4
    qint32 p = (qint32)v.toUInt();
    #elif QT_POINTER_SIZE == 8
    qint64 p = (qint64)v.toUInt();
    #endif
    #else
    #error Invalid QT_POINTER_SIZE value stored!
    #endif
    return reinterpret_cast<T*>(p);
}


/*!
 * \brief The PrintManager class
 */
class PrintManager : public QObject
{
    Q_OBJECT
    Q_ENUMS(StatusCode)

public:
    explicit PrintManager(class TixRealmCore *mObj, QObject *parent = 0);
    ~PrintManager();

    enum StatusCode {
        Ok, ErrorGeneratingReport, InvalidData, InvalidImageError, ImageConversionError, BadRequest, NothingToPrint,
        ServerError, ServerNotFound, ServerRefusedConnection, ServerTimeout, AppServerError,
        PrintStarted, PagePrinted, PrintPaused, PrintResumed, PrintAborted, ConfirmAbortPrinting, PrintFinished,

        // Application status codes
        PrintNotStarted, PrintNotFinished
    };

    PrintJob *print(QString reportFileName, PrintItems printItems, QObject *threadParent, QWidget *uiParent = 0, QVariant userData = QVariant::Invalid);

    void lockCreatePrintPagesMutex(bool lock = true);
    StatusCode createPrintPages(QList<QByteArray*> *pages_out, QString reportFileName, PrintItems printItems);

    PrintJob *addPrintJob(QString printServerUrl, QObject *threadParent, QWidget *uiParent = 0, QVariant userData = QVariant::Invalid);
    void removePrintJob(PrintJob *printJob);

    void showPrintJob(PrintJob *printJob);
    void hidePrintJob(PrintJob *printJob);

    static QString statusCodeToString(StatusCode statusCode, const QString format = tr("Status %2: %1"));
    static QString generalStatusMsg(StatusCode statusCode);

    class TixRealmCore *getScMainObj() const { return tixrCore; }

    class QNetworkAccessManager *netAccMgr;

private:
    class TixRealmCore *tixrCore;

    QList<PrintJob*> printJobs;
    PrintJob *visiblePrintJob;

    QMutex createPrintPagesMutex;
    int numPages;

signals:
    // Need to use fully qualified params for registered meta types!
    void printResult(PrintManager::StatusCode statusCode, QString statusMsg, PrintResultData printResultData);

public slots:
    void on_reportDataSourceOpened(const QString &dsId, int totalRows);
};


/*!
 * \brief The PrintJob class
 */
class PrintJob : public QObject
{
    Q_OBJECT

public:
    PrintJob(PrintManager *printMgr, QString printServerUrl, QObject *threadParent, QWidget *parent = 0, QVariant userData = QVariant::Invalid);
    ~PrintJob();

    void print(QList<QByteArray*> pages);
    void print(QString reportFileName, PrintItems printItems);
    void confirmAbortPrinting();

    void show();
    void hide();

private:
    QString printServerUrl;
    PrintManager *printMgr;
    QString reportFileName;
    PrintItems printItems;
    QList<QByteArray*> pages;
    QNetworkReply *netReply;
    QVariant userData;

    int totalPages;
    int currentPage;
    int itemsIndex;
    PrintProgressWidget *printProgressWidget;
    bool networkReplyErrorSignalled;
    int lastHttpStatusCode;
    QString lastHttpReasonPhrase;
    QString lastResponseErrorMessage;
    int printId;
    volatile bool printingPaused;
    bool abortPrintPending;
    PrintManager::StatusCode abortReason;

    void sendPrintPageRequest();
    static void createPrintPage(QByteArray *page, QString reportFileName, QString id);
    void sendCreatedPrintPageRequest();
    void createXmlReqBody(QIODevice &xmlReqBody, QXmlStreamWriter &xmlStream, const QByteArray &page);
    PrintManager::StatusCode readResponse();
    void signalNetworkReplyError(PrintManager::StatusCode statusCode);
    void emitSignal_printResult(PrintManager::StatusCode statusCode, QString statusMsg = QString::null);
    void deferredEmitSignal_printResult(PrintManager::StatusCode statusCode, QString statusMsg = QString::null);
    PrintResultData prepareEmitSignal_printResult(PrintManager::StatusCode statusCode, QString *statusMsg);

    void setNextTicketInSeqPrintState(QString state);

signals:
    // Need to use fully qualified params for registered meta types!
    void printResult(PrintManager::StatusCode statusCode, QString statusMsg, PrintResultData printResultData);

private slots:
    void pagePrinted();
    void abortPrinting(PrintManager::StatusCode abortReason = PrintManager::PrintAborted);
    void finishPrinting();
    void on_pagesGenerated(PrintManager::StatusCode statusCode);
    void responseSetNextTicketInSeqPrintState(QString respError, PendingRequest *curReq);

    void on_networkReply_finish(QNetworkReply *reply);
    void on_networkReply_readyRead();
    void on_networkReply_error(QNetworkReply::NetworkError);
    void on_networkReply_sslErrors(QNetworkReply::NetworkError);

    friend class PrintJobWorker;
};


/*!
 * \brief The PrintJobWorker class
 */
class PrintJobWorker : public QThread
{
    Q_OBJECT

public:
    PrintJobWorker(PrintJob *owner) : owner(owner) {}
    void startGeneratingPages(QList<QByteArray*> *pages, QString reportFileName, PrintItems printItems);

private:
    PrintJob *owner;
    QString reportFileName;
    PrintItems printItems;
    QList<QByteArray*> *pages;

    void run();

signals:
    void pagesGenerated(PrintManager::StatusCode statusCode);
};


/*!
 * \brief The PrintProgressWidget class
 */
class PrintProgressWidget : public QProgressBar //QDockWidget
{
    Q_OBJECT

public:
    PrintProgressWidget(int min, int max, QWidget *parent = 0);
    ~PrintProgressWidget();

    void setText(QString text) { progressBar->setFormat(text); }
    void setValue(int value) { progressBar->setValue(value); }
    void setMaximum(int max) { progressBar->setMaximum(max); }
    int maximum() { return progressBar->maximum(); }
    void update(int currentPage);

private:
//  QBoxLayout *layout;
    QProgressBar *progressBar;
//  QPushButton *cancelButton;
};


#endif // PrintManager_H
