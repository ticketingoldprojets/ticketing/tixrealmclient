#include "customproxymodel.h"

TixrSortFilterProxyModel::TixrSortFilterProxyModel(QObject *parent) : QSortFilterProxyModel(parent)
{
    rootItem = 0;
    stdModel = 0;
}

void TixrSortFilterProxyModel::setSourceModel(QAbstractItemModel *sourceModel)
{
    stdModel = static_cast<QStandardItemModel*>(sourceModel);
    QSortFilterProxyModel::setSourceModel(sourceModel);
}

bool TixrSortFilterProxyModel::descendant(QModelIndex *idx) const
{
    QStandardItem *stdItem = stdModel->itemFromIndex(*idx);

    if(!(stdItem && rootItem))
        return true;

    //hijo del nodo raíz
    QStandardItem *aux = stdItem;
    do
    {
        if(aux == rootItem)
            return true;

        aux = aux->parent();
    }
    while(aux);

    return false;
}

bool TixrSortFilterProxyModel::ascendant(QModelIndex *idx) const
{
    QStandardItem *stdItem = stdModel->itemFromIndex(*idx);

    if(!(stdItem && rootItem))
        return true;

    //padre del nodo raíz
    QStandardItem *aux = rootItem;
    do
    {
        if(aux == stdItem)
            return true;

        aux = aux->parent();
    }
    while(aux);

    return false;
}

bool TixrSortFilterProxyModel::filterAcceptsRow(int sourceRow, const QModelIndex &sourceParent) const
{
    QModelIndex index = sourceModel()->index(sourceRow, 0, sourceParent);

    if(ascendant(&index))
        return true;

    if(!descendant(&index))
        return false;

    return QSortFilterProxyModel::filterAcceptsRow(sourceRow, sourceParent);
}
//-----------------------------------------------------------------------------

TixrPaymFilterProxyModel::TixrPaymFilterProxyModel(QObject *parent) : TixrSortFilterProxyModel(parent)
{
}

bool TixrPaymFilterProxyModel::filterAcceptsRow(int sourceRow, const QModelIndex &sourceParent) const
{
    QModelIndex index = sourceModel()->index(sourceRow, 0, sourceParent);

    if(ascendant(&index))
        return true;

    if(!descendant(&index))
        return false;

    QRegExp rx = filterRegExp();
    //QString rxstr = rx.pattern();

    if(rx.isEmpty() || !rx.isValid())
        return true;

    int col = filterKeyColumn();

    for(int i=0, n=sourceModel()->rowCount(index); i<n; i++)
    {
        int role = filterRole();
        QString data = index.child(i,col).data(role).toString();
        if(rx.exactMatch(data))
            return true;
    }

    return false;
}
