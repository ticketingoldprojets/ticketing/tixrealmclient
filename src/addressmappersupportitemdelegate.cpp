#include "addressmappersupportitemdelegate.h"

#include "core/datarole.h"
#include "core/datacolumn.h"
#include "undo/setmodeldatacommand.h"
#include "undo/setaddressdatacommand.h"
#include <QComboBox>
#include <QLabel>
#include <QPushButton>
#include <QItemEditorFactory>
#include <QGroupBox>

AddressMapperSupportItemDelegate::AddressMapperSupportItemDelegate(QObject *parent) :
    QStyledItemDelegate(parent)
{
    undoStack = new QUndoStack(this);
}

QString AddressMapperSupportItemDelegate::concatAddressFields(const QModelIndex &index) const
{
    QModelIndex parentIndex = index.parent();
    QString label;
    const QAbstractItemModel *model = index.model();
    for(int j=0, n=model->columnCount(parentIndex); j<n; j++)
    {
        QString a = model->data(parentIndex.child(0, j)).toString();
        label += a.isEmpty() ? QString::null : a + ((j+1<n)?QStringLiteral(". "):QString::null);
    }
    return label;
}

void AddressMapperSupportItemDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
    if(editor->inherits("QComboBox"))
    {
        QString tixrRelevantRole = editor->property("tixrRelevantRole").toString();
        QComboBox *cb = static_cast<QComboBox*>(editor);
        if(tixrRelevantRole.isEmpty())//caso de uso #1
        {
            QString str = index.data().toString();
            cb->setCurrentText(str);
        }
        else
        {
            int idx = cb->findData(index.data(), DataRole::FromName(tixrRelevantRole));
            Q_ASSERT(idx > -1);//unknown value
            if(idx > -1)
                cb->setCurrentIndex(idx);
        }
    }else
        QStyledItemDelegate::setEditorData(editor, index);

    QString address = concatAddressFields(index);
    QGroupBox *gb = static_cast<QGroupBox*>(parent());
    gb->setTitle(address);
}

void AddressMapperSupportItemDelegate::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const
{
    if(editor->inherits("QComboBox"))
    {
        QString tixrRelevantRole = editor->property("tixrRelevantRole").toString();
        QComboBox *cb = static_cast<QComboBox*>(editor);

        QVariant v = tixrRelevantRole.isEmpty() ? QVariant(cb->currentText()) : cb->currentData(DataRole::FromName(tixrRelevantRole));
        Qt::ItemDataRole r = tixrRelevantRole.isEmpty() ? Qt::EditRole : (Qt::ItemDataRole)DataRole::FromName(tixrRelevantRole);

        undoStack->push( new SetAddressDataCommand(model, index, r, v) );
    }else

    {
        QByteArray n = editor->metaObject()->userProperty().name();
        if (n.isEmpty())
            n = itemEditorFactory()->valuePropertyName( model->data(index, Qt::EditRole).userType() );

        if (!n.isEmpty())
            undoStack->push( new SetAddressDataCommand(model, index, Qt::EditRole, editor->property(n)) );
    }
}
