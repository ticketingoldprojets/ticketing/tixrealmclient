#include "subleveldatawidgetmapper.h"
#include <QModelIndex>

SubLevelDataWidgetMapper::SubLevelDataWidgetMapper(QObject *parent) : QDataWidgetMapper(parent)
{

}

void SubLevelDataWidgetMapper::setCurrentSublevelRootIndex(int index)
{
    QDataWidgetMapper *parentMapper = static_cast<QDataWidgetMapper*>(parent());

    QModelIndex newRoot = model()->index(index, rootIndex().column(), parentMapper->rootIndex());

    setRootIndex(newRoot);
}
