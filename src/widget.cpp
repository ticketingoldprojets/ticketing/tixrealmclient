#include "widget.h"
#include "core/jsonqstandarditemfactory.h"
#include "mappersupportitemdelegate.h"
#include "addressmappersupportitemdelegate.h"
#include "undo/setmodeldatacommand.h"
#include "subleveldatawidgetmapper.h"
#include <QtUiTools/QUiLoader>

TixrWidget::TixrWidget(QString uiName, TixRealmCore *mObj, QString icon, QWidget *parent, QStandardItem *r, QStandardItem *cItem) :
    QWidget(parent), tixrCore(mObj), rootItem(r), parentTabWidget(0), waitMovie(0),
    dataWidgetMapper(0), currentItem(0), accepted(false), tixrWidgetType(TIXR_UNDEFINED)
{
    QUiLoader loader;
    QFile f(":/formconfig/" + uiName + ".ui");
    f.open(QFile::ReadOnly);
    dynWidget =  loader.load(&f, this);
    f.close();

    this->uiName = uiName;

    if(!dynWidget)
    {
        errorStr = tr("Cannot load the graphical user interface.");
        return;
    }

    QString wName = dynWidget->objectName();
    dynWidget->setObjectName("dynWidget");
    setObjectName(wName);

    QVBoxLayout *layout = new QVBoxLayout;
    setLayout(layout);
    layout->addWidget(dynWidget);

    if(!icon.isEmpty())
        setWindowIcon(QIcon(":images/btn/" + icon));

    setWindowTitle(dynWidget->windowTitle());
    dynWidget->setWindowTitle(QString::null);

    updateMode = currentItem = cItem;

    if(rootItem)
        mapWidgetsToModel();

    if(parent)
    {
        QString s = parent->metaObject()->className();
        if(s == "QTabWidget")//si es una pestaña mostrar una animación mientras se espera la respuesta del servidor
        {
            parentTabWidget = static_cast<QTabWidget*>(parent);
            waitMovie = new QMovie(":/ajax-loader.gif");
            connect(waitMovie, SIGNAL(updated(QRect)), this, SLOT(upDateTabIcon(QRect)));
            waitMovie->start();
        }
    }
}

bool TixrWidget::beginHandleResponse(QString respError, PendingRequest *curReq)
{
    errorStr = respError;
    emit responseReady(curReq);

    if(curReq)
    {
        currentTaskList.removeOne(curReq);
        if(currentTaskList.isEmpty())
        {
            emit noPendingRequests();
        }
    }

    if(!respError.isEmpty())
    {
        QMessageBox::information(this, qApp->applicationName(), tr(respError.toUtf8().constData()));
        return false;
    }

    return true;
}

int TixrWidget::endHandleResponse(PendingRequest *curReq)
{
    Q_UNUSED(curReq);

    if(!parentTabWidget)
        return -1;

    int wIndex = parentTabWidget->indexOf(this);
    if(wIndex == -1)//la ventana fue cerrada sin esperar la respuesta del servidor
        return -1;

    if(currentTaskList.isEmpty())
    {
        parentTabWidget->setTabIcon(wIndex, windowIcon());
    }

    parentTabWidget->setCurrentIndex(wIndex);

    return wIndex;
}

TixrWidget::~TixrWidget()
{
    if(waitMovie)
        delete waitMovie;

    //undo chages
    if(tixrWidgetType==TIXR_NEWSPECT || tixrWidgetType==TIXR_NEWSEANCE)
    {
        if(!accepted)
        {
            if(updateMode)
            {
                //undo changes to currentItem
                static_cast<MapperSupportItemDelegate*>(this->dataWidgetMapper->itemDelegate())->undoStack->setIndex(0);
            }
            else
            {
                this->currentItem->parent()->removeRow(this->currentItem->row());
            }
        }
    }

    //delete dataWidgetMapper;
}

void TixrWidget::commonResponseHandler(QString respError, PendingRequest *curReq)
{
    if(!beginHandleResponse(respError, curReq))
        return;

    //Specific treatment
    if(curReq->senderSlot2)
        QMetaObject::invokeMethod(curReq->sender, curReq->senderSlot2, Q_ARG(PendingRequest*,curReq));

    endHandleResponse(curReq);
}

void TixrWidget::upDateTabIcon(const QRect & rect)
{
    Q_UNUSED(rect)

    if(currentTaskList.isEmpty())
        return;

    QIcon icon(waitMovie->currentPixmap());

    int wIndex = parentTabWidget->indexOf(this);

    if(wIndex>-1)
        parentTabWidget->setTabIcon(wIndex, icon);
}

void TixrWidget::on_pageSwitcher_clicked()
{
    QWidget *b = static_cast<QWidget*>(sender());

    QStackedWidget *stackedWidget = findChild<QStackedWidget*>(b->property("tixrPageHolder").toString());
    Q_ASSERT_X(stackedWidget, "skin error", "This skin is buggy");

    int idx = b->property("tixrPageIdx").toInt();

    if(stackedWidget && idx >= 0 && idx < stackedWidget->count())
        stackedWidget->setCurrentIndex(idx);
}

void TixrWidget::on_imgSelector_clicked()
{
    QString fileName = QFileDialog::getOpenFileName(this,
         tr("Open Image"), "", tr("Image Files (*.png *.jpg *.bmp)"));

    if(fileName.isEmpty())
        return;

    if(!QFile::exists(fileName))
    {
        QMessageBox::warning(this, qApp->applicationName(), tr("File not found"));
        return;
    }

    QLabel *imgViewer = findChild<QLabel*>("imgViewer");
    if(imgViewer)
    {
        QPixmap pixmap(fileName);
        if(pixmap.isNull())
        {
            QMessageBox::warning(this, qApp->applicationName(), tr("Error loading image file"));
            return;
        }

        imgViewer->setPixmap(pixmap);

        if(this->currentItem)//por el momento el itemdelegate no funciona con el qlabel, así que hago la asignación de datos manualmente
        {
            QString ptyName = imgViewer->property("tixrJsonKey").toString();
            QStandardItem *tgtItem = JsonQStandardItemFactory::getTargetItem(ptyName, this->currentItem);
            static_cast<MapperSupportItemDelegate*>(this->dataWidgetMapper->itemDelegate())->undoStack->push( new SetModelDataCommand(tgtItem->model(), tgtItem->index(), (Qt::ItemDataRole)DataRole::FromName(ptyName), pixmap) );
        }
    }
}

void TixrWidget::on_colorSelector_clicked()
{
    QColor c = QColorDialog::getColor();
    if(c.isValid())
    {
        static_cast<QWidget*>(sender())->setStyleSheet(QString("background-color: %1;").arg(c.name()));
    }
}

void TixrWidget::mapWidgetsToModel(const QWidget *w)
{
    if(!w)
        w = dynWidget;

    dataWidgetMapper = new QDataWidgetMapper(this);
    dataWidgetMapper->setModel(tixrCore->stdModel);
    dataWidgetMapper->setRootIndex(this->rootItem->index());
    dataWidgetMapper->setSubmitPolicy(QDataWidgetMapper::AutoSubmit);

    dataWidgetMapper->setItemDelegate(new MapperSupportItemDelegate(this));

    bool editMode = true;

    if(!this->currentItem)//this->currentItem != 0 si se trata de una actualización
    {
        this->currentItem = new QStandardItem;
        this->rootItem->appendRow(this->currentItem);
        editMode = false;
    }

    QMap<QGroupBox *, QList<QWidget *> > subLevelsMap = tixrCore->getSubLevelsContainerMap(w);

    QList<QWidget*> wList = w->findChildren<QWidget*>();
    for(int i=0, n=wList.size(); i<n; i++)
    {
        QWidget *child = wList.at(i);

        QString attName = child->property("tixrJsonKey").toString();

        if(attName.isEmpty())
            continue;

        if(tixrCore->subLevelsContains(subLevelsMap, child))
            continue;

        QStandardItem *tgtItm = mapData(child, attName, this->currentItem, dataWidgetMapper);
        assignDefaultData(child, tgtItm, editMode, attName);
    }

    foreach(QGroupBox *container, subLevelsMap.keys())
    {
        QString attName = container->property("tixrJsonKey").toString();
        QStandardItem *subRoot = JsonQStandardItemFactory::getTargetItem(attName, this->currentItem),
                      *subCurrentItem = 0;

        SubLevelDataWidgetMapper *subLevelDataWidgetMapper = new SubLevelDataWidgetMapper(dataWidgetMapper);
        subLevelDataWidgetMapper->setModel(tixrCore->stdModel);
        subLevelDataWidgetMapper->setRootIndex(subRoot->index());
        subLevelDataWidgetMapper->setSubmitPolicy(QDataWidgetMapper::AutoSubmit);
        subLevelDataWidgetMapper->setItemDelegate(new AddressMapperSupportItemDelegate(container));
        subLevelsDataWidgetMapperList << subLevelDataWidgetMapper;

        connect(dataWidgetMapper, &QDataWidgetMapper::currentIndexChanged, subLevelDataWidgetMapper, &SubLevelDataWidgetMapper::setCurrentSublevelRootIndex);

        QList<QWidget*> wList = subLevelsMap.value(container);

        for(int i=0, n=wList.size(); i<n; i++)
        {
            QWidget *child = wList.at(i);

            QString attName = child->property("tixrJsonKey").toString();

            if(attName.isEmpty())
                continue;

            if(!subCurrentItem)
            {
                if(editMode)
                {
                    for(int j=0, n=subRoot->columnCount(); j<n; j++)
                    {
                        if(subCurrentItem = subRoot->child(0,j))
                            break;
                    }
                    if(!subCurrentItem)
                    {
                        subCurrentItem = JsonQStandardItemFactory::createItem();
                        subRoot->appendRow(subCurrentItem);
                    }
                }
                else
                {
                    subCurrentItem = JsonQStandardItemFactory::createItem();
                    subRoot->appendRow(subCurrentItem);
                }
            }

            QStandardItem *tgtItm = mapData(child, attName, subCurrentItem, subLevelDataWidgetMapper);
            assignDefaultAddressData(child, tgtItm, editMode, subLevelDataWidgetMapper);
        }

        subLevelDataWidgetMapper->setCurrentModelIndex(subCurrentItem->index());
    }

    dataWidgetMapper->setCurrentModelIndex(this->currentItem->index());
}

QStandardItem *TixrWidget::mapData(QWidget *w, QString &roleName, QStandardItem *cI, QDataWidgetMapper *mapper)
{
    QStandardItem *tgtItem = JsonQStandardItemFactory::getTargetItem(roleName, cI);

    if(w->inherits("QLabel"))
    {
        mapper->addMapping(w, tgtItem->column(), "pixmap");
    }else

    if(w->inherits("QComboBox"))
    {
        mapper->addMapping(w, tgtItem->column(), "currentText");
    }else

    if(w->inherits("QPushButton"))
    {
        mapper->addMapping(w, tgtItem->column(), "styleSheet");
    }else

    if(w->inherits("QAbstractItemView"))
    {
        QAbstractItemView *itemView = qobject_cast<QAbstractItemView*>(w);
        itemView->setModel(tixrCore->stdModel);
        itemView->setRootIndex(tgtItem->index());
    }else

    {
        mapper->addMapping(w, tgtItem->column());
    }

    return tgtItem;
}

void TixrWidget::assignDefaultData(QWidget *w, QStandardItem *targetItem, bool editMode, QString &roleName)
{
    if(w->inherits("QLabel"))
    {
        assignDefaultLabelData(static_cast<QLabel*>(w), targetItem, editMode);
    }else

    if(w->inherits("QComboBox"))
    {
        assignDefaultComboBoxData(static_cast<QComboBox*>(w), targetItem, editMode);
    }else

    if(w->inherits("QPushButton"))
    {
        assignDefaultPushButtonData(static_cast<QPushButton*>(w), targetItem, editMode, roleName);
    }else

    if(!w->inherits("QAbstractItemView"))
    {
        assignDefaultWidgetData(w, targetItem, editMode);
    }
}

void TixrWidget::assignDefaultAddressData(QWidget *w, QStandardItem *targetItem, bool editMode, QDataWidgetMapper *mapper)
{
    AddressMapperSupportItemDelegate *addressDelegate = static_cast<AddressMapperSupportItemDelegate*>(mapper->itemDelegate());
    if(editMode)
        addressDelegate->setEditorData(w, targetItem->index());
    else
        addressDelegate->setModelData(w, targetItem->model(), targetItem->index());
}

void TixrWidget::assignDefaultLabelData(QLabel *lb, QStandardItem *stdItem, bool editMode)
{
    if(editMode)
    {
        const QPixmap pxm = stdItem->data(Qt::DecorationRole).value<QPixmap>();
        lb->setPixmap(pxm);
    }
    else
    {
        const QPixmap *pxm = lb->pixmap();
        stdItem->setData(*pxm, Qt::DecorationRole);
    }
}

void TixrWidget::assignDefaultComboBoxData(QComboBox *cb, QStandardItem *stdItem, bool editMode)
{
    QString tixrRelevantRole = cb->property("tixrRelevantRole").toString();
    if(editMode)
    {
        if(tixrRelevantRole.isEmpty())//caso de uso #1
            cb->setCurrentText(stdItem->text());
        else
            cb->setItemData(cb->currentIndex(), stdItem->data(DataRole::FromName(tixrRelevantRole)), DataRole::FromName(tixrRelevantRole));
    }
    else
    {
        if(tixrRelevantRole.isEmpty())//caso de uso #1
            stdItem->setData(QVariant(cb->currentText()), Qt::DisplayRole);
        else
            stdItem->setData(cb->currentData(DataRole::FromName(tixrRelevantRole)), DataRole::FromName(tixrRelevantRole));
    }
}

void TixrWidget::assignDefaultPushButtonData(QPushButton *pb, QStandardItem *stdItem, bool editMode, const QString attName)
{
    if(editMode)
    {
        QColor c = stdItem->data(DataRole::FromName(attName)).value<QColor>();
        QPalette newPal(pb->palette());
        newPal.setColor(QPalette::Background, c);
        pb->setPalette(newPal);
    }
    else
    {
        QColor c = pb->palette().color(QPalette::Background);
        stdItem->setData(c, DataRole::FromName(attName));
    }
}

void TixrWidget::assignDefaultWidgetData(QWidget *w, QStandardItem *stdItem, bool editMode)
{
    const char *pty = w->metaObject()->userProperty().name();
    if(editMode)
    {
        w->setProperty(pty, stdItem->data(Qt::EditRole));
        stdItem->setData(pty);
    }
    else
    {
        stdItem->setData(w->property(pty), Qt::EditRole);
    }
}

