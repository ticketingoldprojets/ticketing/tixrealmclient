ď»ż/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the QtQuick3D examples of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of Nokia Corporation and its Subsidiary(-ies) nor
**     the names of its contributors may be used to endorse or promote
**     products derived from this software without specific prior written
**     permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
** $QT_END_LICENSE$
**
****************************************************************************/

#include "modelview.h"
#include <Qt3D/qglabstractscene.h>
#include <Qt3D/qglscenenode.h>
#include <Qt3D/qglcamera.h>

ModelView::ModelView(QWidget *parent)
    : QGLView(parent)
    , m_scene(0)
    , m_main(0)
{
}

//! [initialize]
#include <QXmlStreamReader>
#include <QFile>
void ModelView::initializeGL(QGLPainter *painter)
{
    Q_UNUSED(painter);
    /*QGLCamera *theCam = camera();
    theCam->setEye(QVector3D(-300, 0, 2500));
    theCam->setFarPlane(5000);*/
    //theCam->setNearPlane(3);

    //m_scene = QGLAbstractScene::loadScene(QLatin1String(":/penguin.3ds"));
    m_scene = QGLAbstractScene::loadScene(QLatin1String("D:/Yovanis/Emplois/MaPlace/TixRealmClient/Theater.dae"));
    //m_scene = QGLAbstractScene::loadScene(QLatin1String("D:/TixRealmClient/Theater.dae"));
    if (m_scene)
        m_main = m_scene->mainNode();

    /*QQuaternion xt = QQuaternion::fromAxisAndAngle(1.0f, 0.0f, 0.0f, -90.0f);
    QQuaternion yt = QQuaternion::fromAxisAndAngle(0.0f, 1.0f, 0.0f, 30.0f);
    m_pose = yt * xt;*/

    box0 = m_scene->mainNode()->boundingBox();
    //center0 = box0.minimum();// + QVector3D(2500,0,2500);

    sceneCenter = 0;
    QList<QGLSceneNode*> list = m_main->allChildren();
    for(int i=0, n=list.size(); i<n; i++)
    {
        if(list.at(i)->objectName()=="sceneCenter")
        {
            sceneCenter = list.at(i);
            center = sceneCenter->boundingBox().center();
            //theCam->setEye(pos3d);//QVector3D(x, y, z));
            break;
        }
    }

    //camera()->setCenter(center0+center);
    center0 = (box0.minimum()+box0.maximum())/2;
    //camera()->setEye((box0.minimum()+box0.maximum())/2);

    m_fa = new QFocusAdaptor(this);

    //sceneCenter = qobject_cast<QGLSceneNode *>(m_scene->object("sceneCenter"));
    /*qreal x = sceneCenter->x();
    qreal y = sceneCenter->y();
    qreal z = sceneCenter->z();
    QVector3D pos3d = sceneCenter->position();
    theCam->setEye(pos3d);//QVector3D(x, y, z));
    pos3d.setX(x);
    pos3d.setY(y);
    pos3d.setZ(z);*/

    /*QFile f("D:/TixRealmClient/Theater.dae");
    f.open(QIODevice::ReadOnly);

    QXmlStreamReader xml;
    xml.setDevice(&f);
    QRegExp rx("matrix\\(([\\d.-]+),[\\d.-]+,[\\d.-]+,[\\d.-]+,([\\d.-]+),([\\d.-]+)\\)");
    QRegExp rx2("^use\\d+");
    if(xml.readNextStartElement() && xml.name() == "COLLADA")
    {
        while(!xml.atEnd())
        {
            xml.readNext();
            if(xml.name()=="node" && xml.isStartElement())
            {
                QString nodeName = xml.attributes().value("name").toString();
                QString mtxStr;
                if(nodeName=="sceneCenter" || rx2.exactMatch(nodeName))
                {
                    while(xml.readNextStartElement())
                    {
                        if(xml.name()=="matrix")
                        {
                            mtxStr = xml.readElementText();
                            break;
                        }
                    }
                    if(nodeName=="sceneCenter")
                        sceneMtxStr = mtxStr;
                    else
                        idToMtxMap.insert(rx2.cap(), mtxStr);
                }
            }
        }
    }
    else
        qWarning("Invalid file format.");
    f.close();*/
}
//! [initialize]

//! [paint]
void ModelView::paintGL(QGLPainter *painter)
{
     //painter->modelViewMatrix().rotate(m_pose);

    if (m_main)
        m_main->draw(painter);
}
//! [paint]

/*!

    \property QGLSceneNode::center
    \brief Returns the coordinates of the center of the portion of the geometry
    referenced by this scene node.


    The center is calculated as the centroid or geometric barycenter
    of the vertices (the average of the vertices).  For a convex hull this
    is guaranteed to be inside the figure.

    If the value of start() is 0, and count() is the same
    as geometry()->size() then the center will be the same as
    geometry()->center().  However if the scene node only references
    some part of the geometry, a center for this part is calculated.

    Note also that the center is relative to the transformed frame of reference
    for this node.  To get the absolute value, add the center and the position.

*/

/*QVector3D QGLSceneNode::center() const
{
    Q_D(const QGLSceneNode);

    QVector3D center;

    if (d->geometry.count() > 0)
    {
        if (d->start == 0 && (d->count == d->geometry.count() || d->count == 0))
        {
            center = d->geometry.center();
        }
        else
        {
            QGL::IndexArray ix = d->geometry.indices();
            for (int i = d->start; i < (d->start + d->count); ++i)
            {
                center += d->geometry.vertexAt(ix[i]);
            }

            center /= (float)d->geometry.count();
        }
    }

    return center;
}*/

//void ModelView::sitAt(double x, double y, double z)
void ModelView::sitAt(QString placeId)
{
    QGLSceneNode* placeItem = 0;
    QList<QGLSceneNode*> list = m_main->allChildren();
    QVector3D placePos;
    for(int i=0, n=list.size(); i<n; i++)
    {
        if(list.at(i)->objectName()==placeId)//"use7741")
        {
            placeItem = list.at(i);
            placePos = placeItem->boundingBox().center();
            break;
        }
    }

    //center0 = (box0.minimum()+box0.maximum())/2;
    QGLCamera *theCam = camera();
    //center0 += QVector3D(100,0,0);
    /*theCam->setCenter(center0);
    theCam->setEye(center0+QVector3D(100,100,100));*/

    //placePos.setZ(-placePos.z()-800);
    //center.setZ(-center.z()-800);

    center += box0.minimum();
    placePos += box0.minimum();

    /*qreal tmp = center.z();
    center.setZ(center.x());
    center.setX(tmp);*/

    /*theCam->setCenter((0.1*center)+QVector3D(0,0,-350));
    theCam->setEye((1.32*center0)+QVector3D(300,-40,350));*/

    theCam->setCenter((0.1*center)+QVector3D(0,0,-350));
    theCam->setEye((0.5*placePos)+QVector3D(300,-150,0));
    theCam->setFieldOfView(60);

    //theCam->setUpVector(QVector3D(0,-10,0));
    //camera()->setEye(center0+QVector3D(1000,1000,200));
   /* m_fa->setTarget(m_main);
    m_fa->setProgress(1.0);*/
    //camera()->setCenter((box0.maximum()+box0.maximum())/2);
    //camera()->setEye(center0+placePos);
    //camera()->setUpVector(QVector3D(0,200,0));
    //camera()->setEye(QVector3D(x, y, z+500));
    //QGLSceneNode *currentPlace = (QGLSceneNode*)m_scene->object(placeId);
    //QGLSceneNode *currentPlace = (QGLSceneNode*)m_scene->object(placeId);
    //QString placeMtx = idToMtxMap.value(placeId);
    //placeMtx += currentPlace->objectName();
}

