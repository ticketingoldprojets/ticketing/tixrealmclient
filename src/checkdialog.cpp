#include "checkdialog.h"
#include "mainwindow.h"

TixrCheckDialog::TixrCheckDialog(QWidget *blockedWidget, QWidget *parent) :
    TixrDialog(":/formconfig/cheque.ui", blockedWidget, parent)
{
    leChkNumb = findChild<QLineEdit*>("leChkNumb");
    cbBank = findChild<QComboBox*>("cbBank");
    checkPayItem = 0;
    modeItem = 0;
    MainWindow::tixrConnectSlotsByName(this);
}

void TixrCheckDialog::showEvent(QShowEvent * event)
{
    if(_blockedWidget->isVisible())
    {
        if(checkPayItem)
        {
            leChkNumb->setText(checkPayItem->data(DataRole::pdtnumcheque()).toString());
            cbBank->setCurrentIndex(cbBank->findData(checkPayItem->data(DataRole::pdtbankid()), DataRole::ikey()));
            QStandardItem *paymItm = checkPayItem->parent()->child(checkPayItem->row(), 0);
            bool v;
            leChkNumb->setEnabled(v = !paymItm->data(DataRole::ikey()).isValid());
            cbBank->setEnabled(v);
        }
        else
        {
            leChkNumb->setText(QString::null);
            if(cbBank->count())
                cbBank->setCurrentIndex(0);
            leChkNumb->setEnabled(true);
            cbBank->setEnabled(true);
        }
    }

    TixrDialog::showEvent(event);
}

void TixrCheckDialog::on_pbOk_clicked()
{
    if(leChkNumb->text().isEmpty())
    {
        QMessageBox::information(this, qApp->applicationName(), tr("Invalid check number."));
        return;
    }

    if(!checkPayItem)
    {
        checkPayItem = new QStandardItem;
        checkPayItem->setText(modeItem->text());
        checkPayItem->setData(modeItem->data(DataRole::ikey()), DataRole::ikey());
        checkPayItem->setData(modeItem->data(DataRole::mptypepaiem()), DataRole::mptypepaiem());
    }

    checkPayItem->setData(QVariant(leChkNumb->text()), DataRole::pdtnumcheque());//check number
    int bId = cbBank->itemData(cbBank->currentIndex(), DataRole::ikey()).toInt();
    checkPayItem->setData(QVariant(bId), DataRole::pdtbankid());

    TixrDialog::on_pbOk_clicked();
}
