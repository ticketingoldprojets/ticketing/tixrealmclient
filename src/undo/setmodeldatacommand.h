#ifndef SETIMAGECOMMAND_H
#define SETIMAGECOMMAND_H

#include <QUndoCommand>
#include <QAbstractItemModel>

class SetModelDataCommand : public QUndoCommand
{
protected:
    QVariant newData;
    QVariant oldData;
    QAbstractItemModel *model;
    QModelIndex index;
    Qt::ItemDataRole role;

public:
    explicit SetModelDataCommand(QAbstractItemModel *m, const QModelIndex &idx,  Qt::ItemDataRole r, const QVariant &data, QUndoCommand *parent = 0);
    virtual void undo() Q_DECL_OVERRIDE;
    virtual void redo() Q_DECL_OVERRIDE;
};

#endif // SETIMAGECOMMAND_H
