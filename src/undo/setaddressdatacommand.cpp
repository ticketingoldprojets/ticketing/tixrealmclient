#include "setaddressdatacommand.h"

SetAddressDataCommand::SetAddressDataCommand(QAbstractItemModel *m, const QModelIndex &idx, Qt::ItemDataRole r, const QVariant &data, QUndoCommand *parent) :
    SetModelDataCommand(m, idx, r, data, parent)
{
}

void SetAddressDataCommand::undo()
{
    SetModelDataCommand::undo();

    QModelIndex parentIndex = index.parent();
    QString address = concatAddressFields(index);
    model->setData(parentIndex, address, role);
}

void SetAddressDataCommand::redo()
{
    SetModelDataCommand::redo();

    QModelIndex parentIndex = index.parent();
    QString address = concatAddressFields(index);
    model->setData(parentIndex, address, role);
}

QString SetAddressDataCommand::concatAddressFields(const QModelIndex &index)
{
    QModelIndex parentIndex = index.parent();
    QString address;
    for(int j=0, n=model->columnCount(parentIndex); j<n; j++)
    {
        address += model->data(parentIndex.child(0, j)).toString() + ((j+1<n)?QStringLiteral(". "):QStringLiteral("."));
    }
    return address;
}
