#include "setmodeldatacommand.h"

SetModelDataCommand::SetModelDataCommand(QAbstractItemModel *m, const QModelIndex &idx, Qt::ItemDataRole r, const QVariant &data, QUndoCommand *parent) :
    QUndoCommand(parent), newData(data), model(m), index(idx), role(r)
{
}

void SetModelDataCommand::undo()
{
    model->setData(index, oldData, role);
}

void SetModelDataCommand::redo()
{
    oldData = model->data(index, role);
    model->setData(index, newData, role);
}
