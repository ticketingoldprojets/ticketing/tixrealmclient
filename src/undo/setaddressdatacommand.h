#ifndef SETADDRESSDATACOMMAND_H
#define SETADDRESSDATACOMMAND_H

#include "setmodeldatacommand.h"

class SetAddressDataCommand : public SetModelDataCommand
{
    QString concatAddressFields(const QModelIndex &index);

public:
    explicit SetAddressDataCommand(QAbstractItemModel *m, const QModelIndex &idx,  Qt::ItemDataRole r, const QVariant &data, QUndoCommand *parent = 0);
    void undo() Q_DECL_OVERRIDE;
    void redo() Q_DECL_OVERRIDE;
};

#endif // SETADDRESSDATACOMMAND_H
