//-----------------------------------------------------------------------------
#ifndef SCREPORTDIALOG_H
#define SCREPORTDIALOG_H
//-----------------------------------------------------------------------------
#include "dialog.h"
//-----------------------------------------------------------------------------
class TixrReportDialog : public TixrDialog
{
    Q_OBJECT

    protected:
        QPushButton *pbPreview;
        QPushButton *pbPrint;
        QPushButton *pbCancel;
        QListView   *lvReports;

    public:
        explicit TixrReportDialog(QWidget *blockedWidget, TixRealmCore *mObj, QWidget *parent = 0);

    protected:
        void showEvent(QShowEvent *event);

    private slots:
        void on_pbPreview_clicked();
};
//-----------------------------------------------------------------------------
#endif // SCREPORTDIALOG_H
//-----------------------------------------------------------------------------
