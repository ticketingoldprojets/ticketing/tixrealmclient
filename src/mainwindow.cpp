#include "mainwindow.h"
#if (QT_VERSION < QT_VERSION_CHECK(5, 0, 0))
#include <QtGui/QPrintDialog>
#include <QtGui/QPrinter>
#else
#include <QtPrintSupport/QPrintDialog>
#include <QtPrintSupport/QPrinter>
#endif
#include <QtUiTools/QUiLoader>
#include <QtGui/QPainter>
#include "crm/crmwidget.h"
#include "stock/ticketingstockwidget.h"
#include "stock/transportstockwidget.h"
#include "configwidget.h"
#include "acceuilwidget.h"
#include "../TixrClientEditorCommon/site2dview.h"

MainWindow::MainWindow(TixRealmCore *mainObj) :
    QMainWindow(), tixrCore(mainObj)
{
    setupUI();

    mainTabWidget = findChild<QTabWidget*>("mainTabWidget");
    cbProductType = findChild<QComboBox*>("cbProductType");

    //cargar la página de bienvenida
    new TixrAcceuilWidget(tixrCore, mainTabWidget);

    pbFloatingPanel = findChild<QPushButton*>("pbFloatingPanel");

    connect(cartWidget, SIGNAL(visibilityChanged(bool)), pbFloatingPanel, SLOT(setChecked(bool)));

    //tb3dView = findChild<QToolButton*>("tb3dView");
    //connect(tb3dView, SIGNAL(clicked()), this, SLOT(on_tb3dView_clicked()));

    /*tbLoadCart = findChild<QToolButton*>("tbLoadCart");
    connect(tbLoadCart, SIGNAL(clicked()), this, SLOT(on_tbLoadCart_clicked()));*/

    //view3D = 0;

    installEventFilter(this);
 }

MainWindow::~MainWindow()
{
}

void MainWindow::setupUI()
{
    QUiLoader loader;
    QFile file(":/formconfig/mainwindow.ui");
    file.open(QFile::ReadOnly);
    QMainWindow *clone =  (QMainWindow*)loader.load(&file, this);
    file.close();

    setGeometry(clone->geometry());

    setCentralWidget(clone->centralWidget());

    setMenuBar(clone->menuBar());

    setStatusBar(clone->statusBar());

    //Cargar los DockWidgets
    mainToolBar = 0;
    QList<QDockWidget*> dockWidgetList = clone->findChildren<QDockWidget*>(QRegExp("main[A-Z]\\w+"));
    for(int i=0, n=dockWidgetList.size(); i<n; i++)
    {
        QDockWidget *dw = dockWidgetList.at(i);
        if(dw->objectName() == "mainToolBar")
            mainToolBar = dw;
        addDockWidget(Qt::LeftDockWidgetArea, dw);
    }

    if(mainToolBar)
    {
        //mainToolBar->setWindowTitle(tr("Barre d'outils"));
        connect(mainToolBar, SIGNAL(dockLocationChanged(Qt::DockWidgetArea)), this, SLOT(on_cartWidget_dockLocationChanged(Qt::DockWidgetArea)));
    }

    //cargar el carro de compras
    cartWidget = new TixrCartWidget(tixrCore, this);
    QString e = cartWidget->getErrorMsg();

    if(e.isEmpty())
    {
        QDockWidget *cdw = static_cast<QDockWidget*>(cartWidget);
        addDockWidget(Qt::LeftDockWidgetArea, cdw);
        splitDockWidget(mainToolBar, cdw, Qt::Horizontal);
        cartWidget->hide();
    }
    else
    {
        QMessageBox::information(this, qApp->applicationName(), tr(e.toUtf8().constData()));
        delete cartWidget;
        cartWidget = 0;
    }

    tixrConnectSlotsByName(this);
}

void MainWindow::setAlwaysOnTopWidgetsVisibility(bool visible)
{
    cartWidget->setAlwaysOnTopDialogVisibility(visible);

    /*QWidget *w = mainTabWidget->currentWidget();
    if(w->objectName()=="Site2dView")
    {
        Site2dView *s2dV = static_cast<Site2dView*>(w);
        s2dV->setOnTopWidgetsVisibility(visible);
    }*/

    TixrWidget *currentWidget = static_cast<TixrWidget*>(mainTabWidget->currentWidget());
    if(currentWidget)
        currentWidget->setOnTopWidgetsVisibility(visible);
}

void MainWindow::on_pbConfig_clicked()
{
    for(int i=0, n=mainTabWidget->count(); i<n; i++)
    {
        TixrWidget *scW = static_cast<TixrWidget*>(mainTabWidget->widget(i));
        if(scW->tixrWtype() == TIXR_CONFIG)
        {
            mainTabWidget->setCurrentIndex(i);
            return;
        }
    }

    TixrConfigWidget *configPage = new TixrConfigWidget(tixrCore, 0, mainTabWidget);

    QString e = configPage->getErrorMsg();

    if(!e.isEmpty())
    {
        delete configPage;
        QMessageBox::information(0, qApp->applicationName(), tr(e.toUtf8().constData()));
    }
}

/*!
 * cypc
 */
void MainWindow::on_pbCRM_clicked()
{
    for(int i=0, n=mainTabWidget->count(); i<n; i++)
    {
        TixrWidget *scW = static_cast<TixrWidget*>(mainTabWidget->widget(i));
        if(scW->tixrWtype() == TIXR_CRM)
        {
            mainTabWidget->setCurrentIndex(i);
            return;
        }
    }

    new TixrCrmWidget(tixrCore, mainTabWidget);
}

void MainWindow::on_pbStock_clicked()
{
    int curStkW;
    switch(cbProductType->currentIndex())
    {
    case TicketingProductType: curStkW = TIXR_TICKETINGSTOCK; break;
    case ShopProductType: curStkW = TIXR_BOUTIQUESTOCK; break;
    case TravelProductType: curStkW = TIXR_TRANSPORTSTOCK; break;
    default: curStkW = TIXR_TICKETINGSTOCK;
    }

    TixrWidget *scW;

    for(int i=0, n=mainTabWidget->count(); i<n; i++)
    {
        scW = static_cast<TixrWidget*>(mainTabWidget->widget(i));
        if(scW->tixrWtype() == curStkW)
        {
            mainTabWidget->setCurrentIndex(i);
            return;
        }
    }

    switch(curStkW)
    {
        case TIXR_TICKETINGSTOCK:
            scW = new TixrTicketingStockWidget(tixrCore, cartWidget, "stockPage", "stockbtn.png", mainTabWidget); break;

        case TIXR_BOUTIQUESTOCK:
            QMessageBox::information(this, qApp->applicationName(), tr("Aquí no hay nada todavía")); return;

        case TIXR_TRANSPORTSTOCK:
            scW = new TixrTransportStockWidget(tixrCore, cartWidget, "transportStockPage", "transtockbtn.png", mainTabWidget);
    }

    QString e = scW->getErrorMsg();
    if(!e.isEmpty())
    {
        QMessageBox::information(this, qApp->applicationName(), tr(e.toUtf8().constData()));
        delete scW;
    }
}

void MainWindow::on_mainTabWidget_tabCloseRequested(int index)
{
    TixrWidget *scW = static_cast<TixrWidget*>(mainTabWidget->widget(index));

    if(scW->tixrWtype()!=TIXR_ACCEUIL && scW->isClosable())
    {
        mainTabWidget->removeTab(index);
        if(scW->tixrWtype() != TIXR_PLAN)
            scW->deleteLater();
    }
}

void MainWindow::on_pbFloatingPanel_clicked()
{
    cartWidget->setVisible(pbFloatingPanel->isChecked());
    if(cartWidget->isVisible())
    {
        QDockWidget *cdw = static_cast<QDockWidget*>(cartWidget);
        if(tabifiedDockWidgets(mainToolBar).contains(cdw))
        {
            splitDockWidget(mainToolBar, static_cast<QDockWidget*>(cartWidget), Qt::Horizontal);
        }
    }
}

void MainWindow::on_cartWidget_dockLocationChanged(Qt::DockWidgetArea /*area*/)
{
    QDockWidget *cdw = static_cast<QDockWidget*>(cartWidget);
    if(tabifiedDockWidgets(mainToolBar).contains(cdw))
    {
        splitDockWidget(mainToolBar, static_cast<QDockWidget*>(cartWidget), Qt::Horizontal);
    }
}

void MainWindow::on_pbClose_clicked()
{
    close();
}

/*void MainWindow::print()
{
    QPrinter printer;
    QPrintDialog *dialog = new QPrintDialog(&printer, ui);
    dialog->setWindowTitle(tr("Print Document"));

    QTextEdit *textEdit = qFindChild<QTextEdit*>(this, "textEdit");

    if(textEdit->textCursor().hasSelection())
        dialog->addEnabledOption(QAbstractPrintDialog::PrintSelection);

    if(dialog->exec() != QDialog::Accepted)
        return;
}*/

/*void MainWindow::on_tb3dView_clicked()
{
    if(mainTabWidget->currentWidget()->objectName()!="graphicSiteView")
        return;

    Site2dView *site2dView = (Site2dView*)mainTabWidget->currentWidget();
    QList<QGraphicsItem*> selItemsList = site2dView->getSelItems();
    if(selItemsList.size()!=1)
    {
        return;
    }

    QString plan3dFile = site2dView->catProdItem->data(35).toString();
    if(plan3dFile.isEmpty())
        return;*/

    /*int tabIndex;
    if(!view3D)
    {
        view3D = new ModelView();
        tabIndex = mainTabWidget->addTab(view3D, tr("Plan 3D"));
        connect(this, SIGNAL(sitAtSignal(QString)), view3D, SLOT(sitAt(QString)), Qt::UniqueConnection);
        //connect(this, SIGNAL(sitAtSignal(double,double,double)), view3D, SLOT(sitAt(double,double,double)), Qt::UniqueConnection);
        //mainTabWidget->setCurrentIndex(tabIndex);
        //return;
    }
    else
        tabIndex = mainTabWidget->indexOf(view3D);
    mainTabWidget->setCurrentIndex(tabIndex);*/

    /*QDoubleSpinBox *dsb1 = findChild<QDoubleSpinBox*>("doubleSpinBox");
    QDoubleSpinBox *dsb2 = findChild<QDoubleSpinBox*>("doubleSpinBox_2");
    QDoubleSpinBox *dsb3 = findChild<QDoubleSpinBox*>("doubleSpinBox_3");*/

    //emit sitAtSignal(dsb1->value(), dsb2->value(), dsb3->value());//"use8226");//silla central al fondo
    /*Place *selItem = (Place*)selItemsList.at(0);
    emit sitAtSignal(selItem->id);
}*/

/*void MainWindow::on_toolButton_2_clicked()
{
    if(mainTabWidget->tabPosition()==QTabWidget::West)
        mainTabWidget->setTabPosition(QTabWidget::North);
    else
        mainTabWidget->setTabPosition(QTabWidget::West);
}*/

/*
 * \brief Reimplementación de <em>QMetaObject::connectSlotsByName</em>.
 *
 * Varias ventajas con relación a <em>QMetaObject::connectSlotsByName</em>:
 * Permite parametrar la raiz \a r dentro del arbol de hijos de \a o. Si \a r es 0 la raiz será \a o.
 * \a t es el tipo de conexión a establecer, con diferencia al método original se puede ejecutar varias veces sin riesgo de crear conexiones duplicadas (usando el valor por defecto de \a t, por supuesto).
 * Permite conectar las señales de varios objectos a un mismo slot siguiendo una convención en los nombres de objetos receptores y slots:
 * todos los objetos cuyo nombre termina en ___D, dónde D es un número natural, se considerarán teniendo el mismo nombre resultado de eliminar dicho sufijo.
 */
void MainWindow::tixrConnectSlotsByName(QObject *o, QObject *r, Qt::ConnectionType t)
{
    if(!o)
        return;

    if(!r)
        r = o;

    const QMetaObject *mo = o->metaObject();
    Q_ASSERT(mo);

    QObjectList list = r->findChildren<QObject*>();
    list.insert(0, r);

    QRegExp rx("___\\d+$");//Patrón del sufijo presente en el nombre de los objetos que siguen la convención para la conexión a un slot común.

    for(int i=0, n=mo->methodCount(); i<n; ++i)
    {
        if(mo->method(i).methodType() != QMetaMethod::Slot)
            continue;

#if (QT_VERSION < QT_VERSION_CHECK(5, 0, 0))
        const char *slot = mo->method(i).signature();
#else
        QByteArray mn = mo->method(i).methodSignature();
        const char *slot = mn.constData();
#endif

        Q_ASSERT(slot);
        if (slot[0] != 'o' || slot[1] != 'n' || slot[2] != '_')
            continue;

        int found = 0;
        for(int j=0; j<list.count(); ++j)
        {
            const QObject *co = list.at(j);

            QString objName = co->objectName();

            int pos = rx.indexIn(objName);
            if(pos != -1)
            {
                objName = objName.left(pos);//Se considera que todos estos objetos tienen el mismo nombre con el objetivo de conectarlos al mismo slot.
            }

            QByteArray baObjName = objName.toUtf8();
            int len = baObjName.length();

            if(!len || qstrncmp(slot+3, baObjName.data(), len) || slot[len+3]!='_')
                continue;

            const QMetaObject *smo = co->metaObject();

            int sigIndex = smo->indexOfSignal(slot + len + 4);
            const char *signal;
            if(sigIndex >= 0)
            { // search for compatible signals
                int slotlen = qstrlen(slot + len + 4) - 1;
                for (int k=0, m=co->metaObject()->methodCount(); k<m; ++k)
                {
                    QMetaMethod method = smo->method(k);
                    if(method.methodType() != QMetaMethod::Signal)
                        continue;

#if (QT_VERSION < QT_VERSION_CHECK(5, 0, 0))
                    signal = method.signature();
#else
                    QByteArray mn = method.methodSignature();
                    signal = mn.constData();
#endif

                    if(!qstrncmp(signal, slot + len + 4, slotlen))
                    {
                        /*int signalOffset, methodOffset;
                        computeOffsets(method.enclosingMetaObject(), &signalOffset, &methodOffset);
                        sigIndex = k + - methodOffset + signalOffset;
                        break;*/

                        if(QObject::connect(co, QString("2%1").arg(signal).toUtf8().constData(), o, QString("1%1").arg(slot).toUtf8().constData(), t))
                        {
                            ++found;
                            break;
                        }
                    }
                }
            }
            /*if(sigIndex < 0)
                continue;

            if(QObject::connect(co, signal, o, slot, t))
            {
                foundIt = true;
                //break; //!<Permitir conectar las señales de más de un hijo al mismo slot del padre.
            }*/
        }
        if(found)
        {
            // we've found our slot, now skip all overloads
            while(mo->method(i + 1).attributes() & QMetaMethod::Cloned)
                ++i;
        }
#ifdef DEBUG_MODE
        else
            if(!(mo->method(i).attributes() & QMetaMethod::Cloned))
            {
                qWarning("tixrConnectSlotsByName: No matching signal for %s", slot);
            }
#endif
    }
}
