#include "../core/tixrcore.h"
#include "../customeventfilter.h"
#if (QT_VERSION < QT_VERSION_CHECK(5, 0, 0))
#include <QtGui/QApplication>
#include <QtGui/QMessageBox>
#else
#include <QtWidgets/QApplication>
#include <QtWidgets/QMessageBox>
#endif
#include <QtCore/QDate>

static QString curPath;

#if (QT_VERSION < QT_VERSION_CHECK(5, 0, 0))
void scMessageOutput(QtMsgType type, const char *msg)
#else
void scMessageOutput(QtMsgType type, const QMessageLogContext &context, const QString &msg)
#endif
{
    QDir dir(curPath+"tixrLog");
    if(!dir.exists())
        return;

    QString strType;
    switch (type)
    {
        case QtDebugMsg: strType = "Debug"; break;
        case QtWarningMsg: strType = "Warning"; break;
        case QtCriticalMsg: strType = "Critical"; break;
        case QtFatalMsg: strType = "Fatal";
    }

    /*QDir dir(curPath+"tixrLog");
    if(!(dir.exists() || dir.mkdir(curPath+"tixrLog")))
    {
        fprintf(stderr, "Cannot create log file\n");
        return;
    }*/

    QString fname = curPath+"tixrLog/"+QDate::currentDate().toString("dd-MM-yyyy")+".log";
    QFile f(fname);
    if(!f.open(QIODevice::WriteOnly|QIODevice::Append|QIODevice::Text))
    {
        fprintf(stderr, "Cannot create log file\n");
        return;
    }
    f.write((QTime::currentTime().toString("hh:mm:ss.zzz")+" "+QString(msg)).toUtf8().constData());
    f.write("\n");
    f.close();
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    a.setApplicationName("TixRealm client");
    a.setWindowIcon(QIcon(":/sc.png"));
    //a.connect(&a, SIGNAL(lastWindowClosed()), &a, SLOT(quit()));

#if (QT_VERSION < QT_VERSION_CHECK(5, 0, 0))
    QTextCodec::setCodecForLocale( QTextCodec::codecForName("UTF-8") );
    QTextCodec::setCodecForTr( QTextCodec::codecForName("UTF-8") );
    QTextCodec::setCodecForCStrings ( QTextCodec::codecForName("UTF-8") );
#endif

    TixRealmCore mainObj;

    CustomEventFilter eventFilter(&mainObj); //para ocultar/mostrar las ventanas "always on top" cuando la aplicación es desactivada/activada
    a.installEventFilter(&eventFilter);

    curPath = mainObj.curPath;
    QCoreApplication::addLibraryPath(curPath);
/*
#if (QT_VERSION < QT_VERSION_CHECK(5, 0, 0))
    qInstallMsgHandler(scMessageOutput);
#else
    qInstallMessageHandler(scMessageOutput);
#endif
*/
    QString error = mainObj.getLastError();
    if (!error.isEmpty())
    {
#ifdef DEBUG_MODE
        qFatal(error.toUtf8().constData());
#endif
        QMessageBox::information(0, a.applicationName(), QObject::tr(error.toUtf8().constData()));
        return -1;
    }

    if (!QSslSocket::supportsSsl())
    {
#ifdef DEBUG_MODE
        qFatal("This system does not support OpenSSL.");
#endif
        QMessageBox::information(0, a.applicationName(), QObject::tr("This system does not support OpenSSL."));
        return -1;
    }

    return mainObj.start();
}

