#include "mappersupportitemdelegate.h"
#include "core/datarole.h"
#include "core/datacolumn.h"
#include "undo/setmodeldatacommand.h"
#include "undo/setaddressdatacommand.h"
#include <QComboBox>
#include <QLabel>
#include <QPushButton>
#include <QItemEditorFactory>
#include <QGroupBox>

MapperSupportItemDelegate::MapperSupportItemDelegate(QObject *parent) :
    QStyledItemDelegate(parent)
{
    undoStack = new QUndoStack(this);
}

bool MapperSupportItemDelegate::eventFilter(QObject *editor, QEvent *event)
{
    bool r = QStyledItemDelegate::eventFilter(editor, event);

    return r;
}

void MapperSupportItemDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
    if(editor->inherits("QPushButton"))
    {
        QColor c = index.data(Qt::DecorationRole).value<QColor>();
        editor->setStyleSheet(QString("background-color: %1;").arg(c.name()));
    }else

    if(editor->inherits("QComboBox"))
    {
        QString tixrRelevantRole = editor->property("tixrRelevantRole").toString();
        QComboBox *cb = static_cast<QComboBox*>(editor);
        if(tixrRelevantRole.isEmpty())//caso de uso #1
            cb->setCurrentText(index.data().toString());
        else
        {
            int idx = cb->findData(index.data(), DataRole::FromName(tixrRelevantRole));
            Q_ASSERT(idx > -1);//unknown value
            if(idx > -1)
                cb->setCurrentIndex(idx);
        }
    }else

    if(editor->inherits("QLabel"))
    {
        QPixmap pxm = index.data(Qt::DecorationRole).value<QPixmap>();
        static_cast<QLabel*>(editor)->setPixmap(pxm);
    }else

    if(editor->inherits("QGroupBox"))
    {
        const QString &title = index.data(Qt::DisplayRole).toString();
        static_cast<QGroupBox*>(editor)->setTitle(title);
    }else
        QStyledItemDelegate::setEditorData(editor, index);
}

void MapperSupportItemDelegate::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const
{
    if(editor->inherits("QPushButton"))
    {
        QColor c = qobject_cast<QPushButton*>(editor)->palette().color(QPalette::Background);
        undoStack->push( new SetModelDataCommand(model, index, Qt::DecorationRole, c) );
    }else

    if(editor->inherits("QComboBox"))
    {
        QString tixrRelevantRole = editor->property("tixrRelevantRole").toString();
        QComboBox *cb = static_cast<QComboBox*>(editor);

        QVariant v = tixrRelevantRole.isEmpty() ? QVariant(cb->currentText()) : cb->currentData(DataRole::FromName(tixrRelevantRole));
        Qt::ItemDataRole r = tixrRelevantRole.isEmpty() ? Qt::EditRole : (Qt::ItemDataRole)DataRole::FromName(tixrRelevantRole);

        undoStack->push( new SetModelDataCommand(model, index, r, v) );
    }else

    if(editor->inherits("QLabel"))
    {
        const QPixmap *pxm = static_cast<QLabel*>(editor)->pixmap();
        undoStack->push( new SetModelDataCommand(model, index, Qt::DecorationRole, *pxm) );
    }else

    if(editor->inherits("QGroupBox"))
    {
        const QString &title = static_cast<QGroupBox*>(editor)->title();
        undoStack->push( new SetAddressDataCommand(model, index, Qt::DisplayRole, title) );
    }else

    {
        QByteArray n = editor->metaObject()->userProperty().name();
        if (n.isEmpty())
            n = itemEditorFactory()->valuePropertyName( model->data(index, Qt::EditRole).userType() );

        if (!n.isEmpty())
            undoStack->push( new SetModelDataCommand(model, index, Qt::EditRole, editor->property(n)) );
    }
}
