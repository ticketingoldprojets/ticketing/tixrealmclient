//-----------------------------------------------------------------------------
#include "tvspinboxdelegate.h"
#include "acceuilwidget.h"
#include "crm/newtierswidget.h"
#include "mainwindow.h"
#include "reportdialog.h"
//! NCReport report generator/rendering engine
/*#include <ncreport/ncreport.h>
#include <ncreport/ncreportoutput.h>
#include <ncreport/ncreportpreviewoutput.h>
#include <ncreport/ncreportpreviewwindow.h>*/
#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlError>
#include <float.h>
//-----------------------------------------------------------------------------
// SCACCEUEILWIDGET CLASS
//-----------------------------------------------------------------------------
TixrAcceuilWidget::TixrAcceuilWidget(TixRealmCore *mObj, QWidget *parent) :
    TixrWidget("accueilPage", mObj, "pb4btn.png", parent)
{
    this->tixrWidgetType = TIXR_ACCEUIL;
    if(getErrorMsg().isEmpty())
    {
        if(parentTabWidget)
            parentTabWidget->addTab(this, windowIcon(), windowTitle());
    }
    else
    {
        deleteLater();
        return;
    }

    this->currOper          = new QStandardItem();

    this->dsbTotalEspece    = this->findChild<QDoubleSpinBox *>("dsbTotalEspece");
    this->dsbFondCaisse     = this->findChild<QDoubleSpinBox *>("dsbFondCaisse");
    this->deDernierCloture  = this->findChild<QDateTimeEdit *>("deDernierCloture");
    this->deArretCloture    = this->findChild<QDateTimeEdit *>("deArretCloture");
    this->pbEquilibrage     = this->findChild<QPushButton *>("deArretCloture");
    this->tvPayments        = this->findChild<QTableView *>("tvPayments");
    this->tvClotures        = this->findChild<QTableView *>("tvClotures");
    this->tvTiersTrans      = this->findChild<QTableView *>("tvTiersTrans");
    this->twPieceDevise     = this->findChild<QTabWidget *>("twPieceDevise");
    this->tvTransactions    = this->findChild<QTableView *>("tvTransactions");
    this->tvEncaissements   = this->findChild<QTableView *>("tvEncaissements");
    this->pbApercu          = this->findChild<QPushButton *>("pbApercu");
    this->pbCloturer        = this->findChild<QPushButton *>("pbCloturer");
    this->lbOpName          = this->findChild<QLabel *>("lbOpName");//Yovanis
    this->lbConnTime        = this->findChild<QLabel *>("lbConnTime");//Yovanis

    QJsonObject jObj;
    jObj.insert("p_iddevise", 1);
    jObj.insert("p_dateend", QDateTime::currentDateTime().toString(Qt::ISODate));

    this->tixrCore->setPgFunction("main.getamounts");
    this->currentTaskList << this->tixrCore->sendMsg(this->tixrCore->stdModel->invisibleRootItem(), HTTP_METHOD_ID::POST, "/api", this, "responseGetAmounts", QJsonDocument(jObj).toJson());

    if(!this->tixrCore->keyToItemMap.contains("civilites"))
    {
        this->tixrCore->setPgFunction("main.getcivilites");
        PendingRequest *pReq = this->tixrCore->sendMsg(this->tixrCore->stdModel->invisibleRootItem(), HTTP_METHOD_ID::GET, "/api", this, "responseGetCivilites");
        pReq->setRespDataMapKey("civilites");
        this->currentTaskList << pReq;
    }

    if(!this->tixrCore->keyToItemMap.contains("typetiers"))
    {
        this->tixrCore->setPgFunction("main.gettypetiers");
        PendingRequest *pReq = this->tixrCore->sendMsg(this->tixrCore->stdModel->invisibleRootItem(), HTTP_METHOD_ID::GET, "/api", this, "responseGetTypeTiers");
        pReq->setRespDataMapKey("typetiers");
        this->currentTaskList << pReq;
    }

    if(!this->tixrCore->keyToItemMap.contains("pays"))
    {
        this->tixrCore->setPgFunction("main.getpays");
        PendingRequest *pReq = this->tixrCore->sendMsg(this->tixrCore->stdModel->invisibleRootItem(), HTTP_METHOD_ID::GET, "/api", this, "responseGetPays");
        pReq->setRespDataMapKey("pays");
        this->currentTaskList << pReq;
    }

    if(!this->tixrCore->keyToItemMap.contains("EncaissementsAControler"))
    {
        QJsonObject jObj;
        jObj.insert("p_dateend", QDateTime::currentDateTime().toString(Qt::ISODate));

        this->tixrCore->setPgFunction("main.getencaissementsbydate");
        PendingRequest *pReq = this->tixrCore->sendMsg(this->tixrCore->stdModel->invisibleRootItem(), HTTP_METHOD_ID::POST, "/api", this, "responseGetEncaissementsByDate", QJsonDocument(jObj).toJson());
        pReq->setRespDataMapKey("EncaissementsAControler");
        this->currentTaskList << pReq;
    }

    if(!this->tixrCore->keyToItemMap.contains("TiersCommandePayment"))
    {
        QJsonObject jObj;
        jObj.insert("p_iddevise", 1);
        jObj.insert("p_dateend", QDateTime::currentDateTime().toString(Qt::ISODate));

        this->tixrCore->setPgFunction("main.gettierscommandepayments");
        PendingRequest *pReq = this->tixrCore->sendMsg(this->tixrCore->stdModel->invisibleRootItem(), HTTP_METHOD_ID::POST, "/api", this, "responseGetTiersCommandePayments", QJsonDocument(jObj).toJson());
        pReq->setRespDataMapKey("TiersCommandePayment");
        this->currentTaskList << pReq;
    }

    if(!this->tixrCore->keyToItemMap.contains("Clotures"))
    {
        this->tixrCore->setPgFunction("main.getcloturescaisse");
        PendingRequest *pReq = this->tixrCore->sendMsg(this->tixrCore->stdModel->invisibleRootItem(), HTTP_METHOD_ID::GET, "/api", this, "responseGetCloturesCaisse");
        pReq->setRespDataMapKey("Clotures");
        this->currentTaskList << pReq;
    }

    if(!this->tixrCore->keyToItemMap.contains("TransactionsAControler"))
    {
        QJsonObject jObj;
        jObj.insert("p_dateend", QDateTime::currentDateTime().toString(Qt::ISODate));

        this->tixrCore->setPgFunction("main.gettransactionsbydate");
        PendingRequest *pReq = this->tixrCore->sendMsg(this->tixrCore->stdModel->invisibleRootItem(), HTTP_METHOD_ID::POST, "/api", this, "responseGetTransactionsByDate", QJsonDocument(jObj).toJson());
        pReq->setRespDataMapKey("TransactionsAControler");
        this->currentTaskList << pReq;
    }

    if(!this->tixrCore->keyToItemMap.contains("TotalsCommandePayments"))
    {
        QJsonObject jObj;
        jObj.insert("p_dateend", QDateTime::currentDateTime().toString(Qt::ISODate));

        this->tixrCore->setPgFunction("main.gettotalscommandepayments");
        PendingRequest *pReq = this->tixrCore->sendMsg(this->tixrCore->stdModel->invisibleRootItem(), HTTP_METHOD_ID::POST, "/api", this, "responseGetTotalsCommandePayments", QJsonDocument(jObj).toJson());
        pReq->setRespDataMapKey("TotalsCommandePayments");
        this->currentTaskList << pReq;
    }

    //YOVANIS: cargar los parámetros de conexión a la BD (únicamente para los reportes)
    //currentTaskList << tixrCore->sendMsg(this->tixrCore->stdModel->invisibleRootItem(), HTTP_METHOD_ID::GET, "/dbCP", this, "respDbCP");

    //lo relacionado con los skins (Yovanis)
    QString skinsDir = tixrCore->curPath + "skins";
#ifndef QT_NO_FILESYSTEMWATCHER
    dirWatcher = new QFileSystemWatcher(QStringList()<<skinsDir, this);
    connect(dirWatcher, SIGNAL(directoryChanged(QString)), this, SLOT(reloadSkins(QString)));
#endif
    lblApercu = findChild<QLabel*>("lblApercu");
    gbThemes = findChild<QGroupBox*>("gbThemes");
    lwSkins = findChild<QListWidget*>("lwSkins");
    pbRestorer = findChild<QPushButton*>("pbRestorer");
    reloadSkins(skinsDir);

    MainWindow::tixrConnectSlotsByName(this);
}
//-----------------------------------------------------------------------------YOVANIS
void TixrAcceuilWidget::respDbCP(QString respError, QStandardItem * /*requestItem*/, QStandardItem *responseItem, PendingRequest *curReq)
{
    currentTaskList.removeOne(curReq);
    if(currentTaskList.isEmpty())
        parentTabWidget->setTabIcon(parentTabWidget->indexOf(this), windowIcon());

    if(!respError.isEmpty())
    {
        QMessageBox::information(0, qApp->applicationName(), tr(respError.toUtf8().constData()));
        return;
    }

    //QStandardItem *dbCPitem = tixrCore->itemByNameFromModel(responseItem, "DbConnParams");
    QStandardItem *userItm = tixrCore->keyToItemMap.value("login");
    userItm->setData(responseItem->data(DataRole::host()), DataRole::host());
    userItm->setData(responseItem->data(DataRole::port()), DataRole::port());
    userItm->setData(responseItem->data(DataRole::database()), DataRole::database());

    delete responseItem;

    //ya que tengo el userItm aprovecho aqui mismo para mostrar el nombre del operador conectado
    operName = userItm->data(DataRole::sprenom()).toString() + " " + userItm->data(DataRole::snom()).toString().toUpper();
    lbOpName->setText(operName);
    startDT = userItm->data(DataRole::starttime()).toDateTime();
    startTimer(1000);
}
//-----------------------------------------------------------------------------
void TixrAcceuilWidget::timerEvent(QTimerEvent * event)
{
    tixrqptr connectedTime = startDT.secsTo(QDateTime::currentDateTimeUtc());
    int m = connectedTime / 60;
    int s = connectedTime % 60;
    int h = m / 60;
    m %= 60;
    lbConnTime->setText(QString("Connected since %1:%2:%3").arg(QString::number(h), QString::number(m).rightJustified(2,'0'), QString::number(s).rightJustified(2,'0')));
    event->accept();
}
//-----------------------------------------------------------------------------
void TixrAcceuilWidget::on_tvTransactions_doubleClicked(const QModelIndex & index)
{
    QStandardItem * item;
    int idTiers = this->tixrCore->stdModel->itemFromIndex(index.sibling(index.row(), 0))->data(DataRole::ikey()).toInt();

    if(!this->tixrCore->keyToItemMap.contains("crm"))
        if(idTiers > 0)
        {
            QStandardItem *modelSubItem = new QStandardItem(1, 1);
            modelSubItem->setData("ItemId", DataRole::jsonProperty());
            modelSubItem->setData(JsonObject, DataRole::jsonType());

            modelSubItem->setData(idTiers, DataRole::ikey());

            this->tixrCore->stdModel->invisibleRootItem()->appendRow(modelSubItem);

            this->tixrCore->setPgFunction("main.gettiers");
            PendingRequest *pReq = this->tixrCore->sendMsg(modelSubItem, HTTP_METHOD_ID::POST, "/api", this, "responseGetTiers");
            pReq->setRespDataMapKey("crm");
            this->currentTaskList << pReq;
        }
        else
            QMessageBox::information(this, qApp->applicationName(), QObject::trUtf8("The command does not belong to any user"));
    else
    {
        if(idTiers > 0)
        {
            QStandardItem * tiersParent = this->tixrCore->keyToItemMap["crm"];
            for(int i = 0, countRow = tiersParent->rowCount(); i < countRow; i++)
            {
                item = tiersParent->child(i);
                if(item->data(DataRole::ikey()).toInt() == idTiers)
                    break;
            }

            TixrNewTiersWidget * tabWidget = NULL;
            if(item->data(DataRole::indicator()).isValid())
                tabWidget = item->data(DataRole::indicator()).value<TixrNewTiersWidget *>();
            if(this->parentTabWidget->indexOf(tabWidget) != -1)
            {
                this->parentTabWidget->setCurrentWidget(tabWidget);
                return;
            }

            TixrNewTiersWidget * tiers = new TixrNewTiersWidget(this->tixrCore, item, this->parentTabWidget);
            QString e = tiers->getErrorMsg();
            if(!e.isEmpty())
            {
                delete tiers;
                QMessageBox::information(NULL, qApp->applicationName(), tr(e.toUtf8().constData()));
            }
        }
        else
            QMessageBox::information(this, qApp->applicationName(), QObject::trUtf8("The command does not belong to any user"));
    }
}
//-----------------------------------------------------------------------------
void TixrAcceuilWidget::on_tvEncaissements_doubleClicked(const QModelIndex & index)
{
    QStandardItem * item;
    int idTiers = this->tixrCore->stdModel->itemFromIndex(index.sibling(index.row(), 0))->data(DataRole::ikey()).toInt();
    if(!this->tixrCore->keyToItemMap.contains("crm"))
        if(idTiers > 0)
        {
            QStandardItem *modelSubItem = new QStandardItem(1,1);
            modelSubItem->setData("ItemId", DataRole::jsonProperty());
            modelSubItem->setData(JsonObject, DataRole::jsonType());

            modelSubItem->setData(idTiers, DataRole::ikey());
            this->tixrCore->stdModel->invisibleRootItem()->appendRow(modelSubItem);

            this->tixrCore->setPgFunction("main.gettiers");
            PendingRequest *pReq = this->tixrCore->sendMsg(modelSubItem, HTTP_METHOD_ID::POST, "/api", this, "responseGetTiers");
            pReq->setRespDataMapKey("crm");
            this->currentTaskList << pReq;
        }
        else
            QMessageBox::information(this, qApp->applicationName(), QObject::trUtf8("The command does not belong to any user"));
    else
    {
        if(idTiers > 0)
        {
            QStandardItem * tiersParent = this->tixrCore->keyToItemMap["crm"];
            for(int i = 0, countRow = tiersParent->rowCount(); i < countRow; i++)
            {
                item = tiersParent->child(i);
                if(item->data(DataRole::ikey()).toInt() == idTiers)
                    break;
            }

            TixrNewTiersWidget * tabWidget = NULL;
            if(item->data(DataRole::indicator()).isValid())
                tabWidget = item->data(DataRole::indicator()).value<TixrNewTiersWidget *>();
            if(this->parentTabWidget->indexOf(tabWidget) != -1)
            {
                this->parentTabWidget->setCurrentWidget(tabWidget);
                return;
            }

            TixrNewTiersWidget * tiers = new TixrNewTiersWidget(this->tixrCore, item, this->parentTabWidget);
            QString e = tiers->getErrorMsg();
            if(!e.isEmpty())
            {
                delete tiers;
                QMessageBox::information(NULL, qApp->applicationName(), tr(e.toUtf8().constData()));
            }
        }
        else
            QMessageBox::information(this, qApp->applicationName(), QObject::trUtf8("The command does not belong to any user"));
    }
}
//-----------------------------------------------------------------------------
void TixrAcceuilWidget::on_tvTiersTrans_doubleClicked(const QModelIndex & index)
{
    QStandardItem * item;
    int idTiers = this->tixrCore->stdModel->itemFromIndex(index.sibling(index.row(), 0))->data(DataRole::ikey()).toInt();
    if(!this->tixrCore->keyToItemMap.contains("crm"))
        if(idTiers > 0)
        {
            QStandardItem *modelSubItem = new QStandardItem(1,1);
            modelSubItem->setData("ItemId", DataRole::jsonProperty());
            modelSubItem->setData(JsonObject, DataRole::jsonType());

            modelSubItem->setData(idTiers, DataRole::ikey());
            this->tixrCore->stdModel->invisibleRootItem()->appendRow(modelSubItem);

            this->tixrCore->setPgFunction("main.gettiers");
            PendingRequest *pReq = this->tixrCore->sendMsg(modelSubItem, HTTP_METHOD_ID::POST, "/api", this, "responseGetTiers");
            pReq->setRespDataMapKey("crm");
            this->currentTaskList << pReq;
        }
        else
            QMessageBox::information(this, qApp->applicationName(), QObject::trUtf8("The command does not belong to any user"));
    else
    {
        if(idTiers > 0)
        {
            QStandardItem * tiersParent = this->tixrCore->keyToItemMap["crm"];
            for(int i = 0, countRow = tiersParent->rowCount(); i < countRow; i++)
            {
                item = tiersParent->child(i);
                if(item->data(DataRole::ikey()).toInt() == idTiers)
                    break;
            }

            TixrNewTiersWidget * tabWidget = NULL;
            if(item->data(DataRole::indicator()).isValid())
                tabWidget = item->data(DataRole::indicator()).value<TixrNewTiersWidget *>();
            if(this->parentTabWidget->indexOf(tabWidget) != -1)
            {
                this->parentTabWidget->setCurrentWidget(tabWidget);
                return;
            }

            TixrNewTiersWidget * tiers = new TixrNewTiersWidget(this->tixrCore, item, this->parentTabWidget);
            QString e = tiers->getErrorMsg();
            if(!e.isEmpty())
            {
                delete tiers;
                QMessageBox::information(NULL, qApp->applicationName(), tr(e.toUtf8().constData()));
            }
        }
        else
            QMessageBox::information(this, qApp->applicationName(), QObject::trUtf8("The command does not belong to any user"));
    }
}
//-----------------------------------------------------------------------------
void TixrAcceuilWidget::on_pbApercu_clicked(bool checked)
{
    /*
    QString reportPath = tixrCore->curPath + "reports";
    QMessageBox::information(NULL, "", reportPath);

    QDirIterator dit(reportPath, QStringList() << "*.xml", QDir::Files);
    while(dit.hasNext())
    {
        QString fn = dit.next();

        QMessageBox::information(NULL, "", fn);
    }
    */
    TixrReportDialog *reportDialog = new TixrReportDialog((QWidget *)this, this->tixrCore, (QWidget *)this->parent());
    reportDialog->show();
    reportDialog->setWindowFlags(Qt::Dialog | Qt::WindowStaysOnTopHint | Qt::FramelessWindowHint);
}
//-----------------------------------------------------------------------------
void TixrAcceuilWidget::on_pbCloturer_clicked(bool checked)
{
    QTabWidget * tabPage = this->twPieceDevise->findChild<QTabWidget *>(QString::number(1));
    if(!tabPage)
        return;

    QStringList deviseinfoList = QStringList() << tabPage->objectName()
                                               << QString::number(tabPage->findChild<QDoubleSpinBox *>("dsbFondCaisse")->value())
                                               << QString::number(tabPage->findChild<QDoubleSpinBox *>("dsbEspecesTheoriques")->value())
                                               << QString::number(tabPage->findChild<QDoubleSpinBox *>("dsbEspecesReelles")->value())
                                               << QString::number(tabPage->findChild<QDoubleSpinBox *>("dsbTotalTheorique")->value())
                                               << QString::number(tabPage->findChild<QDoubleSpinBox *>("dsbTotalReel")->value());

    QStandardItem * itemPiecesDevice = this->tixrCore->keyToItemMap[tabPage->windowTitle()];

    for(int i = 0, rowCount = itemPiecesDevice->rowCount(); i < rowCount; i++)
        deviseinfoList << itemPiecesDevice->child(i, 1)->text();

    QString deviseInfo = deviseinfoList.join(",").prepend("{").append("}");

    if(this->tixrCore->keyToItemMap.contains("Clotures"))
    {
        QStandardItem *modelSubItem = new QStandardItem(1, 1);
        modelSubItem->setData("InsClotureCaisse", DataRole::jsonProperty());
        modelSubItem->setData(JsonObject, DataRole::jsonType());

        this->tixrCore->keyToItemMap["Clotures"]->appendRow(modelSubItem);

        QStandardItem *modelSubItem2 = new QStandardItem(this->tixrCore->keyToItemMap["login"]->data(DataRole::operatid()).toString());
        modelSubItem2->setData("p_idoperateur", DataRole::jsonProperty());
        modelSubItem2->setData(JsonSimple, DataRole::jsonType());
        modelSubItem->setChild(0, 0, modelSubItem2);

        modelSubItem2 = new QStandardItem(QString::number(0));
        modelSubItem2->setData("p_totalsales", DataRole::jsonProperty());
        modelSubItem2->setData(JsonSimple, DataRole::jsonType());
        modelSubItem->setChild(0, 1, modelSubItem2);

        modelSubItem2 = new QStandardItem(QString::number(0));
        modelSubItem2->setData("p_totalpays", DataRole::jsonProperty());
        modelSubItem2->setData(JsonSimple, DataRole::jsonType());
        modelSubItem->setChild(0, 2, modelSubItem2);

        modelSubItem2 = new QStandardItem(QString::number(0));
        modelSubItem2->setData("p_totalbills", DataRole::jsonProperty());
        modelSubItem2->setData(JsonSimple, DataRole::jsonType());
        modelSubItem->setChild(0, 3, modelSubItem2);

        modelSubItem2 = new QStandardItem(deviseInfo);
        modelSubItem2->setData("p_deviseinfo", DataRole::jsonProperty());
        modelSubItem2->setData(JsonSimple, DataRole::jsonType());
        modelSubItem->setChild(0, 4, modelSubItem2);

        this->tixrCore->setPgFunction("main.inscloturecaisse");
        this->currentTaskList << this->tixrCore->sendMsg(modelSubItem, HTTP_METHOD_ID::POST, "/api", this, "responseInsClotureCaisse");
    }
    else;   // hay que agregar la rama Clotures al modelo
}
//-----------------------------------------------------------------------------
void TixrAcceuilWidget::reloadSkins(const QString & path)
{
    QString skinsPath = tixrCore->curPath + "skins";
    if(path != skinsPath)
        return;

    lwSkins->clear();

    QDirIterator dit(skinsPath, QStringList() << "*.skin", QDir::Files);
    while(dit.hasNext())
    {
        QString fn = dit.next();

        if(!QResource::registerResource(fn))
            continue;

        QPixmap apercuPxm(":/images/apercu.png");

        QListWidgetItem *lwItm = new QListWidgetItem(QIcon(":/images/btn/apercu.png"), dit.fileName(), lwSkins);
        lwItm->setData(DataRole::skinapercu(), QVariant(apercuPxm));
        lwItm->setData(DataRole::skinfname(), QVariant(fn));

        QResource::unregisterResource(fn);
    }
}
//-----------------------------------------------------------------------------
void TixrAcceuilWidget::on_lwSkins_itemClicked(QListWidgetItem *item)
{
    QPixmap pxm = item->data(DataRole::skinapercu()).value<QPixmap>();
    lblApercu->setPixmap(pxm);
}
//-----------------------------------------------------------------------------
void TixrAcceuilWidget::on_pbAppliquer_clicked()
{
    QListWidgetItem *item = lwSkins->currentItem();
    if(!item)
        return;
    if(!tixrCore->loadSkin(item->data(DataRole::skinfname()).toString()))
        QMessageBox::information(NULL, qApp->applicationName(), tr("Can not load skin!"));
    else
        pbRestorer->setEnabled(true);
}
//-----------------------------------------------------------------------------
void TixrAcceuilWidget::on_pbRestorer_clicked()
{
    tixrCore->loadSkin();
    pbRestorer->setEnabled(false);
}
//------------------------------------------------------------------------------
QStandardItem * TixrAcceuilWidget::fillNewCloture(QStandardItem * reqItem, QStandardItem * respItem)
{
    QStandardItem * newModelItem = new QStandardItem;
    newModelItem->setData("Cloture", DataRole::jsonProperty());

    QStandardItem * Item = reqItem->child(0, 1);
    for(int i = 0, n = Item->rowCount(); i < n; i++)
    {
        QStandardItem * attItem = Item->child(i);
        QString attName = attItem->accessibleText();

        if(attName == "p_totalsales")
            newModelItem->setData(attItem->text(), DataRole::cctotalsales());
        else if(attName == "p_totalpays")
            newModelItem->setData(attItem->text(), DataRole::cctotalpays());
        else if(attName == "p_totalbills")
            newModelItem->setData(attItem->text(), DataRole::cctotalbills());
        else if(attName == "p_deviseinfo")
            newModelItem->setData(attItem->text(), DataRole::ccdeviseinfo());
        else if(attName == "p_idoperateur")
            newModelItem->setData(attItem->text(), DataRole::ccidoperateur());
        else if(attName == "p_idcloturer")
            newModelItem->setData(attItem->text(), DataRole::ccidcloturer());
    }

    QStandardItem *loginItem = this->tixrCore->keyToItemMap["login"];
    newModelItem->setData(loginItem->data(DataRole::sprenom()).toString() + " " + loginItem->data(DataRole::snom()).toString(), DataRole::ccidoperateur());
    newModelItem->setData(QDateTime::currentDateTime().toString(Qt::ISODate).replace("T", " "), DataRole::ccdatecloturer());

    if(reqItem->child(0)->text() == "main.inscloturecaisse")
    {
        int i, n = respItem->rowCount();
        for(i = 0; i < n; i++)
        {
            QStandardItem * elemItem = respItem->child(i);
            if(elemItem->accessibleText() == "ItemId")
            {
                newModelItem->setData(elemItem->data(DataRole::ikey()), DataRole::ccidcloturer());
                break;
            }
        }
        Q_ASSERT(i < n);
    }

    return newModelItem;
}
//-----------------------------------------------------------------------------
void TixrAcceuilWidget::responseGetAmounts(QString respError, PendingRequest *curReq)
{
    if(!beginHandleResponse(respError, curReq))
        return;

    QStandardItem *responseItem = curReq->getResponseItem();

    // QMessageBox::information(NULL, "", dbfnName);
    this->dsbTotalEspece->setValue(responseItem->child(0)->data(DataRole::totalespece()).toDouble());
    this->dsbFondCaisse->setValue(responseItem->child(0)->data(DataRole::fondcaisse()).toDouble());
    this->deDernierCloture->setDateTime(responseItem->child(0)->data(DataRole::derniercloture()).toDateTime());
    this->deArretCloture->setDateTime(QDateTime::currentDateTime());

    endHandleResponse(curReq);
}

void TixrAcceuilWidget::responseGetCivilites(QString respError, PendingRequest *curReq)
{
    if(!beginHandleResponse(respError, curReq))
        return;

    endHandleResponse(curReq);
}

void TixrAcceuilWidget::responseGetTypeTiers(QString respError, PendingRequest *curReq)
{
    if(!beginHandleResponse(respError, curReq))
        return;

    endHandleResponse(curReq);
}

void TixrAcceuilWidget::responseGetPays(QString respError, PendingRequest *curReq)
{
    if(!beginHandleResponse(respError, curReq))
        return;

    endHandleResponse(curReq);
}

void TixrAcceuilWidget::responseGetEncaissementsByDate(QString respError, PendingRequest *curReq)
{
    if(!beginHandleResponse(respError, curReq))
        return;

    this->tvEncaissements->setModel(this->tixrCore->stdModel);
    this->tvEncaissements->setRootIndex(curReq->getResponseItem()->index());

    endHandleResponse(curReq);
}

void TixrAcceuilWidget::responseGetTiersCommandePayments(QString respError, PendingRequest *curReq)
{
    if(!beginHandleResponse(respError, curReq))
        return;

    this->tvTiersTrans->setModel(this->tixrCore->stdModel);
    this->tvTiersTrans->setRootIndex(curReq->getResponseItem()->index());

    endHandleResponse(curReq);
}

void TixrAcceuilWidget::responseGetCloturesCaisse(QString respError, PendingRequest *curReq)
{
    if(!beginHandleResponse(respError, curReq))
        return;

    this->tvClotures->setModel(this->tixrCore->stdModel);
    this->tvClotures->setRootIndex(curReq->getResponseItem()->index());

    endHandleResponse(curReq);
}

void TixrAcceuilWidget::responseGetTransactionsByDate(QString respError, PendingRequest *curReq)
{
    if(!beginHandleResponse(respError, curReq))
        return;

    this->tvTransactions->setModel(this->tixrCore->stdModel);
    this->tvTransactions->setRootIndex(curReq->getResponseItem()->index());

    endHandleResponse(curReq);
}

void TixrAcceuilWidget::responseGetTotalsCommandePayments(QString respError, PendingRequest *curReq)
{
    if(!beginHandleResponse(respError, curReq))
        return;

    QStandardItem *responseItem = curReq->getResponseItem();

    this->tvPayments->setModel(this->tixrCore->stdModel);
    this->tvPayments->setRootIndex(responseItem->index());

    int idDevise, idModePay;
    QStandardItem * responseChild;
    QDoubleSpinBox * dsbEspecesTheoriques, * dsbFondCaisse,  * dsbARemettre;
    QDoubleSpinBox * dsbEspecesReelles, * dsbTotalTheorique, * dsbTotalReel;
    QLabel * lb;

    for(int i = 0, count = responseItem->rowCount(); i < count; i++)
    {
        responseChild = responseItem->child(i);
        idDevise  = responseChild->data(DataRole::tcpiddevise()).toInt();
        idModePay = responseChild->data(DataRole::tcpidmodepay()).toInt();

        if(idDevise > 0 /* idDevise == 1 */ && !this->twPieceDevise->findChild<QTabWidget *>(QString::number(idDevise)))
        {
            // this->twPieceDevise->addTab(new QTabWidget(this), responseItem->child(i, 3)->data(TixrQDataRole::qtdisplayrole()).toString());
            QString deviseName = responseItem->child(i, 3)->data(Qt::DisplayRole).toString();
            QTabWidget * tabPage = new QTabWidget(this->twPieceDevise);
            tabPage->setObjectName(QString::number(idDevise));
            tabPage->setWindowTitle(deviseName);
            this->twPieceDevise->addTab(tabPage, QObject::trUtf8(deviseName.toUtf8().constData()));

            QHBoxLayout * workLayout  = new QHBoxLayout(tabPage);
            QVBoxLayout * leftLayout  = new QVBoxLayout(tabPage);
            QVBoxLayout * rightLayout = new QVBoxLayout(tabPage);

            // LEFT LAYOUT
            QTableView * tvPiecesDevice = new QTableView(tabPage);
            QTableView * tvMontant = new QTableView(tabPage);

            tvPiecesDevice->setObjectName("tvPiecesDevice");
            tvMontant->setObjectName("tvMontant");

            tvPiecesDevice->setItemDelegateForColumn(1, new TixrTVSpinBoxDelegate(tvPiecesDevice, new TVObject(tvPiecesDevice)));
            tvMontant->setItemDelegateForColumn(3, new TixrTVSpinBoxDelegate(tvMontant, NULL));
            tvMontant->setItemDelegateForColumn(4, new TixrTVSpinBoxDelegate(tvMontant, new TVObject(tvMontant)));

            leftLayout->addWidget(tvPiecesDevice);
            leftLayout->addWidget(tvMontant);

            if(!this->tixrCore->keyToItemMap.contains(deviseName))
            {
                QStandardItem *modelSubItem = new QStandardItem(1,1);
                modelSubItem->setData(JsonObject, DataRole::jsonType());

                modelSubItem->setData(reinterpret_cast<tixrqptr>(tvPiecesDevice), DataRole::auxptr1());

                this->tixrCore->stdModel->invisibleRootItem()->appendRow(modelSubItem);

                QStandardItem *modelSubItem2 = new QStandardItem(QString::number(idDevise));
                modelSubItem2->setData("p_iddevise", DataRole::jsonProperty());
                modelSubItem2->setData(JsonSimple, DataRole::jsonType());
                modelSubItem->setChild(0, 0, modelSubItem2);

                modelSubItem2 = new QStandardItem(QDateTime::currentDateTime().toString(Qt::ISODate));
                modelSubItem2->setData("p_dateend", DataRole::jsonProperty());
                modelSubItem2->setData(JsonSimple, DataRole::jsonType());
                modelSubItem->setChild(0, 1, modelSubItem2);

                this->tixrCore->setPgFunction("main.getpiecescaisse_1");
                PendingRequest *pReq = this->tixrCore->sendMsg(modelSubItem, HTTP_METHOD_ID::POST, "/api", this, "responseGetPiecesCaisse_1");
                pReq->setRespDataMapKey(deviseName);
                this->currentTaskList << pReq;
            }

            QString mountByDevise = QString("MountBy") + deviseName;
            if(!this->tixrCore->keyToItemMap.contains(mountByDevise))
            {
                QStandardItem *modelSubItem = new QStandardItem(1,1);
                modelSubItem->setData(JsonObject, DataRole::jsonType());

                this->tixrCore->stdModel->invisibleRootItem()->appendRow(modelSubItem);

                QStandardItem *modelSubItem2 = new QStandardItem(QString::number(idDevise));
                modelSubItem2->setData("p_iddevise", DataRole::jsonProperty());
                modelSubItem2->setData(JsonSimple, DataRole::jsonType());
                modelSubItem->setChild(0, 0, modelSubItem2);

                modelSubItem2 = new QStandardItem(QDateTime::currentDateTime().toString(Qt::ISODate));
                modelSubItem2->setData("p_dateend", DataRole::jsonProperty());
                modelSubItem2->setData(JsonSimple, DataRole::jsonType());
                modelSubItem->setChild(0, 1, modelSubItem2);

                //parámetros para el handler de la respuesta
                modelSubItem->setData(reinterpret_cast<tixrqptr>(tvMontant), DataRole::auxptr1());

                this->tixrCore->setPgFunction("main.getpaymentbymode");
                PendingRequest *pReq = this->tixrCore->sendMsg(modelSubItem, HTTP_METHOD_ID::POST, "/api", this, "responseGetPaymentByMode");
                pReq->setRespDataMapKey(mountByDevise);
                this->currentTaskList << pReq;
            }

            // RIGHT LAYOUT
            QWidget * wMount = new QWidget(tabPage);

            QVBoxLayout * regionLayout  = new QVBoxLayout(wMount);
            QGridLayout * mountLayout = new QGridLayout(wMount);

            mountLayout->addWidget(lb = new QLabel(tr("Theoretical currencies"), wMount), 0, 0);
            mountLayout->addWidget(dsbEspecesTheoriques = new QDoubleSpinBox(wMount), 0, 1);
            lb->setFont(QFont(lb->font().family(), -1, QFont::Bold));
            lb->setStyleSheet("color:white");
            dsbEspecesTheoriques->setObjectName("dsbEspecesTheoriques");
            dsbEspecesTheoriques->setMaximum(FLT_MAX);
            dsbEspecesTheoriques->setFont(QFont(dsbEspecesTheoriques->font().family(), -1, QFont::Bold));
            dsbEspecesTheoriques->setStyleSheet("background:black;color:white");
            dsbEspecesTheoriques->setReadOnly(true);

            mountLayout->addWidget(lb = new QLabel(tr("Cash fund"), wMount), 1, 0);
            mountLayout->addWidget(dsbFondCaisse = new QDoubleSpinBox(wMount), 1, 1);
            lb->setFont(QFont(lb->font().family(), -1, QFont::Bold));
            lb->setStyleSheet("color:white");
            dsbFondCaisse->setObjectName("dsbFondCaisse");
            dsbFondCaisse->setMaximum(FLT_MAX);
            dsbFondCaisse->setFont(QFont(dsbFondCaisse->font().family(), -1, QFont::Bold));
            dsbFondCaisse->setStyleSheet("background:black;color:white");
            dsbFondCaisse->setValue(this->dsbFondCaisse->value());
            dsbFondCaisse->setReadOnly(true);

            mountLayout->addWidget(lb = new QLabel(tr("To return"), wMount), 2, 0);
            mountLayout->addWidget(dsbARemettre = new QDoubleSpinBox(wMount), 2, 1);
            lb->setFont(QFont(lb->font().family(), -1, QFont::Bold));
            lb->setStyleSheet("color:white");
            dsbARemettre->setObjectName("dsbARemettre");
            dsbARemettre->setMaximum(FLT_MAX);
            dsbARemettre->setFont(QFont(dsbARemettre->font().family(), -1, QFont::Bold));
            dsbARemettre->setStyleSheet("background:black;color:white");
            dsbARemettre->setReadOnly(true);

            mountLayout->addWidget(lb = new QLabel(tr("Especes reelles"), wMount), 3, 0);
            mountLayout->addWidget(dsbEspecesReelles = new QDoubleSpinBox(wMount), 3, 1);
            lb->setFont(QFont(lb->font().family(), -1, QFont::Bold));
            lb->setStyleSheet("color:white");
            dsbEspecesReelles->setObjectName("dsbEspecesReelles");
            dsbEspecesReelles->setMaximum(FLT_MAX);
            dsbEspecesReelles->setFont(QFont(dsbEspecesReelles->font().family(), -1, QFont::Bold));
            dsbEspecesReelles->setStyleSheet("background:black;color:white");
            dsbEspecesReelles->setReadOnly(true);

            mountLayout->setRowStretch(4, 40);

            mountLayout->addWidget(lb = new QLabel(tr("Theoretical total"), wMount),5, 0);
            mountLayout->addWidget(dsbTotalTheorique = new QDoubleSpinBox(wMount), 5, 1);
            lb->setFont(QFont(lb->font().family(), -1, QFont::Bold));
            lb->setStyleSheet("color:white");
            dsbTotalTheorique->setObjectName("dsbTotalTheorique");
            dsbTotalTheorique->setMaximum(FLT_MAX);
            dsbTotalTheorique->setFont(QFont(dsbTotalTheorique->font().family(), -1, QFont::Bold));
            dsbTotalTheorique->setStyleSheet("background:black;color:white");
            dsbTotalTheorique->setReadOnly(true);

            mountLayout->addWidget(lb = new QLabel(tr("Real total"), wMount), 6, 0);
            mountLayout->addWidget(dsbTotalReel = new QDoubleSpinBox(wMount), 6, 1);
            lb->setFont(QFont(lb->font().family(), -1, QFont::Bold));
            lb->setStyleSheet("color:white");
            dsbTotalReel->setObjectName("dsbTotalReel");
            dsbTotalReel->setMaximum(FLT_MAX);
            dsbTotalReel->setFont(QFont(dsbTotalReel->font().family(), -1, QFont::Bold));
            dsbTotalReel->setStyleSheet("background:black;color:red");
            dsbTotalReel->setReadOnly(true);

            mountLayout->setColumnMinimumWidth(1, 120);
            regionLayout->addLayout(mountLayout);

            wMount->setLayout(regionLayout);
            rightLayout->addWidget(wMount);

            workLayout->addLayout(leftLayout);
            workLayout->addLayout(rightLayout);
            tabPage->setLayout(workLayout);
        }

        if(idDevise > 0  && idModePay == 1)
        {
            dsbEspecesTheoriques->setValue(responseItem->child(i, 2)->data(Qt::DisplayRole).toDouble());
            dsbARemettre->setValue(dsbEspecesTheoriques->value() - dsbFondCaisse->value());
        }
        if(idDevise > 0 && (idModePay == 1 || idModePay == 2))
            dsbTotalTheorique->setValue(dsbTotalTheorique->value() + responseItem->child(i, 2)->data(Qt::DisplayRole).toDouble());
    }

    endHandleResponse(curReq);
}

void TixrAcceuilWidget::responseGetTiers(QString respError, PendingRequest *curReq)
{
    if(!beginHandleResponse(respError, curReq))
        return;

    QStandardItem *requestItem = curReq->getRequestItem();
    QStandardItem *responseItem = curReq->getResponseItem();

    QStandardItem *item;
    int k = requestItem->data(DataRole::ikey()).toInt();
    for(int i = 0, countRow = responseItem->rowCount(); i < countRow; i++)
    {
        item = responseItem->child(i);
        if(item->data(DataRole::ikey()).toInt() == k)
            break;
    }

    TixrNewTiersWidget *tabWidget = NULL;
    if(item->data(DataRole::indicator()).isValid())
        tabWidget = item->data(DataRole::indicator()).value<TixrNewTiersWidget *>();
    if(this->parentTabWidget->indexOf(tabWidget) != -1)
    {
        this->parentTabWidget->setCurrentWidget(tabWidget);
        return;
    }

    TixrNewTiersWidget *tiers = new TixrNewTiersWidget(this->tixrCore, item, this->parentTabWidget);
    QString e = tiers->getErrorMsg();
    if(!e.isEmpty())
    {
        delete tiers;
        QMessageBox::information(NULL, qApp->applicationName(), tr(e.toUtf8().constData()));
    }

    endHandleResponse(curReq);
}

void TixrAcceuilWidget::responseInsClotureCaisse(QString respError, PendingRequest *curReq)
{
    if(!beginHandleResponse(respError, curReq))
        return;

    QStandardItem *requestItem = curReq->getRequestItem();
    QStandardItem *responseItem = curReq->getResponseItem();

    QStandardItem *itemParent = requestItem->parent();
    int row = requestItem->row();
    QStandardItem *newCloture = this->fillNewCloture(requestItem, responseItem);
    itemParent->setChild(row, newCloture);
    itemParent->setChild(row, 1, new QStandardItem(newCloture->data(DataRole::ccdatecloturer()).toString()));
    this->tvClotures->setModel(this->tixrCore->stdModel);
    this->tvClotures->setRootIndex(itemParent->index());

    curReq->setResponseItemParent(0);

    endHandleResponse(curReq);
}

void TixrAcceuilWidget::responseGetPiecesCaisse_1(QString respError, PendingRequest *curReq)
{
    if(!beginHandleResponse(respError, curReq))
        return;

    QStandardItem *requestItem = curReq->getRequestItem();
    QStandardItem *responseItem = curReq->getResponseItem();

    QTableView *tvPiecesDevice = reinterpret_cast<QTableView *>(tixrqptrFromVariant(requestItem->data(DataRole::auxptr1())));
    tvPiecesDevice->setModel(this->tixrCore->stdModel);
    tvPiecesDevice->setRootIndex(responseItem->index());
    for(int i = 0, countRow = responseItem->rowCount(); i < countRow; i++)
    {
        responseItem->child(i, 0)->setEditable(false);
        responseItem->child(i, 2)->setEditable(false);
    }

    endHandleResponse(curReq);
}

void TixrAcceuilWidget::responseGetPaymentByMode(QString respError, PendingRequest *curReq)
{
    if(!beginHandleResponse(respError, curReq))
        return;

    QStandardItem *requestItem = curReq->getRequestItem();
    QStandardItem *responseItem = curReq->getResponseItem();

    QTableView *tvMontant = reinterpret_cast<QTableView *>(tixrqptrFromVariant(requestItem->data(DataRole::auxptr1())));
    tvMontant->setModel(this->tixrCore->stdModel);
    tvMontant->setRootIndex(responseItem->index());
    for(int i = 0, countRow = responseItem->rowCount(); i < countRow; i++)
    {
        responseItem->child(i, 0)->setEditable(false);
        responseItem->child(i, 1)->setEditable(false);
        responseItem->child(i, 2)->setEditable(false);
    }

    endHandleResponse(curReq);
}
