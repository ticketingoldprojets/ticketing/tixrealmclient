#ifndef CUSTOMPROXYMODEL_H
#define CUSTOMPROXYMODEL_H

#include <QSortFilterProxyModel>
#include <QStandardItem>

class TixrSortFilterProxyModel : public QSortFilterProxyModel
{
    Q_OBJECT

public:
    TixrSortFilterProxyModel(QObject *parent = 0);
    void setRootItem(QStandardItem *ri){rootItem = ri;}
    void setSourceModel(QAbstractItemModel *sourceModel);

protected:
    virtual bool filterAcceptsRow(int sourceRow, const QModelIndex &sourceParent) const;
    bool descendant(QModelIndex *idx) const;
    bool ascendant(QModelIndex *idx) const;

private:
    QStandardItem *rootItem;
    QStandardItemModel *stdModel;
};
//-----------------------------------------------------------------------------

class TixrPaymFilterProxyModel : public TixrSortFilterProxyModel
{
    Q_OBJECT

public:
    TixrPaymFilterProxyModel(QObject *parent = 0);

protected:
    bool filterAcceptsRow(int sourceRow, const QModelIndex &sourceParent) const;
};

#endif // CUSTOMPROXYMODEL_H
