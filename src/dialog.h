#ifndef SCDIALOG_H
#define SCDIALOG_H

#include <QtCore/QString>
#if (QT_VERSION < QT_VERSION_CHECK(5, 0, 0))
#include <QtGui>
#else
#include <QtWidgets>
#endif
#include "core/tixrcore.h"

class TixrDialog : public QDialog
{
    Q_OBJECT

public:
    QString getErrorMsg(){return errorStr;}
    bool wasVisible(){return _wasVisible;}
    TixRealmCore *getMainObj(){return tixrCore;}

protected:
    explicit TixrDialog(QString uiName, QWidget *blockedWidget, QWidget *parent = 0);
    void showEvent(QShowEvent *event);
    bool eventFilter(QObject *obj, QEvent *event);
    QWidget *_blockedWidget;
    QString errorStr;
    TixRealmCore *tixrCore;

protected slots:
    virtual void on_pbOk_clicked();
    void on_pbCancel_clicked();
    void animateIcon(const QRect & rect);

private:
    QPoint dragPosition;
    bool _wasVisible;

    QLabel *waitLabel;
    PendingRequest *curTask;
    QMovie *waitMovie;
};

#endif // SCDIALOG_H
