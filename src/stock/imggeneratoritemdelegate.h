#ifndef IMGGENERATORITEMDELEGATE_H
#define IMGGENERATORITEMDELEGATE_H

#include <QStyledItemDelegate>

class ImgGeneratorItemDelegate : public QStyledItemDelegate
{
    Q_OBJECT

public:
    explicit ImgGeneratorItemDelegate(QObject *parent = 0);
    void paint(QPainter * painter, const QStyleOptionViewItem & option, const QModelIndex & index) const;
};

#endif // IMGGENERATORITEMDELEGATE_H
