#include "ticketingstockwidget.h"
#include "../mainwindow.h"

TixrTicketingStockWidget::TixrTicketingStockWidget(TixRealmCore *mObj, TixrCartWidget *cw, QString uiName, QString iconName, QWidget *parent) :
    TixrStockWidget(mObj, cw, uiName, iconName, parent)
{
    this->tixrWidgetType = TIXR_TICKETINGSTOCK;
    if(!getErrorMsg().isEmpty())
        return;

    stockModelEntryKey = "stock";

    initUI();

    parentTabWidget->addTab(this, windowIcon(), windowTitle());

    loadTopLevelProductClasification(QStringLiteral("main.getallfamcatprods"), 0);
}

void TixrTicketingStockWidget::requestShowCatProds(const QModelIndex & index)
{
    QStandardItem *targetModelItem = tixrCore->stdModel->itemFromIndex(index);

    int r = targetModelItem->rowCount();
    if(r)
    {
        showResp(targetModelItem->child(0));
    }
    else
    {
        QStandardItem *modelSubItem = new QStandardItem(1,1);
        modelSubItem->setData("ItemId", DataRole::jsonProperty());
        modelSubItem->setData(JsonObject, DataRole::jsonType());

        QString idSpectStr = tixrCore->stdModel->data(index.sibling(index.row(),DataColumn::fptitlecol()), DataRole::ikey()).toString();
        QStandardItem *modelSubItem2 = new QStandardItem(idSpectStr);
        modelSubItem2->setData("srvikey", DataRole::jsonProperty());
        modelSubItem2->setData(JsonSimple, DataRole::jsonType());
        modelSubItem->setChild(0, 0, modelSubItem2);

        targetModelItem->appendRow(modelSubItem);

        tixrCore->setPgFunction("main.getallcatsforfamprods");
        currentTaskList << tixrCore->sendMsg(modelSubItem, HTTP_METHOD_ID::POST, "/api", this, "showResponse");
    }
}
