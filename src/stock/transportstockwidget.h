#ifndef SCTRANSPORTSTOCKWIDGET_H
#define SCTRANSPORTSTOCKWIDGET_H

#include "stockwidget.h"
#include "transportwidget.h"  // AGREGADO POR CYPC

class TixrTransportStockWidget : public TixrStockWidget
{
    Q_OBJECT

public:
    explicit TixrTransportStockWidget(TixRealmCore *mObj, TixrCartWidget *cw, QString uiName, QString iconName, QWidget *parent = 0);

private:
    void initUI();
    void requestShowCatProds(const QModelIndex &index);
    void requestAddToCar(const QModelIndex &index);
    void upDateInterface(QAbstractItemView *iViewL1, QAbstractItemView *iViewL2, int *L1 = 0, int *L2 = 0);

    QToolButton *tbSaison;

private slots:
    void stockPageTransport();  // Agregado por CYPC
    void itemViewClicked(const QModelIndex &index);
};

#endif // SCTRANSPORTSTOCKWIDGET_H

