#include "transportstockwidget.h"
#include "../mainwindow.h"

TixrTransportStockWidget::TixrTransportStockWidget(TixRealmCore *mObj, TixrCartWidget *cw, QString uiName, QString iconName, QWidget *parent) :
    TixrStockWidget(mObj, cw, uiName, iconName, parent)
{
    this->tixrWidgetType = TIXR_TRANSPORTSTOCK;
    if(!getErrorMsg().isEmpty())
        return;

    stockModelEntryKey = "transtock";

    initUI();

    parentTabWidget->addTab(this, windowIcon(), windowTitle());

    loadTopLevelProductClasification("main.getallfamcatprods", 2);
}

void TixrTransportStockWidget::initUI()
{
    TixrStockWidget::initUI();

    tbSaison = findChild<QToolButton*>("tbSaison");
    // AGREGADO POR CYPC
    connect(tbSaison, SIGNAL(clicked()), this, SLOT(stockPageTransport()), Qt::UniqueConnection);
    // FIN DE AGREGADO POR CYPC
}

void TixrTransportStockWidget::requestShowCatProds(const QModelIndex & index)
{
    QStandardItem *targetModelItem = tixrCore->stdModel->itemFromIndex(index);

    int r = targetModelItem->rowCount();
    if(r)
    {
        showResp(targetModelItem->child(0));
    }
    else
    {
        QStandardItem *modelSubItem = new QStandardItem(1,1);
        modelSubItem->setData("GetAllTransCatsForFamProdsParams", DataRole::jsonProperty());
        modelSubItem->setData(JsonObject, DataRole::jsonType());

        targetModelItem->appendRow(modelSubItem);

        QStandardItem *pi = tixrCore->stdModel->itemFromIndex(index);

        bool c = pi->accessibleText() == "TransportProdCateg" && pi->data(DataRole::tpcissubcat()).isValid() && pi->data(DataRole::tpcissubcat()).toBool();

        QString idSpectStr = c ? pi->parent()->child(pi->index().row(), 1)->data(DataRole::ikey()).toString() : pi->data(DataRole::ikey()).toString();

        QStandardItem *modelSubItem2 = new QStandardItem(idSpectStr);
        modelSubItem2->setData("idfam", DataRole::jsonProperty());
        modelSubItem2->setData(JsonSimple, DataRole::jsonType());
        modelSubItem->setChild(0, 0, modelSubItem2);

        if(c)
        {
            modelSubItem2 = new QStandardItem("1");
            modelSubItem2->setData("issubcat", DataRole::jsonProperty());
            modelSubItem2->setData(JsonSimple, DataRole::jsonType());
            modelSubItem->appendRow(modelSubItem2);
        }

        tixrCore->setPgFunction("main.getalltranspcatsforfamprods");
        currentTaskList << tixrCore->sendMsg(modelSubItem, HTTP_METHOD_ID::POST, "/api", this, "showResponse");
    }
}

void TixrTransportStockWidget::requestAddToCar(const QModelIndex &index)
{
    QStandardItem *targetModelItem = tixrCore->stdModel->itemFromIndex(index);

    QStandardItem *modelSubItem = new QStandardItem(1,2);
    modelSubItem->setData("AddToCar1Params", DataRole::jsonProperty());
    modelSubItem->setData(JsonObject, DataRole::jsonType());

    targetModelItem->appendRow(modelSubItem);

    QString idZone = tixrCore->stdModel->data(index.parent().parent(), DataRole::ikey()).toString();
    QStandardItem *modelSubItem2 = new QStandardItem(idZone);
    modelSubItem2->setData("ztar", DataRole::jsonProperty());
    modelSubItem2->setData(JsonSimple, DataRole::jsonType());
    modelSubItem->setChild(0, 0, modelSubItem2);

    modelSubItem2 = new QStandardItem(quickSellSP->value() ? quickSellSP->text() : "1");
    modelSubItem2->setData("n", DataRole::jsonProperty());
    modelSubItem2->setData(JsonSimple, DataRole::jsonType());
    modelSubItem->setChild(0, 1, modelSubItem2);

    tixrCore->setPgFunction("main.addnonlabeledprodtocart");
    currentTaskList << tixrCore->sendMsg(modelSubItem, HTTP_METHOD_ID::POST, "/api", cartWidget, "responseAddToCar");

    quickSellSP->setValue(1);
}

void TixrTransportStockWidget::upDateInterface(QAbstractItemView *iViewL1, QAbstractItemView *iViewL2, int *L1, int *L2)
{
    Q_UNUSED(L1);
    Q_UNUSED(L2);

    int l1;
    TixrStockWidget::upDateInterface(iViewL1, iViewL2, &l1);

    tbSaison->setEnabled((l1==0) && iViewL1->currentIndex().isValid());
}

void TixrTransportStockWidget::itemViewClicked(const QModelIndex &index)
{
    QAbstractItemView *iView = static_cast<QAbstractItemView*>(sender());

    int viewLevel = iView->property("tixrStockViewLevel").toInt();
    if(!viewLevel)
    {
        QAbstractItemView *iView2 = static_cast<QAbstractItemView*>(stackedWidgetLevel2->currentWidget()->children().at(1));
        if(iView2->rootIndex().parent()==index)
            return;
    }

    level2Clicked = viewLevel;

    QStandardItem *pi = tixrCore->stdModel->itemFromIndex(index);
    QString at = pi->accessibleText();

    if(at=="FamilleProduits") requestShowCatProds(index); else
    if(at=="ZoneAvecPrix") requestShowCategCli(index); else
    if(at=="CategCli") requestAddToCar(index); else
    if(at=="TransportProdCateg")
    {
        if(pi->data(DataRole::ikey()).isValid() && pi->data(DataRole::ikey()).toInt()!=-1)//corresponte a una categ. y no a una supracat.
            requestShowZonesAvecPrix(index);
        else
            requestShowCatProds(index);
    }
}

// AGREGADO POR CYPC
void TixrTransportStockWidget::stockPageTransport()
{
    QAbstractItemView *iViewL1 = static_cast<QAbstractItemView*>(stackedWidgetLevel1->currentWidget()->children().at(1));
    QModelIndex idx = iViewL1->currentIndex();
    if(!idx.isValid())
        return;

    int famcatid = idx.data(DataRole::ikey()).toInt();

    TixrTransportWidget * transportUI = new TixrTransportWidget(this->tixrCore, famcatid, this->parentTabWidget);
    QString e = transportUI->getErrorMsg();
    if(!e.isEmpty())
    {
        delete transportUI;
        QMessageBox::information(NULL, qApp->applicationName(), tr(e.toUtf8().constData()));
    }
}
// FIN DE AGREGADO POR CYPC
