#ifndef SCNEWSEANCEWIDGET_H
#define SCNEWSEANCEWIDGET_H

#include "../widget.h"
#include "../TixrClientEditorCommon/site2dview.h"

class TixrNewSeanceWidget : public TixrWidget
{
    Q_OBJECT
    
    QStackedWidget *seanceStackedW;
    QPushButton *seancePb2;
    QTableView *seanceGriTable;
    QLineEdit *seanceGriNameLE;
    QListView *seanceGriList;
    Site2dView *site2dView;
    QLineEdit *lePlano;
    QGroupBox *gbVenuePlan;
    QTableView *tvQuotes;

    bool loadVenuePlan(QString vplanFile);
    void setHeaderView(QTableView *tv);
    void setCurrentTariff(QStandardItem *currentTariffItem);
    void showTariffsAndSelectCurrentOne(QStandardItem *tariffsRootItem);

private slots:
    void on_pbValiderNewSeance_clicked();
    void on_pbPlanNewSeance_clicked();
    void on_pbAdmGri_clicked();
    void seanceSelectModeleBillet();
    void on_seanceGriList_clicked(QModelIndex index);
    void on_pbRejectGri_clicked();
    void responseGetAllGrilles(QString respError, PendingRequest *curReq);
    void seanceShowGrille(QString respError, PendingRequest *curReq);
    void responseNewSeance(QString respError, PendingRequest *curReq);
    void tarifPageActivated();
    void on_pbCancel_clicked();

public:
    explicit TixrNewSeanceWidget(TixRealmCore *mObj, QStandardItem *r, QWidget *parent = 0, QStandardItem *cItem = 0);

//unit testing purposes
friend class NewSeanceTest;
};

#endif // SCNEWSEANCEWIDGET_H

