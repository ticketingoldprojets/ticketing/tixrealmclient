#include "newspectwidget.h"
#include "../mainwindow.h"
#include "../mappersupportitemdelegate.h"

TixrNewSpectWidget::TixrNewSpectWidget(TixRealmCore *mObj, QStandardItem *r, QWidget *parent, QStandardItem *cItem) :
    TixrWidget("newSpect", mObj, "", parent, r, cItem)
{
    this->tixrWidgetType = TIXR_NEWSPECT;

    if(parentTabWidget)
    {
        int tabIndex = parentTabWidget->addTab(this, updateMode?tr("Update performance"):tr("New performance"));
        if(tabIndex>-1)
        {
            parentTabWidget->setCurrentIndex(tabIndex);
        }

        Q_ASSERT(tabIndex>-1);
    }

    MainWindow::tixrConnectSlotsByName(this);
}

void TixrNewSpectWidget::on_pbValiderNewSpect_clicked()
{
    QJsonObject *jsonObj = tixrCore->widgetToJson(dynWidget);

    if(updateMode)
    {
        QStandardItem *tgtItem = JsonQStandardItemFactory::getTargetItem(QStringLiteral("fptitlecol_ikey"), currentItem);
        QVariant v = tgtItem->data(DataRole::ikey());
        jsonObj->insert(QStringLiteral("ikey"), QJsonValue(v.toInt()));
    }

    tixrCore->setPgFunction(QStringLiteral("main.insfamcatprods"));
    currentTaskList << tixrCore->sendMsg(0, HTTP_METHOD_ID::POST, "/api", this, "handleResponse", jsonObj);

    accepted = true;
}

void TixrNewSpectWidget::handleResponse(QString respError, PendingRequest *curReq)
{
    if(!beginHandleResponse(respError, curReq))
        return;

    QStandardItem *responseItem = curReq->getResponseItem();

    QStandardItem *tgtItem = JsonQStandardItemFactory::getTargetItem(QStringLiteral("fptitlecol_ikey"), this->currentItem);

    Q_ASSERT(responseItem->rowCount() == 1);

    QStandardItem *ikeyItem = responseItem->child(0);

    QVariant v = ikeyItem->data(DataRole::ikey());

    tgtItem->setData(v, DataRole::ikey());

    endHandleResponse(curReq);

    curReq->setResponseItemParent(0);//delete responseItem;

    if(parentTabWidget)
    {
        emit parentTabWidget->tabCloseRequested(parentTabWidget->indexOf(this));
    }
}

void TixrNewSpectWidget::on_pbAnnulerNewSpect_clicked()
{
    accepted = false;
    if(parentTabWidget)
    {
        emit parentTabWidget->tabCloseRequested(parentTabWidget->indexOf(this));
    }
}
