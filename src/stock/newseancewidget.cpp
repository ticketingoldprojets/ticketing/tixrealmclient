#include "newseancewidget.h"
#include "../mainwindow.h"
#include "../tariff/grillesadminpage.h"
#include "../tariff/grilleheaderview.h"
#include "../mappersupportitemdelegate.h"
#include "../undo/setmodeldatacommand.h"

Q_DECLARE_METATYPE( QStandardItem* )

TixrNewSeanceWidget::TixrNewSeanceWidget(TixRealmCore *mObj, QStandardItem *r, QWidget *parent, QStandardItem *cItem) :
    TixrWidget("newSeance", mObj, "stockbtn.png", parent, r, cItem), site2dView(0)
{
    this->tixrWidgetType = TIXR_NEWSEANCE;

    seancePb2 = findChild<QPushButton*>("pageSwitcher___2");
    connect(seancePb2, SIGNAL(clicked()), this, SLOT(tarifPageActivated()));

    QPushButton *pbModeleBillet = findChild<QPushButton*>("pbModeleBillet");
    connect(pbModeleBillet, SIGNAL(clicked()), this, SLOT(seanceSelectModeleBillet()), Qt::UniqueConnection);

    seanceStackedW = findChild<QStackedWidget*>("seanceStackedW");

    seanceGriList = findChild<QListView*>("seanceGriList");

    seanceGriTable = findChild<QTableView*>("seanceGriTable");
    setHeaderView(seanceGriTable);

    seanceGriNameLE = findChild<QLineEdit*>("seanceGriNameLE");

    lePlano = findChild<QLineEdit*>("lePlano");

    gbVenuePlan = findChild<QGroupBox*>("gbVenuePlan");

    QStandardItem *tgtItem = JsonQStandardItemFactory::getTargetItem(QStringLiteral("fptitlecol"), r->parent());

    QString title_fam = tgtItem->data(Qt::DisplayRole).toString();
    QLineEdit *lineEdit_5 = findChild<QLineEdit*>("lineEdit_5");
    lineEdit_5->setText(title_fam);

    if(parentTabWidget)
    {
        int tabIndex = parentTabWidget->addTab(this, updateMode?tr("Update event date"):tr("New event date"));
        if(tabIndex>-1)
        {
            parentTabWidget->setCurrentIndex(tabIndex);
        }

        Q_ASSERT(tabIndex>-1);
    }

    tvQuotes = findChild<QTableView*>("tvQuotes");

    MainWindow::tixrConnectSlotsByName(this);

    if(tixrCore->keyToItemMap.contains("grilles"))//detectar si los datos ya han sido cargados con anterioridad
    {
        showTariffsAndSelectCurrentOne(tixrCore->keyToItemMap.value("grilles"));
    }
    else
    {
        tixrCore->setPgFunction("main.getallgrilles");
        PendingRequest *pReq = tixrCore->sendMsg(tixrCore->stdModel->invisibleRootItem(), HTTP_METHOD_ID::GET, "/api", this, "responseGetAllGrilles");
        currentTaskList << pReq;
        pReq->setRespDataMapKey(QStringLiteral("grilles"));
    }
}

void TixrNewSeanceWidget::setHeaderView(QTableView *tv)
{
    GrilleHeaderView *myHView = new GrilleHeaderView(Qt::Horizontal, tv);
    myHView->setDragDropMode(QAbstractItemView::DragOnly);
    myHView->setAcceptDrops(false);
#if (QT_VERSION < QT_VERSION_CHECK(5, 0, 0))
    myHView->setResizeMode(QHeaderView::ResizeToContents);
#else
    myHView->setSectionResizeMode(QHeaderView::ResizeToContents);
#endif
    tv->setHorizontalHeader(myHView);

    myHView = new GrilleHeaderView(Qt::Vertical, tv);
    myHView->setDragDropMode(QAbstractItemView::DragOnly);
    myHView->setAcceptDrops(false);
#if (QT_VERSION < QT_VERSION_CHECK(5, 0, 0))
    myHView->setResizeMode(QHeaderView::ResizeToContents);
#else
    myHView->setSectionResizeMode(QHeaderView::ResizeToContents);
#endif
    tv->setVerticalHeader(myHView);
}

void TixrNewSeanceWidget::responseGetAllGrilles(QString respError, PendingRequest *curReq)
{
    if(!beginHandleResponse(respError, curReq))
        return;

    QStandardItem *responseItem = curReq->getResponseItem();

    showTariffsAndSelectCurrentOne(responseItem);

    endHandleResponse(curReq);
}

void TixrNewSeanceWidget::showTariffsAndSelectCurrentOne(QStandardItem *tariffsRootItem)
{
    seanceGriList->setModel(tixrCore->stdModel);
    seanceGriList->setRootIndex(tariffsRootItem->index());

    QStandardItem *griKeyItem = JsonQStandardItemFactory::getTargetItem(QStringLiteral("cpgritar"), currentItem);
    if(griKeyItem->data(DataRole::ikey()).isValid())
    {
        QStandardItem *currentTariffItem = 0;
        for(int i=0, n=tariffsRootItem->rowCount(); i<n; i++)
        {
            QStandardItem *gItm = tariffsRootItem->child(i);
            if(griKeyItem->data(DataRole::ikey()) == gItm->data(DataRole::ikey()))
            {
                currentTariffItem = gItm;
                break;
            }
        }
        Q_ASSERT(currentTariffItem);
        if(currentTariffItem)
        {
            on_seanceGriList_clicked(currentTariffItem->index());
        }
    }
}

void TixrNewSeanceWidget::on_pbValiderNewSeance_clicked()
{
    bool hasVplan = gbVenuePlan && gbVenuePlan->isChecked();

    if(hasVplan && !site2dView)
    {
        QMessageBox::information(0, qApp->applicationName(), tr("You should choice a venue's configuration file."));
        return;
    }

    QJsonObject planObj;
    QString expError;

    if(hasVplan)
    {
        site2dView->exportAsJson(planObj, expError);

        if(!expError.isEmpty())
        {
            QMessageBox::information(0, qApp->applicationName(), expError);
            return;
        }
    }

    QJsonObject *mainObj = tixrCore->widgetToJson(dynWidget);

    QStandardItem *tgtItem = JsonQStandardItemFactory::getTargetItem(QStringLiteral("fptitlecol_ikey"), rootItem->parent());
    QVariant v = tgtItem->data(DataRole::ikey());
    mainObj->insert(QStringLiteral("fam"), QJsonValue(v.toInt()));

    if(updateMode)
    {
        tgtItem = JsonQStandardItemFactory::getTargetItem(QStringLiteral("cptitre_ikey"), currentItem);
        v = tgtItem->data(DataRole::ikey());
        mainObj->insert(QStringLiteral("ikey"), QJsonValue(v.toInt()));
    }

    if(seanceGriTable->rootIndex().isValid())
    {
        v = seanceGriTable->rootIndex().data(DataRole::ikey());
        mainObj->insert(QStringLiteral("cpgritar"), QJsonValue(v.toInt()));
    }

    if(hasVplan)
    {
        mainObj->insert(QStringLiteral("plan"), QJsonValue(planObj));

        //Poner en la solicitud la asociación de zonas de tar. a columnas de la grille
        QJsonArray zTgArr;
        QList< QPair<QString,QString> > zonesToGrilleMap = site2dView->getZonesToGrilleMap();
        for(int i=zonesToGrilleMap.size()-1; i>=0; i--)
        {
            QPair<QString,QString> pair = zonesToGrilleMap.at(i);
            QJsonObject zpObj;
            zpObj.insert(QStringLiteral("zone"), QJsonValue(pair.first));
            zpObj.insert(QStringLiteral("gricol"), QJsonValue(pair.second));
            zTgArr << zpObj;
        }

        mainObj->insert(QStringLiteral("ztarmap"), QJsonValue(zTgArr));

        mainObj->remove(QStringLiteral("capacity"));
    }

    tixrCore->setPgFunction(QStringLiteral("main.inscategprods"));
    currentTaskList << tixrCore->sendMsg(0, HTTP_METHOD_ID::POST, "/api", this, "responseNewSeance", mainObj);

    accepted = true;
}

void TixrNewSeanceWidget::responseNewSeance(QString respError, PendingRequest *curReq)
{
    if(!beginHandleResponse(respError, curReq))
        return;

    QStandardItem *responseItem = curReq->getResponseItem();

    QStandardItem *tgtItem = JsonQStandardItemFactory::getTargetItem(QStringLiteral("cptitre_ikey"), this->currentItem);
    QStandardItem *ikeyItem = responseItem->child(0);
    Q_ASSERT(ikeyItem);
    QVariant v = ikeyItem->data(DataRole::ikey());
    tgtItem->setData(v, DataRole::ikey());

    QStandardItem *planoIdItem = responseItem->child(0, DataColumn::cpplano());
    Q_ASSERT(planoIdItem);
    v = planoIdItem->data(Qt::DisplayRole);
    if(v.isValid())
    {
        tgtItem = JsonQStandardItemFactory::getTargetItem(QStringLiteral("cpplano"), this->currentItem);
        tgtItem->setData(v, DataRole::ikey());
    }

    endHandleResponse(curReq);

    curReq->setResponseItemParent(0);//delete responseItem;

    if(parentTabWidget)
    {
        emit parentTabWidget->tabCloseRequested(parentTabWidget->indexOf(this));
    }
}

void TixrNewSeanceWidget::seanceSelectModeleBillet()
{
    QString fileName = QFileDialog::getOpenFileName(this,
         tr("Open Ticket Config."), "", tr("Xml Files (*.xml)"));
    QLineEdit *lineEdit_4 = findChild<QLineEdit*>("lineEdit_4");
    lineEdit_4->setText(fileName);
}

void TixrNewSeanceWidget::on_pbPlanNewSeance_clicked()
{
    QString fileName = QFileDialog::getOpenFileName(this, tr("Open Place Config."), "D:/Yovanis/SantRosSoft/venue_plans", tr("Svg Files (*.svg)"));
    if(fileName.isEmpty())
        return;

    if(!loadVenuePlan(fileName))
    {
        QMessageBox::information(0, qApp->applicationName(), tr(site2dView->errorMsg().toUtf8().constData()));
    }
}

bool TixrNewSeanceWidget::loadVenuePlan(QString vplanFile)
{
    QStackedWidget *seanceStackedW = findChild<QStackedWidget*>("seanceStackedW");
    QWidget *progBarPage = seanceStackedW->findChild<QWidget*>("progBarPage");

    QPixmap originalPixmap = grab(QRect(progBarPage->mapTo(this, progBarPage->pos()), seanceStackedW->size()));
    QPalette palette = progBarPage->palette();
    palette.setBrush(QPalette::Window, QBrush(originalPixmap));
    progBarPage->setPalette(palette);

    seancePb2->setProperty("tixrPageIdx", QVariant(4));
    seanceStackedW->setCurrentIndex(4);

    //Cargar el Plan de site
    QLayout *layout = findChild<QWidget*>("widget_8")->layout();
    site2dView = static_cast<Site2dView*>(seanceStackedW->findChild<QWidget*>("Site2dView"));
    if(site2dView)
    {
        layout->removeWidget(site2dView);
        delete site2dView;
    }

    site2dView = new Site2dView(static_cast<QProgressBar*>(progBarPage->findChild<QWidget*>("progressBar")), true, false, tixrCore, seanceStackedW);

    QFile f(vplanFile);
    site2dView->loadPlan(&f);

    seancePb2->setProperty("tixrPageIdx", QVariant(1));
    seanceStackedW->setCurrentIndex(1);

    bool r = site2dView->errorMsg().isEmpty();

    if(r)
    {
        lePlano->setText(vplanFile);
        layout->addWidget(site2dView);
    }
    else
    {
        delete site2dView;
        site2dView = 0;
    }

    return r;
}

void TixrNewSeanceWidget::on_seanceGriList_clicked(QModelIndex index)
{
    if(seanceGriTable->rootIndex().parent()==index)
        return;

    QStandardItem *targetModelItem = tixrCore->stdModel->itemFromIndex(index);
    if(targetModelItem->hasChildren())
    {
        setCurrentTariff(targetModelItem->child(0));
    }
    else
    {
        QStandardItem *tgtItem = JsonQStandardItemFactory::getTargetItem(QStringLiteral("cptitre_ikey"), this->currentItem);
        QVariant v = tgtItem->data(DataRole::ikey());

        QString idGrilleStr = tixrCore->stdModel->data(index, DataRole::ikey()).toString();

        QByteArray baReq = (v.isValid() ? QStringLiteral("{\"srvikey\": \"%1\", \"catprodkey\": \"%2\"}").arg(idGrilleStr, v.toString()) : QStringLiteral("{\"srvikey\": \"%1\"}").arg(idGrilleStr)).toUtf8();

        tixrCore->setInHandlerIndex(1);
        tixrCore->setPgFunction("main.fetchgrille");
        PendingRequest *pReq = tixrCore->sendMsg(targetModelItem, HTTP_METHOD_ID::POST, "/api", this, "seanceShowGrille", baReq);
        tgtItem = JsonQStandardItemFactory::getTargetItem(QStringLiteral("cpsizes"), this->currentItem);
        pReq->respHandlerParams.insert("currentCatProd", QVariant::fromValue(tgtItem));
        currentTaskList << pReq;
    }
}

void TixrNewSeanceWidget::on_pbRejectGri_clicked()
{
    seanceGriTable->horizontalHeader()->setModel(0);
    seanceGriTable->verticalHeader()->setModel(0);
    seanceGriTable->setModel(0);
    seanceGriNameLE->clear();

    QStandardItem *tgtItem = JsonQStandardItemFactory::getTargetItem(QStringLiteral("cpgritar"), currentItem);
    MapperSupportItemDelegate *msItmDel = static_cast<MapperSupportItemDelegate*>(dataWidgetMapper->itemDelegate());
    msItmDel->undoStack->push( new SetModelDataCommand(tgtItem->model(), tgtItem->index(), Qt::DisplayRole, QVariant(QString::null)) );
    msItmDel->undoStack->push( new SetModelDataCommand(tgtItem->model(), tgtItem->index(), (Qt::ItemDataRole)DataRole::ikey(), QVariant()) );
}

void TixrNewSeanceWidget::seanceShowGrille(QString respError, PendingRequest *curReq)
{
    if(!beginHandleResponse(respError, curReq))
        return;

    QStandardItem *responseItem = curReq->getResponseItem();

    setCurrentTariff(responseItem);

    endHandleResponse(curReq);
}

void TixrNewSeanceWidget::setCurrentTariff(QStandardItem *currentTariffItem)
{
    if(!seanceGriTable->model())
        seanceGriTable->setModel(tixrCore->stdModel);
    seanceGriTable->setRootIndex(currentTariffItem->index());
    seanceGriNameLE->setText(currentTariffItem->text());

    QStandardItemModel *headersModel = tixrCore->itemToHeaderModelMap.value(currentTariffItem);

    GrilleHeaderView *myHView = static_cast<GrilleHeaderView*>(seanceGriTable->horizontalHeader());
    myHView->setGriItem(currentTariffItem);
    myHView->setModel(headersModel);
    myHView->show();

    myHView = static_cast<GrilleHeaderView*>(seanceGriTable->verticalHeader());
    myHView->setGriItem(currentTariffItem);
    myHView->setModel(headersModel);
    myHView->show();

    QHeaderView *quotesHeader = static_cast<QHeaderView*>(tvQuotes->horizontalHeader());
    quotesHeader->setModel(headersModel);
    quotesHeader->setRootIndex(tvQuotes->horizontalHeader()->rootIndex());
    quotesHeader->show();

    //asignar la tabla de precios a la presentación
    QStandardItem *tgtItem = JsonQStandardItemFactory::getTargetItem(QStringLiteral("cpgritar"), currentItem);
    QVariant v = currentTariffItem->data(DataRole::ikey());

    MapperSupportItemDelegate *msItmDel = static_cast<MapperSupportItemDelegate*>(dataWidgetMapper->itemDelegate());
    msItmDel->undoStack->push( new SetModelDataCommand(tgtItem->model(), tgtItem->index(), Qt::DisplayRole, currentTariffItem->text()) );
    msItmDel->undoStack->push( new SetModelDataCommand(tgtItem->model(), tgtItem->index(), (Qt::ItemDataRole)DataRole::ikey(), v) );
}

void TixrNewSeanceWidget::on_pbAdmGri_clicked()
{
    for(int i=0, n=parentTabWidget->count(); i<n; i++)
    {
        TixrWidget *scW = static_cast<TixrWidget*>(parentTabWidget->widget(i));
        if(scW->tixrWtype() == TIXR_GRILLES)
        {
            parentTabWidget->setCurrentIndex(i);
            return;
        }
    }

    GrillesAdminPage *grillesAdminPage =  new GrillesAdminPage(tixrCore, parentTabWidget);

    QString e = grillesAdminPage->getErrorMsg();

    if(!e.isEmpty())
        QMessageBox::information(0, qApp->applicationName(), tr(e.toUtf8().constData()));
}

void TixrNewSeanceWidget::tarifPageActivated()
{
    seanceGriList->setRootIndex(tixrCore->keyToItemMap.value("grilles")->index());
}

void TixrNewSeanceWidget::on_pbCancel_clicked()
{
    accepted = false;
    if(parentTabWidget)
    {
        emit parentTabWidget->tabCloseRequested(parentTabWidget->indexOf(this));
    }
}
