#ifndef SCNEWSPECTWIDGET_H
#define SCNEWSPECTWIDGET_H

#include "../widget.h"

class TixrNewSpectWidget : public TixrWidget
{
    Q_OBJECT

private slots:
    void on_pbValiderNewSpect_clicked();
    void on_pbAnnulerNewSpect_clicked();
    void handleResponse(QString respError, PendingRequest *curReq);

public:
    explicit TixrNewSpectWidget(TixRealmCore *mObj, QStandardItem *r, QWidget *parent = 0, QStandardItem *cItem = 0);
};

#endif // SCNEWSPECTWIDGET_H

