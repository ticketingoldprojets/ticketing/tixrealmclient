#ifndef SCTICKETINGSTOCKWIDGET_H
#define SCTICKETINGSTOCKWIDGET_H

#include "stockwidget.h"

class TixrTicketingStockWidget : public TixrStockWidget
{
    Q_OBJECT

public:
    explicit TixrTicketingStockWidget(TixRealmCore *mObj, TixrCartWidget *cw, QString uiName, QString iconName, QWidget *parent = 0);

private:
    void requestShowCatProds(const QModelIndex &index);
};

#endif // SCTICKETINGSTOCKWIDGET_H

