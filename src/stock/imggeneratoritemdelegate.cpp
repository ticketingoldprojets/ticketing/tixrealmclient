#include "imggeneratoritemdelegate.h"

ImgGeneratorItemDelegate::ImgGeneratorItemDelegate(QObject *parent) :
    QStyledItemDelegate(parent)
{
}

void ImgGeneratorItemDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    //if(!(index.data(Qt::DecorationRole).isValid() || index.column()))
    //if(!index.data(Qt::DecorationRole).isValid())
    if(!index.column())
    {
        QImage img;
        QByteArray ba = index.data().toByteArray();
        //ba = QByteArray::fromBase64(ba);
        img.fromData(ba);
        const_cast<QAbstractItemModel*>(index.model())->setData(index, img, Qt::DisplayRole);
        const_cast<QAbstractItemModel*>(index.model())->setData(index, img, Qt::DecorationRole);
    }

    QStyledItemDelegate::paint(painter, option, index);
}
