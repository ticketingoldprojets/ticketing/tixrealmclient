#ifndef SCSTOCKWIDGET_H
#define SCSTOCKWIDGET_H

#include "../widget.h"
#include "../TixRealmClientPlugins/PictureFlowViewPlugin/pictureflow.h"
#include "../cartwidget.h"
#include "transportwidget.h"  // AGREGADO POR CYPC
#include "../mainwindow.h"

class TixrStockWidget : public TixrWidget
{
    Q_OBJECT

    QMap< QString, QList<void (TixrStockWidget::*)(const QModelIndex &)>* > stockViewActionsMap;

    QStackedWidget *getStockViewStackedWidget(int level);
    void setHeaders(QAbstractItemView *itemView);

protected:
    QString stockModelEntryKey;//llave en el modelo para este tipo de stock

    QListView *listViewLevel1;
    QTableView *tableViewLevel1;
    PictureFlowView *pflowLevel1View;
    QListView *listViewLevel2;
    QTableView *tableViewLevel2;
    PictureFlowView *pflowLevel2View;
    QToolButton *tbShowPictView;
    QToolButton *tbShowListView;
    QToolButton *tbShowTableView;
    QToolButton *tbNewLevel1;
    QToolButton *tbShowPictL2View;
    QToolButton *tbShowListL2View;
    QToolButton *tbShowTableL2View;
    QToolButton *tbNewLevel2;
    QToolButton *tbUP;
    QToolButton *tbEditLevel1;
    QToolButton *tbEditLevel2;
    QStackedWidget *stackedWidgetLevel1;
    QStackedWidget *stackedWidgetLevel2;
    QToolButton *tbShowPlanL1;
    QToolButton *tbShowPlanL2;
    QWidget *salesWidget;
    QList<QAbstractButton*> level1Btns;
    QList<QAbstractButton*> level2Btns;
    //Interfaz de venta rápida
    QToolButton *tbSup;
    QSpinBox *quickSellSP;
    QToolButton *tbDelLevel1;
    QToolButton *tbDelLevel2;

    TixrCartWidget *cartWidget;

    bool level2Clicked;

    QStandardItemModel *famCatProdHeaders;
    QStandardItemModel *catProdHeaders;
    QStandardItemModel *zoneTarHeaders;
    QStandardItemModel *catCliHeaders;

    QRegExp tweakPatternRX;

    void initUI();//inicialización de componentes, conexiones y otras cosas de inicio
    void loadTopLevelProductClasification(QString method, int prodType);

    void tweaking(QStandardItem *r, QAbstractItemView *v);

    void newFamilleCategProduit(const QModelIndex &root);
    void newCategorieProduit(const QModelIndex &root);

    void editFamilleCategProduit(const QModelIndex &index);
    void editCategorieProduit(const QModelIndex &index);

    void delFamilleOrCategProduit(const QModelIndex &index);

    int currentViewLevel(QStandardItem *item);//Nivel que está siendo visualizado en la gerarquía del stock
    virtual void requestShowCatProds(const QModelIndex &index) = 0;
    void requestShowZonesAvecPrix(const QModelIndex &index);
    void requestShowCategCli(const QModelIndex &index);
    virtual void requestAddToCar(const QModelIndex &index);
    void showResp(QStandardItem *responseItem);

    virtual void upDateInterface(QAbstractItemView *iViewL1, QAbstractItemView *iViewL2, int *L1 = 0, int *L2 = 0);

protected slots:
    void changeStockProdView();//!<Switch between ListView, TableView or CoverFlowView
    virtual void itemViewClicked(const QModelIndex &index);//!<Stock produit view's click event handler
    void on_tbUP_clicked();
    void stockPageActionHandler();

    void showResponse(QString respError, PendingRequest *curReq);
    void handleDelCmdResponse(QString respError, PendingRequest *curReq);

    void showStockProds(QString respError, PendingRequest *curReq);
    void showPlanSalle();

    //Interfaz de venta rápida
    void on_tbDigit_clicked();//Actualizar el QSpinBox de la cantidad de productos a agregar al carro de compras
    void on_tbSup_clicked();

    //Notificar que una tarea, cuya respuesta se gestionó en otra clase, ya terminó
    void taskWasDone(PendingRequest *curReq);

public:
    explicit TixrStockWidget(TixRealmCore *mObj, TixrCartWidget *cw, QString uiName, QString iconName, QWidget *parent = 0);
    ~TixrStockWidget();
};

#endif // SCSTOCKWIDGET_H

