//-----------------------------------------------------------------------------
#ifndef SCTRANSPORTWIDGET_H
#define SCTRANSPORTWIDGET_H
//-----------------------------------------------------------------------------
#include "../widget.h"
//-----------------------------------------------------------------------------
class TixrTransportWidget : public TixrWidget
{
    Q_OBJECT

    public:
        explicit TixrTransportWidget(TixRealmCore * mObj, int famcatid, QWidget * parent = 0);

    protected:
        QStackedWidget * swMain;
        QTableWidget   * twInnerPorts;
        QTableWidget   * twCalendrier;
        QToolButton    * pbNewTraject;
        QToolButton    * pbNewStation;
        QToolButton    * pbValiderTraject;
        QToolButton    * pbPortValider;
        QToolButton    * pbPortEnlever;
        QToolButton    * pbPortAjouter;
        QToolButton    * pbDateValider;
        QToolButton    * pbDateEnlever;
        QToolButton    * pbDateAjouter;
        QToolButton    * pbNewCalend;
        QToolButton    * pbValiderCalend;
        QLineEdit      * lePortName;
        QDateEdit      * deDateIni;
        QDateEdit      * deDateEnd;
        QTableView     * tvPorts;
        QTableView     * tvDates;

        QListView      * seanceGriList;
        QTableView     * seanceGriTable;
        QLineEdit      * seanceGriNameLE;

        int currRow;
        int currCol;
        int famcatid;

        QStandardItemModel * transportModel;
        // QStandardItemModel * datesModel;

    private slots:       
        void on_pbNewTraject_clicked();
        void on_pbNewStation_clicked();
        void on_pbValiderTraject_clicked();
        void on_pbPortValider_clicked();
        void on_pbPortEnlever_clicked();
        void on_pbPortAjouter_clicked();
        void on_pbNewCalend_clicked();
        void on_pbValiderCalend_clicked();
        void on_pbDateValider_clicked();
        void on_pbDateAjouter_clicked();

        void on_twInnerPorts_currentIndexChanged(int index);
        void on_twInnerPorts_itemSelectionChanged();
        void on_lePortName_textChanged(const QString & text);
        void on_tvPorts_clicked(const QModelIndex & index);
        void on_tvDates_clicked(const QModelIndex & index);
        // void on_twInnerPorts_cellChanged(int row, int column);
        QStandardItem * fillNewPort(QStandardItem * reqItem, QStandardItem * respItem);
        QStandardItem * fillNewTraject(QStandardItem * reqItem, QStandardItem * respItem);
        QStandardItem * fillNewCalend(QStandardItem * reqItem, QStandardItem * respItem);
        void serverResponse(QString respError, PendingRequest *curReq);
        void on_seanceGriList_clicked(QModelIndex index);
};
//-----------------------------------------------------------------------------
#endif // SCTRANSPORTWIDGET_H
//-----------------------------------------------------------------------------
