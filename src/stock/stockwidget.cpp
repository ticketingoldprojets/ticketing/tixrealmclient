#include "stockwidget.h"
#include "../mainwindow.h"
#include "../TixrClientEditorCommon/site2dview.h"
#include "newspectwidget.h"
#include "newseancewidget.h"
#include "imggeneratoritemdelegate.h"

TixrStockWidget::TixrStockWidget(TixRealmCore *mObj, TixrCartWidget *cw, QString uiName, QString iconName, QWidget *parent) :
    TixrWidget(uiName, mObj, iconName, parent), cartWidget(cw)
{
    this->tixrWidgetType = TIXR_STOCK;
    tweakPatternRX.setPattern("^((?:\\w+_\\w+-\\w+_\\w+,)*\\w+_\\w+-\\w+_\\w+)\\|((?:\\w+_\\w+-\\w+_\\w+,)*\\w+_\\w+-\\w+_\\w+)\\|((?:\\w+_\\w+-\\w+_\\w+,)*\\w+_\\w+-\\w+_\\w+)$");

    auto stockViewActions = new QList< void (TixrStockWidget::*)(const QModelIndex &) >;
    *stockViewActions << &TixrStockWidget::newFamilleCategProduit;
    *stockViewActions << &TixrStockWidget::newCategorieProduit;
    stockViewActionsMap.insert(QStringLiteral("New"), stockViewActions);

    stockViewActions = new QList< void (TixrStockWidget::*)(const QModelIndex &) >;
    *stockViewActions << &TixrStockWidget::editFamilleCategProduit
                      << &TixrStockWidget::editCategorieProduit;
    stockViewActionsMap.insert(QStringLiteral("Edit"), stockViewActions);

    stockViewActions = new QList< void (TixrStockWidget::*)(const QModelIndex &) >;
    *stockViewActions << &TixrStockWidget::delFamilleOrCategProduit;
    *stockViewActions << &TixrStockWidget::delFamilleOrCategProduit;
    stockViewActionsMap.insert(QStringLiteral("Del"), stockViewActions);
}

TixrStockWidget::~TixrStockWidget()
{
    delete stockViewActionsMap[QStringLiteral("New")];
    delete stockViewActionsMap[QStringLiteral("Edit")];
    delete stockViewActionsMap[QStringLiteral("Del")];
}

void TixrStockWidget::loadTopLevelProductClasification(QString method, int prodType)
{
    QStandardItem *stockItem = 0;
    if(tixrCore->keyToItemMap.contains(stockModelEntryKey))// determina si el stock ya fue cargado con anterioridad
    {
        stockItem = tixrCore->keyToItemMap.value(stockModelEntryKey); // obtiene el item del mapa asociado a la clave stock
    }

    if(stockItem)
    {
        showResp(stockItem);
    }
    else
    {
        QStandardItem *modelSubItem = new QStandardItem(1,1);
        modelSubItem->setData("ItemId", DataRole::jsonProperty());
        modelSubItem->setData(JsonObject, DataRole::jsonType());

        QStandardItem *modelSubItem2 = new QStandardItem(QString::number(prodType));
        modelSubItem2->setData("srvikey", DataRole::jsonProperty());
        modelSubItem2->setData(JsonSimple, DataRole::jsonType());
        modelSubItem->setChild(0, 0, modelSubItem2);

        tixrCore->stdModel->appendRow(modelSubItem);

        tixrCore->setPgFunction(method);
        PendingRequest *pReq = tixrCore->sendMsg(modelSubItem, HTTP_METHOD_ID::POST, "/api", this, "showResponse");
        pReq->setRespDataMapKey(stockModelEntryKey);

        currentTaskList << pReq;
    }
}

void TixrStockWidget::initUI()
{
    //conexiones
    MainWindow::tixrConnectSlotsByName(this);

    tbShowListView = findChild<QToolButton*>("tbShowListView");
    connect(tbShowListView, SIGNAL(clicked()), this, SLOT(changeStockProdView()), Qt::UniqueConnection);

    tbShowListL2View = findChild<QToolButton*>("tbShowListL2View");
    connect(tbShowListL2View, SIGNAL(clicked()), this, SLOT(changeStockProdView()), Qt::UniqueConnection);

    tbShowTableView = findChild<QToolButton*>("tbShowTableView");
    connect(tbShowTableView, SIGNAL(clicked()), this, SLOT(changeStockProdView()), Qt::UniqueConnection);

    tbShowTableL2View = findChild<QToolButton*>("tbShowTableL2View");
    connect(tbShowTableL2View, SIGNAL(clicked()), this, SLOT(changeStockProdView()), Qt::UniqueConnection);

    tbShowPictView = findChild<QToolButton*>("tbShowPictView");
    connect(tbShowPictView, SIGNAL(clicked()), this, SLOT(changeStockProdView()), Qt::UniqueConnection);

    tbShowPictL2View = findChild<QToolButton*>("tbShowPictL2View");
    connect(tbShowPictL2View, SIGNAL(clicked()), this, SLOT(changeStockProdView()), Qt::UniqueConnection);

    //Interfaz de venta rápida
    quickSellSP = findChild<QSpinBox*>("quickSellSP");

    level1Btns << tbShowListView;
    level1Btns << tbShowTableView;
    level1Btns << tbShowPictView;

    level2Btns << tbShowListL2View;
    level2Btns << tbShowTableL2View;
    level2Btns << tbShowPictL2View;

    tbNewLevel1 = findChild<QToolButton*>("tbNewLevel1");
    connect(tbNewLevel1, SIGNAL(clicked()), this, SLOT(stockPageActionHandler()), Qt::UniqueConnection);

    tbNewLevel2 = findChild<QToolButton*>("tbNewLevel2");
    connect(tbNewLevel2, SIGNAL(clicked()), this, SLOT(stockPageActionHandler()), Qt::UniqueConnection);

    tbEditLevel1 = findChild<QToolButton*>("tbEditLevel1");
    connect(tbEditLevel1, SIGNAL(clicked()), this, SLOT(stockPageActionHandler()), Qt::UniqueConnection);

    tbEditLevel2 = findChild<QToolButton*>("tbEditLevel2");
    connect(tbEditLevel2, SIGNAL(clicked()), this, SLOT(stockPageActionHandler()), Qt::UniqueConnection);

    tbDelLevel1 = findChild<QToolButton*>("tbDelLevel1");
    connect(tbDelLevel1, SIGNAL(clicked()), this, SLOT(stockPageActionHandler()), Qt::UniqueConnection);

    tbDelLevel2 = findChild<QToolButton*>("tbDelLevel2");
    connect(tbDelLevel2, SIGNAL(clicked()), this, SLOT(stockPageActionHandler()), Qt::UniqueConnection);

    tbShowPlanL1 = findChild<QToolButton*>("tbShowPlanL1");
    connect(tbShowPlanL1, SIGNAL(clicked()), this, SLOT(showPlanSalle()), Qt::UniqueConnection);
    tbShowPlanL1->setEnabled(false);

    tbShowPlanL2 = findChild<QToolButton*>("tbShowPlanL2");
    connect(tbShowPlanL2, SIGNAL(clicked()), this, SLOT(showPlanSalle()), Qt::UniqueConnection);
    tbShowPlanL2->setEnabled(false);

    tbUP = findChild<QToolButton*>("tbUP");

    stackedWidgetLevel1 = findChild<QStackedWidget*>("stackedWidgetLevel1");

    listViewLevel1 = findChild<QListView*>("listViewLevel1");
    connect(listViewLevel1, SIGNAL(activated(QModelIndex)), this, SLOT(itemViewClicked(QModelIndex)), Qt::UniqueConnection);
    listViewLevel1->setModel(tixrCore->stdModel);

    pflowLevel1View = (PictureFlowView*)findChild<QAbstractItemView*>("pflowLevel1View");
    connect(pflowLevel1View, SIGNAL(clicked(QModelIndex)), this, SLOT(itemViewClicked(QModelIndex)), Qt::UniqueConnection);
    pflowLevel1View->setModel(tixrCore->stdModel);

    tableViewLevel1 = findChild<QTableView*>("tableViewLevel1");
    connect(tableViewLevel1, SIGNAL(activated(QModelIndex)), this, SLOT(itemViewClicked(QModelIndex)), Qt::UniqueConnection);
    tableViewLevel1->setModel(tixrCore->stdModel);

    listViewLevel2 = findChild<QListView*>("listViewLevel2");
    connect(listViewLevel2, SIGNAL(activated(QModelIndex)), this, SLOT(itemViewClicked(QModelIndex)), Qt::UniqueConnection);
    listViewLevel2->setModel(tixrCore->stdModel);

    tableViewLevel2 = findChild<QTableView*>("tableViewLevel2");
    connect(tableViewLevel2, SIGNAL(activated(QModelIndex)), this, SLOT(itemViewClicked(QModelIndex)), Qt::UniqueConnection);
    tableViewLevel2->setModel(tixrCore->stdModel);

    pflowLevel2View = (PictureFlowView*)findChild<QAbstractItemView*>("pflowLevel2View");
    connect(pflowLevel2View, SIGNAL(clicked(QModelIndex)), this, SLOT(itemViewClicked(QModelIndex)), Qt::UniqueConnection);
    pflowLevel2View->setModel(tixrCore->stdModel);

    stackedWidgetLevel2 = findChild<QStackedWidget*>("stackedWidgetLevel2");

    connect(cartWidget, SIGNAL(notifyTaskDone(PendingRequest*)), this, SLOT(taskWasDone(PendingRequest*)));

    //visualización y solicitud de datos al servidor
    salesWidget = findChild<QWidget*>("salesWidget");
    salesWidget->setVisible(false);

    level2Clicked = false;
}

void TixrStockWidget::taskWasDone(PendingRequest *curReq)
{
    if(curReq)
    {
        currentTaskList.removeOne(curReq);
        int wIndex = parentTabWidget->indexOf(this);
        if(currentTaskList.isEmpty() && wIndex>=0)
            parentTabWidget->setTabIcon(wIndex, windowIcon());
    }
}

void TixrStockWidget::showResponse(QString respError, PendingRequest *curReq)
{
    QStandardItem *requestItem = curReq->getRequestItem();
    QStandardItem *responseItem = curReq->getResponseItem();

    if(!beginHandleResponse(respError, curReq))
    {
        setErrorMsg(tr(respError.toUtf8().constData()));

        if(tixrCore->keyToItemMap.value(stockModelEntryKey) == requestItem)
        {
            tixrCore->keyToItemMap.remove(stockModelEntryKey);
            parentTabWidget->removeTab( parentTabWidget->indexOf(this) );
            deleteLater();
        }

        return;
    }

    showResp(responseItem);

    endHandleResponse(curReq);
}

void TixrStockWidget::showResp(QStandardItem *responseItem)
{
    QAbstractItemView *iViewL1 = static_cast<QAbstractItemView*>(stackedWidgetLevel1->currentWidget()->children().at(1));
    QAbstractItemView *iViewL2 = static_cast<QAbstractItemView*>(stackedWidgetLevel2->currentWidget()->children().at(1));

    if(level2Clicked)//si la solicitud se originó en el nivel 2 pasar el contenido de aquí al nivel 1 antes de mostrar los resultados
    {
        tweaking(tixrCore->stdModel->itemFromIndex(iViewL2->rootIndex()), iViewL1);

        iViewL1->setRootIndex(iViewL2->rootIndex());
        iViewL1->setCurrentIndex(responseItem->parent()->index());

        level2Clicked = false;
    }

    if(iViewL1->rootIndex().isValid())
    {
        tweaking(responseItem, iViewL2);

        iViewL2->setRootIndex(responseItem->index());
    }
    else
    {
        tweaking(responseItem, iViewL1);

        iViewL1->setRootIndex(responseItem->index());
        if(responseItem->hasChildren())
        {
            QModelIndex idx = responseItem->child(0)->index();
            iViewL1->setCurrentIndex(idx);
            requestShowCatProds(idx);
        }
    }

    upDateInterface(iViewL1, iViewL2);

    setHeaders(iViewL1);
    setHeaders(iViewL2);
}

void TixrStockWidget::upDateInterface(QAbstractItemView *iViewL1, QAbstractItemView *iViewL2, int *L1, int *L2)
{
    QStandardItem *curItem = tixrCore->stdModel->itemFromIndex(iViewL1->rootIndex());

    tbShowPlanL1->setVisible(false);

    QStandardItem *planoIdItem = curItem ? curItem->child(0, DataColumn::cpplano()) : NULL;
    if (planoIdItem){
        QVariant v = planoIdItem->data(DataRole::ikey());
        if(v.isValid() && v.toInt() > 0){
            tbShowPlanL1->setVisible(true);
        }
    }

    int dataLevel = currentViewLevel(curItem);
    tbUP->setEnabled(dataLevel > 0);
    tbShowPlanL1->setEnabled(dataLevel > 0);


    QModelIndex index = iViewL1->rootIndex();


    if(L1)
        *L1 = dataLevel;

    curItem = tixrCore->stdModel->itemFromIndex(iViewL2->rootIndex());
    dataLevel = currentViewLevel(curItem);
    tbShowPlanL2->setEnabled(dataLevel > 1);

    tbShowPlanL2->setVisible(false);

    planoIdItem = curItem ? curItem->child(0, DataColumn::cpplano()) : NULL;
    if (planoIdItem){
        QVariant v = planoIdItem->data(DataRole::ikey());
        if(v.isValid() && v.toInt() > 0){
            tbShowPlanL2->setVisible(true);
        }
    }

    salesWidget->setVisible(dataLevel==3);

    if(L2)
        *L2 = dataLevel;
}

int TixrStockWidget::currentViewLevel(QStandardItem *item)//Nivel que está siendo visualizado en la gerarquía del stock
{
    if(!item)
        return -1;

    int level = 0;

    QStandardItem *root = tixrCore->keyToItemMap.value(stockModelEntryKey);
    while(root != item && item)
    {
        ++level;
        item = item->parent();
    }

    return level/2;
}

void TixrStockWidget::stockPageActionHandler()
{
    QString btnName = sender()->objectName();
    QString actionName = btnName.mid(2,btnName.length()-8);
    int btnLevel = btnName.right(1).toInt()-1;

    QStackedWidget *stackedWidget = getStockViewStackedWidget(btnLevel);

    QAbstractItemView *iView = static_cast<QAbstractItemView*>(stackedWidget->currentWidget()->children().at(1));
    QModelIndex levelIndex = iView->rootIndex();

    if(actionName != QStringLiteral("New") && !iView->currentIndex().isValid())
        return;

    int level = currentViewLevel(tixrCore->stdModel->itemFromIndex(levelIndex));//detectar el nivel en la gerarquía

    const QModelIndex mIdx = actionName == QStringLiteral("New") ? levelIndex : iView->currentIndex();

    //llamar al método apropiado
    (this->*stockViewActionsMap[actionName]->at(level))(mIdx);
}

QStackedWidget *TixrStockWidget::getStockViewStackedWidget(int level)
{
    return level ? stackedWidgetLevel2 : stackedWidgetLevel1;
}

void TixrStockWidget::newFamilleCategProduit(const QModelIndex &root)
{
    TixrNewSpectWidget *newSpect = new TixrNewSpectWidget(tixrCore, tixrCore->stdModel->itemFromIndex(root), parentTabWidget);

    QString e = newSpect->getErrorMsg();

    if(!e.isEmpty())
    {
        QMessageBox::information(0, qApp->applicationName(), tr(e.toUtf8().constData()));
    }
}

void TixrStockWidget::editFamilleCategProduit(const QModelIndex &index)
{
    QStandardItem *currentItem = tixrCore->stdModel->itemFromIndex(index);
    TixrNewSpectWidget *newSpect = new TixrNewSpectWidget(tixrCore, currentItem->parent(), parentTabWidget, currentItem);

    QString e = newSpect->getErrorMsg();

    if(!e.isEmpty())
    {
        QMessageBox::information(0, qApp->applicationName(), tr(e.toUtf8().constData()));
    }
}

void TixrStockWidget::delFamilleOrCategProduit(const QModelIndex &index)
{
    QStandardItem *cItem = tixrCore->stdModel->itemFromIndex(index);
    int level = currentViewLevel(cItem);
    QStandardItem *targetItem = JsonQStandardItemFactory::getTargetItem(level ? QStringLiteral("cptitre_ikey") : QStringLiteral("fptitlecol_ikey"), cItem);

    if(QMessageBox::question(0, qApp->applicationName(), tr("Do you want to remove the \"%1\" item").arg(targetItem->text()), QMessageBox::Yes|QMessageBox::Cancel) == QMessageBox::Cancel)
        return;

    QJsonObject jsonObj;
    QVariant v = targetItem->data(DataRole::ikey());
    jsonObj.insert(QStringLiteral("ikey"), QJsonValue(v.toInt()));

    tixrCore->setPgFunction(level ? QStringLiteral("main.delcategproduit") : QStringLiteral("main.delfamcatprods"));
    currentTaskList << tixrCore->sendMsg(targetItem, HTTP_METHOD_ID::POST, "/api", this, "handleDelCmdResponse", QJsonDocument(jsonObj).toJson(/*QJsonDocument::Compact*/));
}

void TixrStockWidget::handleDelCmdResponse(QString respError, PendingRequest *curReq)
{
    QStandardItem *targetItem = curReq->getResponseItemParent();
    curReq->setRequestItem(0);

    if(!beginHandleResponse(respError, curReq))
    {
        curReq->setResponseItemParent(0);
        return;
    }

    targetItem->parent()->removeRow(targetItem->row());
}

void TixrStockWidget::newCategorieProduit(const QModelIndex &root)
{
    TixrNewSeanceWidget *newSeance = new TixrNewSeanceWidget(tixrCore, tixrCore->stdModel->itemFromIndex(root), parentTabWidget);

    QString e = newSeance->getErrorMsg();

    if(!e.isEmpty())
    {
        QMessageBox::information(0, qApp->applicationName(), tr(e.toUtf8().constData()));
    }
}

void TixrStockWidget::editCategorieProduit(const QModelIndex &index)
{
    bool b = index.isValid();
    b = !b;

    QStandardItem *currentItem = tixrCore->stdModel->itemFromIndex(index);
    TixrNewSeanceWidget *newSeance = new TixrNewSeanceWidget(tixrCore, currentItem->parent(), parentTabWidget, currentItem);

    QString e = newSeance->getErrorMsg();

    if(!e.isEmpty())
    {
        QMessageBox::information(0, qApp->applicationName(), tr(e.toUtf8().constData()));
    }
}

void TixrStockWidget::on_tbDigit_clicked()
{
    QToolButton *tb = static_cast<QToolButton*>(sender());
    QString str = quickSellSP->text() + tb->text();
    quickSellSP->setValue( str.toInt() );
}

void TixrStockWidget::on_tbSup_clicked()
{
    QString str = QString::number(quickSellSP->value());
    str.chop(1);
    quickSellSP->setValue(str.toInt());
}

void TixrStockWidget::requestAddToCar(const QModelIndex &index)
{
    QStandardItem *targetModelItem = tixrCore->stdModel->itemFromIndex(index);

    QString reqStr = QStringLiteral("{\"ztar\":\"%1\",\"ccli\":\"%2\",\"n\":%3,\"plan\":%4}");

    QStandardItem *seanceStdItem = tixrCore->getItemAtLevelFromChild(targetModelItem, 1);
    QVariant v = JsonQStandardItemFactory::getTargetItem(QStringLiteral("cpplano"), seanceStdItem)->data(DataRole::ikey());
    QString strN = quickSellSP->value() ? quickSellSP->text() : "1";
    QString strPlan = v.isValid() ? "true" : "false";
    QString idZone = tixrCore->stdModel->data(index.parent().parent(), DataRole::ikey()).toString();
    reqStr = reqStr.arg(idZone, targetModelItem->text(), strN, strPlan);

    tixrCore->setPgFunction("main.add1ToCar");
    currentTaskList << tixrCore->sendMsg(targetModelItem, HTTP_METHOD_ID::POST, "/api", cartWidget, "responseAddToCar", reqStr.toUtf8());

    quickSellSP->setValue(1);
}

void TixrStockWidget::changeStockProdView()
{
    QAbstractButton *btn = static_cast<QAbstractButton*>(sender());

    int viewLevel = btn->property("tixrStockViewLevel").toInt();
    QStackedWidget *stackedWidget = viewLevel ? stackedWidgetLevel2 : stackedWidgetLevel1;
    QAbstractItemView *iView = static_cast<QAbstractItemView*>(stackedWidget->currentWidget()->children().at(1));

    QList<QAbstractButton*> levelBtns = viewLevel ? level2Btns : level1Btns;

    QModelIndex root, curIdx;
    root = iView->rootIndex();
    curIdx = iView->currentIndex();

    for(int i=0, n=levelBtns.size(); i<n; i++)
    {
        QAbstractButton *ab = levelBtns.at(i);
        ab->setChecked(ab == btn);
    }

    int viewType = btn->property("tixrStockViewType").toInt();
    stackedWidget->setCurrentIndex(viewType);
    iView = static_cast<QAbstractItemView*>(stackedWidget->currentWidget()->children().at(1));

    tweaking(tixrCore->stdModel->itemFromIndex(root), iView);

    if(!iView->model())
        iView->setModel(tixrCore->stdModel);
    iView->setRootIndex(root);
    iView->setCurrentIndex(curIdx);

    setHeaders(iView);
}

void TixrStockWidget::setHeaders(QAbstractItemView *itemView)
{
    if(itemView->inherits("QTableView"))
    {
        int level = currentViewLevel(tixrCore->stdModel->itemFromIndex(itemView->rootIndex()));//detectar el nivel en la gerarquía

        QStandardItemModel *hModel = tixrCore->levelToHeaderModelMap.contains(level) ? tixrCore->levelToHeaderModelMap[level] : 0;

        Q_ASSERT(hModel);

        static_cast<QTableView*>(itemView)->horizontalHeader()->setModel( hModel );
    }
}

void TixrStockWidget::itemViewClicked(const QModelIndex &index)
{
    QAbstractItemView *iView = static_cast<QAbstractItemView*>(sender());

    int viewLevel = iView->property("tixrStockViewLevel").toInt();
    if(!viewLevel)
    {
        QAbstractItemView *iView2 = static_cast<QAbstractItemView*>(stackedWidgetLevel2->currentWidget()->children().at(1));
        if(iView2->rootIndex().parent()==index)
            return;
    }

    level2Clicked = viewLevel;

    int level = currentViewLevel(tixrCore->stdModel->itemFromIndex(iView->rootIndex()));//detectar el nivel en la gerarquía

    switch(level)
    {
        case 0: requestShowCatProds(index);
                break;

        case 1: requestShowZonesAvecPrix(index);
                break;

        case 2: requestShowCategCli(index);
                break;

        case 3: requestAddToCar(index);
    }
}

void TixrStockWidget::requestShowZonesAvecPrix(const QModelIndex &index)
{
    QStandardItem *targetModelItem = tixrCore->stdModel->itemFromIndex(index);

    int r = targetModelItem->rowCount();
    if(r)
    {
        showResp(targetModelItem->child(0));
    }
    else
    {
        QStandardItem *modelSubItem = new QStandardItem(1,1);
        modelSubItem->setData("ItemId", DataRole::jsonProperty());
        modelSubItem->setData(JsonObject, DataRole::jsonType()); //elemento

        targetModelItem->appendRow(modelSubItem);

        QString idCatProdStr = tixrCore->stdModel->data(index.sibling(index.row(),DataColumn::cptitre()), DataRole::ikey()).toString();
        QStandardItem *modelSubItem2 = new QStandardItem(idCatProdStr);
        modelSubItem2->setData("srvikey", DataRole::jsonProperty());
        modelSubItem2->setData(JsonSimple, DataRole::jsonType());
        modelSubItem->setChild(0, 0, modelSubItem2);

        tixrCore->setPgFunction("main.getallzonetaravecprixincat");
        currentTaskList << tixrCore->sendMsg(modelSubItem, HTTP_METHOD_ID::POST, "/api", this, "showResponse");
    }
}

void TixrStockWidget::requestShowCategCli(const QModelIndex &index)
{
    QStandardItem *targetModelItem = tixrCore->stdModel->itemFromIndex(index);

    int r = targetModelItem->rowCount();
    if(r)
    {
        showResp(targetModelItem->child(0));
    }
    else
    {
        QStandardItem *modelSubItem = new QStandardItem(1,1);
        modelSubItem->setData("ItemId", DataRole::jsonProperty());
        modelSubItem->setData(JsonObject, DataRole::jsonType()); //elemento

        targetModelItem->appendRow(modelSubItem);

        QString idZTarStr = tixrCore->stdModel->data(index, DataRole::zapidgricol()).toString();
        QStandardItem *modelSubItem2 = new QStandardItem(idZTarStr);
        modelSubItem2->setData("srvikey", DataRole::jsonProperty());
        modelSubItem2->setData(JsonSimple, DataRole::jsonType());
        modelSubItem->setChild(0, 0, modelSubItem2);

        tixrCore->setPgFunction("main.getallcatcliandprixinztar");
        currentTaskList << tixrCore->sendMsg(modelSubItem, HTTP_METHOD_ID::POST, "/api", this, "showResponse");
    }
}

void TixrStockWidget::on_tbUP_clicked()
{
    salesWidget->setVisible(false);

    QAbstractItemView *iViewL1 = static_cast<QAbstractItemView*>(stackedWidgetLevel1->currentWidget()->children().at(1));
    QAbstractItemView *iViewL2 = static_cast<QAbstractItemView*>(stackedWidgetLevel2->currentWidget()->children().at(1));

    QModelIndex root = iViewL1->rootIndex();

    tweaking(tixrCore->stdModel->itemFromIndex(root), iViewL2);

    iViewL2->setRootIndex(root);

    QModelIndex curIdx = root.parent();
    root = curIdx.parent();

    tweaking(tixrCore->stdModel->itemFromIndex(root), iViewL1);

    iViewL1->setRootIndex(root);
    iViewL1->setCurrentIndex(curIdx);

    upDateInterface(iViewL1, iViewL2);

    QString cn = iViewL1->metaObject()->className();
    if(cn=="QTableView")
    {
        QHeaderView *hView = static_cast<QTableView*>(iViewL1)->horizontalHeader();
        //hView->setModel(hModel);
    }

    cn = iViewL2->metaObject()->className();
    if(cn=="QTableView")
    {
        QHeaderView *hView = static_cast<QTableView*>(iViewL2)->horizontalHeader();
        //hView->setModel(hModel);
    }
}

void TixrStockWidget::showPlanSalle()
{
    //Me baso en el nivel 2 para no depender de la selección actual en la lista de elementos (el elemento podría no estar más seleccionado, aunque sigue siendo el elemento actual)
    QAbstractItemView *iViewL2 = static_cast<QAbstractItemView*>(stackedWidgetLevel2->currentWidget()->children().at(1));

    QModelIndex index = iViewL2->rootIndex();
    int level = currentViewLevel(tixrCore->stdModel->itemFromIndex(index));

    while(level>2)
    {
        index = index.parent().parent();
        --level;
    }

    index = index.parent();//item seance

    QStandardItem *seanceItem = tixrCore->stdModel->itemFromIndex(index);
    QModelIndex titleIndex = index.sibling(index.row(),DataColumn::cptitre());
    QString seanceLabel = titleIndex.data(Qt::DisplayRole).toString();
    QVariant v = seanceItem->data(DataRole::site2dviewptr());
    if(v.isValid())//si los datos ya han sido cargados con anterioridad
    {
        Site2dView *site2dView = v.value<Site2dView*>();
        int idx = parentTabWidget->indexOf(site2dView);
        if(idx == -1)//detectar si ya se está visualizando
        {
            idx = parentTabWidget->addTab(site2dView, site2dView->windowIcon(), seanceLabel);
        }

        parentTabWidget->setCurrentIndex(idx);
    }
    else
    {
        QStandardItem *modelSubItem = new QStandardItem(1,1);
        modelSubItem->setData("ItemId", DataRole::jsonProperty());
        modelSubItem->setData(JsonObject, DataRole::jsonType());

        QVariant v2 = titleIndex.data(DataRole::ikey());
        Q_ASSERT(v2.isValid());
        QString idCatProdStr = v2.toString();
        QStandardItem *modelSubItem2 = new QStandardItem(idCatProdStr);
        modelSubItem2->setData("srvikey", DataRole::jsonProperty());
        modelSubItem2->setData(JsonSimple, DataRole::jsonType());
        modelSubItem->setChild(0, 0, modelSubItem2);

        seanceItem->appendRow(modelSubItem);

        Site2dView *site2dView = new Site2dView((QProgressBar*)0, false, false, tixrCore, parentTabWidget);
        site2dView->setObjectName("Site2dView");
        QImage img = seanceItem->data(Qt::DecorationRole).value<QImage>();
        site2dView->setWindowIcon(QIcon(QPixmap::fromImage(img)));
        parentTabWidget->addTab(site2dView, seanceLabel);

        site2dView->catProdItem = tixrCore->stdModel->itemFromIndex(index);

        seanceItem->setData(QVariant::fromValue(site2dView), DataRole::site2dviewptr());

        connect(site2dView, SIGNAL(addToCart(QList<QStandardItem*>)), cartWidget, SLOT(addToCarFromPlan(QList<QStandardItem*>)));

        tixrCore->setPgFunction("main.fetchseance");
        tixrCore->setInHandlerIndex(2);
        site2dView->currentTaskList << tixrCore->sendMsg(modelSubItem, HTTP_METHOD_ID::POST, "/api", this, "showStockProds");
    }
}

void TixrStockWidget::showStockProds(QString respError, PendingRequest *curReq)
{
    QStandardItem *requestItem = curReq->getRequestItem();
    Site2dView *site2dView = requestItem->parent()->data(DataRole::site2dviewptr()).value<Site2dView*>();

    if(!beginHandleResponse(respError, curReq))
    {
        requestItem->parent()->setData(QVariant(), DataRole::site2dviewptr());
        delete site2dView;
        site2dView = 0;
        return;
    }

    if(curReq)
    {
        site2dView->currentTaskList.removeOne(curReq);
    }

    //int wIndex = parentTabWidget->addTab(site2dView, "seanceLabel");

    int wIndex = parentTabWidget->indexOf(site2dView);
    if(wIndex > -1)
    {
        parentTabWidget->setTabIcon(wIndex, site2dView->windowIcon());
        parentTabWidget->setCurrentIndex(wIndex);
        parentTabWidget->update();
    }
}

void TixrStockWidget::tweaking(QStandardItem *r, QAbstractItemView *v)
{
    if(!(r && v))
        return;

    QString cn = v->metaObject()->className();
    int vIdx = cn=="QListView" ? 0 : (cn=="QTableView" ? 1 : 2);

    QList<int> rolesToResetList;
    rolesToResetList << Qt::DisplayRole << Qt::DecorationRole;

    for(int i=0, n=r->rowCount(); i<n; i++)
    {
        QStandardItem *c = r->child(i);

        QString tPatt = c->data(DataRole::tweak()).toString();

        if(!tweakPatternRX.exactMatch(tPatt))
            continue;

        if(vIdx<0 || vIdx>=tweakPatternRX.captureCount())
            continue;

        //reset roles
        for(int j=0, m=rolesToResetList.size(); j<m; j++)
        {
            for(int k=0, N=r->columnCount(); k<N; k++)
            {
                c = r->child(i, k);
                if(c)
                    c->setData(QVariant(), rolesToResetList.at(j));
            }
        }

        tPatt = tweakPatternRX.cap(vIdx + 1);
        QStringList tweakPairList = tPatt.split(",", QString::SkipEmptyParts);

        for(int j=0, m=tweakPairList.size(); j<m; j++)
        {
            QStringList srcDstPair = tweakPairList.at(j).split("-", QString::SkipEmptyParts);

            QStringList srcIdxList = srcDstPair.at(0).split("_", QString::SkipEmptyParts);
            QStringList dstIdxList = srcDstPair.at(1).split("_", QString::SkipEmptyParts);

            bool ok;
            int srcCol = srcIdxList.at(1).toInt(&ok);
            if(!ok)
                srcCol = DataColumn::FromName(srcIdxList.at(1));
            Q_ASSERT(srcCol>=0);
            int srcRole = DataRole::FromName(srcIdxList.at(0));
            Q_ASSERT(srcRole>=0);
            int dstCol = dstIdxList.at(1).toInt(&ok);
            if(!ok)
                dstCol = DataColumn::FromName(dstIdxList.at(1));
            Q_ASSERT(dstCol>=0);
            int dstRole = DataRole::FromName(dstIdxList.at(0));
            Q_ASSERT(dstRole>=0);

            QStandardItem *srcItem = r->child(i, srcCol);
            QStandardItem *dstItem = r->child(i, dstCol);

            if(!(srcItem && dstItem))
                continue;

            dstItem->setData(srcItem->data(srcRole), dstRole);
        }
    }
}
