//-----------------------------------------------------------------------------
#include "transportwidget.h"
#include "../mainwindow.h"
//-----------------------------------------------------------------------------
#define TWCOMBOHEIGHT 21
#define TWCOMBOWIDTH 200
enum TransportDataIndex{tdiIDTRAJECT = 33, tdiFAMCATID, tdiTRAJECTLIST, tdiIDSHEAP, tdiCAPACITY, tdiTWEAKSUB};
enum CalendDataIndex{cdiDATEINI = 34, cdiDATEEND, cdiDAY1, cdiDAY2, cdiDAY3, cdiDAY4, cdiDAY5, cdiDAY6, cdiDAY7, cdiIDTABLE, cdiIDCOL, cdiTWEAKCAT, cdiTWEAKSUB};
//-----------------------------------------------------------------------------
TixrTransportWidget::TixrTransportWidget(TixRealmCore * mObj, int famcatid, QWidget * parent) :
    TixrWidget("transport", mObj, "", parent)
{
    this->tixrWidgetType = TIXR_TRANSPORT;
    if(!this->getErrorMsg().isEmpty())
        return;

    this->parentTabWidget->addTab(this, windowIcon(), tr("Transport"));
    this->parentTabWidget->setCurrentWidget(this);

    this->swMain              = this->findChild<QStackedWidget *>("swMain");
    this->twInnerPorts        = this->findChild<QTableWidget *>("twInnerPorts");
    this->twCalendrier        = this->findChild<QTableWidget *>("twCalendrier");
    this->pbNewStation        = this->findChild<QToolButton *>("pbNewStation");
    this->pbValiderTraject    = this->findChild<QToolButton *>("pbValiderTraject");
    this->pbPortValider       = this->findChild<QToolButton *>("pbPortValider");
    this->pbPortEnlever       = this->findChild<QToolButton *>("pbPortEnlever");
    this->pbPortAjouter       = this->findChild<QToolButton *>("pbPortAjouter");
    this->pbDateValider       = this->findChild<QToolButton *>("pbDateValider");
    this->pbDateEnlever       = this->findChild<QToolButton *>("pbDateEnlever");
    this->pbDateAjouter       = this->findChild<QToolButton *>("pbDatetAjouter");
    this->pbNewCalend         = this->findChild<QToolButton *>("pbNewCalend");
    this->pbValiderCalend     = this->findChild<QToolButton *>("pbValiderCalend");
    this->lePortName          = this->findChild<QLineEdit *>("lePortName");
    this->deDateIni           = this->findChild<QDateEdit *>("deDateIni");
    this->deDateEnd           = this->findChild<QDateEdit *>("deDateEnd");
    this->tvPorts             = this->findChild<QTableView *>("tvPorts");
    this->tvDates             = this->findChild<QTableView *>("tvDates");

    this->seanceGriList       = this->findChild<QListView *>("seanceGriList");
    this->seanceGriTable      = this->findChild<QTableView *>("seanceGriTable");
    this->seanceGriNameLE     = this->findChild<QLineEdit *>("seanceGriNameLE");

    this->currRow = this->currCol = 0;
    this->famcatid = famcatid;

    this->twCalendrier->setColumnCount(14);
    this->twCalendrier->setHorizontalHeaderLabels(QStringList() << "LUNDI" << "" << "MARDI" << "" << "MERCREDI" << "" << "JEUDI" << "" << "VENDREDI" << "" << "SAMEDI" << "" << "DIMANCHE" << "");

    if(!this->tixrCore->keyToItemMap.contains("Ports"))
    {
        QStandardItem *modelSubItem = new QStandardItem(1,1);
        modelSubItem->setData("ItemId", DataRole::jsonProperty());
        modelSubItem->setData(JsonObject, DataRole::jsonType());

        QStandardItem *modelSubItem2 = new QStandardItem(QString::number(this->famcatid));
        modelSubItem2->setData("srvikey", DataRole::jsonProperty());
        modelSubItem2->setData(JsonSimple, DataRole::jsonType());
        modelSubItem->setChild(0, 0, modelSubItem2);

        this->tixrCore->stdModel->invisibleRootItem()->appendRow(modelSubItem);

        this->tixrCore->setPgFunction("main.getports");
        this->currentTaskList << this->tixrCore->sendMsg(modelSubItem, HTTP_METHOD_ID::POST, "/api", this, "serverResponse");
    }
    else
    {
        this->tvPorts->setModel(this->tixrCore->stdModel);
        this->tvPorts->setRootIndex(this->tixrCore->keyToItemMap["Ports"]->index());
    }

    // int tabIndex = this->parentTabWidget->addTab(this, tr("Nouvelle séance"));

    if(this->tixrCore->keyToItemMap.contains("grilles"))//detectar si los datos ya han sido cargados con anterioridad
    {
        this->seanceGriList->setModel(this->tixrCore->stdModel);
        this->seanceGriList->setRootIndex(this->tixrCore->keyToItemMap.value("grilles")->index());

        // this->parentTabWidget->setTabIcon(tabIndex, windowIcon());
        // this->parentTabWidget->setCurrentIndex(tabIndex);
    }
    else
    {
        this->tixrCore->setPgFunction("main.getallgrilles");
        this->currentTaskList << this->tixrCore->sendMsg(this->tixrCore->stdModel->invisibleRootItem(), HTTP_METHOD_ID::GET, "/api", this, "serverResponse");
    }

    this->transportModel = new QStandardItemModel(this);
    // this->datesModel = new QStandardItemModel(this);

    MainWindow::tixrConnectSlotsByName(this);
}
//-----------------------------------------------------------------------------
void TixrTransportWidget::on_pbNewTraject_clicked()
{   
    this->currRow = this->twInnerPorts->rowCount();
    this->currCol = 0;
    this->twInnerPorts->setRowCount(this->currRow + 1);
    if(!this->twInnerPorts->columnCount())
        this->twInnerPorts->setColumnCount(this->currCol + 2);

    QTableWidgetItem * twItem = new QTableWidgetItem();
    QLineEdit * le = new QLineEdit(this->twInnerPorts);
    le->setObjectName(QString::number(reinterpret_cast<tixrqptr>(twItem)));
    le->setMinimumHeight(TWCOMBOHEIGHT);
    le->setMaximumHeight(TWCOMBOHEIGHT);
    le->setMinimumWidth(TWCOMBOWIDTH);
    le->setMaximumWidth(TWCOMBOWIDTH);

    this->twInnerPorts->setRowHeight(this->currRow, le->height());
    this->twInnerPorts->setColumnWidth(this->currCol, le->width());
    this->twInnerPorts->setItem(this->currRow, this->currCol, twItem);
    this->twInnerPorts->setCellWidget(this->currRow, this->currCol++, le);

    QSpinBox * spBox = new QSpinBox(this->twInnerPorts);
    spBox->setObjectName(QString::number(reinterpret_cast<tixrqptr>(twItem = new QTableWidgetItem())));
    spBox->setMaximum(INT_MAX);
    this->twInnerPorts->setColumnWidth(this->currCol, 60);
    this->twInnerPorts->setItem(this->currRow, this->currCol, twItem);
    this->twInnerPorts->setCellWidget(this->currRow, this->currCol++, spBox);

    for(int i = 0; i < 2; i++)
    {
        QComboBox * cb = new QComboBox(this->twInnerPorts);
        cb->setObjectName(QString::number(reinterpret_cast<tixrqptr>(twItem = new QTableWidgetItem())));
        cb->setMinimumHeight(TWCOMBOHEIGHT);
        cb->setMaximumHeight(TWCOMBOHEIGHT);
        cb->setMinimumWidth(TWCOMBOWIDTH);
        cb->setMaximumWidth(TWCOMBOWIDTH);
        cb->setModel(this->tixrCore->stdModel);
        cb->setRootModelIndex(this->tixrCore->keyToItemMap["Ports"]->index());

        if(this->currCol == this->twInnerPorts->columnCount())
            this->twInnerPorts->setColumnCount(this->currCol + 2);

        this->twInnerPorts->setRowHeight(this->currRow, cb->height());
        this->twInnerPorts->setColumnWidth(this->currCol, cb->width());
        this->twInnerPorts->setItem(this->currRow, this->currCol, twItem);
        this->twInnerPorts->setCellWidget(this->currRow, this->currCol++, cb);

        QCheckBox * chkBox = new QCheckBox(this->twInnerPorts);
        chkBox->setObjectName(QString::number(reinterpret_cast<tixrqptr>(twItem = new QTableWidgetItem())));
        chkBox->setCheckState(Qt::Checked);
        this->twInnerPorts->setColumnWidth(this->currCol, 16);
        this->twInnerPorts->setItem(this->currRow, this->currCol, twItem);
        this->twInnerPorts->setCellWidget(this->currRow, this->currCol++, chkBox);

        QObject::connect(cb, SIGNAL(currentIndexChanged(int)), this, SLOT(on_twInnerPorts_currentIndexChanged(int)), Qt::UniqueConnection);
        QObject::connect(cb, SIGNAL(highlighted(int)), this, SLOT(on_twInnerPorts_currentIndexChanged(int)), Qt::UniqueConnection);
    }

    this->twInnerPorts->selectRow(this->currRow);
    this->pbNewStation->setEnabled(this->twInnerPorts->rowCount());
}
//-----------------------------------------------------------------------------
void TixrTransportWidget::on_pbNewStation_clicked()
{
    // if(this->currCol / 2 < this->tixrCore->keyToItemMap["Ports"]->rowCount())
    // {
        QTableWidgetItem * twItem = new QTableWidgetItem();
        QComboBox * cb = new QComboBox(this->twInnerPorts);
        cb->setObjectName(QString::number(reinterpret_cast<tixrqptr>(twItem)));
        cb->setMinimumHeight(TWCOMBOHEIGHT);
        cb->setMaximumHeight(TWCOMBOHEIGHT);
        cb->setMinimumWidth(TWCOMBOWIDTH);
        cb->setMaximumWidth(TWCOMBOWIDTH);
        cb->setModel(this->tixrCore->stdModel);
        cb->setRootModelIndex(this->tixrCore->keyToItemMap["Ports"]->index());

        if(this->currCol == this->twInnerPorts->columnCount())
            this->twInnerPorts->setColumnCount(this->currCol + 2);

        for(this->currCol = 2; this->currCol < this->twInnerPorts->columnCount() && this->twInnerPorts->cellWidget(this->twInnerPorts->currentRow(), this->currCol); this->currCol++);

        this->twInnerPorts->setRowHeight(this->twInnerPorts->currentRow(), cb->height());
        this->twInnerPorts->setColumnWidth(this->currCol, cb->width());
        this->twInnerPorts->setItem(this->twInnerPorts->currentRow(), this->currCol, twItem);
        this->twInnerPorts->setCellWidget(this->twInnerPorts->currentRow(), this->currCol++, cb);

        QCheckBox * chkBox = new QCheckBox(this->twInnerPorts);
        chkBox->setObjectName(QString::number(reinterpret_cast<tixrqptr>(twItem = new QTableWidgetItem())));
        chkBox->setCheckState(Qt::Checked);
        this->twInnerPorts->setColumnWidth(this->currCol, 16);
        this->twInnerPorts->setItem(this->twInnerPorts->currentRow(), this->currCol, twItem);
        this->twInnerPorts->setCellWidget(this->twInnerPorts->currentRow(), this->currCol++, chkBox);

        QObject::connect(cb, SIGNAL(currentIndexChanged(int)), this, SLOT(on_twInnerPorts_currentIndexChanged(int)), Qt::UniqueConnection);
        QObject::connect(cb, SIGNAL(highlighted(int)), this, SLOT(on_twInnerPorts_currentIndexChanged(int)), Qt::UniqueConnection);
    // }
        this->pbValiderTraject->setEnabled(false);
}
//-----------------------------------------------------------------------------
void TixrTransportWidget::on_twInnerPorts_currentIndexChanged(int index)
{
    QTableWidgetItem * twItem = reinterpret_cast<QTableWidgetItem *>(QObject::sender()->objectName().toInt());
    this->twInnerPorts->selectRow(twItem->row());
    if(QString(QObject::sender()->metaObject()->className()) == QString("QComboBox"))
    {
        QComboBox * cb = static_cast<QComboBox *>(QObject::sender());
        bool flag = true;
        for(int i = 2, count = this->twInnerPorts->columnCount(); i < count; i += 2)
        {
            QComboBox * twCb = static_cast<QComboBox *>(this->twInnerPorts->cellWidget(twItem->row(), i));
            // if(twCb != cb && twCb->currentIndex() == cb->currentIndex())
                // cb->setCurrentIndex(-1);

            flag = flag && !twCb->currentText().isEmpty();
        }
        this->pbValiderTraject->setEnabled(flag);
    }
}
//-----------------------------------------------------------------------------
void TixrTransportWidget::on_twInnerPorts_itemSelectionChanged()
{
    int row = this->twInnerPorts->currentRow();
    QStandardItem * trajectItem = this->transportModel->item(row);
    this->tvDates->setModel(this->transportModel);
    if(trajectItem && trajectItem->hasChildren())
    {
        this->tvDates->setRootIndex(trajectItem->index());
        this->tvDates->setCurrentIndex(trajectItem->child(0)->index());
        this->on_tvDates_clicked(trajectItem->child(0)->index());
    }
    else
    {
        this->tvDates->setModel(NULL);
        this->twCalendrier->setRowCount(0);
    }
}
//-----------------------------------------------------------------------------
void TixrTransportWidget::on_pbValiderTraject_clicked()
{
    QString sheapName = static_cast<QLineEdit *>(this->twInnerPorts->cellWidget(this->twInnerPorts->currentRow(), 0))->text();

    if(sheapName.isEmpty())
    {
        QMessageBox::information(this, qApp->applicationName(), QObject::trUtf8("Enter name of Sheap"));
        return;
    }

    int row = this->twInnerPorts->currentRow();
    QStringList voyageList;
    for(int i = 2, count = this->twInnerPorts->columnCount(); i < count; i += 2)
        if(static_cast<QCheckBox *>(this->twInnerPorts->cellWidget(row, i + 1))->checkState() == Qt::Checked)
            voyageList << this->tixrCore->keyToItemMap["Ports"]->child(static_cast<QComboBox *>(this->twInnerPorts->cellWidget(row, i))->currentIndex())->data(DataRole::ikey()).toString();

    if(!voyageList.isEmpty())
    {
        QStandardItem * trajectItem = this->transportModel->item(row);
        if(!trajectItem)
        {
            QStandardItem *modelSubItem = new QStandardItem(1,1);
            modelSubItem->setData("InsVoyageParams", DataRole::jsonProperty());
            modelSubItem->setData(JsonObject, DataRole::jsonType());

            this->transportModel->appendRow(modelSubItem);

            QStandardItem *modelSubItem2 = new QStandardItem(QString::number(this->famcatid));
            modelSubItem2->setData("p_famcatid", DataRole::jsonProperty());
            modelSubItem2->setData(JsonSimple, DataRole::jsonType());
            modelSubItem->setChild(0, 0, modelSubItem2);

            modelSubItem2 = new QStandardItem(voyageList.join(QString(",")));
            modelSubItem2->setData("p_voyage", DataRole::jsonProperty());
            modelSubItem2->setData(JsonSimple, DataRole::jsonType());
            modelSubItem->setChild(0, 1, modelSubItem2);

            modelSubItem2 = new QStandardItem(sheapName);
            modelSubItem2->setData("p_sheapname", DataRole::jsonProperty());
            modelSubItem2->setData(JsonSimple, DataRole::jsonType());
            modelSubItem->setChild(0, 2, modelSubItem2);

            modelSubItem2 = new QStandardItem(QString::number(static_cast<QSpinBox *>(this->twInnerPorts->cellWidget(row, 1))->value()));
            modelSubItem2->setData("p_capacity", DataRole::jsonProperty());
            modelSubItem2->setData(JsonSimple, DataRole::jsonType());
            modelSubItem->setChild(0, 3, modelSubItem2);

            modelSubItem2 = new QStandardItem("pdescrip_0-qtdisplayrole_0,img_0-qtdecorole_0|pdescrip_qtdisplayrole-0_0|pdescrip_0-qtdisplayrole_0,img_0-qtdecorole_0");
            modelSubItem2->setData("p_tweaksub", DataRole::jsonProperty());
            modelSubItem2->setData(JsonSimple, DataRole::jsonType());
            modelSubItem->setChild(0, 4, modelSubItem2);

            this->tixrCore->setPgFunction("main.insvoyage");
            this->currentTaskList << this->tixrCore->sendMsg(modelSubItem, HTTP_METHOD_ID::POST, "/api", this, "serverResponse");
        }
        else
        {
            QStandardItem *modelSubItem = new QStandardItem(1,1);
            modelSubItem->setData("UpdVoyageParams", DataRole::jsonProperty());
            modelSubItem->setData(JsonObject, DataRole::jsonType());

            trajectItem->parent()->setChild(trajectItem->row(), 0, modelSubItem);

            QStandardItem *modelSubItem2 = new QStandardItem(QString::number(this->famcatid));
            modelSubItem2->setData("p_famcatid", DataRole::jsonProperty());
            modelSubItem2->setData(JsonSimple, DataRole::jsonType());
            modelSubItem->setChild(0, 0, modelSubItem2);

            modelSubItem2 = new QStandardItem(voyageList.join(QString(",")));
            modelSubItem2->setData("p_voyage", DataRole::jsonProperty());
            modelSubItem2->setData(JsonSimple, DataRole::jsonType());
            modelSubItem->setChild(0, 1, modelSubItem2);

            modelSubItem2 = new QStandardItem(sheapName);
            modelSubItem2->setData("p_sheapname", DataRole::jsonProperty());
            modelSubItem2->setData(JsonSimple, DataRole::jsonType());
            modelSubItem->setChild(0, 2, modelSubItem2);

            modelSubItem2 = new QStandardItem(QString::number(static_cast<QSpinBox *>(this->twInnerPorts->cellWidget(row, 1))->value()));
            modelSubItem2->setData("p_capacity", DataRole::jsonProperty());
            modelSubItem2->setData(JsonSimple, DataRole::jsonType());
            modelSubItem->setChild(0, 3, modelSubItem2);

            modelSubItem2 = new QStandardItem("pdescrip_0-qtdisplayrole_0,img_0-qtdecorole_0|pdescrip_qtdisplayrole-0_0|pdescrip_0-qtdisplayrole_0,img_0-qtdecorole_0");
            modelSubItem2->setData("p_tweaksub", DataRole::jsonProperty());
            modelSubItem2->setData(JsonSimple, DataRole::jsonType());
            modelSubItem->setChild(0, 4, modelSubItem2);

            modelSubItem2 = new QStandardItem(trajectItem->data(tdiIDTRAJECT).toString());
            modelSubItem2->setData("p_idvoyage", DataRole::jsonProperty());
            modelSubItem2->setData(JsonSimple, DataRole::jsonType());
            modelSubItem->setChild(0, 4, modelSubItem2);

            this->tixrCore->setPgFunction("main.updvoyage");
            this->currentTaskList << this->tixrCore->sendMsg(modelSubItem, HTTP_METHOD_ID::POST, "/api", this, "serverResponse");
        }

    }
}
//-----------------------------------------------------------------------------
void TixrTransportWidget::on_lePortName_textChanged(const QString & text)
{
    this->pbPortValider->setEnabled(!text.isEmpty());
}
//-----------------------------------------------------------------------------
void TixrTransportWidget::on_pbPortValider_clicked()
{
    QList<QPair<QString, QString> > paramList = QLISTITEMQPAIRSTRSTR << QPAIRITEMSTRSTR("p_catid", "-1")
                                                                     << QPAIRITEMSTRSTR("p_attnames", "PORT")
                                                                     << QPAIRITEMSTRSTR("p_attvalues", this->lePortName->text())
                                                                     << QPAIRITEMSTRSTR("p_atttypes", "")
                                                                     << QPAIRITEMSTRSTR("p_famcatid", QString::number(-1))
                                                                     << QPAIRITEMSTRSTR("p_tweaksub", "36_0-0_0|36_0-0_0|36_0-0_0");

    if(this->tvPorts->currentIndex().isValid())
    {
        QStandardItem *modelSubItem = new QStandardItem(1, 1);
        modelSubItem->setData("UpdPortParams", DataRole::jsonProperty());
        modelSubItem->setData(JsonObject, DataRole::jsonType());

        modelSubItem->setData(QString::number(this->tvPorts->currentIndex().row()), DataRole::ikey());

        this->tixrCore->stdModel->invisibleRootItem()->appendRow(modelSubItem);

        QStandardItem *modelSubItem2 = new QStandardItem("-1");
        modelSubItem2->setData("p_catid", DataRole::jsonProperty());
        modelSubItem2->setData(JsonSimple, DataRole::jsonType());
        modelSubItem->setChild(0, 0, modelSubItem2);

        modelSubItem2 = new QStandardItem("PORT");
        modelSubItem2->setData("p_attnames", DataRole::jsonProperty());
        modelSubItem2->setData(JsonSimple, DataRole::jsonType());
        modelSubItem->setChild(0, 1, modelSubItem2);

        modelSubItem2 = new QStandardItem(this->lePortName->text());
        modelSubItem2->setData("p_attvalues", DataRole::jsonProperty());
        modelSubItem2->setData(JsonSimple, DataRole::jsonType());
        modelSubItem->setChild(0, 2, modelSubItem2);

        modelSubItem2 = new QStandardItem("");
        modelSubItem2->setData("p_atttypes", DataRole::jsonProperty());
        modelSubItem2->setData(JsonSimple, DataRole::jsonType());
        modelSubItem->setChild(0, 3, modelSubItem2);

        modelSubItem2 = new QStandardItem(QString::number(-1));
        modelSubItem2->setData("p_famcatid", DataRole::jsonProperty());
        modelSubItem2->setData(JsonSimple, DataRole::jsonType());
        modelSubItem->setChild(0, 4, modelSubItem2);

        modelSubItem2 = new QStandardItem("pdescrip_0-qtdisplayrole_0,img_0-qtdecorole_0|pdescrip_qtdisplayrole-0_0|pdescrip_0-qtdisplayrole_0,img_0-qtdecorole_0");
        modelSubItem2->setData("p_tweaksub", DataRole::jsonProperty());
        modelSubItem2->setData(JsonSimple, DataRole::jsonType());
        modelSubItem->setChild(0, 5, modelSubItem2);

        modelSubItem2 = new QStandardItem(this->tixrCore->keyToItemMap["Ports"]->child(this->tvPorts->currentIndex().row())->data(DataRole::ikey()).toString());
        modelSubItem2->setData("p_idport", DataRole::jsonProperty());
        modelSubItem2->setData(JsonSimple, DataRole::jsonType());
        modelSubItem->setChild(0, 6, modelSubItem2);

        this->tixrCore->setPgFunction("main.updport");
        this->currentTaskList << this->tixrCore->sendMsg(modelSubItem, HTTP_METHOD_ID::POST, "/api", this, "serverResponse");
    }
    else
    {
        QStandardItem *modelSubItem = new QStandardItem(1, 1);
        modelSubItem->setData("InsPortParams", DataRole::jsonProperty());
        modelSubItem->setData(JsonObject, DataRole::jsonType());

        this->tixrCore->stdModel->invisibleRootItem()->appendRow(modelSubItem);

        QStandardItem *modelSubItem2 = new QStandardItem("-1");
        modelSubItem2->setData("p_catid", DataRole::jsonProperty());
        modelSubItem2->setData(JsonSimple, DataRole::jsonType());
        modelSubItem->setChild(0, 0, modelSubItem2);

        modelSubItem2 = new QStandardItem("PORT");
        modelSubItem2->setData("p_attnames", DataRole::jsonProperty());
        modelSubItem2->setData(JsonSimple, DataRole::jsonType());
        modelSubItem->setChild(0, 1, modelSubItem2);

        modelSubItem2 = new QStandardItem(this->lePortName->text());
        modelSubItem2->setData("p_attvalues", DataRole::jsonProperty());
        modelSubItem2->setData(JsonSimple, DataRole::jsonType());
        modelSubItem->setChild(0, 2, modelSubItem2);

        modelSubItem2 = new QStandardItem("");
        modelSubItem2->setData("p_atttypes", DataRole::jsonProperty());
        modelSubItem2->setData(JsonSimple, DataRole::jsonType());
        modelSubItem->setChild(0, 3, modelSubItem2);

        modelSubItem2 = new QStandardItem(QString::number(-1));
        modelSubItem2->setData("p_famcatid", DataRole::jsonProperty());
        modelSubItem2->setData(JsonSimple, DataRole::jsonType());
        modelSubItem->setChild(0, 4, modelSubItem2);

        modelSubItem2 = new QStandardItem("pdescrip_0-qtdisplayrole_0,img_0-qtdecorole_0|pdescrip_qtdisplayrole-0_0|pdescrip_0-qtdisplayrole_0,img_0-qtdecorole_0");
        modelSubItem2->setData("p_tweaksub", DataRole::jsonProperty());
        modelSubItem2->setData(JsonSimple, DataRole::jsonType());
        modelSubItem->setChild(0, 5, modelSubItem2);

        this->tixrCore->setPgFunction("main.insport");
        this->currentTaskList << this->tixrCore->sendMsg(modelSubItem, HTTP_METHOD_ID::POST, "/api", this, "serverResponse");
    }
    this->lePortName->clear();
}
//-----------------------------------------------------------------------------
void TixrTransportWidget::on_pbPortEnlever_clicked()
{
    QStandardItem *modelSubItem = new QStandardItem(1, 1);
    modelSubItem->setData("ItemId", DataRole::jsonProperty());
    modelSubItem->setData(JsonObject, DataRole::jsonType());

    this->tixrCore->stdModel->invisibleRootItem()->appendRow(modelSubItem);

    QStandardItem *modelSubItem2 = new QStandardItem(this->tixrCore->keyToItemMap["Ports"]->child(this->tvPorts->currentIndex().row())->data(DataRole::ikey()).toString());
    modelSubItem2->setData("srvikey", DataRole::jsonProperty());
    modelSubItem2->setData(JsonSimple, DataRole::jsonType());
    modelSubItem->setChild(0, 0, modelSubItem2);

    this->tixrCore->setPgFunction("main.delport");
    this->currentTaskList << this->tixrCore->sendMsg(modelSubItem, HTTP_METHOD_ID::POST, "/api", this, "serverResponse");
}
//-----------------------------------------------------------------------------
void TixrTransportWidget::on_pbPortAjouter_clicked()
{
    this->tvPorts->setCurrentIndex(QModelIndex());
    this->lePortName->clear();
}
//-----------------------------------------------------------------------------
void TixrTransportWidget::on_pbNewCalend_clicked()
{
    int row = this->twCalendrier->rowCount();
    this->twCalendrier->setRowCount(row + 1);
    for(int i = 0; i < 14; i += 2)
    {
        QTableWidgetItem * twItem = new QTableWidgetItem();
        QTimeEdit * te = new QTimeEdit(this->twCalendrier);
        te->setObjectName(QString::number(reinterpret_cast<tixrqptr>(twItem)));
        te->setTime(QTime::currentTime());
        te->setMinimumHeight(21);
        te->setMaximumHeight(21);
        te->setMinimumWidth(100);
        te->setMaximumWidth(100);

        this->twCalendrier->setRowHeight(row, te->height());
        this->twCalendrier->setColumnWidth(i, te->width());
        this->twCalendrier->setItem(row, i, twItem);
        this->twCalendrier->setCellWidget(row, i, te);

        QCheckBox * chkBox = new QCheckBox(this->twCalendrier);
        chkBox->setObjectName(QString::number(reinterpret_cast<tixrqptr>(twItem = new QTableWidgetItem())));
        chkBox->setCheckState(Qt::Checked);
        this->twCalendrier->setColumnWidth(i + 1, 16);
        this->twCalendrier->setItem(row, i + 1, twItem);
        this->twCalendrier->setCellWidget(row, i + 1, chkBox);
    }

    this->pbValiderCalend->setEnabled(this->twCalendrier->rowCount());
}
//-----------------------------------------------------------------------------
void TixrTransportWidget::on_pbValiderCalend_clicked()
{
    int rowTraject = this->twInnerPorts->currentRow();
    if(rowTraject == -1)
    {
        QMessageBox::information(this, qApp->applicationName(), QObject::tr("Select Voyage"));
        return;
    }

    int row = this->tvDates->currentIndex().row();
    if(row == -1)
    {
        QMessageBox::information(this, qApp->applicationName(), QObject::tr("Select Dates"));
        return;
    }

    int  col = this->seanceGriTable->currentIndex().column();
    if(col == -1)
    {
        QMessageBox::information(this, qApp->applicationName(), QObject::tr("Select Column Price"));
        return;
    }

    QStandardItem * trajectItem = this->transportModel->item(rowTraject);
    QList<QPair<QString, QString> > paramList = QLISTITEMQPAIRSTRSTR << QPAIRITEMSTRSTR("p_famcatid", QString::number(this->famcatid))
                                                                     << QPAIRITEMSTRSTR("p_dateini",  trajectItem->child(row, 0)->text())
                                                                     << QPAIRITEMSTRSTR("p_dateend",  trajectItem->child(row, 1)->text());

    for(int j = 0, colCount = this->twCalendrier->columnCount(); j < colCount; j += 2)
    {
        QStringList calendList;
        for(int i = 0, rowCount = this->twCalendrier->rowCount(); i < rowCount; i++)
            if(static_cast<QCheckBox *>(this->twCalendrier->cellWidget(i, j + 1))->checkState() == Qt::Checked)
                calendList << static_cast<QTimeEdit *>(this->twCalendrier->cellWidget(i, j))->time().toString();
        paramList << QPAIRITEMSTRSTR(QString("p_day") + QString::number(j / 2 + 1), calendList.join(QString(",")).prepend(QString("{")).append(QString("}")));
    }

    if(true)
    {
        QStandardItem *modelSubItem = new QStandardItem(1, 1);
        modelSubItem->setData("InsCalendParams", DataRole::jsonProperty());
        modelSubItem->setData(JsonObject, DataRole::jsonType());

        trajectItem->child(row)->appendRow(modelSubItem);

        QStandardItem *modelSubItem2 = new QStandardItem(trajectItem->data(tdiIDTRAJECT).toString());
        modelSubItem2->setData("p_idvoyage", DataRole::jsonProperty());
        modelSubItem2->setData(JsonSimple, DataRole::jsonType());
        modelSubItem->setChild(0, 0, modelSubItem2);

        modelSubItem2 = new QStandardItem(this->seanceGriList->currentIndex().data(DataRole::ikey()).toString());
        modelSubItem2->setData("p_idtable", DataRole::jsonProperty());
        modelSubItem2->setData(JsonSimple, DataRole::jsonType());
        modelSubItem->setChild(0, 1, modelSubItem2);

        modelSubItem2 = new QStandardItem(this->seanceGriTable->horizontalHeader()->model()->headerData(col, Qt::Horizontal, DataRole::ikey()).toString());
        modelSubItem2->setData("p_idcolumn", DataRole::jsonProperty());
        modelSubItem2->setData(JsonSimple, DataRole::jsonType());
        modelSubItem->setChild(0, 2, modelSubItem2);

        modelSubItem2 = new QStandardItem("cpheure_0-qtdisplayrole_0,img_0-qtdecorole_0|cpheure_0-qtdisplayrole_0|cpheure_0-qtdisplayrole_0,img_0-qtdecorole_0");
        modelSubItem2->setData("p_tweakcat", DataRole::jsonProperty());
        modelSubItem2->setData(JsonSimple, DataRole::jsonType());
        modelSubItem->setChild(0, 3, modelSubItem2);

        modelSubItem2 = new QStandardItem(QString::number(static_cast<QSpinBox *>(this->twInnerPorts->cellWidget(rowTraject, 1))->value()));
        modelSubItem2->setData("p_capacity", DataRole::jsonProperty());
        modelSubItem2->setData(JsonSimple, DataRole::jsonType());
        modelSubItem->setChild(0, 4, modelSubItem2);

        this->tixrCore->setPgFunction("main.inscalend");
        this->currentTaskList << this->tixrCore->sendMsg(modelSubItem, HTTP_METHOD_ID::POST, "/api", this, "serverResponse");
    }
}
//-----------------------------------------------------------------------------
void TixrTransportWidget::on_tvPorts_clicked(const QModelIndex & index)
{
     this->pbPortEnlever->setEnabled(index.isValid());
    this->lePortName->setText(this->tixrCore->keyToItemMap["Ports"]->child(index.row())->data(DataRole::qtdisplayrole()).toString());
}
//-----------------------------------------------------------------------------
void TixrTransportWidget::on_pbDateValider_clicked()
{     
    QDate dateIni = this->deDateIni->date();
    QDate dateEnd = this->deDateEnd->date();
    if(dateIni > dateEnd)
    {
        QMessageBox::information(this, qApp->applicationName(), QObject::tr("Date Init is greater than Date End"));
        return;
    }
    QModelIndex index = this->tvDates->currentIndex();
    int rowTraject = this->twInnerPorts->currentRow();
    QStandardItem * trajectItem = this->transportModel->item(rowTraject);
    if(!trajectItem)
    {
        QMessageBox::information(this, qApp->applicationName(), QObject::tr("Insert selected Traject"));
        return;
    }
    int row = trajectItem->rowCount();
    // if(index.isValid())
        // trajectItem->removeRow(row = index.row());
    trajectItem->setChild(row, 0, new QStandardItem(dateIni.toString(Qt::ISODate))); // "yyyy-MM-dd"
    trajectItem->setChild(row, 1, new QStandardItem(dateEnd.toString(Qt::ISODate)));
    this->tvDates->setModel(this->transportModel);
    this->tvDates->setRootIndex(trajectItem->index());
    this->tvDates->setCurrentIndex(trajectItem->child(row)->index());

    if(!index.isValid())
    {
        this->twCalendrier->setRowCount(0);
    }
}
//-----------------------------------------------------------------------------
void TixrTransportWidget::on_pbDateAjouter_clicked()
{
    this->tvDates->setCurrentIndex(QModelIndex());
}
//-----------------------------------------------------------------------------
void TixrTransportWidget::on_tvDates_clicked(const QModelIndex & index)
{
    this->twCalendrier->setRowCount(0);
    int rowTraject = this->twInnerPorts->currentRow();
    QStandardItem * trajetItem = this->transportModel->item(rowTraject),
                  * datesItem = trajetItem->child(index.row());
    if(datesItem && datesItem->hasChildren())
    {
        QVector<QStringList> day = QVector<QStringList>() << datesItem->child(0)->data(cdiDAY1).toString().replace("{", "").replace("}", "").split(",")
                                                          << datesItem->child(0)->data(cdiDAY2).toString().replace("{", "").replace("}", "").split(",")
                                                          << datesItem->child(0)->data(cdiDAY3).toString().replace("{", "").replace("}", "").split(",")
                                                          << datesItem->child(0)->data(cdiDAY4).toString().replace("{", "").replace("}", "").split(",")
                                                          << datesItem->child(0)->data(cdiDAY5).toString().replace("{", "").replace("}", "").split(",")
                                                          << datesItem->child(0)->data(cdiDAY6).toString().replace("{", "").replace("}", "").split(",")
                                                          << datesItem->child(0)->data(cdiDAY7).toString().replace("{", "").replace("}", "").split(",");

        for(int i = 0; i < 14; i += 2)
        {
            int count = day[i / 2].count();
            if(count > this->twCalendrier->rowCount())
                this->twCalendrier->setRowCount(count);

            for(int j = 0; j < count; j++)
            {
                QTableWidgetItem * twItem = new QTableWidgetItem();
                QTimeEdit * te = new QTimeEdit(this->twCalendrier);
                te->setObjectName(QString::number(reinterpret_cast<tixrqptr>(twItem)));
                te->setTime(QTime::fromString(day[i / 2].at(j), QString("hh:mm:ss")));
                te->setMinimumHeight(21);
                te->setMaximumHeight(21);
                te->setMinimumWidth(100);
                te->setMaximumWidth(100);

                this->twCalendrier->setRowHeight(j, te->height());
                this->twCalendrier->setColumnWidth(i, te->width());
                this->twCalendrier->setItem(j, i, twItem);
                this->twCalendrier->setCellWidget(j, i, te);

                QCheckBox * chkBox = new QCheckBox(this->twCalendrier);
                chkBox->setObjectName(QString::number(reinterpret_cast<tixrqptr>(twItem = new QTableWidgetItem())));
                chkBox->setCheckState(Qt::Checked);
                this->twCalendrier->setColumnWidth(i + 1, 16);
                this->twCalendrier->setItem(j, i + 1, twItem);
                this->twCalendrier->setCellWidget(j, i + 1, chkBox);
            }
        }
    }
    else
        this->twCalendrier->setRowCount(0);
}
//-----------------------------------------------------------------------------
QStandardItem * TixrTransportWidget::fillNewPort(QStandardItem * reqItem, QStandardItem * respItem)
{
    QStandardItem * newModelItem = new QStandardItem();
    newModelItem->setData("Port", DataRole::jsonProperty());

    QStandardItem * Item = reqItem->child(0, 1);
    for(int i = 0, n = Item->rowCount(); i < n; i++)
    {
        QStandardItem * attItem = Item->child(i);
        QString attName = attItem->accessibleText();

        // if(attName == "p_catid") newModelItem->setData(attItem->text(), );
        // else if(attName == "p_attnames") newModelItem->setData(attItem->text(), );
        if(attName == "p_attvalues")
            newModelItem->setData(attItem->text(), DataRole::qtdisplayrole());
        // else if(attName == "p_atttypes") newModelItem->setData(attItem->text(), );
        // else if(attName == "p_famcatid") newModelItem->setData(attItem->text(), );
        else if(attName == "p_idport")
            newModelItem->setData(attItem->text(), DataRole::ikey());
    }

    if(!reqItem->parent())
    {
        QStandardItem *idItm = tixrCore->itemByNameFromModel(respItem, "ItemId");
        Q_ASSERT(idItm);
        newModelItem->setData(idItm->data(DataRole::ikey()), DataRole::ikey());
    }

    return newModelItem;
}
//-----------------------------------------------------------------------------
QStandardItem * TixrTransportWidget::fillNewTraject(QStandardItem * reqItem, QStandardItem * respItem)
{
    QStandardItem * newModelItem = new QStandardItem();
    newModelItem->setData("Traject", DataRole::jsonProperty());

    QStandardItem * Item = reqItem->child(0, 1);
    for(int i = 0, n = Item->rowCount(); i < n; i++)
    {
        QStandardItem * attItem = Item->child(i);
        QString attName = attItem->accessibleText();

        if(attName == "p_famcatid")
            newModelItem->setData(attItem->text(), tdiFAMCATID);
        else if(attName == "p_voyage")
            newModelItem->setData(attItem->text(), tdiTRAJECTLIST);
        else if(attName == "p_idsheap")
            newModelItem->setData(attItem->text(), tdiIDSHEAP);
        else if(attName == "p_capacity")
            newModelItem->setData(attItem->text(), tdiCAPACITY);
        else if(attName == "p_tweaksub")
            newModelItem->setData(attItem->text(), tdiTWEAKSUB);
        // else if(attName == "p_attnames") newModelItem->setData(attItem->text(), tdi);
    }

    if(reqItem->child(0)->text() == "main.insvoyage")
    {
        int i, n = respItem->rowCount();
        for(i = 0; i < n; i++)
        {
            QStandardItem * elemItem = respItem->child(i);
            if(elemItem->accessibleText() == "ItemId")
            {
                newModelItem->setData(elemItem->data(DataRole::qtdisplayrole()), tdiIDTRAJECT);
                break;
            }
        }
        Q_ASSERT(i < n);
    }

    return newModelItem;
}
//-----------------------------------------------------------------------------
QStandardItem * TixrTransportWidget::fillNewCalend(QStandardItem * reqItem, QStandardItem * respItem)
{
    QStandardItem * newModelItem = new QStandardItem();
    newModelItem->setData("Calend", DataRole::jsonProperty());

    QStandardItem * Item = reqItem->child(0, 1);
    for(int i = 0, n = Item->rowCount(); i < n; i++)
    {
        QStandardItem * attItem = Item->child(i);
        QString attName = attItem->accessibleText();

        if(attName == "p_dateini")
            newModelItem->setData(attItem->text(), cdiDATEINI);
        else if(attName == "p_dateend")
            newModelItem->setData(attItem->text(), cdiDATEEND);
        else if(attName == "p_day1")
            newModelItem->setData(attItem->text(), cdiDAY1);
        else if(attName == "p_day2")
            newModelItem->setData(attItem->text(), cdiDAY2);
        else if(attName == "p_day3")
            newModelItem->setData(attItem->text(), cdiDAY3);
        else if(attName == "p_day4")
            newModelItem->setData(attItem->text(), cdiDAY4);
        else if(attName == "p_day5")
            newModelItem->setData(attItem->text(), cdiDAY5);
        else if(attName == "p_day6")
            newModelItem->setData(attItem->text(), cdiDAY6);
        else if(attName == "p_day7")
            newModelItem->setData(attItem->text(), cdiDAY7);
        else if(attName == "p_idtable")
            newModelItem->setData(attItem->text(), cdiIDTABLE);
        else if(attName == "p_idcolumn")
            newModelItem->setData(attItem->text(), cdiIDCOL);
        else if(attName == "p_tweakcat")
            newModelItem->setData(attItem->text(), cdiTWEAKCAT);
        else if(attName == "p_tweaksub")
            newModelItem->setData(attItem->text(), cdiTWEAKSUB);
    }

    return newModelItem;
}
//-----------------------------------------------------------------------------
void TixrTransportWidget::serverResponse(QString respError, PendingRequest *curReq)
{
    QString dbfnName;
    if(curReq)
    {
        this->currentTaskList.removeOne(curReq);
        if(this->currentTaskList.isEmpty())
            this->parentTabWidget->setTabIcon(this->parentTabWidget->indexOf(this), windowIcon());
        dbfnName = curReq->getPgFunction();
    }

    QStandardItem *requestItem = curReq->getRequestItem();
    QStandardItem *responseItem = curReq->getResponseItem();

    if(!respError.isEmpty())
    {
        QMessageBox::information(this, qApp->applicationName(), tr(respError.toUtf8().constData()));
        return;
    }

    if(dbfnName == "main.getports")
    {
        int row = requestItem->row();
        this->tixrCore->stdModel->setItem(row, responseItem);
        this->tixrCore->keyToItemMap.insert("Ports", responseItem);

        this->tvPorts->setModel(this->tixrCore->stdModel);
        this->tvPorts->setRootIndex(responseItem->index());
    }
    else if(dbfnName == "main.insport")
    {
        this->tixrCore->keyToItemMap["Ports"]->appendRow(this->fillNewPort(requestItem, responseItem));
    }
    else if(dbfnName == "main.updport")
    {
        // int row = requestItem->parent()->text().toInt();
        int row = requestItem->data(DataRole::ikey()).toInt();
        this->tixrCore->keyToItemMap["Ports"]->removeRow(row);
        this->tixrCore->keyToItemMap["Ports"]->insertRow(row, this->fillNewPort(requestItem, responseItem));
        this->tvPorts->setModel(this->tixrCore->stdModel);
        this->tvPorts->setRootIndex(this->tixrCore->keyToItemMap["Ports"]->index());
    }
    else if(dbfnName == "main.delport")
    {
        this->tixrCore->keyToItemMap["Ports"]->removeRow(this->tvPorts->currentIndex().row());
    }
    else if(dbfnName == "main.insvoyage")
    {
        QStandardItem * requestParent = requestItem->parent();
        int row = requestParent->row();
        QStandardItem * trajectItem = this->fillNewTraject(requestItem, responseItem);
        // this->transportModel->removeRow(row);
        this->transportModel->insertRow(row, trajectItem);
    }
    else if(dbfnName == "main.updvoyage")
    {
        int row = requestItem->row();
        QStandardItem * trajectItem = this->fillNewTraject(requestItem, responseItem);
        // this->transportModel->removeRow(row);
        this->transportModel->insertRow(row, trajectItem);
    }
    else if(dbfnName == "main.getallgrilles")
    {
        //incorporar la respuesta al modelo
        /*
        this->tixrCore->stdModel->setItem(requestItem->row(), responseItem);
        this->tixrCore->keyToItemMap.insert("grilles", responseItem);
        */

        int row = requestItem->row();
        this->tixrCore->stdModel->setItem(row, responseItem);
        this->tixrCore->keyToItemMap.insert("grilles", responseItem);

        this->seanceGriList->setModel(this->tixrCore->stdModel);
        this->seanceGriList->setRootIndex(responseItem->index());

        int wIndex = this->parentTabWidget->indexOf(this);
        if(wIndex == -1)
            return;

        this->parentTabWidget->setTabIcon(wIndex, windowIcon());
        this->parentTabWidget->setCurrentIndex(wIndex);
    }
    else if(dbfnName == "main.fetchgrille")
    {
        if(!seanceGriTable->model())
            seanceGriTable->setModel(tixrCore->stdModel);
        requestItem->parent()->setChild(requestItem->row(), responseItem);
        seanceGriTable->setRootIndex(tixrCore->stdModel->indexFromItem(responseItem));
        seanceGriNameLE->setText(responseItem->text());
    }
    else if(dbfnName == "main.inscalend")
    {
        QStandardItem * parentRequest = requestItem->parent();
        int row = requestItem->row();
        QStandardItem * calendItem = this->fillNewCalend(requestItem, responseItem);
        parentRequest->removeRow(row);
        parentRequest->insertRow(row, calendItem);
    }
}
//-----------------------------------------------------------------------------
void TixrTransportWidget::on_seanceGriList_clicked(QModelIndex index)
{
    QListView *seanceGriList = static_cast<QListView*>(sender());

    if(seanceGriList->rootIndex().parent() == index)
        return;

    QStandardItem * targetModelItem = tixrCore->stdModel->itemFromIndex(index),
                  * modelItem;
    /*
    if(targetModelItem->hasChildren())
    {
        seanceShowGrille("", 0, targetModelItem->child(0), 0);
    }
    else
    */
    {
        QString idGrilleStr = tixrCore->stdModel->data(index, DataRole::ikey()).toString();
        tixrCore->setInHandlerIndex(1);
        tixrCore->setPgFunction("main.fetchgrille");
        currentTaskList << tixrCore->sendMsg(targetModelItem, HTTP_METHOD_ID::POST, "/api", this, "serverResponse", QStringLiteral("{\"srvikey\": \"%1\"}").arg(idGrilleStr).toUtf8());
    }
}
