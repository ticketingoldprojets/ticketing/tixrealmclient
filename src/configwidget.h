#ifndef SCCONFIGWIDGET_H
#define SCCONFIGWIDGET_H
#include "widget.h"
#include "stock/stockwidget.h"
#define TIERSCOLSVIEW   12

class TixrConfigWidget : public TixrWidget
{
    Q_OBJECT

    public:
        explicit TixrConfigWidget(TixRealmCore * mObj, QStandardItem * respItem = 0, QWidget * parent = 0);

        void loadOperatorInfo(TixRealmCore *mObj, const QModelIndex &tiersItemIndex, QStandardItem *respItem = NULL, QWidget *parent = NULL);

    protected:
        QLineEdit * leNom;
        QLineEdit * lePrenom;
        QLineEdit * leIdentifiant;
        QLineEdit * leMotPasse;
        QDateEdit * deDateNaissance;
//        QComboBox * cbTTiers;
        QComboBox * cbCivilite;
        QLineEdit * leModifPrivileges;
        QComboBox * cbSex;

//        QLineEdit * leValeur;
//        QLineEdit * leLibelle;
//        QComboBox * cbTypeValeur;
//        QLineEdit * lePriorite;

        QComboBox * cbPays;
//        QComboBox * cbDepartement;
        QLineEdit * leAdreese1;
        QLineEdit * leAdreese2;
        QComboBox * cbVille;
        QLineEdit * leCodePostal;
        QComboBox * cbUsageHabituel;

//        QListView * lvContacts;
        QListView * lvAdresse;
//        QTreeView * tvHistorique;

        QStandardItem * tiersItem;
        QStandardItem * currentAddress;
        QStandardItem * livraisonAdress;
        QStandardItem * facturationAdress;
        QStandardItem * currentContact;
        QStandardItem * partnerItem;

        QWidget * pageNewTiers;
        QWidget * pageCarnetAdresse;
//        QWidget * pageContacts;
        QWidget * wgtDataPartner;

        QTableView * tvOperators;
        QTableView * tvPartners;

        QLineEdit * lePartnerNom;
        QLineEdit * lePartnerUtilisateur;
        QLineEdit * lePartnerPassword;
        QCheckBox * cbPartnerDirectSales;
        QCheckBox * cbPartnerSendContact;
        QLineEdit * lePartnerAddress;
        QLineEdit * lePartnerPort;
        QPushButton * pbConnectParner;

        void fillComboBox(QComboBox *, const QList<QPair<QString, int> >&);

    private slots:
        void on_pbDelete_clicked();
        void on_pbNew_clicked();
        void on_pbValider_clicked();
        void on_tvOperators_doubleClicked(const QModelIndex & index);
        void on_cbPays_currentIndexChanged(int index);

        void on_pbValiderParner_clicked();
        void on_pbAnnulerParner_clicked();
        void on_pbAjouterParner_clicked();
        void on_pbEnleverParner_clicked();
        void on_pbConnectParner_clicked();
        void on_tvPartners_doubleClicked(const QModelIndex & index);

//        void serverResponse(QString respError, PendingRequest *curReq);
        void readResponse(QString respError, PendingRequest *curReq);
        void responseGetCivilites(QString respError, PendingRequest *curReq);

        void handleResponse_getoperators(QString respError, PendingRequest *curReq);
        void handleResponse_getpartners(QString respError, PendingRequest *curReq);
        void handleResponse_instiers(QString respError, PendingRequest *curReq);
        void handleResponse(QString respError, PendingRequest *curReq);
        void handleResponse_getvilles(QString respError, PendingRequest *curReq);
        void handleResponse_deleting(QString respError, PendingRequest *curReq);
        void handleResponse_inspartner(QString respError, PendingRequest *curReq);

        void on_pbAdmGri_clicked();

        void getProducts();

        QStandardItem* getPartnersItemList(bool justGet=false);
};
Q_DECLARE_METATYPE(TixrConfigWidget *)
#endif // SCCONFIGWIDGET_H
