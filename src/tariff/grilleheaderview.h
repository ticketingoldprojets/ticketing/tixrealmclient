#ifndef GRILLEHEADERVIEW_H
#define GRILLEHEADERVIEW_H

#include <QHeaderView>
#include <QStandardItem>

class GrilleHeaderView : public QHeaderView
{
    Q_OBJECT

public:
    GrilleHeaderView(Qt::Orientation orientation, QWidget *parent = 0);
    void setGriItem(QStandardItem *gItem){_griItem = gItem;}
    QStandardItem *griItem(){return _griItem;}

protected:
    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void dragEnterEvent(QDragEnterEvent *event);
    void dragMoveEvent(QDragMoveEvent *event);
    void dropEvent(QDropEvent *event);

private:
    QPoint dragStartPosition;
    QStandardItem *_griItem;//datos de la grille a la cual pertenece el header

private slots:
    QString newHeaderItemName(QString pattern);
};

#endif // GRILLEHEADERVIEW_H
