#include "grilleheaderview.h"
#include "../core/datarole.h"
#include "../core/datacolumn.h"
#include <QMouseEvent>
#include <QApplication>
#include <QDrag>

GrilleHeaderView::GrilleHeaderView(Qt::Orientation orientation, QWidget *parent) : QHeaderView(orientation, parent)
{
    _griItem = 0;
    setDragEnabled(true);
#if (QT_VERSION < QT_VERSION_CHECK(5, 0, 0))
    setMovable(true);
#else
    setSectionsMovable(true);
#endif
    setDropIndicatorShown(true);
}

void GrilleHeaderView::mousePressEvent(QMouseEvent *event)
{
    if(event->button() == Qt::LeftButton)
        dragStartPosition = event->pos();

    QHeaderView::mousePressEvent(event);
}

void GrilleHeaderView::mouseMoveEvent(QMouseEvent *event)
{
    if(!(event->buttons() & Qt::LeftButton) || !_griItem)
    {
        QHeaderView::mouseMoveEvent(event);
        return;
    }

    QPoint p = event->pos();

    if((p - dragStartPosition).manhattanLength() < QApplication::startDragDistance())
    {
        QHeaderView::mouseMoveEvent(event);
        return;
    }

    int k = visualIndex(logicalIndexAt(p));
    QStringList sect;

    if(orientation()==Qt::Horizontal)
    {
        QStandardItem *hItem = static_cast<QStandardItemModel*>(model())->horizontalHeaderItem(k);
        if(hItem)
        {
            sect << _griItem->data(DataRole::ikey()).toString();
            sect << hItem->data(DataRole::ikey()).toString();
            sect << hItem->text();

            for(int i=0, m=_griItem->rowCount(); i<m; i++)
            {
                QStandardItem *stdItem = _griItem->child(i, k);
                sect << stdItem->text();
            }
        }
        else
        {
            sect << QString::null;
            sect << QString::null;
        }
    }
    else
    {
        QStandardItem *hItem = static_cast<QStandardItemModel*>(model())->verticalHeaderItem(k);
        if(hItem)
        {
            sect << hItem->text();
            sect << hItem->data(DataRole::ikey()).toString();

            for(int j=0, n=_griItem->columnCount(); j<n; j++)
            {
                QStandardItem *stdItem = _griItem->child(k, j);
                sect << stdItem->text();
            }
        }
        else
        {
            sect << QString::null;
            sect << QString::null;
        }
    }

    //sect << QString::number(p.x());
    //sect << QString::number(p.y());

    QMimeData *mimeData = new QMimeData;
    mimeData->setText(sect.join("|"));

    QDrag *drag = new QDrag(this);
    drag->setMimeData(mimeData);

    drag->exec(Qt::CopyAction | Qt::MoveAction);

    QHeaderView::mouseMoveEvent(event);
}

void GrilleHeaderView::dragEnterEvent(QDragEnterEvent *event)
{
    if(event->mimeData()->hasFormat("text/plain"))
    {
        if(!event->source()->inherits("QHeaderView"))
        {
            event->ignore();
            return;
        }

        QHeaderView *myHeader = static_cast<QHeaderView*>(event->source());
        QStringList sect = event->mimeData()->text().split("|", QString::SkipEmptyParts);

        if(myHeader->model() != this->model() && myHeader->orientation() == this->orientation() && sect.size() > 3)
        {
            event->acceptProposedAction();
            return;
        }
    }

    event->ignore();
}

void GrilleHeaderView::dragMoveEvent(QDragMoveEvent *event)
{
    if(event->mimeData()->hasFormat("text/plain") && this->acceptDrops())
    {
        if(!event->source()->inherits("QHeaderView"))
        {
            event->ignore();
            return;
        }

        QHeaderView *myHeader = static_cast<QHeaderView*>(event->source());
        QStringList sect = event->mimeData()->text().split("|", QString::SkipEmptyParts);

        if(myHeader->model() != this->model() && myHeader->orientation() == this->orientation() && sect.size() > 3)
        {
            event->acceptProposedAction();
            return;
        }
    }

    event->ignore();
}

QString GrilleHeaderView::newHeaderItemName(QString pattern)
{
    QMap<int, QString> idxNameMap;
    QRegExp rx(pattern + " (\\d+)");
    bool found = false;

    QStandardItemModel *headerModel = static_cast<QStandardItemModel*>(this->model());
    Qt::Orientation ori = this->orientation();
    for(int i=0, n=(ori==Qt::Horizontal?headerModel->columnCount():headerModel->rowCount()); i<n; i++)
    {
        QString griName = (ori==Qt::Horizontal?headerModel->horizontalHeaderItem(i):headerModel->verticalHeaderItem(i))->text();
        if(griName==pattern)
        {
            found = true;
        }
        if(rx.exactMatch(griName))
        {
            idxNameMap.insert(rx.cap(1).toInt(), griName);
        }
    }

    if(!found)
        return pattern;

    QList<int> kList = idxNameMap.keys();
    int k = 1;
    for(int i=0, n=kList.size(); i<n; i++)
    {
        if(k < kList.at(i))
            break;
        else
            k++;
    }

    return pattern + " " + QString::number(k);
}

void GrilleHeaderView::dropEvent(QDropEvent *event)
{
    if(event->mimeData()->hasFormat("text/plain") && this->acceptDrops())
    {
        if(!event->source()->inherits("QHeaderView"))
        {
            event->ignore();
            return;
        }

        QHeaderView *myHeader = static_cast<QHeaderView*>(event->source());

        QStandardItemModel *headerModel = static_cast<QStandardItemModel*>(this->model());
        QStringList sect = event->mimeData()->text().split("|", QString::SkipEmptyParts);

        if(myHeader->model() != headerModel && myHeader->orientation() == this->orientation() && sect.size() > 3)
        {
            //QPoint p(sect.takeAt(0).toInt(), sect.takeAt(0).toInt());

            int k = visualIndex(logicalIndexAt(event->pos()));

            sect.removeFirst();//La llave de la grille en la BD, aquí no tiene ninguna utilidad
            sect.removeFirst();//La llave de la columna de la grille en la BD, aquí no tiene ninguna utilidad
            QString headerItemName = newHeaderItemName(sect.takeFirst());

            if(orientation()==Qt::Horizontal)
            {
                QStringList lblList;
                for(int i=0, n=headerModel->columnCount(); i<n; i++)
                {
                    lblList << headerModel->horizontalHeaderItem(i)->text();
                }
                if(k!=-1)
                {
                    lblList.insert(k, headerItemName);
                }
                else
                {
                    lblList.append(headerItemName);
                }
                headerModel->setHorizontalHeaderLabels(lblList);

                QList<QStandardItem*> iList;
                int i = 0, m = _griItem->rowCount();
                for(; i<m && !sect.isEmpty(); i++)
                {
                    QStandardItem *stdItem = new QStandardItem(sect.takeFirst());
                    iList << stdItem;
                }

                for(; i<m; i++)
                {
                    QStandardItem *stdItem = new QStandardItem("0");
                    iList << stdItem;
                }

                if(k!=-1)
                {
                    _griItem->insertColumn(k, iList);
                }
                else
                {
                    _griItem->appendColumn(iList);
                }
            }
            else
            {
                QStringList lblList;
                for(int i=0, n=headerModel->rowCount(); i<n; i++)
                {
                    lblList << headerModel->verticalHeaderItem(i)->text();
                }
                if(k!=-1)
                {
                    lblList.insert(k, headerItemName);
                }
                else
                {
                    lblList.append(headerItemName);
                }
                headerModel->setVerticalHeaderLabels(lblList);

                QList<QStandardItem*> iList;
                int i = 0, m = _griItem->columnCount();
                for(; i<m && !sect.isEmpty(); i++)
                {
                    QStandardItem *stdItem = new QStandardItem(sect.takeFirst());
                    iList << stdItem;
                }

                for(; i<m; i++)
                {
                    QStandardItem *stdItem = new QStandardItem("0");
                    iList << stdItem;
                }

                if(k!=-1)
                {
                    _griItem->insertRow(k, iList);
                }
                else
                {
                    _griItem->appendRow(iList);
                }
            }

            event->acceptProposedAction();
            return;
        }
    }

    event->ignore();
}
