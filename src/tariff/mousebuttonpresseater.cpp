#include "mousebuttonpresseater.h"
#include <QEvent>

MouseButtonPressEater::MouseButtonPressEater(QObject *parent) : QObject(parent)
{
}

bool MouseButtonPressEater::eventFilter(QObject *obj, QEvent *event)
{
    if(event->type() == QEvent::MouseButtonPress)
    {
        emit clicked();
        return true;
    }
    else
    {
        return QObject::eventFilter(obj, event);// standard event processing
    }
}
