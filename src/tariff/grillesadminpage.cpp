//-----------------------------------------------------------------------------
#include "grillesadminpage.h"
#include "../mainwindow.h"
#include "mousebuttonpresseater.h"
#include "doublespinboxdelegate.h"
#include "grilleheaderview.h"

GrillesAdminPage::GrillesAdminPage(TixRealmCore *mObj, QWidget *parent) :
    TixrWidget("grillesAdminPage", mObj, "pb2btn.png", parent), currentGrilleModel(0), sourceGrilleItem(0), isGrilleChanged(false)
{
    this->tixrWidgetType = TIXR_GRILLES;
    if(parentTabWidget)
        parentTabWidget->addTab(this, tr("Tariff administration"));

    grilleListView = findChild<QListView*>("grilleListView");
    grilleListView->setModel(tixrCore->stdModel);

    tbGrilleDet = findChild<QToolButton*>("tbGrilleDet");
    pbGrilleEnr = findChild<QPushButton*>("pbGrilleEnr");
    tbGrilleDetContainer = findChild<QWidget*>("tbGrilleDetContainer");

    QLineEdit *lineEdit_2 = findChild<QLineEdit*>("lineEdit_2");
    connect(lineEdit_2, SIGNAL(textEdited(QString)), this, SLOT(griNameChanged(QString)));

    if(tixrCore->keyToItemMap.contains("grilles"))//detectar si los datos ya han sido cargados con anterioridad
    {
        QStandardItem *griTarItem = tixrCore->keyToItemMap.value("grilles");
        PendingRequest *pReq = new PendingRequest(HTTP_METHOD_ID::NONE);
        pReq->setResponseItem(griTarItem);
        showAllGrilles("", pReq);//Asumo que una pestaña no se puede cerrar mientras la operación no ha terminado (el ícono de carga está visible);
    }
    else
    {
        tixrCore->setPgFunction("main.getallgrilles");
        PendingRequest *pReq = tixrCore->sendMsg(tixrCore->stdModel->invisibleRootItem(), HTTP_METHOD_ID::GET, "/api", this, "showAllGrilles");
        currentTaskList << pReq;
        pReq->setRespDataMapKey(QStringLiteral("grilles"));
    }

    MainWindow::tixrConnectSlotsByName(this);
}

void GrillesAdminPage::showAllGrilles(QString respError, PendingRequest *curReq)
{
    if(!beginHandleResponse(respError, curReq))
        return;

    QStandardItem *responseItem = curReq->getResponseItem();

    grilleListView->setRootIndex(responseItem->index());
    connect(grilleListView, SIGNAL(clicked(QModelIndex)), this, SLOT(on_tarifsListView_clicked(QModelIndex)), Qt::UniqueConnection);

    grilleTableView = findChild<QTableView*>("tableView");

    GrilleHeaderView *myHView = new GrilleHeaderView(Qt::Horizontal, grilleTableView);
    myHView->setDragDropMode(QAbstractItemView::DragOnly);
    myHView->setAcceptDrops(false);
    grilleTableView->setHorizontalHeader(myHView);

#if (QT_VERSION < QT_VERSION_CHECK(5, 0, 0))
    myHView->setResizeMode(QHeaderView::ResizeToContents);
#else
    myHView->setSectionResizeMode(QHeaderView::ResizeToContents);
#endif

    myHView->show();

    myHView = new GrilleHeaderView(Qt::Vertical, grilleTableView);
    myHView->setDragDropMode(QAbstractItemView::DragOnly);
    myHView->setAcceptDrops(false);
    grilleTableView->setVerticalHeader(myHView);
#if (QT_VERSION < QT_VERSION_CHECK(5, 0, 0))
    myHView->setResizeMode(QHeaderView::ResizeToContents);
#else
    myHView->setSectionResizeMode(QHeaderView::ResizeToContents);
#endif
    myHView->show();

    grilleTableView_2 = findChild<QTableView*>("tableView_2");
    griDelegate = new DoubleSpinBoxDelegate(this);
    grilleTableView_2->setItemDelegate(griDelegate);

    myHView = new GrilleHeaderView(Qt::Horizontal, grilleTableView_2);
    grilleTableView_2->setHorizontalHeader(myHView);
    myHView->setAcceptDrops(true);
    myHView->setDragDropMode(QAbstractItemView::DragDrop);
#if (QT_VERSION < QT_VERSION_CHECK(5, 0, 0))
    myHView->setResizeMode(QHeaderView::ResizeToContents);
#else
    myHView->setSectionResizeMode(QHeaderView::ResizeToContents);
#endif
    myHView->show();
    connect(myHView, SIGNAL(sectionDoubleClicked(int)), this, SLOT(onHeaderSectionDoubleClicked(int)));

    myHView = new GrilleHeaderView(Qt::Vertical, grilleTableView_2);
    grilleTableView_2->setVerticalHeader(myHView);
    myHView->setAcceptDrops(true);
    myHView->setDragDropMode(QAbstractItemView::DragDrop);
#if (QT_VERSION < QT_VERSION_CHECK(5, 0, 0))
    myHView->setResizeMode(QHeaderView::ResizeToContents);
#else
    myHView->setSectionResizeMode(QHeaderView::ResizeToContents);
#endif
    myHView->show();
    connect(myHView, SIGNAL(sectionDoubleClicked(int)), this, SLOT(onHeaderSectionDoubleClicked(int)));

    grilleLineEdit = findChild<QLineEdit*>("lineEdit");
    grilleLineEdit_2 = findChild<QLineEdit*>("lineEdit_2");

    switcher = findChild<QStackedWidget*>("switcher");
    msgScreen = switcher->findChild<QWidget*>("msgScreen");
    localModalMsg = switcher->findChild<QWidget*>("localModalMsg");
    MouseButtonPressEater *mbPressEater = new MouseButtonPressEater(this);
    connect(mbPressEater, SIGNAL(clicked()), this, SLOT(on_msgScreen_clicked()), Qt::UniqueConnection);
    msgScreen->installEventFilter(mbPressEater);
    mbPressEater = new MouseButtonPressEater(this);
    connect(mbPressEater, SIGNAL(clicked()), this, SLOT(on_localModalMsg_clicked()), Qt::UniqueConnection);
    localModalMsg->installEventFilter(mbPressEater);
    msgScreenBtnYes = localModalMsg->findChild<QPushButton*>("pbYes");
    msgScreenBtnNo = localModalMsg->findChild<QPushButton*>("pbNo");

    msgScreen2 = switcher->findChild<QWidget*>("msgScreen2");
    localModalMsg2 = switcher->findChild<QWidget*>("localModalMsg2");
    mbPressEater = new MouseButtonPressEater(this);
    connect(mbPressEater, SIGNAL(clicked()), this, SLOT(on_msgScreen_clicked()), Qt::UniqueConnection);
    msgScreen2->installEventFilter(mbPressEater);
    mbPressEater = new MouseButtonPressEater(this);
    connect(mbPressEater, SIGNAL(clicked()), this, SLOT(on_localModalMsg_clicked()), Qt::UniqueConnection);
    localModalMsg2->installEventFilter(mbPressEater);
    msg2ScreenBtn = localModalMsg2->findChild<QPushButton*>("pbYes2");

    dlgScreen = switcher->findChild<QWidget*>("dlgScreen");
    localModalDlg = switcher->findChild<QWidget*>("localModalDlg");
    mbPressEater = new MouseButtonPressEater(this);
    connect(mbPressEater, SIGNAL(clicked()), this, SLOT(on_msgScreen_clicked()), Qt::UniqueConnection);
    dlgScreen->installEventFilter(mbPressEater);
    mbPressEater = new MouseButtonPressEater(this);
    connect(mbPressEater, SIGNAL(clicked()), this, SLOT(on_localModalMsg_clicked()), Qt::UniqueConnection);
    localModalDlg->installEventFilter(mbPressEater);
    dlgScreenBtn = localModalDlg->findChild<QPushButton*>("dlgScreenBtn");
    lmDlgLE = localModalDlg->findChild<QLineEdit*>("lmDlgLE");
    lmDlgLbl = localModalDlg->findChild<QLabel*>("lmDlgLbl");
    lmDlgErrLbl = localModalDlg->findChild<QLabel*>("lmDlgErrLbl");

    endHandleResponse(curReq);
}

void GrillesAdminPage::on_tarifsListView_clicked(const QModelIndex &index)
{
    if(grilleTableView->rootIndex().parent().parent()==index)
        return;

    QStandardItem *targetModelItem = tixrCore->stdModel->itemFromIndex(index);
    if(targetModelItem->hasChildren())
    {
        PendingRequest *pReq = new PendingRequest(HTTP_METHOD_ID::NONE);
        pReq->setResponseItem(targetModelItem->child(0));
        showGrille("", pReq);
    }
    else
    {
        QString idGrilleStr = tixrCore->stdModel->data(index, DataRole::ikey()).toString();
        tixrCore->setInHandlerIndex(1);
        tixrCore->setPgFunction("main.fetchgrille");
        currentTaskList << tixrCore->sendMsg(targetModelItem, HTTP_METHOD_ID::POST, "/api", this, "showGrille", QStringLiteral("{\"srvikey\": \"%1\"}").arg(idGrilleStr).toUtf8());
    }
}

void GrillesAdminPage::showGrille(QString respError, PendingRequest *curReq)
{
    if(!beginHandleResponse(respError, curReq))
        return;

    QStandardItem *responseItem = curReq->getResponseItem();

    grilleTableView->setModel(tixrCore->stdModel);
    grilleTableView->setRootIndex(responseItem->index());
    grilleLineEdit->setText(responseItem->text());

    //Tomar el modelo que guarda los encabezados y establecerlo para la tabla actual
    QStandardItemModel *headersModel = tixrCore->itemToHeaderModelMap.value(responseItem);

    GrilleHeaderView *myHView = static_cast<GrilleHeaderView*>(grilleTableView->horizontalHeader());
    myHView->setGriItem(responseItem);
    myHView->setModel(headersModel);
    myHView->show();

    myHView = static_cast<GrilleHeaderView*>(grilleTableView->verticalHeader());
    myHView->setGriItem(responseItem);
    myHView->setModel(headersModel);
    myHView->show();

    endHandleResponse(curReq);
}

void GrillesAdminPage::on_pbGrilleMod_clicked()
{
    if(!grilleTableView->rootIndex().isValid())
        return;

    if(isGrilleChanged)
    {
        QPixmap originalPixmap = grab(QRect(msgScreen->mapTo(this, msgScreen->pos()), switcher->size()));
        QPalette palette = msgScreen->palette();
        palette.setBrush(QPalette::Window, QBrush(originalPixmap));
        msgScreen->setPalette(palette);
        msgScreenBtnYes->disconnect(this);
        msgScreenBtnNo->disconnect(this);
        connect(msgScreenBtnYes, SIGNAL(clicked()), this, SLOT(modGrille()), Qt::UniqueConnection);
        connect(msgScreenBtnNo, SIGNAL(clicked()), this, SLOT(modGrille()), Qt::UniqueConnection);
        switcher->setCurrentIndex(1);
    }
    else
    {
        PendingRequest *pReq = new PendingRequest(HTTP_METHOD_ID::NONE);
        pReq->setResponseItem(0);
        modGrilleResponse("", pReq);
    }
}

void GrillesAdminPage::on_tbAddRow_clicked()
{
    QPixmap originalPixmap = grab(QRect(dlgScreen->mapTo(this, dlgScreen->pos()), switcher->size()));
    QPalette palette = dlgScreen->palette();
    palette.setBrush(QPalette::Window, QBrush(originalPixmap));
    dlgScreen->setPalette(palette);

    dlgScreenBtn->disconnect(this);
    connect(dlgScreenBtn, SIGNAL(clicked()), this, SLOT(on_addRowdlgBtns_click()), Qt::UniqueConnection);

    lmDlgErrLbl->clear();
    lmDlgLE->clear();
    lmDlgLbl->setText(tr("User category"));
    switcher->setCurrentIndex(2);
}

void GrillesAdminPage::on_addRowdlgBtns_click()
{
    QString str = lmDlgLE->text().trimmed();
    QString emsg = validateGrilleHeaderName(str);

    if(emsg.isEmpty())
    {
        if(!currentGrilleModel)
        {
            currentGrilleModel = new QStandardItemModel(this);
            QStandardItem *rootItem = new QStandardItem;//Estoy obligado de crear un item raiz diferente de la raiz de la grille, de lo contrario no me deja modificarlo.
            QStandardItem *newGriItem = new QStandardItem;
            QString griName = newGrilleName();
            newGriItem->setText(griName);
            rootItem->appendRow(newGriItem);
            currentGrilleModel->appendRow(rootItem);
            connect(currentGrilleModel, SIGNAL(itemChanged(QStandardItem*)), this, SLOT(setGrilleChanged(QStandardItem*)), Qt::UniqueConnection);
            connect(currentGrilleModel, SIGNAL(columnsInserted(const QModelIndex &, int, int)), this, SLOT(setGrilleChanged2(QModelIndex,int,int)));
            connect(currentGrilleModel, SIGNAL(columnsRemoved(const QModelIndex &, int, int)), this, SLOT(setGrilleChanged2(QModelIndex,int,int)));
            connect(currentGrilleModel, SIGNAL(columnsMoved(const QModelIndex &, int, int, const QModelIndex &, int)), this, SLOT(setGrilleChanged3(QModelIndex,int,int,QModelIndex,int)));
            connect(currentGrilleModel, SIGNAL(rowsInserted(const QModelIndex &, int, int)), this, SLOT(setGrilleChanged2(QModelIndex,int,int)));
            connect(currentGrilleModel, SIGNAL(rowsRemoved(const QModelIndex &, int, int)), this, SLOT(setGrilleChanged2(QModelIndex,int,int)));
            connect(currentGrilleModel, SIGNAL(rowsMoved(const QModelIndex &, int, int, const QModelIndex &, int)), this, SLOT(setGrilleChanged3(QModelIndex,int,int,QModelIndex,int)));
            connect(currentGrilleModel, SIGNAL(headerDataChanged(Qt::Orientation,int,int)), this, SLOT(setGrilleChanged4(Qt::Orientation,int,int)));

            sourceGrilleItem = 0;

            grilleLineEdit_2->setText(griName);
            grilleTableView_2->setModel(currentGrilleModel);
            grilleTableView_2->setRootIndex(newGriItem->index());

            GrilleHeaderView *myHeader = static_cast<GrilleHeaderView*>(grilleTableView_2->horizontalHeader());
            myHeader->setGriItem(newGriItem);
            myHeader = static_cast<GrilleHeaderView*>(grilleTableView_2->verticalHeader());
            myHeader->setGriItem(newGriItem);

            isGrilleChanged = true;
            pbGrilleEnr->setEnabled(true);
        }

        QList<QStandardItem*> r;
        QStandardItem *griItem = currentGrilleModel->item(0)->child(0);
        for(int j=0,n=griItem->columnCount(); j<n; j++)
        {
            QStandardItem *rItem = new QStandardItem;
            rItem->setData(0., Qt::EditRole);
            r << rItem;
        }
        griItem->appendRow(r);
        currentGrilleModel->setVerticalHeaderItem(griItem->rowCount()-1, new QStandardItem(str));

        switcher->setCurrentIndex(0);
    }
    else
        lmDlgErrLbl->setText(emsg);
}

void GrillesAdminPage::onHeaderSectionDoubleClicked(int logicalIndex)
{
    QPixmap originalPixmap = grab(QRect(dlgScreen->mapTo(this, dlgScreen->pos()), switcher->size()));
    QPalette palette = dlgScreen->palette();
    palette.setBrush(QPalette::Window, QBrush(originalPixmap));
    dlgScreen->setPalette(palette);

    QHeaderView *header = static_cast<QHeaderView*>(sender());
    int k = header->visualIndex(logicalIndex);

    QStandardItem *hItem ;
    if(header->orientation()==Qt::Horizontal)
    {
        dlgScreenBtn->setProperty("tixrHeaderOrientation", QVariant(true));
        hItem = currentGrilleModel->horizontalHeaderItem(k);
    }
    else
    {
        dlgScreenBtn->setProperty("tixrHeaderOrientation", QVariant(false));
        hItem = currentGrilleModel->verticalHeaderItem(k);
    }

    dlgScreenBtn->disconnect(this);
    connect(dlgScreenBtn, SIGNAL(clicked()), this, SLOT(editRowOrColLabel()), Qt::UniqueConnection);

    dlgScreenBtn->setProperty("tixrHeaderIndex", QVariant(k));

    lmDlgErrLbl->clear();
    lmDlgLE->setText(hItem->text());
    lmDlgLbl->setText(tr("User category"));
    switcher->setCurrentIndex(2);
}

void GrillesAdminPage::editRowOrColLabel()
{
    QString str = lmDlgLE->text().trimmed();
    QString emsg = validateGrilleHeaderName(str);

    if(emsg.isEmpty())
    {
        int k = dlgScreenBtn->property("tixrHeaderIndex").toInt();

        QStandardItem *hItem;
        if(dlgScreenBtn->property("tixrHeaderOrientation").toBool())
        {
            hItem = currentGrilleModel->horizontalHeaderItem(k);
        }
        else
        {
            hItem = currentGrilleModel->verticalHeaderItem(k);
        }

        hItem->setText(str);
        switcher->setCurrentIndex(0);
    }
    else
        lmDlgErrLbl->setText(emsg);
}

QString GrillesAdminPage::validateGrilleHeaderName(QString name)
{
    if(!currentGrilleModel)
        return QString::null;

    if(name.isEmpty() || name.contains('|'))
        return tr("Invalid name.");

    int k = 0;
    while(QStandardItem *hItem = currentGrilleModel->horizontalHeaderItem(k))
    {
        if(!name.compare(hItem->text(), Qt::CaseInsensitive))
            return tr("Duplicated name.");
        k++;
    }

    k = 0;
    while(QStandardItem *hItem = currentGrilleModel->verticalHeaderItem(k))
    {
        if(!name.compare(hItem->text(), Qt::CaseInsensitive))
            return tr("Duplicated name.");
        k++;
    }

    return QString::null;
}

void GrillesAdminPage::on_tbAddCol_clicked()
{
    QPixmap originalPixmap = grab(QRect(dlgScreen->mapTo(this, dlgScreen->pos()), switcher->size()));
    QPalette palette = dlgScreen->palette();
    palette.setBrush(QPalette::Window, QBrush(originalPixmap));
    dlgScreen->setPalette(palette);

    dlgScreenBtn->disconnect(this);
    connect(dlgScreenBtn, SIGNAL(clicked()), this, SLOT(on_addColdlgBtns_click()), Qt::UniqueConnection);

    lmDlgErrLbl->clear();
    lmDlgLE->clear();
    lmDlgLbl->setText(tr("Product category"));
    switcher->setCurrentIndex(2);
}

void GrillesAdminPage::on_addColdlgBtns_click()
{
    QString str = lmDlgLE->text().trimmed();
    QString emsg = validateGrilleHeaderName(str);

    if(emsg.isEmpty())
    {
        if(!currentGrilleModel)
        {
            currentGrilleModel = new QStandardItemModel(this);
            QStandardItem *rootItem = new QStandardItem;//Estoy obligado de crear un item raiz diferente de la raiz de la grille, de lo contrario no me deja modificarlo.
            QStandardItem *newGriItem = new QStandardItem;
            QString griName = newGrilleName();
            newGriItem->setText(griName);
            rootItem->appendRow(newGriItem);
            currentGrilleModel->appendRow(rootItem);
            connect(currentGrilleModel, SIGNAL(itemChanged(QStandardItem*)), this, SLOT(setGrilleChanged(QStandardItem*)), Qt::UniqueConnection);
            connect(currentGrilleModel, SIGNAL(columnsInserted(const QModelIndex &, int, int)), this, SLOT(setGrilleChanged2(QModelIndex,int,int)));
            connect(currentGrilleModel, SIGNAL(columnsRemoved(const QModelIndex &, int, int)), this, SLOT(setGrilleChanged2(QModelIndex,int,int)));
            connect(currentGrilleModel, SIGNAL(columnsMoved(const QModelIndex &, int, int, const QModelIndex &, int)), this, SLOT(setGrilleChanged3(QModelIndex,int,int,QModelIndex,int)));
            connect(currentGrilleModel, SIGNAL(rowsInserted(const QModelIndex &, int, int)), this, SLOT(setGrilleChanged2(QModelIndex,int,int)));
            connect(currentGrilleModel, SIGNAL(rowsRemoved(const QModelIndex &, int, int)), this, SLOT(setGrilleChanged2(QModelIndex,int,int)));
            connect(currentGrilleModel, SIGNAL(rowsMoved(const QModelIndex &, int, int, const QModelIndex &, int)), this, SLOT(setGrilleChanged3(QModelIndex,int,int,QModelIndex,int)));
            connect(currentGrilleModel, SIGNAL(headerDataChanged(Qt::Orientation,int,int)), this, SLOT(setGrilleChanged4(Qt::Orientation,int,int)));

            sourceGrilleItem = 0;

            grilleLineEdit_2->setText(griName);
            grilleTableView_2->setModel(currentGrilleModel);
            grilleTableView_2->setRootIndex(newGriItem->index());

            GrilleHeaderView *myHeader = static_cast<GrilleHeaderView*>(grilleTableView_2->horizontalHeader());
            myHeader->setGriItem(newGriItem);
            myHeader = static_cast<GrilleHeaderView*>(grilleTableView_2->verticalHeader());
            myHeader->setGriItem(newGriItem);

            isGrilleChanged = true;
            pbGrilleEnr->setEnabled(true);
        }

        QList<QStandardItem*> c;
        QStandardItem *griItem = currentGrilleModel->item(0)->child(0);
        for(int i=0,m=griItem->rowCount(); i<m; i++)
        {
            QStandardItem *cItem = new QStandardItem;
            cItem->setData(0., Qt::EditRole);
            c << cItem;
        }
        griItem->appendColumn(c);
        currentGrilleModel->setHorizontalHeaderItem(griItem->columnCount()-1, new QStandardItem(str));

        switcher->setCurrentIndex(0);
    }
    else
        lmDlgErrLbl->setText(emsg);
}

void GrillesAdminPage::on_tbDelRow_clicked()
{
    if(!currentGrilleModel)
        return;

    QModelIndexList idxList = grilleTableView_2->selectionModel()->selectedRows();
    if(idxList.isEmpty())
        return;

    QList<QStandardItem*> iList;
    for(int i=0, n=idxList.size(); i<n; i++)
        iList << currentGrilleModel->itemFromIndex(idxList.at(i));

    QStandardItem *griItem = currentGrilleModel->item(0)->child(0);

    QStringList headerLabels;
    for(int i=0, n=griItem->rowCount(); i<n; i++)
    {
        headerLabels << currentGrilleModel->verticalHeaderItem(i)->text();
    }

    for(int i=0, n=iList.size(); i<n; i++)
    {
        int k = iList.at(i)->row();
        griItem->removeRow(k);
        headerLabels.removeAt(k);
    }

    currentGrilleModel->setVerticalHeaderLabels(headerLabels);

    for(int i=headerLabels.size(), m=currentGrilleModel->rowCount(); i<m; i++)
        currentGrilleModel->verticalHeaderItem(i)->setText(QString::null);
}

void GrillesAdminPage::on_tbDelCol_clicked()
{
    if(!currentGrilleModel)
        return;

    QModelIndexList idxList = grilleTableView_2->selectionModel()->selectedColumns();
    if(idxList.isEmpty())
        return;

    QList<QStandardItem*> iList;
    for(int i=0, n=idxList.size(); i<n; i++)
        iList << currentGrilleModel->itemFromIndex(idxList.at(i));

    QStandardItem *griItem = currentGrilleModel->item(0)->child(0);

    QStringList headerLabels;
    for(int i=0, n=griItem->columnCount(); i<n; i++)
    {
        headerLabels << currentGrilleModel->horizontalHeaderItem(i)->text();
    }

    for(int i=0, n=iList.size(); i<n; i++)
    {
        int k = iList.at(i)->column();
        griItem->removeColumn(k);
        headerLabels.removeAt(k);
    }

    currentGrilleModel->setHorizontalHeaderLabels(headerLabels);

    for(int j=headerLabels.size(), n=currentGrilleModel->columnCount(); j<n; j++)
        currentGrilleModel->horizontalHeaderItem(j)->setText(QString::null);
}

void GrillesAdminPage::modGrille()
{
    switcher->setCurrentIndex(0);
    if(sender()->objectName()=="pbYes")
    {
        QString cmd = sourceGrilleItem?"upGrille":"insGrille";
        QString serverPath = sourceGrilleItem?"editGrille":"newGrille";
        QStandardItem *rootItem = currentGrilleModel->item(0)->child(0);
        rootItem->setData(QVariant(cmd), DataRole::griadmincmd());
        tixrCore->setOutHandlerIndex(1);
        currentTaskList << tixrCore->sendMsg(rootItem, HTTP_METHOD_ID::POST, "/api" + serverPath, this, "modGrilleResponse");
    }
    else
    {
        PendingRequest *pReq = new PendingRequest(HTTP_METHOD_ID::NONE);
        pReq->setResponseItem(0);
        modGrilleResponse("", pReq);
    }
}

void GrillesAdminPage::modGrilleResponse(QString respError, PendingRequest *curReq)
{
    if(!beginHandleResponse(respError, curReq))
        return;

    //ACTUALIZAR sourceGrilleItem CON LOS CAMBIOS QUE ACABO DE GUARDAR EN EL SERVIDOR
    assignGrille(tixrCore->stdModel->itemFromIndex(grilleTableView_2->rootIndex()), sourceGrilleItem);

    QStandardItem *stdItem = tixrCore->stdModel->itemFromIndex(grilleTableView->rootIndex());

    if(currentGrilleModel)
        delete currentGrilleModel;
    currentGrilleModel = cloneGrille(stdItem, this);
    connect(currentGrilleModel, SIGNAL(itemChanged(QStandardItem*)), this, SLOT(setGrilleChanged(QStandardItem*)), Qt::UniqueConnection);
    connect(currentGrilleModel, SIGNAL(columnsInserted(const QModelIndex &, int, int)), this, SLOT(setGrilleChanged2(QModelIndex,int,int)));
    connect(currentGrilleModel, SIGNAL(columnsRemoved(const QModelIndex &, int, int)), this, SLOT(setGrilleChanged2(QModelIndex,int,int)));
    connect(currentGrilleModel, SIGNAL(columnsMoved(const QModelIndex &, int, int, const QModelIndex &, int)), this, SLOT(setGrilleChanged3(QModelIndex,int,int,QModelIndex,int)));
    connect(currentGrilleModel, SIGNAL(rowsInserted(const QModelIndex &, int, int)), this, SLOT(setGrilleChanged2(QModelIndex,int,int)));
    connect(currentGrilleModel, SIGNAL(rowsRemoved(const QModelIndex &, int, int)), this, SLOT(setGrilleChanged2(QModelIndex,int,int)));
    connect(currentGrilleModel, SIGNAL(rowsMoved(const QModelIndex &, int, int, const QModelIndex &, int)), this, SLOT(setGrilleChanged3(QModelIndex,int,int,QModelIndex,int)));
    connect(currentGrilleModel, SIGNAL(headerDataChanged(Qt::Orientation,int,int)), this, SLOT(setGrilleChanged4(Qt::Orientation,int,int)));

    sourceGrilleItem = stdItem;

    grilleLineEdit_2->setText(stdItem->text());
    grilleTableView_2->setModel(currentGrilleModel);
    QStandardItem *griItem = currentGrilleModel->item(0);
    GrilleHeaderView *myHeader = static_cast<GrilleHeaderView*>(grilleTableView_2->horizontalHeader());
    myHeader->setGriItem(griItem);
    myHeader = static_cast<GrilleHeaderView*>(grilleTableView_2->verticalHeader());
    myHeader->setGriItem(griItem);
    grilleTableView_2->setRootIndex(griItem->index());

    isGrilleChanged = false;

    pbGrilleEnr->setEnabled(false);

    endHandleResponse(curReq);
}

QStandardItemModel *GrillesAdminPage::cloneGrille(QStandardItem *griItem, QObject *parent)
{
    int M = griItem->rowCount(), N = griItem->columnCount();
    QStandardItemModel *result = new QStandardItemModel(parent);
    //QStandardItem *rootItem = new QStandardItem;//Estoy obligado de crear un item raiz diferente de la raiz de la grille, de lo contrario no me deja modificarlo.
    QStandardItem *clonedGriItem = new QStandardItem(M, N);
    result->appendRow(clonedGriItem);//rootItem);
    //rootItem->appendRow(clonedGriItem);
    QStandardItemModel *headersModel = tixrCore->itemToHeaderModelMap.value(griItem);

    clonedGriItem->setData(griItem->text(), Qt::EditRole);
    QString griKey = griItem->data(DataRole::ikey()).toString();
    clonedGriItem->setData(griKey, DataRole::ikey()); // llave

    for(int i=0; i<M; i++)
    {
        result->setVerticalHeaderItem(i, new QStandardItem(headersModel->verticalHeaderItem(i)->text()));
    }

    for(int j=0; j<N; j++)
    {
        result->setHorizontalHeaderItem(j, new QStandardItem(headersModel->horizontalHeaderItem(j)->text()));
        for(int i=0; i<M; i++)
        {
            QStandardItem *stdItem = new QStandardItem;
            QStandardItem *srcItem = griItem->child(i,j);
            stdItem->setData(srcItem->data(Qt::DisplayRole), Qt::EditRole);
            clonedGriItem->setChild(i, j, stdItem);
        }
    }

    return result;
}

void GrillesAdminPage::assignGrille(QStandardItem *srcItem, QStandardItem *dstItem)
{
    if(!srcItem || !dstItem)
        return;

    dstItem->setText(srcItem->text());

    QString griKey = srcItem->data(DataRole::ikey()).toString();
    dstItem->setData(griKey, DataRole::ikey());

    dstItem->removeColumns(0, dstItem->columnCount());

    int m = srcItem->rowCount(), n = srcItem->columnCount();
    dstItem->setRowCount(m);
    dstItem->setColumnCount(n);

    QStandardItemModel *srcHeadersModel = srcItem->model();
    QStandardItemModel *dstHeadersModel = tixrCore->itemToHeaderModelMap.value(dstItem);
    dstHeadersModel->clear();

    for(int j=0; j<n; j++)
    {
        dstHeadersModel->setHorizontalHeaderItem(j, new QStandardItem(srcHeadersModel->horizontalHeaderItem(j)->text()));
    }

    for(int i=0; i<m; i++)
    {
        dstHeadersModel->setVerticalHeaderItem(i, new QStandardItem(srcHeadersModel->verticalHeaderItem(i)->text()));
        for(int j=0; j<n; j++)
        {
            QStandardItem *childItem = srcItem->child(i, j);
            QStandardItem *stdItem = new QStandardItem(childItem->text());
            stdItem->setData(childItem->data(Qt::EditRole), Qt::DisplayRole);
            stdItem->setEditable(false);
            dstItem->setChild(i, j, stdItem);
        }
    }
}

bool GrillesAdminPage::grillesAreDistincts(QStandardItem *griItem, QStandardItem *editedGriItem)
{
    if(!griItem)
        return true;

    int M = griItem->rowCount(), N = griItem->columnCount();
    if(griItem->text()!=editedGriItem->text() || M!=editedGriItem->rowCount() || N!=editedGriItem->columnCount())
        return true;

    QStandardItemModel *griHeadersModel = tixrCore->itemToHeaderModelMap.value(griItem);
    QStandardItemModel *editedGriHeadersModel = editedGriItem->model();

    for(int i=0; i<M; i++)
    {
        QStandardItem *griHeaderItem = griHeadersModel->verticalHeaderItem(i);
        QStandardItem *editedGriHeaderItem = editedGriHeadersModel->verticalHeaderItem(i);
        if(griHeaderItem->text()!=editedGriHeaderItem->text())
            return true;
    }

    for(int j=0; j<N; j++)
    {
        QStandardItem *griHeaderItem = griHeadersModel->horizontalHeaderItem(j);
        QStandardItem *editedGriHeaderItem = editedGriHeadersModel->horizontalHeaderItem(j);
        if(griHeaderItem->text()!=editedGriHeaderItem->text())
            return true;
    }

    for(int i=0; i<M; i++)
    {
        for(int j=0; j<N; j++)
        {
            QStandardItem *griCellItem = griItem->child(i,j);
            QStandardItem *editedGriCellItem = editedGriItem->child(i,j);
            if(griCellItem->text()!=editedGriCellItem->text())
                return true;
        }
    }

    return false;
}

void GrillesAdminPage::on_msgScreen_clicked()
{
    switcher->setCurrentIndex(0);
}

void GrillesAdminPage::on_localModalMsg_clicked()
{
    //
}

void GrillesAdminPage::on_tbGrilleDet_clicked()
{
    if(!grilleTableView->rootIndex().isValid())
        return;

    if(isGrilleChanged && sourceGrilleItem && sourceGrilleItem->index()==grilleTableView->rootIndex())
    {
        QPixmap originalPixmap = grab(QRect(msgScreen2->mapTo(this, msgScreen2->pos()), switcher->size()));
        QPalette palette = msgScreen2->palette();
        palette.setBrush(QPalette::Window, QBrush(originalPixmap));
        msgScreen2->setPalette(palette);
        msg2ScreenBtn->disconnect(this);
        connect(msg2ScreenBtn, SIGNAL(clicked()), this, SLOT(on_msg2Btns_click()), Qt::UniqueConnection);
        switcher->setCurrentIndex(3);
    }
    else
    {
        QStandardItem *griItem = tixrCore->stdModel->itemFromIndex(grilleTableView->rootIndex());
        if(!griItem)
            return;

        //griItem->parent()->appendRow(rootItem);

        tixrCore->setPgFunction(QStringLiteral("main.delgrilletarif"));
        currentTaskList << tixrCore->sendMsg(0, HTTP_METHOD_ID::POST, "/api", this, "detGrilleResponse", QStringLiteral("{\"srvikey\": \"%1\"}").arg(griItem->data(DataRole::ikey()).toString()).toUtf8());
    }
}

void GrillesAdminPage::on_msg2Btns_click()
{
    switcher->setCurrentIndex(0);

    QStandardItem *griItem = tixrCore->stdModel->itemFromIndex(grilleTableView->rootIndex())->parent();
    if(!griItem)
        return;

    //griItem->parent()->appendRow(rootItem);

    tixrCore->setPgFunction(QStringLiteral("main.delgrilletarif"));
    QString keyStr = griItem->data(DataRole::ikey()).toString();
    currentTaskList << tixrCore->sendMsg(0, HTTP_METHOD_ID::POST, "/api", this, "detGrilleResponse", QStringLiteral("{\"srvikey\": \"%1\"}").arg(keyStr).toUtf8());
}

void GrillesAdminPage::detGrilleResponse(QString respError, PendingRequest *curReq)
{
    if(!beginHandleResponse(respError, curReq))
        return;

    QStandardItem *griItem = tixrCore->stdModel->itemFromIndex(grilleTableView->rootIndex());
    if(!griItem)
        return;

    GrilleHeaderView *myHView = static_cast<GrilleHeaderView*>(grilleTableView->horizontalHeader());
    myHView->setGriItem(0);
    myHView->setModel(0);

    myHView = static_cast<GrilleHeaderView*>(grilleTableView->verticalHeader());
    myHView->setGriItem(0);
    myHView->setModel(0);

    grilleTableView->setModel(0);
    if(sourceGrilleItem == griItem && currentGrilleModel)//La grille a eliminar es la misma que se está visualizando, debo limpiar la interfaz
    {
        myHView = static_cast<GrilleHeaderView*>(grilleTableView_2->horizontalHeader());
        myHView->setGriItem(0);
        myHView->setModel(0);

        myHView = static_cast<GrilleHeaderView*>(grilleTableView_2->verticalHeader());
        myHView->setGriItem(0);
        myHView->setModel(0);

        grilleTableView_2->setModel(0);
        delete currentGrilleModel;
        currentGrilleModel = 0;
        grilleLineEdit_2->clear();
    }

    sourceGrilleItem = 0;
    QStandardItemModel *headerModel = tixrCore->itemToHeaderModelMap.take(griItem);
    delete headerModel;

    QStandardItem *p = griItem->parent();
    p->removeRow(griItem->row());
    p->parent()->removeRow(p->row());

    grilleLineEdit->clear();
    grilleTableView->setModel(0);
    isGrilleChanged = false;
    pbGrilleEnr->setEnabled(false);

    endHandleResponse(curReq);
}

void GrillesAdminPage::on_pbGrilleCre_clicked()
{
    if(isGrilleChanged)
    {
        QPixmap originalPixmap = grab(QRect(msgScreen->mapTo(this, msgScreen->pos()), switcher->size()));
        QPalette palette = msgScreen->palette();
        palette.setBrush(QPalette::Window, QBrush(originalPixmap));
        msgScreen->setPalette(palette);
        msgScreenBtnYes->disconnect(this);
        msgScreenBtnNo->disconnect(this);
        connect(msgScreenBtnYes, SIGNAL(clicked()), this, SLOT(on_msgBtnsForCre_click()), Qt::UniqueConnection);
        connect(msgScreenBtnNo, SIGNAL(clicked()), this, SLOT(on_msgBtnsForCre_click()), Qt::UniqueConnection);
        switcher->setCurrentIndex(1);
    }
    else
    {
        PendingRequest *pReq = new PendingRequest(HTTP_METHOD_ID::NONE);
        pReq->setResponseItem(0);
        creGrilleResponse("", pReq);
    }
}

void GrillesAdminPage::on_msgBtnsForCre_click()
{
    switcher->setCurrentIndex(0);
    if(sender()->objectName()=="pbYes")
    {
        QString cmd = sourceGrilleItem?"upGrille":"insGrille";
        QString serverPath = sourceGrilleItem?"editGrille":"newGrille";
        QStandardItem *rootItem = currentGrilleModel->item(0)->child(0);
        rootItem->setData(QVariant(cmd), DataRole::griadmincmd());
        tixrCore->setOutHandlerIndex(1);
        currentTaskList << tixrCore->sendMsg(rootItem, HTTP_METHOD_ID::POST, "/api" + serverPath, this, "creGrilleResponse");
    }
    else
    {
        PendingRequest *pReq = new PendingRequest(HTTP_METHOD_ID::NONE);
        pReq->setResponseItem(0);
        creGrilleResponse("", pReq);
    }
}

void GrillesAdminPage::creGrilleResponse(QString respError, PendingRequest *curReq)
{
    if(!beginHandleResponse(respError, curReq))
        return;

    if(currentGrilleModel)
        delete currentGrilleModel;

    currentGrilleModel = new QStandardItemModel(this);
    QStandardItem *rootItem = new QStandardItem;//Estoy obligado de crear un item raiz diferente de la raiz de la grille, de lo contrario no me deja modificarlo.
    QStandardItem *newGriItem = new QStandardItem(1, 1);
    QStandardItem *stdItem = new QStandardItem;
    stdItem->setData(0., Qt::EditRole);
    newGriItem->setChild(0,0,stdItem);

    currentGrilleModel->appendRow(rootItem);

    currentGrilleModel->setVerticalHeaderLabels(QStringList()<<tr("Cli. Cat. 1"));
    currentGrilleModel->setHorizontalHeaderLabels(QStringList()<<tr("Prod. Cat. 1"));

    rootItem->appendRow(newGriItem);
    connect(currentGrilleModel, SIGNAL(itemChanged(QStandardItem*)), this, SLOT(setGrilleChanged(QStandardItem*)), Qt::UniqueConnection);
    connect(currentGrilleModel, SIGNAL(columnsInserted(const QModelIndex &, int, int)), this, SLOT(setGrilleChanged2(QModelIndex,int,int)));
    connect(currentGrilleModel, SIGNAL(columnsRemoved(const QModelIndex &, int, int)), this, SLOT(setGrilleChanged2(QModelIndex,int,int)));
    connect(currentGrilleModel, SIGNAL(columnsMoved(const QModelIndex &, int, int, const QModelIndex &, int)), this, SLOT(setGrilleChanged3(QModelIndex,int,int,QModelIndex,int)));
    connect(currentGrilleModel, SIGNAL(rowsInserted(const QModelIndex &, int, int)), this, SLOT(setGrilleChanged2(QModelIndex,int,int)));
    connect(currentGrilleModel, SIGNAL(rowsRemoved(const QModelIndex &, int, int)), this, SLOT(setGrilleChanged2(QModelIndex,int,int)));
    connect(currentGrilleModel, SIGNAL(rowsMoved(const QModelIndex &, int, int, const QModelIndex &, int)), this, SLOT(setGrilleChanged3(QModelIndex,int,int,QModelIndex,int)));
    connect(currentGrilleModel, SIGNAL(headerDataChanged(Qt::Orientation,int,int)), this, SLOT(setGrilleChanged4(Qt::Orientation,int,int)));

    sourceGrilleItem = 0;

    QString griName = newGrilleName();
    newGriItem->setText(griName);
    grilleLineEdit_2->setText(griName);
    grilleTableView_2->setModel(currentGrilleModel);
    grilleTableView_2->setRootIndex(newGriItem->index());

    GrilleHeaderView *myHeader = static_cast<GrilleHeaderView*>(grilleTableView_2->horizontalHeader());
    myHeader->setGriItem(newGriItem);
    myHeader = static_cast<GrilleHeaderView*>(grilleTableView_2->verticalHeader());
    myHeader->setGriItem(newGriItem);

    isGrilleChanged = true;
    pbGrilleEnr->setEnabled(true);

    endHandleResponse(curReq);
}

void GrillesAdminPage::on_pbGrilleEnr_clicked()
{
    QString cmd = sourceGrilleItem?"upGrille":"insGrille";
    QString serverPath = sourceGrilleItem?"editGrille":"newGrille";
    QStandardItem *rootItem = currentGrilleModel->item(0)->child(0);
    rootItem->setData(QVariant(cmd), DataRole::griadmincmd());
    tixrCore->setOutHandlerIndex(1);
    const char *slotName = sourceGrilleItem ? "enrGrilleResponse"\
                                            : "enrSousGrilleResponse";
    currentTaskList << tixrCore->sendMsg(rootItem, HTTP_METHOD_ID::POST, "/api" + serverPath, this, slotName);
}

void GrillesAdminPage::enrGrilleResponse(QString respError, PendingRequest *curReq)
{
    if(!beginHandleResponse(respError, curReq))
        return;

    assignGrille(currentGrilleModel->item(0)->child(0), sourceGrilleItem);//ACTUALIZAR sourceGrilleItem CON LOS CAMBIOS QUE ACABO DE GUARDAR EN EL SERVIDOR

    QString griName = sourceGrilleItem->text();
    sourceGrilleItem->parent()->parent()->setText(griName);

    if(grilleTableView->rootIndex()==sourceGrilleItem->index())
        grilleLineEdit->setText(griName);

    isGrilleChanged = false;
    pbGrilleEnr->setEnabled(false);

    endHandleResponse(curReq);
}

void GrillesAdminPage::on_pbGrilleEnrSous_clicked()
{
    if(!currentGrilleModel)
        return;

    QStandardItem *rootItem = currentGrilleModel->item(0);
    rootItem->setData(QVariant(), DataRole::ikey());//Quitar la llave pues se trata ahora de una nueva grille
    tixrCore->setOutHandlerIndex(1);
    tixrCore->setPgFunction("main.insgrilletarif");
    currentTaskList << tixrCore->sendMsg(rootItem, HTTP_METHOD_ID::POST, "/api", this, "enrSousGrilleResponse");
}

QString GrillesAdminPage::newGrilleName()
{
    QStandardItem *rootItem = tixrCore->keyToItemMap.value("grilles");

    QString pattern = tr("New price table");
    QMap<int, QString> idxNameMap;
    QRegExp rx(pattern + " (\\d+)");

    for(int i=0, n=rootItem->rowCount(); i<n; i++)
    {
        QString griName = rootItem->child(i)->text();
        if(rx.exactMatch(griName))
        {
            idxNameMap.insert(rx.cap(1).toInt(), griName);
        }
    }

    QList<int> kList = idxNameMap.keys();
    int k = 1;
    for(int i=0, n=kList.size(); i<n; i++)
    {
        if(k < kList.at(i))
            break;
        else
            k++;
    }

    return pattern + " " + QString::number(k);
}

void GrillesAdminPage::enrSousGrilleResponse(QString respError, PendingRequest *curReq)
{
    if(!beginHandleResponse(respError, curReq))
        return;

    QStandardItem *responseItem = curReq->getResponseItem();

    QStandardItem *griItem = currentGrilleModel->item(0);
    QStandardItem *rootItem = tixrCore->keyToItemMap.value("grilles");

    QString griName = griItem->text();
    sourceGrilleItem = new QStandardItem(griName);

    //TOMAR LA LLAVE Y PONERLA EN EL NUEVO QStandardItem
    Q_ASSERT(responseItem->rowCount() == 1);

    responseItem = responseItem->child(0);

    griItem->setData(responseItem->data(DataRole::ikey()), DataRole::ikey());

    QStandardItemModel *dstHeadersModel = new QStandardItemModel;
    tixrCore->itemToHeaderModelMap.insert(sourceGrilleItem, dstHeadersModel);

    assignGrille(griItem, sourceGrilleItem);//ACTUALIZAR sourceGrilleItem CON LOS CAMBIOS QUE ACABO DE GUARDAR EN EL SERVIDOR
    isGrilleChanged = false;
    grilleLineEdit->setText(griName);

    QStandardItem *listGriItem = new QStandardItem(griName);
    listGriItem->appendRow(sourceGrilleItem);
    rootItem->appendRow(listGriItem);

    grilleTableView->setModel(tixrCore->stdModel);
    grilleTableView->setRootIndex(sourceGrilleItem->index());
    grilleListView->selectionModel()->select(listGriItem->index(), QItemSelectionModel::ClearAndSelect);

    //Tomar el modelo que guarda los encabezados y establecerlo para la tabla actual
    GrilleHeaderView *header = static_cast<GrilleHeaderView*>(grilleTableView->horizontalHeader());
    header->setModel(dstHeadersModel);
    header->setGriItem(sourceGrilleItem);
    header = static_cast<GrilleHeaderView*>(grilleTableView->verticalHeader());
    header->setModel(dstHeadersModel);
    header->setGriItem(sourceGrilleItem);

    pbGrilleEnr->setEnabled(false);

    endHandleResponse(curReq);
}

void GrillesAdminPage::setGrilleChanged(QStandardItem *item)
{
    Q_UNUSED(item)
    isGrilleChanged = grillesAreDistincts(sourceGrilleItem, currentGrilleModel->item(0)->child(0));
    pbGrilleEnr->setEnabled(isGrilleChanged);
}

void GrillesAdminPage::setGrilleChanged2(const QModelIndex &parent, int start, int end)
{
    Q_UNUSED(parent)
    Q_UNUSED(start)
    Q_UNUSED(end)
    isGrilleChanged = grillesAreDistincts(sourceGrilleItem, currentGrilleModel->item(0)->child(0));
    pbGrilleEnr->setEnabled(isGrilleChanged);
}

void GrillesAdminPage::setGrilleChanged3(const QModelIndex & sourceParent, int sourceStart, int sourceEnd, const QModelIndex &destinationParent, int destinationColumn)
{
    Q_UNUSED(sourceParent)
    Q_UNUSED(sourceStart)
    Q_UNUSED(sourceEnd)
    Q_UNUSED(destinationParent)
    Q_UNUSED(destinationColumn)
    isGrilleChanged = grillesAreDistincts(sourceGrilleItem, currentGrilleModel->item(0)->child(0));
    pbGrilleEnr->setEnabled(isGrilleChanged);
}

void GrillesAdminPage::setGrilleChanged4(Qt::Orientation orientation, int first, int last)
{
    Q_UNUSED(orientation)
    Q_UNUSED(first)
    Q_UNUSED(last)
    isGrilleChanged = grillesAreDistincts(sourceGrilleItem, currentGrilleModel->item(0)->child(0));
    pbGrilleEnr->setEnabled(isGrilleChanged);
}

void GrillesAdminPage::griNameChanged(QString newName)
{
    if(currentGrilleModel)
    {
        QStandardItem *rootItem = currentGrilleModel->item(0);
        rootItem->setText(newName);
    }
}

