#ifndef GRILLESADMINPAGE_H
#define GRILLESADMINPAGE_H

#include "../widget.h"

class MouseButtonPressEater;
class DoubleSpinBoxDelegate;
class GrilleHeaderView;

class GrillesAdminPage : public TixrWidget
{
    Q_OBJECT
public:
    explicit GrillesAdminPage(TixRealmCore *mObj, QWidget *parent = 0);

private:
    QStandardItemModel *currentGrilleModel;//Contenido de la grille que está siendo editada en estos momentos
    bool isGrilleChanged;
    QStandardItem *sourceGrilleItem;//La grille que está siendo editada (en el modelo principal de datos), 0 para el caso de una nueva grille
    DoubleSpinBoxDelegate *griDelegate;
    QTableView *grilleTableView;
    QTableView *grilleTableView_2;
    QLineEdit *grilleLineEdit;
    QLineEdit *grilleLineEdit_2;
    QPushButton *pbGrilleEnr;
    QToolButton *tbGrilleDet;
    QWidget *tbGrilleDetContainer;
    QListView *grilleListView;
    //mecanismo de mensajes (diálogos) modales a la pestaña actual
    QStackedWidget *switcher;
    QWidget *msgScreen;
    QWidget *localModalMsg;
    QWidget *msgScreen2;
    QWidget *localModalMsg2;
    QWidget *dlgScreen;
    QWidget *localModalDlg;
    QPushButton *dlgScreenBtn;
    QLineEdit *lmDlgLE;
    QLabel *lmDlgLbl;
    QLabel *lmDlgErrLbl;
    QPushButton *msg2ScreenBtn;
    QPushButton *msgScreenBtnYes;
    QPushButton *msgScreenBtnNo;

    QStandardItemModel *cloneGrille(QStandardItem *griItem, QObject *parent);
    void assignGrille(QStandardItem *srcItem, QStandardItem *dstItem);
    bool grillesAreDistincts(QStandardItem *griItem, QStandardItem *editedGriItem);
    QString validateGrilleHeaderName(QString name);
    QString newGrilleName();

private slots:
    void on_tarifsListView_clicked(const QModelIndex &index);
    void showAllGrilles(QString respError, PendingRequest *curReq);
    void showGrille(QString respError, PendingRequest *curReq);
    void modGrilleResponse(QString respError, PendingRequest *curReq);
    void detGrilleResponse(QString respError, PendingRequest *curReq);
    void creGrilleResponse(QString respError, PendingRequest *curReq);
    void enrGrilleResponse(QString respError, PendingRequest *curReq);
    void enrSousGrilleResponse(QString respError, PendingRequest *curReq);
    void on_pbGrilleMod_clicked();
    void on_tbGrilleDet_clicked();
    void on_pbGrilleCre_clicked();
    void on_pbGrilleEnr_clicked();
    void on_pbGrilleEnrSous_clicked();
    void setGrilleChanged(QStandardItem * item);
    void setGrilleChanged2(const QModelIndex &parent, int start, int end);
    void setGrilleChanged3(const QModelIndex & sourceParent, int sourceStart, int sourceEnd, const QModelIndex &destinationParent, int destinationColumn);
    void setGrilleChanged4(Qt::Orientation orientation, int first, int last);
    void modGrille();
    void onHeaderSectionDoubleClicked(int logicalIndex);

    //mecanismo de mensajes (diálogos) modales a la pestańa actual
    void on_msgScreen_clicked();
    void on_localModalMsg_clicked();
    void on_msg2Btns_click();
    void on_msgBtnsForCre_click();
    void griNameChanged(QString newName);
    void on_tbAddRow_clicked();
    void on_tbAddCol_clicked();
    void on_tbDelRow_clicked();
    void on_tbDelCol_clicked();
    void on_addRowdlgBtns_click();
    void on_addColdlgBtns_click();
    void editRowOrColLabel();

friend class TariffsTest;
};

Q_DECLARE_METATYPE(GrillesAdminPage*)
#endif // GRILLESADMINPAGE_H

