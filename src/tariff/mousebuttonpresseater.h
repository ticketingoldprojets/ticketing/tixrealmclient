#ifndef MOUSEBUTTONPRESSEATER_H
#define MOUSEBUTTONPRESSEATER_H

#include <QObject>

class MouseButtonPressEater : public QObject
{
    Q_OBJECT

public:
    MouseButtonPressEater(QObject *parent);

signals:
    void clicked();

protected:
    bool eventFilter(QObject *obj, QEvent *event);
};


#endif // MOUSEBUTTONPRESSEATER_H
