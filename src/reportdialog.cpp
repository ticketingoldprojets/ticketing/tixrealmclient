//-----------------------------------------------------------------------------
#include "reportdialog.h"
#include "mainwindow.h"
//! NCReport report generator/rendering engine
/*#include <ncreport/ncreport.h>
#include <ncreport/ncreportoutput.h>
#include <ncreport/ncreportpreviewoutput.h>
#include <ncreport/ncreportpreviewwindow.h>*/
#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlError>
//-----------------------------------------------------------------------------
TixrReportDialog::TixrReportDialog(QWidget *blockedWidget, TixRealmCore *mObj, QWidget *parent) :
    TixrDialog(":/formconfig/reports.ui", blockedWidget, parent)
{
    this->pbPreview = findChild<QPushButton *>("pbPreview");
    this->pbPrint   = findChild<QPushButton *>("pbPrint");
    this->pbCancel  = findChild<QPushButton *>("pbCancel");
    this->lvReports = findChild<QListView *>("lvReports");
    this->tixrCore = mObj;

    QDirIterator dit(this->tixrCore->curPath + "reports", QStringList() << "*.xml", QDir::Files);
    QStandardItemModel * reportModel = new QStandardItemModel();
    while(dit.hasNext())
    {
        dit.next();
        reportModel->appendRow(new QStandardItem(dit.fileName()));
    }
    this->lvReports->setModel(reportModel);

    MainWindow::tixrConnectSlotsByName(this);
}
//-----------------------------------------------------------------------------
void TixrReportDialog::showEvent(QShowEvent *event)
{

}
//-----------------------------------------------------------------------------
void TixrReportDialog::on_pbPreview_clicked()
{
    setWindowFlags(Qt::Dialog | Qt::FramelessWindowHint);

    /*NCReport * report = new NCReport();
    report->reset(true);*/

    QStandardItem *userItm = this->tixrCore->keyToItemMap.value("login");
    QString driver = "QPSQL";
    QSqlDatabase defaultDB = QSqlDatabase::addDatabase(driver);
    if(!defaultDB.isValid())
    {
        QMessageBox::warning( 0, "Report error", QObject::tr("Could not load database driver.") );
        //delete report;
        return;
    }

    defaultDB.setHostName(userItm->data(DataRole::host()).toString());
    QString db = userItm->data(DataRole::database()).toString();
    defaultDB.setDatabaseName(db);
    defaultDB.setPort(userItm->data(DataRole::port()).toInt());
    defaultDB.setUserName(db+"_"+userItm->data(DataRole::slogin()).toString());
    defaultDB.setPassword(userItm->data(DataRole::passw()).toString());

    if(!defaultDB.open())
    {
        QMessageBox::warning(0, "Report error", QObject::tr("Cannot open database: ") + defaultDB.lastError().databaseText());
        //delete report;
        return;
    }

    //report->setReportFile(this->tixrCore->curPath + "reports/" + this->lvReports->model()->data(this->lvReports->currentIndex()).toString());
    // report->addParameter(, );

    /*NCReportPreviewOutput * output = new NCReportPreviewOutput();
    output->setAutoDelete( false );
    report->setOutput(output);
    report->runReport();
    if(!report->hasError())
    {
        NCReportPreviewWindow * pv = new NCReportPreviewWindow();
        pv->setReport( report );
        pv->setOutput((NCReportPreviewOutput *) output);
        pv->setWindowModality(Qt::ApplicationModal);
        pv->setAttribute(Qt::WA_DeleteOnClose);
        pv->show();
    }
    else if(report->hasError())
    {
        bool error = report->hasError();
        QMessageBox::about(this,"Error in Report", report->lastErrorMsg());
    }*/
}
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
