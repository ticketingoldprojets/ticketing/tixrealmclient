#ifndef SCCHECKDIALOG_H
#define SCCHECKDIALOG_H
#include "dialog.h"

class TixrCheckDialog : public TixrDialog
{
    Q_OBJECT

public:
    explicit TixrCheckDialog(QWidget *blockedWidget, QWidget *parent = 0);
    QStandardItem *checkPayItem;
    QStandardItem *modeItem;//modo de pago
    QComboBox *cbBank;

protected:
    void showEvent(QShowEvent *event);
    
private:
    QLineEdit *leChkNumb;

private slots:
    void on_pbOk_clicked();
};

#endif // SCCHECKDIALOG_H
