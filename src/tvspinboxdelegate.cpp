//-----------------------------------------------------------------------------
#include "tvspinboxdelegate.h"
//-----------------------------------------------------------------------------
// TVOBJECT CLASS
//-----------------------------------------------------------------------------
TVObject::TVObject(QObject * parent) : QObject(parent)
{
}
//-----------------------------------------------------------------------------
void TVObject::updateCell(QWidget * editor)
{
    QTableView * tv = static_cast<QTableView *>(this->parent());
    QModelIndex index = tv->currentIndex();
    QStandardItem * itemParent = static_cast<QStandardItemModel *>(tv->model())->itemFromIndex(index)->parent();
    int row = index.row();

    QDoubleSpinBox * dsbEspecesReelles = tv->parent()->findChild<QDoubleSpinBox *>("dsbEspecesReelles");
    QDoubleSpinBox * dsbTotalReel = tv->parent()->findChild<QDoubleSpinBox *>("dsbTotalReel");
    QDoubleSpinBox * dsbTotalTheorique = tv->parent()->findChild<QDoubleSpinBox *>("dsbTotalTheorique");
    if(tv->objectName() == "tvPiecesDevice")
    {
        itemParent->child(row, 2)->setData(itemParent->child(row, DataRole::qtdisplayrole())->data(DataRole::qtdisplayrole()).toDouble() * itemParent->child(row, 1)->data(DataRole::qtdisplayrole()).toInt(), 0);
        dsbTotalReel->setValue(dsbTotalReel->value() - dsbEspecesReelles->value());
        dsbEspecesReelles->setValue(0.0);
        for(int i = 0, count = itemParent->rowCount(); i < count; i++)        
            dsbEspecesReelles->setValue(dsbEspecesReelles->value() + itemParent->child(i,  2)->data(DataRole::qtdisplayrole()).toDouble());
        dsbTotalReel->setValue(dsbTotalReel->value() + dsbEspecesReelles->value());
    }
    else if(tv->objectName() == "tvMontant")
    {
        dsbTotalReel->setValue(0.0);
        for(int i = 0, count = itemParent->rowCount(); i < count; i++)
            dsbTotalReel->setValue(dsbTotalReel->value() + itemParent->child(i, 4)->data(DataRole::qtdisplayrole()).toDouble());
        dsbTotalReel->setValue(dsbTotalReel->value() + dsbEspecesReelles->value());
    }

    if(dsbTotalReel->value() == dsbTotalTheorique->value())
        dsbTotalReel->setStyleSheet("background:black;color:white");
    else    
        dsbTotalReel->setStyleSheet("background:black;color:red");
}
//-----------------------------------------------------------------------------
// SCTVSPINBOXDELEGATE CLASS
//-----------------------------------------------------------------------------
TixrTVSpinBoxDelegate::TixrTVSpinBoxDelegate(QObject * parent, TVObject * tvObj) : QItemDelegate(parent)
{
    if(this->tvObj = tvObj)
        QObject::connect(this, SIGNAL(closeEditor(QWidget *)), this->tvObj, SLOT(updateCell(QWidget *)), Qt::UniqueConnection);
}
//-----------------------------------------------------------------------------
QWidget * TixrTVSpinBoxDelegate::createEditor(QWidget * parent, const QStyleOptionViewItem & option, const QModelIndex & index) const
{
    QSpinBox * editor = new QSpinBox(parent);
    editor->setMinimum(0);
    editor->setMaximum(INT_MAX);
    return editor;
}
//-----------------------------------------------------------------------------
void TixrTVSpinBoxDelegate::setEditorData(QWidget * editor, const QModelIndex & index) const
{
    QSpinBox * spinBox = static_cast<QSpinBox*>(editor);
    int value = index.model()->data(index, Qt::EditRole).toInt();
    spinBox->setValue(value);
}
//-----------------------------------------------------------------------------
void TixrTVSpinBoxDelegate::setModelData(QWidget * editor, QAbstractItemModel * model, const QModelIndex & index) const
{
    QSpinBox * spinBox = static_cast<QSpinBox*>(editor);
    spinBox->interpretText();
    model->setData(index, spinBox->value(), Qt::EditRole);
}
//-----------------------------------------------------------------------------
void TixrTVSpinBoxDelegate::updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex & index) const
{
    editor->setGeometry(option.rect);
}
//-----------------------------------------------------------------------------
