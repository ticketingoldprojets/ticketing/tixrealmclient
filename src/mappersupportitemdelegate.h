 #ifndef MAPPERSUPPORTITEMDELEGATE_H
#define MAPPERSUPPORTITEMDELEGATE_H

#include <QStyledItemDelegate>
#include <QUndoStack>

class MapperSupportItemDelegate : public QStyledItemDelegate
{
    Q_OBJECT

protected:
    bool eventFilter(QObject * editor, QEvent * event);

public:
    QUndoStack *undoStack;

    explicit MapperSupportItemDelegate(QObject *parent = 0);
    void setEditorData(QWidget *editor, const QModelIndex &index) const;
    void setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const;
};

#endif // MAPPERSUPPORTITEMDELEGATE_H
