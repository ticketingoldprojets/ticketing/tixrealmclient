#include "dialog.h"
#include <QtUiTools/QUiLoader>

TixrDialog::TixrDialog(QString uiName, QWidget *blockedWidget, QWidget *parent) :
    QDialog(parent), _blockedWidget(blockedWidget)
{
    setWindowFlags(Qt::Dialog | Qt::WindowStaysOnTopHint | Qt::FramelessWindowHint);

    QUiLoader loader;
    QFile f(uiName);
    f.open(QFile::ReadOnly);
    QWidget *widget =  loader.load(&f);
    f.close();

    if(!widget)
    {
        errorStr = "Cannot load the graphical user interface.";
        return;
    }

    QString wName = widget->objectName();
    widget->setObjectName("dynWidget");
    setObjectName(wName);

    QVBoxLayout *layout = new QVBoxLayout;
    layout->addWidget(widget);
    setLayout(layout);

    setWindowIcon(_blockedWidget->windowIcon());

    setWindowTitle(widget->windowTitle());
    widget->setWindowTitle(QString::null);

    _wasVisible = false;

    waitMovie = 0;
    curTask = 0;
    tixrCore = 0;
    waitLabel = widget->findChild<QLabel*>("waitLabel");
    if(waitLabel)
    {
        waitMovie = new QMovie(":/ajax-loader.gif");
        connect(waitMovie, SIGNAL(updated(QRect)), this, SLOT(animateIcon(QRect)));
    }

    installEventFilter(this);
}

void TixrDialog::showEvent(QShowEvent * event)
{
    if(_blockedWidget->isVisible())
    {
        _blockedWidget->setEnabled(false);
        _wasVisible = true;

        event->accept();
    }
    else
        event->ignore();
}

#define stickyMargin 30 //margen para la atracción de las ventanas flotantes

bool TixrDialog::eventFilter(QObject *obj, QEvent *event)
{
    if(event->type() == QEvent::MouseButtonPress)
    {
        if(obj == this)
        {
            QMouseEvent *me = static_cast<QMouseEvent*>(event);

            if(me->button() == Qt::LeftButton)
            {
                dragPosition = me->globalPos() - pos();
                me->accept();
                return true;
            }
        }
    }else

    if(event->type() == QEvent::MouseMove)
    {
        if(obj == this)
        {
            QMouseEvent *me = static_cast<QMouseEvent*>(event);

            if(me->buttons() & Qt::LeftButton)
            {
                QPoint p = me->globalPos() - dragPosition;
                QRect r(_blockedWidget->mapToGlobal(QPoint(0,0)), _blockedWidget->size());

                if(p.x() - stickyMargin < r.left())
                    p.setX(r.left());

                if(p.x() + stickyMargin + size().width() > r.right())
                    p.setX(r.right() - size().width());

                if(p.y() - stickyMargin < r.top())
                    p.setY(r.top());

                if(p.y() + stickyMargin + size().height() > r.bottom())
                    p.setY(r.bottom() - size().height());

                move(p);

                me->accept();
                return true;
            }
        }
    }

    return false;
}

void TixrDialog::on_pbOk_clicked()
{
    _blockedWidget->setEnabled(true);
    _wasVisible = false;
    accept();
}

void TixrDialog::on_pbCancel_clicked()
{
    _blockedWidget->setEnabled(true);
    _wasVisible = false;
    reject();
}

void TixrDialog::animateIcon(const QRect & rect)
{
    Q_UNUSED(rect)

    if(curTask)
    {
        waitLabel->setPixmap(waitMovie->currentPixmap());
        waitLabel->show();
    }
    else
        waitLabel->hide();
}
